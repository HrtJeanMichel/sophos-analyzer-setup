﻿Imports System.ComponentModel
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper
Imports Sophos_Analyzer.Win32

Namespace Tweaks
    <Serializable>
    Public Class Desktop
        Inherits Notifier

#Region " Properties "
        Public Property NoDispCPL As Boolean
        Public Property NoActiveDesktop As Boolean
        Public Property NoDispBackgroundPage As Boolean
        Public Property HideIcons As Boolean
        Public Property NoChangingWallPaper As Boolean
        Public Property NoViewContextMenu As Boolean
        Public Property TurnOffSidebar As Boolean
        Public Property BackgroundDesktop As String
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub

        Public Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "
        Public Sub Save(Optional ByVal FirstCreationAccount As Boolean = False)
            If FirstCreationAccount Then
                m_Bgw.ReportProgress(50, "Paramétrage des stratégies locales ...")
            Else
                m_Bgw.ReportProgress(-1, Messages.ApplyUserStratDesktop)
            End If
            Reg.SZ.SetValue(Paths.UserDatPoliciesSystem, "Wallpaper", If(FirstCreationAccount, GetWallpaper(), Me.BackgroundDesktop))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoActiveDesktop", If(FirstCreationAccount, True, Me.NoActiveDesktop))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoViewContextMenu", If(FirstCreationAccount, True, Me.NoViewContextMenu))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesActiveDesktop, "NoChangingWallPaper", If(FirstCreationAccount, True, Me.NoChangingWallPaper))
            Reg.DWORD.SetBlnValue(Paths.UserDatWindowsSidebar, "TurnOffSidebar", If(FirstCreationAccount, True, Me.TurnOffSidebar))
            Reg.DWORD.SetBlnValue(Paths.UserDatExplorerAdvanced, "HideIcons", If(FirstCreationAccount, True, Me.HideIcons))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesSystem, "NoDispBackgroundPage", If(FirstCreationAccount, True, Me.NoDispBackgroundPage))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesSystem, "NoDispCPL", If(FirstCreationAccount, True, Me.NoDispCPL))
        End Sub

        Public Sub Load()
            Try
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(-1, Messages.LoadUserStratDesktop)
                End If
                Me.NoDispCPL = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesSystem, "NoDispCPL")
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(2, Me.NoDispCPL)
                End If
                Me.NoActiveDesktop = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoActiveDesktop")
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(17, Me.NoActiveDesktop)
                End If
                Me.BackgroundDesktop = Reg.SZ.GetValue(Paths.UserDatPoliciesSystem, "Wallpaper")
                If Me.BackgroundDesktop = String.Empty Then Me.BackgroundDesktop = Environment.ExpandEnvironmentVariables("%WinDir%") & "\Web\Wallpaper\Windows\img0.jpg"
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(21, Me.BackgroundDesktop)
                End If
                Me.NoDispBackgroundPage = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesSystem, "NoDispBackgroundPage")
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(22, Me.NoDispBackgroundPage)
                End If
                Me.HideIcons = Reg.DWORD.RegIsSeted(Paths.UserDatExplorerAdvanced, "HideIcons")
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(23, Me.HideIcons)
                End If
                Me.NoChangingWallPaper = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesActiveDesktop, "NoChangingWallPaper")
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(24, Me.NoChangingWallPaper)
                End If
                Me.TurnOffSidebar = Reg.DWORD.RegIsSeted(Paths.UserDatWindowsSidebar, "TurnOffSidebar")
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(43, Me.TurnOffSidebar)
                End If
                Me.NoViewContextMenu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoViewContextMenu")
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(45, Me.NoViewContextMenu)
                End If
            Catch ex As Exception
            End Try
        End Sub

        Private Function GetWallpaper() As String
            Dim currentWallpaper As New String(ControlChars.NullChar, NativeConstants.MAX_PATH)
            NativeMethods.SystemParametersInfo(NativeConstants.SPI_GETDESKWALLPAPER, currentWallpaper.Length, currentWallpaper, 0)
            Return currentWallpaper.Substring(0, currentWallpaper.IndexOf(ControlChars.NullChar))
        End Function

        Public Sub SetBoolProperties(val As Boolean)
            Dim properties = Me.GetType.GetProperties().Where(Function(f) f.PropertyType Is GetType(Boolean))
            For Each prop In properties
                prop.SetValue(Me, val, Nothing)
            Next
        End Sub

        Public Function EqualsTo(obj As Desktop) As Boolean
            Dim str1 = Utils.Serialize(Of Desktop)(obj)
            Dim str2 = Utils.Serialize(Of Desktop)(Me)
            Dim B = If(str1 = str2, True, False)
            'MsgBox("Desktop : " & B.ToString)
            Return B
        End Function
#End Region

    End Class
End Namespace

