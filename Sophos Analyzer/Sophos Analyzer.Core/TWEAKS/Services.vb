﻿Imports System.ComponentModel
Imports Sophos_Analyzer.Core.Services
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper

Namespace Tweaks
    <Serializable>
    Public Class Services
        Inherits Notifier

#Region " Fields "
        '<XmlIgnore()>
        'Private m_SvcList As List(Of SvcInfos)
#End Region

#Region " Properties "
        Public Property DisabledServices As Boolean

        'Public Property SvcList() As List(Of SvcInfos)
        '    Get
        '        Return m_SvcList
        '    End Get
        '    Set(value As List(Of SvcInfos))
        '        m_SvcList = value
        '    End Set
        'End Property
#End Region

#Region " Constructor "
        Public Sub New()
            'SvcList = New List(Of SvcInfos)
        End Sub

        Public Sub New(Bgw As BackgroundWorker)
            'SvcList = New List(Of SvcInfos)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "
        Public Sub Save(Optional ByVal FirstCreationAccount As Boolean = False)
            If FirstCreationAccount Then
                'm_Bgw.ReportProgress(90, "Paramétrage des services ...")
            Else
                m_Bgw.ReportProgress(-1, Messages.ApplyServices)
            End If
            Svcs.Set(If(FirstCreationAccount, True, DisabledServices))
        End Sub

        Public Sub Load()
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(-1, Messages.LoadServices)
            End If
            Svcs.GetAllServices(m_Bgw)
            DisabledServices = If(Svcs.AllServices.Any(Function(f) f.Disabled = False), False, True)
        End Sub

        Public Sub SetBoolProperty(val As Boolean)
            DisabledServices = val
        End Sub

        Public Function EqualsTo(obj As Services) As Boolean
            Dim str1 = Utils.Serialize(Of Services)(obj)
            Dim str2 = Utils.Serialize(Of Services)(Me)
            Dim B = If(str1 = str2, True, False)
            'MsgBox("Services : " & B.ToString)
            Return B
        End Function
#End Region

    End Class
End Namespace
