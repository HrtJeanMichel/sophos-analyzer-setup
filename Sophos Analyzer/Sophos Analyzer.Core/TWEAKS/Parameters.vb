﻿Imports System.Xml.Serialization
Imports System.ComponentModel

Namespace Tweaks
    <Serializable>
    Public Class Parameters

#Region " Properties "

#Region " User "
        Public User As User
#End Region

#Region " Desktop "
        Public Desktop As Desktop
#End Region

#Region " StartMenu "
        Public StartMenu As StartMenu
#End Region

#Region " System "
        Public Systeme As Systeme
#End Region

#Region " Taskbar "
        Public Taskbar As Taskbar
#End Region

#Region " Explorer "
        Public Explorer As Explorer
#End Region

#Region " Services "
        Public Services As Services
#End Region

#Region " Launcher "
        Public Launcher As Loader
#End Region

#Region " Network "
        Public Network As Network
#End Region

        <XmlIgnore()>
        Public Property Bgw As BackgroundWorker
            Get
                Return m_Bgw
            End Get
            Set(value As BackgroundWorker)
                m_Bgw = value
                User.m_Bgw = m_Bgw
                Desktop.m_Bgw = m_Bgw
                StartMenu.m_Bgw = m_Bgw
                Systeme.m_Bgw = m_Bgw
                Taskbar.m_Bgw = m_Bgw
                Explorer.m_Bgw = m_Bgw
                Network.m_Bgw = m_Bgw
                Services.m_Bgw = m_Bgw
                Launcher.m_Bgw = m_Bgw
            End Set
        End Property
#End Region

#Region " Fields "
        <NonSerialized()>
        Private m_Bgw As BackgroundWorker
#End Region

#Region " Constructor "
        Sub New()
            User = New User
            Desktop = New Desktop
            StartMenu = New StartMenu
            Systeme = New Systeme
            Taskbar = New Taskbar
            Explorer = New Explorer
            Network = New Network
            Services = New Services
            Launcher = New Loader
        End Sub

        Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
            User = New User(Bgw)
            Desktop = New Desktop(Bgw)
            StartMenu = New StartMenu(Bgw)
            Systeme = New Systeme(Bgw)
            Taskbar = New Taskbar(Bgw)
            Explorer = New Explorer(Bgw)
            Network = New Network(Bgw)
            Services = New Services(Bgw)
            Launcher = New Loader(Bgw)
        End Sub
#End Region

#Region " Methods "

#End Region

    End Class
End Namespace
