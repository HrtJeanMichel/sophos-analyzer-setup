﻿Imports System.ComponentModel
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper

Namespace Tweaks
    <Serializable>
    Public Class Explorer
        Inherits Notifier

#Region " Properties "
        Public Property NoFolderOptions As Boolean
        Public Property NoToolbarCustomize As Boolean
        Public Property NoBandCustomize As Boolean
        Public Property NoDriveTypeAutoRun As Boolean
        Public Property NoAddPrinter As Boolean
        Public Property NoProgramsCPL As Boolean
        Public Property NoDrives As Boolean
        Public Property NoViewOnDrive As Boolean
        Public Property NoNetConnectDisconnect As Boolean
#End Region

#Region " Constructor "
        Sub New()
        End Sub

        Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "
        Public Sub Save(Optional ByVal FirstCreationAccount As Boolean = False)
            If FirstCreationAccount Then
                m_Bgw.ReportProgress(60, "Paramétrage des stratégies locales ...")
            Else
                m_Bgw.ReportProgress(-1, Messages.ApplyUserStratExplorer)
            End If
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoFolderOptions", If(FirstCreationAccount, True, Me.NoFolderOptions))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoToolbarCustomize", If(FirstCreationAccount, True, Me.NoToolbarCustomize))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoBandCustomize", If(FirstCreationAccount, True, Me.NoBandCustomize))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoAddPrinter", If(FirstCreationAccount, True, Me.NoAddPrinter))
            Reg.DWORD.SetValue(Paths.UserDatPoliciesExplorer, "NoDriveTypeAutoRun", If(FirstCreationAccount, 255, If(Me.NoDriveTypeAutoRun, "255", "145")))
            Reg.DWORD.SetValue(Paths.UserDatPoliciesExplorer, "NoDrives", If(FirstCreationAccount, 67108863, If(Me.NoDrives, 67108863, 0)))
            Reg.DWORD.SetValue(Paths.UserDatPoliciesExplorer, "NoViewOnDrive", If(FirstCreationAccount, 67108863, If(Me.NoViewOnDrive, 67108863, 0)))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesPrograms, "NoProgramsCPL", If(FirstCreationAccount, True, Me.NoProgramsCPL))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoNetConnectDisconnect", If(FirstCreationAccount, True, Me.NoNetConnectDisconnect))
        End Sub

        Public Sub Load()
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(-1, Messages.LoadUserStratExplorer)
            End If

            Me.NoFolderOptions = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoFolderOptions")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(5, Me.NoFolderOptions)
            End If

            Me.NoToolbarCustomize = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoToolbarCustomize")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(18, Me.NoToolbarCustomize)
            End If

            Me.NoBandCustomize = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoBandCustomize")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(19, Me.NoBandCustomize)
            End If

            Me.NoDriveTypeAutoRun = If(Reg.DWORD.GetValue(Paths.UserDatPoliciesExplorer, "NoDriveTypeAutoRun") = 255, True, False)
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(20, Me.NoDriveTypeAutoRun)
            End If

            Dim noDrive = Reg.DWORD.GetValue(Paths.UserDatPoliciesExplorer, "NoDrives")
            Me.NoDrives = If(noDrive = -2 OrElse noDrive = 0, False, If(noDrive = 67108863, True, False))
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(73, Me.NoDrives)
            End If

            Me.NoAddPrinter = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoAddPrinter")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(76, Me.NoAddPrinter)
            End If

            Dim noViewOnDrive = Reg.DWORD.GetValue(Paths.UserDatPoliciesExplorer, "NoViewOnDrive")
            Me.NoViewOnDrive = If(noViewOnDrive = -2 OrElse noViewOnDrive = 0, False, If(noViewOnDrive = 67108863, True, False))
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(77, Me.NoViewOnDrive)
            End If

            Me.NoProgramsCPL = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesPrograms, "NoProgramsCPL")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(87, Me.NoProgramsCPL)
            End If

            Me.NoNetConnectDisconnect = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoNetConnectDisconnect")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(109, Me.NoNetConnectDisconnect)
            End If
        End Sub

        Public Sub SetBoolProperties(val As Boolean)
            Dim properties = Me.GetType.GetProperties().Where(Function(f) f.PropertyType Is GetType(Boolean))
            For Each prop In properties
                prop.SetValue(Me, val, Nothing)
            Next
        End Sub

        Public Function EqualsTo(obj As Explorer) As Boolean
            Dim str1 = Utils.Serialize(Of Explorer)(obj)
            Dim str2 = Utils.Serialize(Of Explorer)(Me)
            Dim B = If(str1 = str2, True, False)
            'MsgBox("Explorer : " & B.ToString)
            Return B
        End Function

#End Region

    End Class
End Namespace

