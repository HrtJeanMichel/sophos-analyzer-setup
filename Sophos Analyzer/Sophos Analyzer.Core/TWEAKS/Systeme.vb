﻿Imports System.ComponentModel
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper

Namespace Tweaks
    <Serializable>
    Public Class Systeme
        Inherits Notifier

#Region " Properties "
        Public Property NoPropertiesMyComputer As Boolean
        Public Property DisableRegistryTools As Boolean
        Public Property DisableTaskMgr As Boolean
        Public Property NoControlPanel As Boolean
        Public Property DisableCMD As Boolean
        Public Property NoWinKeys As Boolean
        Public Property DisableChangePassword As Boolean
        Public Property NoComputerOnStartMenuDesktop As Boolean
        Public Property NoClose As Boolean
        Public Property DisableScriptsExec As Boolean
        Public Property DisableHibernate As Boolean
#End Region

#Region " Constructor "
        Sub New()
        End Sub

        Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "

        Public Sub Save(Optional ByVal FirstCreationAccount As Boolean = False)
            If FirstCreationAccount Then
                m_Bgw.ReportProgress(90, "Paramétrage des stratégies locales ...")
            Else
                m_Bgw.ReportProgress(-1, Messages.ApplyUserStratSystem)
            End If
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoPropertiesMyComputer", If(FirstCreationAccount, True, Me.NoPropertiesMyComputer))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoControlPanel", If(FirstCreationAccount, True, Me.NoControlPanel))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoClose", If(FirstCreationAccount, True, Me.NoClose))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoWinKeys", If(FirstCreationAccount, True, Me.NoWinKeys))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesSystem, "DisableChangePassword", If(FirstCreationAccount, True, Me.DisableChangePassword))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesSystem, "DisableRegistryTools", If(FirstCreationAccount, True, Me.DisableRegistryTools))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesSystem, "DisableTaskMgr", If(FirstCreationAccount, True, Me.DisableTaskMgr))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesNonEnum, "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", If(FirstCreationAccount, True, Me.NoComputerOnStartMenuDesktop))
            Reg.DWORD.SetValue(Paths.UserDatWindowsSystem, "DisableCMD", If(FirstCreationAccount, 2, If(Me.DisableCMD, "2", "0")))
            Reg.DWORD.SetBlnValue(Paths.UserDatWSH, "Enabled", If(FirstCreationAccount, False, If(Me.DisableScriptsExec, False, True)), True)
            Utils.StartCommand(Environment.ExpandEnvironmentVariables("%WinDir%") & "\System32\powercfg.exe", "-h " & If(FirstCreationAccount, "OFF", If(Me.DisableHibernate, "OFF", "ON")), True)
        End Sub

        Public Sub Load()
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(-1, Messages.LoadUserStratSystem)
            End If

            Me.NoPropertiesMyComputer = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoPropertiesMyComputer")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(4, Me.NoPropertiesMyComputer)
            End If

            Me.DisableRegistryTools = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesSystem, "DisableRegistryTools")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(6, Me.DisableRegistryTools)
            End If

            Me.DisableTaskMgr = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesSystem, "DisableTaskMgr")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(7, Me.DisableTaskMgr)
            End If

            Me.NoControlPanel = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoControlPanel")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(8, Me.NoControlPanel)
            End If

            Me.DisableCMD = If(Reg.DWORD.GetValue(Paths.UserDatWindowsSystem, "DisableCMD") = 2, True, False)
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(9, Me.DisableCMD)
            End If

            Me.NoClose = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoClose")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(15, Me.NoClose)
            End If

            Me.DisableChangePassword = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesSystem, "DisableChangePassword")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(44, Me.DisableChangePassword)
            End If

            Me.NoWinKeys = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoWinKeys")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(46, Me.NoWinKeys)
            End If

            Me.NoComputerOnStartMenuDesktop = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesNonEnum, "{20D04FE0-3AEA-1069-A2D8-08002B30309D}")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(49, Me.NoComputerOnStartMenuDesktop)
            End If

            Dim DisHib = Reg.DWORD.GetValue(Paths.Power, "HibernateEnabled")
            Me.DisableHibernate = If(DisHib = -2, True, If(DisHib = 0, True, False))
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(63, Me.DisableHibernate)
            End If

            Me.DisableScriptsExec = If(Reg.ExistsKey(Paths.UserDatWSH), If(Reg.DWORD.RegIsSeted(Paths.UserDatWSH, "Enabled", True), False, True), False)
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(78, Me.DisableScriptsExec)
            End If
        End Sub

        Public Sub SetBoolProperties(val As Boolean)
            Dim properties = Me.GetType.GetProperties().Where(Function(f) f.PropertyType Is GetType(Boolean))
            For Each prop In properties
                prop.SetValue(Me, val, Nothing)
            Next
        End Sub

        Public Function EqualsTo(obj As Systeme) As Boolean
            Dim str1 = Utils.Serialize(Of Systeme)(obj)
            Dim str2 = Utils.Serialize(Of Systeme)(Me)
            Dim B = If(str1 = str2, True, False)
            'MsgBox("Systeme : " & B.ToString)
            Return B
        End Function
#End Region

    End Class
End Namespace
