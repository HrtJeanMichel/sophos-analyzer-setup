﻿Imports System.ComponentModel
Imports Sophos_Analyzer.Helper
Imports Sophos_Analyzer.Core.Settings

Namespace Tweaks
    <Serializable>
    Public Class User
        Inherits Notifier

#Region " Properties "
        Public Property Name As String
        Public Property Disabled As Boolean
        Public Property UserCannotChangePassword As Boolean
        Public Property PasswordNeverExpires As Boolean
        Public Property Description As String
        Public Property SID As String
        Public Property Group As String
        Public Property IsSophosAdminGroupMember As Boolean

#End Region

#Region " Constructors "
        Public Sub New()
        End Sub

        Public Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "
        Public Sub Save(Optional ByVal FirstCreationAccount As Boolean = False)
            If FirstCreationAccount Then
            Else
                m_Bgw.ReportProgress(-1, Messages.ApplyAccountStrat)

                Dim Account As New UserInfos(Name, False)
                With Account
                    .Disabled = False
                    .Description = Messages.AccountDescription
                    .IsSuperAdmin = False
                    .IsInvite = False
                    .Group = If(OS.GetInstalledUICulture = 1036, "Administrateurs|SophosAdministrator", "Administrators|SophosAdministrator")
                    .UserCannotChangePassword = True
                    .PasswordNeverExpires = True

                    .ChangeUserInfos()
                End With
            End If
        End Sub

        Public Sub Load(UserInfos As UserInfos)
            Try
                If Not m_Bgw Is Nothing Then m_Bgw.ReportProgress(-1, Messages.LoadUserInfos)

                Dim acc = UserInfos

                Me.Name = acc.Name
                Me.Description = acc.Description
                Me.Group = acc.Group
                Me.SID = acc.SID

                Me.Disabled = If(acc.Disabled, False, True)
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(100, Me.Disabled)
                End If

                IsSophosAdminGroupMember = acc.IsCorrectAdminGroup
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(101, Me.IsSophosAdminGroupMember)
                End If

                UserCannotChangePassword = acc.UserCannotChangePassword
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(102, Me.UserCannotChangePassword)
                End If

                PasswordNeverExpires = acc.PasswordNeverExpires
                If Not m_Bgw Is Nothing Then
                    m_Bgw.ReportProgress(103, Me.PasswordNeverExpires)
                End If
            Catch ex As Exception
            End Try
        End Sub

        Public Sub SetBoolProperties(val As Boolean)
            Disabled = False
            Description = Messages.AccountDescription
            Group = If(OS.GetInstalledUICulture = 1036, "Administrateurs|SophosAdministrator", "Administrators|SophosAdministrator")
            UserCannotChangePassword = True
            PasswordNeverExpires = True
            IsSophosAdminGroupMember = True
        End Sub

        Public Function EqualsTo(obj As User) As Boolean
            Dim str1 = Utils.Serialize(Of User)(obj)
            Dim str2 = Utils.Serialize(Of User)(Me)
            Return If(str1 = str2, True, False)
        End Function
#End Region

    End Class
End Namespace
