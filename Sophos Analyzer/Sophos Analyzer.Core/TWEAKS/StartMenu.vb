﻿Imports System.ComponentModel
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper

Namespace Tweaks
    <Serializable>
    Public Class StartMenu
        Inherits Notifier

#Region " Properties "
        Public Property NoChangeStartMenu As Boolean
        Public Property NoRun As Boolean
        Public Property StartMenuLogOff As Boolean
        Public Property NoCommonGroups As Boolean
        Public Property NoFavoritesMenu As Boolean
        Public Property NoFind As Boolean
        Public Property NoSMHelp As Boolean
        Public Property NoStartMenuMorePrograms As Boolean
        Public Property NoStartMenuMFUprogramsList As Boolean
        Public Property NoNetworkConnections As Boolean
        Public Property NoRecentDocsMenu As Boolean
        Public Property NoSMConfigurePrograms As Boolean
        Public Property NoSMMyDocs As Boolean
        Public Property NoStartMenuMyMusic As Boolean
        Public Property NoStartMenuNetworkPlaces As Boolean
        Public Property NoSMMyPictures As Boolean
        Public Property Start_ShowPrinters As Boolean
        Public Property NoUserFolderInStartMenu As Boolean
        Public Property NoSearchFilesInStartMenu As Boolean
        Public Property DisableLockWorkstation As Boolean
        Public Property HideFastUserSwitching As Boolean
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub

        Public Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "
        Public Sub Save(Optional ByVal FirstCreationAccount As Boolean = False)
            If FirstCreationAccount Then
                m_Bgw.ReportProgress(80, "Paramétrage des stratégies locales ...")
            Else
                m_Bgw.ReportProgress(-1, Messages.ApplyUserStratStartMenu)
            End If

            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoChangeStartMenu", If(FirstCreationAccount, True, Me.NoChangeStartMenu))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoRun", If(FirstCreationAccount, True, Me.NoRun))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoCommonGroups", If(FirstCreationAccount, True, Me.NoCommonGroups))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoFavoritesMenu", If(FirstCreationAccount, True, Me.NoFavoritesMenu))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoFind", If(FirstCreationAccount, True, Me.NoFind))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoSMHelp", If(FirstCreationAccount, True, Me.NoSMHelp))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoStartMenuMorePrograms", If(FirstCreationAccount, True, Me.NoStartMenuMorePrograms))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoStartMenuMFUprogramsList", If(FirstCreationAccount, True, Me.NoStartMenuMFUprogramsList))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoNetworkConnections", If(FirstCreationAccount, True, Me.NoNetworkConnections))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoRecentDocsMenu", If(FirstCreationAccount, True, Me.NoRecentDocsMenu))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoSMConfigurePrograms", If(FirstCreationAccount, True, Me.NoSMConfigurePrograms))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoSMMyDocs", If(FirstCreationAccount, True, Me.NoSMMyDocs))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoStartMenuMyMusic", If(FirstCreationAccount, True, Me.NoStartMenuMyMusic))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoStartMenuNetworkPlaces", If(FirstCreationAccount, True, Me.NoStartMenuNetworkPlaces))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoSMMyPictures", If(FirstCreationAccount, True, Me.NoSMMyPictures))
            Reg.DWORD.SetBlnValue(Paths.UserDatExplorerAdvanced, "Start_ShowPrinters", If(FirstCreationAccount, False, If(Me.Start_ShowPrinters, False, True)), True)
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoUserFolderInStartMenu", If(FirstCreationAccount, True, Me.NoUserFolderInStartMenu))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoSearchFilesInStartMenu", If(FirstCreationAccount, True, Me.NoSearchFilesInStartMenu))

            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesSystem, "DisableLockWorkstation", If(FirstCreationAccount, True, Me.DisableLockWorkstation), True)
            Reg.DWORD.SetBlnValue(Paths.PoliciesSystem, "DisableLockWorkstation", If(FirstCreationAccount, True, Me.DisableLockWorkstation), True)

            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesSystem, "HideFastUserSwitching", If(FirstCreationAccount, True, Me.HideFastUserSwitching), True)
            Reg.DWORD.SetBlnValue(Paths.PoliciesSystem, "HideFastUserSwitching", If(FirstCreationAccount, True, Me.HideFastUserSwitching), True)
        End Sub

        Public Sub Load()
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(-1, Messages.LoadUserStratStartMenu)
            End If

            Me.NoChangeStartMenu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoChangeStartMenu")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(13, Me.NoChangeStartMenu)
            End If

            Me.NoRun = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoRun")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(14, Me.NoRun)
            End If

            Me.NoCommonGroups = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoCommonGroups")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(25, Me.NoCommonGroups)
            End If

            Me.NoFavoritesMenu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoFavoritesMenu")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(26, Me.NoFavoritesMenu)
            End If

            Me.NoFind = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoFind")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(27, Me.NoFind)
            End If

            Me.NoSMHelp = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoSMHelp")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(28, Me.NoSMHelp)
            End If

            Me.NoStartMenuMorePrograms = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoStartMenuMorePrograms")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(29, Me.NoStartMenuMorePrograms)
            End If

            Me.NoStartMenuMFUprogramsList = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoStartMenuMFUprogramsList")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(30, Me.NoStartMenuMFUprogramsList)
            End If

            Me.NoNetworkConnections = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoNetworkConnections")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(31, Me.NoNetworkConnections)
            End If

            Me.NoRecentDocsMenu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoRecentDocsMenu")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(32, Me.NoRecentDocsMenu)
            End If

            Me.NoSMConfigurePrograms = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoSMConfigurePrograms")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(33, Me.NoSMConfigurePrograms)
            End If

            Me.NoSMMyDocs = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoSMMyDocs")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(34, Me.NoSMMyDocs)
            End If

            Me.NoStartMenuMyMusic = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoStartMenuMyMusic")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(35, Me.NoStartMenuMyMusic)
            End If

            Me.NoStartMenuNetworkPlaces = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoStartMenuNetworkPlaces")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(36, Me.NoStartMenuNetworkPlaces)
            End If

            Me.NoSMMyPictures = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoSMMyPictures")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(37, Me.NoSMMyPictures)
            End If

            Dim showPrint = Reg.DWORD.GetValue(Paths.UserDatExplorerAdvanced, "Start_ShowPrinters")
            Me.Start_ShowPrinters = If(showPrint = -2, False, If(showPrint = 0, True, False))
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(47, Me.Start_ShowPrinters)
            End If

            Me.NoUserFolderInStartMenu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoUserFolderInStartMenu")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(48, Me.NoUserFolderInStartMenu)
            End If

            Dim DisableLockHklm = Reg.DWORD.RegIsSeted(Paths.PoliciesSystem, "DisableLockWorkstation", True)
            Dim DisableLockHkcu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesSystem, "DisableLockWorkstation", True)
            Me.DisableLockWorkstation = If(DisableLockHklm AndAlso DisableLockHkcu, True, False)
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(52, Me.DisableLockWorkstation)
            End If

            Dim HideFastHklm = Reg.DWORD.RegIsSeted(Paths.PoliciesSystem, "HideFastUserSwitching", True)
            Dim HideFastHkcu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesSystem, "HideFastUserSwitching", True)
            Me.HideFastUserSwitching = If(HideFastHklm AndAlso HideFastHkcu, True, False)
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(54, Me.HideFastUserSwitching)
            End If

            Me.NoSearchFilesInStartMenu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoSearchFilesInStartMenu")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(75, Me.NoSearchFilesInStartMenu)
            End If

        End Sub

        Public Sub SetBoolProperties(val As Boolean)
            Dim properties = Me.GetType.GetProperties().Where(Function(f) f.PropertyType Is GetType(Boolean))
            For Each prop In properties
                prop.SetValue(Me, val, Nothing)
            Next
        End Sub

        Public Function EqualsTo(obj As StartMenu) As Boolean
            Dim str1 = Utils.Serialize(Of StartMenu)(obj)
            Dim str2 = Utils.Serialize(Of StartMenu)(Me)
            Dim B = If(str1 = str2, True, False)
            'MsgBox("StartMenu : " & B.ToString)
            Return B
        End Function
#End Region

    End Class
End Namespace
