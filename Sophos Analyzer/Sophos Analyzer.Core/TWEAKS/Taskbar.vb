﻿Imports System.ComponentModel
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper

Namespace Tweaks
    <Serializable>
    Public Class Taskbar
        Inherits Notifier

#Region " Properties "
        Public Property NoSetTaskbar As Boolean
        Public Property NoTrayItemsDisplay As Boolean
        Public Property LockTaskbar As Boolean
        Public Property NoTrayContextMenu As Boolean
        Public Property NoToolbarsOnTaskbar As Boolean
        Public Property HideSCAHealth As Boolean
        Public Property HideSCANetwork As Boolean
        Public Property TaskbarNoNotification As Boolean
        Public Property TaskbarNoPinnedList As Boolean
        Public Property DisablePreviewDesktop As Boolean

#End Region

#Region " Constructor "
        Public Sub New()
        End Sub

        Public Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "

        Public Sub Save(Optional ByVal FirstCreationAccount As Boolean = False)
            If FirstCreationAccount Then
                m_Bgw.ReportProgress(100, "Paramétrage des stratégies locales ...")
            Else
                m_Bgw.ReportProgress(-1, Messages.ApplyUserStratTaskbar)
            End If
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoSetTaskbar", If(FirstCreationAccount, True, Me.NoSetTaskbar))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "LockTaskbar", If(FirstCreationAccount, True, Me.LockTaskbar))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoTrayContextMenu", If(FirstCreationAccount, True, Me.NoTrayContextMenu))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoToolbarsOnTaskbar", If(FirstCreationAccount, True, Me.NoToolbarsOnTaskbar))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "NoTrayItemsDisplay", If(FirstCreationAccount, True, Me.NoTrayItemsDisplay))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "HideSCAHealth", If(FirstCreationAccount, True, Me.HideSCAHealth))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "HideSCANetwork", If(FirstCreationAccount, True, Me.HideSCANetwork))
            Reg.DWORD.SetBlnValue(Paths.UserDatPoliciesExplorer, "TaskbarNoNotification", If(FirstCreationAccount, True, Me.TaskbarNoNotification))
            Reg.DWORD.SetBlnValue(Paths.UserDatWindowsExplorer, "TaskbarNoPinnedList", If(FirstCreationAccount, True, Me.TaskbarNoPinnedList))
            If If(FirstCreationAccount, True, Me.DisablePreviewDesktop) Then
                Reg.DWORD.SetBlnValue(Paths.UserDatExplorerAdvanced, "DisablePreviewDesktop", True)
            Else
                Reg.DWORD.SetBlnValue(Paths.UserDatExplorerAdvanced, "DisablePreviewDesktop", False, True)
            End If
        End Sub

        Public Sub Load()
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(-1, Messages.LoadUserStratTaskbar)
            End If

            Me.NoSetTaskbar = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoSetTaskbar")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(3, Me.NoSetTaskbar)
            End If

            Me.LockTaskbar = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "LockTaskbar")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(10, Me.LockTaskbar)
            End If

            Me.NoTrayContextMenu = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoTrayContextMenu")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(11, Me.NoTrayContextMenu)
            End If

            Me.NoToolbarsOnTaskbar = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoToolbarsOnTaskbar")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(12, Me.NoToolbarsOnTaskbar)
            End If

            Me.NoTrayItemsDisplay = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "NoTrayItemsDisplay")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(38, Me.NoTrayItemsDisplay)
            End If

            Me.HideSCAHealth = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "HideSCAHealth")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(39, Me.HideSCAHealth)
            End If

            Me.HideSCANetwork = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "HideSCANetwork")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(40, Me.HideSCANetwork)
            End If

            Me.TaskbarNoNotification = Reg.DWORD.RegIsSeted(Paths.UserDatPoliciesExplorer, "TaskbarNoNotification")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(41, Me.TaskbarNoNotification)
            End If

            Me.TaskbarNoPinnedList = Reg.DWORD.RegIsSeted(Paths.UserDatWindowsExplorer, "TaskbarNoPinnedList")
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(42, Me.TaskbarNoPinnedList)
            End If

            Dim DisabledesktopPreviewHKCU = Reg.DWORD.GetValue(Paths.UserDatExplorerAdvanced, "DisablePreviewDesktop")
            Me.DisablePreviewDesktop = If(DisabledesktopPreviewHKCU = -2, False, If(DisabledesktopPreviewHKCU = 0, False, True))
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(51, Me.DisablePreviewDesktop)
            End If

        End Sub

        Public Sub SetBoolProperties(val As Boolean)
            Dim properties = Me.GetType.GetProperties().Where(Function(f) f.PropertyType Is GetType(Boolean))
            For Each prop In properties
                prop.SetValue(Me, val, Nothing)
            Next
        End Sub

        Public Function EqualsTo(obj As Taskbar) As Boolean
            Dim str1 = Utils.Serialize(Of Taskbar)(obj)
            Dim str2 = Utils.Serialize(Of Taskbar)(Me)
            Dim B = If(str1 = str2, True, False)
            'MsgBox("Taskbar : " & B.ToString)
            Return B
        End Function
#End Region

    End Class

End Namespace
