﻿Imports System.ComponentModel
Imports System.Reflection
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper

Namespace Tweaks
    <Serializable>
    Public Class Loader
        Inherits Notifier

#Region " Properties "
        Public Property Enabled As Boolean
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub

        Public Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "
        Public Sub Save(Sid As String, Optional ByVal FirstCreationAccount As Boolean = False)
            If FirstCreationAccount = False Then
                m_Bgw.ReportProgress(-1, Messages.ApplyUserStratLauncher)

                Dim ass = Assembly.GetEntryAssembly
                Dim location = ass.Location
                Dim fName = ass.GetName.Name

                Dim task As New Scheduler

                If FirstCreationAccount Then
                    task.deleteTask(fName)
                    task.CreateTask(Sid, fName, location)
                Else
                    If Enabled Then
                        task.deleteTask(fName)
                        task.CreateTask(Sid, fName, location)
                    Else
                        task.deleteTask(fName)
                    End If
                End If
            End If
        End Sub

        Public Sub Load(Sid As String)
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(-1, Messages.LoadUserStratLauncher)
            End If
            Dim ass = Assembly.GetEntryAssembly
            Dim location = ass.Location
            Dim fName = ass.GetName.Name
            Dim task As New Scheduler

            Me.Enabled = task.ExistsTask(fName)
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(98, Me.Enabled)
            End If
        End Sub

        Public Sub SetBoolProperty(val As Boolean)
            Enabled = val
        End Sub

        Public Function EqualsTo(obj As Loader) As Boolean
            Dim str1 = Utils.Serialize(Of Loader)(obj)
            Dim str2 = Utils.Serialize(Of Loader)(Me)
            Dim B = If(str1 = str2, True, False)
            'MsgBox("Loader : " & B.ToString)
            Return B
        End Function
#End Region

    End Class
End Namespace

