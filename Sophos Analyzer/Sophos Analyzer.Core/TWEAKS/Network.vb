﻿Imports System.ComponentModel
Imports Sophos_Analyzer.Core.Adapters
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper

Namespace Tweaks
    <Serializable>
    Public Class Network
        Inherits Notifier

#Region " Properties "
        Public Property DisabledInterfaces As Boolean
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub

        Public Sub New(Bgw As BackgroundWorker)
            m_Bgw = Bgw
        End Sub
#End Region

#Region " Methods "
        Public Sub Save(Optional ByVal FirstCreationAccount As Boolean = False)
            Card.GetAllNetworkAdapter()

            If FirstCreationAccount Then
                m_Bgw.ReportProgress(70, "Paramétrage des cartes réseaux ...")
                For Each card In Adapters.Card.AllNetworkAdapter
                    If card.NetEnabled = 1 Then
                        card.EnableOrDisableNetworkAdapter("Disable")
                    End If
                Next
                DisabledInterfaces = True
            Else
                If Me.DisabledInterfaces Then
                    m_Bgw.ReportProgress(-1, Messages.ApplyDisabledAdapters)
                    For Each card In Adapters.Card.AllNetworkAdapter
                        If card.NetEnabled = 1 Then
                            card.EnableOrDisableNetworkAdapter("Disable")
                        End If
                    Next
                Else
                    m_Bgw.ReportProgress(-1, Messages.ApplyEnabledAdapters)
                    For Each card In Adapters.Card.AllNetworkAdapter
                        If card.NetEnabled = -1 Then
                            card.EnableOrDisableNetworkAdapter("Enable")
                        End If
                    Next
                End If
            End If
        End Sub

        Public Sub Load()
            If Not m_Bgw Is Nothing Then
                m_Bgw.ReportProgress(-1, Messages.LoadInterfaces)
            End If
            Card.GetAllNetworkAdapter(m_Bgw)
            DisabledInterfaces = If(Card.AllNetworkAdapter.Any(Function(f) f.NetEnabled = 1), False, True)
        End Sub

        Public Sub SetBoolProperty(val As Boolean)
            DisabledInterfaces = val
        End Sub

        Public Function EqualsTo(obj As Network) As Boolean
            Dim str1 = Utils.Serialize(Of Network)(obj)
            Dim str2 = Utils.Serialize(Of Network)(Me)
            Dim B = If(str1 = str2, True, False)
            'MsgBox("Network : " & B.ToString)
            Return B
        End Function
#End Region

    End Class
End Namespace
