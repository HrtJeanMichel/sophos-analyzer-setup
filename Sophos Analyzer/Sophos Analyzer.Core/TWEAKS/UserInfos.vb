﻿Imports System.ComponentModel
Imports System.DirectoryServices.AccountManagement
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports Microsoft.Win32
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Helper
Imports Sophos_Analyzer.Win32

Namespace Tweaks
    Public Class UserInfos

#Region " Properties "
        Public Property Name As String
        Public Property Disabled As Boolean
        Public Property UserCannotChangePassword As Boolean
        Public Property PasswordNeverExpires As Boolean
        Public Property Description As String
        Public Property SID As String
        Public Property Group As String
        Public Property IsSuperAdmin As Boolean
        Public Property IsInvite As Boolean

        Public ReadOnly Property ProfileImagePath() As String
            Get
                Return GetProfileImagePath()
            End Get
        End Property

        Public ReadOnly Property NtUserDatFilePath() As String
            Get
                Return GetProfileImagePath() & "\NTUSER.DAT"
            End Get
        End Property

#End Region

#Region " Constructors "
        Public Sub New(AccountName As String, RetrieveAccount As Boolean)
            Name = AccountName
            If RetrieveAccount Then
                GetUserByName()
            End If
        End Sub

        Public Sub New()
        End Sub
#End Region

#Region " Methods "
        Public Function IsLoggedIn() As Boolean
            Using rk As RegistryKey = Registry.Users.OpenSubKey(SID)
                Return (rk IsNot Nothing)
            End Using
        End Function

        Public Function IsCorrectAdminGroup() As Boolean
            'MsgBox(Group)
            If Not String.IsNullOrEmpty(Group) Then
                If Group.Contains("|") Then
                    Dim Grps = Group.Split("|")
                    Dim containsAdmin As Boolean = Grps.Any(Function(f) f.ToLower = If(OS.GetInstalledUICulture = 1036, "administrateurs", "administrators"))
                    Dim containsSophosAdmin As Boolean = Grps.Any(Function(f) f.ToLower = If(OS.GetInstalledUICulture = 1036, "sophosadministrator", "sophosadministrator"))
                    Return If(containsAdmin And containsSophosAdmin, True, False)
                Else
                    Return False
                End If
            Else
                Return False
            End If

        End Function

        ''' <summary>
        ''' Génère l'arboresence de profile d'un compte existant :
        ''' - recherche si l'utilisateur existe
        ''' - recherche de groupes associés, si inexistant alors ajout au goupe Administrateurs
        ''' - création de l'arborescence du profile
        ''' </summary>
        ''' <param name="acc">Nom utilisateur</param>
        ''' <param name="Bgw">Instance du thread principal</param>
        ''' <returns>Valeur booléenne pour signifier l'aboutissemet ou l'échec</returns>
        Public Function Generate(Bgw As BackgroundWorker) As Boolean
            Try
                Bgw.ReportProgress(-1, Messages.SidStartDelete)
                DeleteSid(SID.ToString)

                Bgw.ReportProgress(-1, Messages.StartUserSearch)
                Dim up As UserPrincipal = Nothing
                Using pc As New PrincipalContext(ContextType.Machine)
                    up = UserPrincipal.FindByIdentity(pc, Name)

                    Bgw.ReportProgress(-1, Messages.StartGroupSearch)

                    If up.GetGroups.Count = 0 Then
                        Bgw.ReportProgress(-1, Messages.StartAddToUsersGroup)
                        Dim gPc1 As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, If(OS.GetInstalledUICulture = 1036, "Administrateurs", "Administrators"))
                        Dim gPc2 As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, If(OS.GetInstalledUICulture = 1036, "SophosAdministrator", "SophosAdministrator"))
                        gPc1.Members.Add(up)
                        gPc1.Save()
                        gPc2.Members.Add(up)
                        gPc2.Save()
                    ElseIf up.GetGroups.Count = 2 Then
                        Dim containsAdmin As Boolean = up.GetGroups.Any(Function(f) f.SamAccountName = If(OS.GetInstalledUICulture = 1036, "Administrateurs", "Administrators"))
                        Dim containsSophosAdmin As Boolean = up.GetGroups.Any(Function(f) f.SamAccountName = If(OS.GetInstalledUICulture = 1036, "SophosAdministrator", "SophosAdministrator"))

                        If containsAdmin = False And containsSophosAdmin = False Then
                            For Each grp In up.GetGroups
                                Dim group As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, grp.Name)
                                If Not group Is Nothing Then
                                    group.Members.Remove(pc, IdentityType.SamAccountName, up.SamAccountName)
                                    group.Save()
                                End If
                            Next

                            Dim gPcAdministrator As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, If(OS.GetInstalledUICulture = 1036, "Administrateurs", "Administrators"))
                            Dim gPcAdminSophos As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, If(OS.GetInstalledUICulture = 1036, "SophosAdministrator", "SophosAdministrator"))

                            If Not gPcAdministrator Is Nothing Then
                                gPcAdministrator.Members.Add(up)
                                gPcAdministrator.Save()
                            End If

                            If Not gPcAdminSophos Is Nothing Then
                                gPcAdminSophos.Members.Add(up)
                                gPcAdminSophos.Save()
                            End If
                        End If
                    Else
                        For Each grp In up.GetGroups
                            Dim group As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, grp.Name)
                            If Not group Is Nothing Then
                                group.Members.Remove(pc, IdentityType.SamAccountName, up.SamAccountName)
                                group.Save()
                            End If
                        Next

                        Dim gPcAdministrator As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, If(OS.GetInstalledUICulture = 1036, "Administrateurs", "Administrators"))
                        Dim gPcAdminSophos As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, If(OS.GetInstalledUICulture = 1036, "SophosAdministrator", "SophosAdministrator"))

                        If Not gPcAdministrator Is Nothing Then
                            gPcAdministrator.Members.Add(up)
                            gPcAdministrator.Save()
                        End If

                        If Not gPcAdminSophos Is Nothing Then
                            gPcAdminSophos.Members.Add(up)
                            gPcAdminSophos.Save()
                        End If
                    End If

                    Bgw.ReportProgress(-1, Messages.ProfilDirCreate)
                    Dim MaxPath% = 240
                    Dim pathBuf As New StringBuilder(MaxPath)
                    Dim pathLen = CUInt(pathBuf.Capacity)
                    Dim Res = NativeMethods.CreateProfile(up.Sid.ToString(), Name.Trim, pathBuf, pathLen)
                    SID = up.Sid.ToString()
                    Return True
                End Using
            Catch ex As Exception
                Bgw.ReportProgress(-1, "Exception non gérée depuis Generate : " & ex.Message)
                Return False
            End Try
            Return False
        End Function

        Public Function DeleteAccountProfilByName(AccountSid As String, AccountProfilPath As String) As Boolean
            Try
                Using computerContext As New PrincipalContext(ContextType.Machine)
                    NativeMethods.DeleteProfile(AccountSid, AccountProfilPath, computerContext.Name)
                    DeleteSid(AccountSid)
                    Return True
                End Using
            Catch ex As Exception
                Return False
            End Try
            Return False
        End Function

        Public Function UserExists(Optional ByVal ExcludeUserName As String = "") As Boolean
            Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                Using user As New UserPrincipal(ctx)
                    user.Name = "*"
                    Using ps As New PrincipalSearcher()
                        ps.QueryFilter = user
                        Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll()
                        For Each p As Principal In result
                            Using up As UserPrincipal = DirectCast(p, UserPrincipal)
                                If ExcludeUserName = "" Then
                                    If up.Name.ToLower = Name.ToLower Then
                                        SID = up.Sid.ToString
                                        Return True
                                    End If
                                Else
                                    If up.Name.ToLower = ExcludeUserName.ToLower Then
                                    Else
                                        If up.Name.ToLower = Name.ToLower Then
                                            SID = up.Sid.ToString
                                            Return True
                                        End If
                                    End If
                                End If
                            End Using
                        Next
                    End Using
                End Using
            End Using
            Return False
        End Function

        ''' <summary>
        ''' Supprime le SID dans le registre.
        ''' </summary>
        ''' <param name="SID">SID utilisateur</param>
        Private Sub DeleteSid(SID As String)
            Try
                DeleteSubKey(Paths.ProfilListPathWow6432, SID)
                DeleteSubKey(Paths.ProfilListPath, SID)
            Catch ex As Exception
            End Try
        End Sub

        Private Sub DeleteSubKey(path As String, Sid As String)
            Using key As RegistryKey = Registry.LocalMachine.OpenSubKey(path, True)
                For Each subkeyName As String In key.GetSubKeyNames()
                    If subkeyName.ToLower = Sid.ToLower OrElse subkeyName.ToLower = (Sid & ".bak").ToLower Then
                        key.DeleteSubKey(subkeyName)
                    End If
                Next
            End Using
        End Sub

        ''' <summary>
        ''' Modifie les informations du compte utilisateur
        ''' </summary>
        ''' <param name="Bgw">Backgroundworker permettant d'afficher les messages de progression</param>
        Public Sub ChangeUserInfos()
            Try
                Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                    Using userP As New UserPrincipal(ctx)
                        userP.Name = "*"
                        Using ps As New PrincipalSearcher()
                            ps.QueryFilter = userP
                            Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll()
                            For Each p As Principal In result
                                Using user As UserPrincipal = DirectCast(p, UserPrincipal)
                                    If user.Name.ToLower = Name.ToLower Then
                                        user.DisplayName = String.Empty
                                        user.Description = Description
                                        user.UserCannotChangePassword = UserCannotChangePassword
                                        user.PasswordNeverExpires = PasswordNeverExpires
                                        user.Enabled = If(Disabled, False, True)

                                        Dim groups As PrincipalSearchResult(Of Principal) = user.GetGroups

                                        For Each grp In groups
                                            Dim group As GroupPrincipal = GroupPrincipal.FindByIdentity(ctx, grp.Name)
                                            If Not group Is Nothing Then
                                                group.Members.Remove(ctx, IdentityType.SamAccountName, user.SamAccountName)
                                                group.Save()
                                            End If
                                        Next

                                        Dim gPcAdministrator As GroupPrincipal = GroupPrincipal.FindByIdentity(ctx, If(OS.GetInstalledUICulture = 1036, "Administrateurs", "Administrators"))
                                        Dim gPcAdminSophos As GroupPrincipal = GroupPrincipal.FindByIdentity(ctx, If(OS.GetInstalledUICulture = 1036, "SophosAdministrator", "SophosAdministrator"))

                                        If Not gPcAdministrator Is Nothing Then
                                            gPcAdministrator.Members.Add(user)
                                            gPcAdministrator.Save()
                                        End If

                                        If Not gPcAdminSophos Is Nothing Then
                                            gPcAdminSophos.Members.Add(user)
                                            gPcAdminSophos.Save()
                                        End If

                                        user.UnlockAccount()
                                        user.Save()
                                    End If
                                End Using
                            Next
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MsgBox("Exception non gérée depuis ChangeUserInfos : " & ex.Message)
            End Try
        End Sub

        Private Function GetProfileImagePath() As String
            Dim ProfPath = Reg.SZ.GetValue(Paths.ProfilListPath & SID, "ProfileImagePath")
            If ProfPath = "" Then
                ProfPath = Reg.SZ.GetValue(Paths.ProfilListPathWow6432 & SID, "ProfileImagePath")
            End If
            If Not Directory.Exists(ProfPath) Then
                ProfPath = Environment.ExpandEnvironmentVariables(Reg.SZ.GetValue(Paths.ProfilListPath, "ProfilesDirectory"))
                Dim pPath As String = Path.GetPathRoot(ProfPath)
                If Not Directory.Exists(pPath) Then
                    Reg.SZ.SetValue(Paths.ProfilListPath, "ProfilesDirectory", "%SystemDrive%\Users", True)
                    Reg.SZ.SetValue(Paths.ProfilListPath & SID, "ProfileImagePath", "%SystemDrive%\Users\" & Name, True)
                    Reg.SZ.SetValue(Paths.ProfilListPathWow6432 & SID, "ProfileImagePath", "%SystemDrive%\Users\" & Name, True)
                    Return Environment.ExpandEnvironmentVariables("%SystemDrive%") & "\Users\" & Name
                Else
                    Reg.SZ.SetValue(Paths.ProfilListPath & SID, "ProfileImagePath", ProfPath & "\" & Name, True)
                    Reg.SZ.SetValue(Paths.ProfilListPathWow6432 & SID, "ProfileImagePath", ProfPath & "\" & Name, True)
                    Return ProfPath & "\" & Name
                End If
            End If
            Return ProfPath
        End Function

        Private Function GetUserName(sessionId As Integer, server As IntPtr) As String
            Dim buffer As IntPtr = IntPtr.Zero
            Dim count As UInteger = 0
            Dim userName As String = String.Empty
            Try
                NativeMethods.WTSQuerySessionInformation(server, sessionId, NativeEnum.WTS_INFO_CLASS.WTSUserName, buffer, count)
                userName = Marshal.PtrToStringAnsi(buffer).ToUpper().Trim()
            Finally
                NativeMethods.WTSFreeMemory(buffer)
            End Try
            Return userName
        End Function

        Public Function LogoffSession() As Boolean
            Dim sessions As NativeStruct.strSessionsInfo() = GetSessions(Environment.MachineName)
            Dim server As IntPtr
            Dim b As Boolean = False
            Try
                server = NativeMethods.WTSOpenServer(Environment.MachineName)
                For Each u In sessions
                    If Name.Trim.ToUpper = u.SessionUserName.Trim.ToUpper Then
                        If u.ConnectionState = "Disconnected" Then
                            b = NativeMethods.WTSLogoffSession(server, u.SessionID, True)
                            Exit For
                        End If
                    End If
                Next
            Catch ex As Exception
                Return False
                'Throw New Exception(ex.Message)
            Finally
                NativeMethods.WTSCloseServer(server)
            End Try
            Return b
        End Function

        Private Function GetSessions(ServerName As String) As NativeStruct.strSessionsInfo()
            Dim ptrOpenedServer As IntPtr
            Dim RetVal As NativeStruct.strSessionsInfo()
            Try
                ptrOpenedServer = NativeMethods.WTSOpenServer(ServerName)
                Dim FRetVal As Integer
                Dim ppSessionInfo As IntPtr = IntPtr.Zero
                Dim Count As Integer = 0
                Try
                    FRetVal = NativeMethods.WTSEnumerateSessions(ptrOpenedServer, 0, 1, ppSessionInfo, Count)
                    If FRetVal <> 0 Then
                        Dim sessionInfo() As NativeStruct.WTS_SESSION_INFO = New NativeStruct.WTS_SESSION_INFO(Count) {}
                        Dim i As Integer
                        Dim DataSize = Marshal.SizeOf(New NativeStruct.WTS_SESSION_INFO)
                        Dim current As Long
                        current = ppSessionInfo.ToInt64
                        For i = 0 To Count - 1
                            sessionInfo(i) = CType(Marshal.PtrToStructure(New IntPtr(current), GetType(NativeStruct.WTS_SESSION_INFO)), NativeStruct.WTS_SESSION_INFO)
                            current = current + DataSize
                        Next
                        NativeMethods.WTSFreeMemory(ppSessionInfo)
                        Dim tmpArr(sessionInfo.GetUpperBound(0)) As NativeStruct.strSessionsInfo
                        For i = 0 To tmpArr.GetUpperBound(0)
                            tmpArr(i).SessionID = sessionInfo(i).SessionID
                            tmpArr(i).ConnectionState = GetConnectionState(sessionInfo(i).State)
                            tmpArr(i).SessionUserName = GetUserName(sessionInfo(i).SessionID, ptrOpenedServer)
                        Next
                        ReDim sessionInfo(-1)
                        RetVal = tmpArr
                    Else
                        Throw New ApplicationException("Aucune donnée retournée !")
                    End If
                Catch ex As Exception
                    Throw New Exception(ex.Message & vbCrLf & Marshal.GetLastWin32Error)
                End Try
            Catch ex As Exception
                Throw New Exception(ex.Message)
                Exit Function
            Finally
                NativeMethods.WTSCloseServer(ptrOpenedServer)
            End Try

            Return RetVal
        End Function

        Private Function GetConnectionState(State As NativeEnum.WTS_CONNECTSTATE_CLASS) As String
            Dim RetVal As String
            Select Case State
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSActive
                    RetVal = "Active"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSConnected
                    RetVal = "Connected"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSConnectQuery
                    RetVal = "Query"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSDisconnected
                    RetVal = "Disconnected"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSDown
                    RetVal = "Down"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSIdle
                    RetVal = "Idle"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSInit
                    RetVal = "Initializing"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSListen
                    RetVal = "Listen"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSReset
                    RetVal = "reset"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSShadow
                    RetVal = "Shadowing"
                Case Else
                    RetVal = "Unknown connect state"
            End Select
            Return RetVal
        End Function

        Private Sub GetUserByName()
            Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                Using user As New UserPrincipal(ctx)
                    user.Name = "*"
                    Using ps As New PrincipalSearcher()
                        ps.QueryFilter = user
                        Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll()
                        For Each p As Principal In result
                            Using up As UserPrincipal = DirectCast(p, UserPrincipal)
                                If Not up.Name = "HomeGroupUser$" AndAlso
                                    Not up.Name = "DefaultAccount" AndAlso
                                    Not up.Name.StartsWith("SophosSAU") AndAlso
                                    Not up.Name = Environment.UserName AndAlso
                                    Not up.Name = OS.GetUsernameBySessionId() AndAlso
                                    up.Sid.ToString.StartsWith("S-1-5") AndAlso up.Sid.ToString.EndsWith("-500") = False AndAlso
                                    up.Sid.ToString.StartsWith("S-1-5") AndAlso up.Sid.ToString.EndsWith("-501") = False Then
                                    If up.Name.ToLower = Name.ToLower Then
                                        Disabled = If(up.Enabled, False, True)
                                        Description = up.Description
                                        Dim groups As String = ""
                                        For Each g In up.GetGroups
                                            groups &= g.Name & "|"
                                        Next
                                        IsSuperAdmin = If(up.Sid.ToString.StartsWith("S-1-5") AndAlso up.Sid.ToString.EndsWith("-500"), True, False)
                                        IsInvite = If(up.Sid.ToString.StartsWith("S-1-5") AndAlso up.Sid.ToString.EndsWith("-501"), True, False)
                                        Group = groups.TrimEnd("|")
                                        UserCannotChangePassword = If(up.UserCannotChangePassword, True, False)
                                        PasswordNeverExpires = If(up.PasswordNeverExpires, True, False)
                                        SID = up.Sid.ToString
                                    End If
                                End If
                            End Using
                        Next
                    End Using
                End Using
            End Using
        End Sub
#End Region

    End Class
End Namespace
