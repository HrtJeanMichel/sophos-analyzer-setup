﻿Imports Sophos_Analyzer.Win32
Imports Sophos_Analyzer.Helper

Namespace Usb
    Public Class Disk

#Region " Fields "
        Private m_Model As String
#End Region

#Region " Properties "
        Public Property DeviceID As String
        Public Property PNPDeviceID As String
        Public Property Index As Integer
        Public Property InterfaceType As String

        Public Property Model() As String
            Get
                Return m_Model.ToLower.Replace("usb device", String.Empty).Trim.ToUpper
            End Get
            Set(ByVal value As String)
                m_Model = value
            End Set
        End Property

        Public Property Partitions As List(Of Partition)
        Public Property SerialNumber As String
        Public Property Size As Long
#End Region

#Region " Constructor "
        Sub New()
            Partitions = New List(Of Partition)
        End Sub
#End Region

#Region " Methods "
        ''' <summary>
        ''' On retourne la taille arrondie avec son suffixe (Eo, Po, To, Go, Mo, Ko, octets).
        ''' </summary>
        Public Function SizeTotalSpace(Optional Ceil As Boolean = False) As String
            ' On récupère la valeur absolue
            Dim absolute_i As Long = (If(Size < 0, -Size, Size))
            ' On détermine le suffixe, son arrondi et sa valeur
            Dim suffix As String
            Dim ceiling As String
            Dim readable As Double
            If absolute_i >= &H1000000000000000L Then
                ' Exabyte
                suffix = "Eo"
                readable = (Size >> 50)
                ceiling = Format(Val((CDbl(Size) / 1000000000000000000)), "#")
            ElseIf absolute_i >= &H4000000000000L Then
                ' Petabyte
                suffix = "Po"
                readable = (Size >> 40)
                ceiling = Format(Val((CDbl(Size) / 1000000000000000)), "#")
            ElseIf absolute_i >= &H10000000000L Then
                ' Terabyte
                suffix = "To"
                readable = (Size >> 30)
                ceiling = Format(Val((CDbl(Size) / 1000000000000)), "#.#")
            ElseIf absolute_i >= &H40000000 Then
                ' Gigabyte
                suffix = "Go"
                readable = (Size >> 20)
                ceiling = Format(Val((CDbl(Size) / 1000000000)), "#,###")
            ElseIf absolute_i >= &H100000 Then
                ' Megabyte
                suffix = "Mo"
                readable = (Size >> 10)
                ceiling = Format(Val((CDbl(Size) / 1000000)), "#,####")
            ElseIf absolute_i >= &H400 Then
                ' Kilobyte
                suffix = "Ko"
                readable = Size
                ceiling = Format(Val((CDbl(Size) / 1000)), "#,#####")
            Else
                ' Byte
                Return Size.ToString("0 octet")
            End If

            'On retourne le nombre formaté et arrondi
            If Ceil Then
                Return ceiling & " " & suffix
            End If
            ' Division par 1024 pour obtenir la valeur fractionnée
            readable = (readable / 1024)
            Return readable.ToString("0.## ") & suffix
        End Function

        Public Function SizeFreeSpace() As String
            Return OS.SizeStrByteFormat(SizeFreeSpaceULong)
        End Function

        Private Function SizeFreeSpaceULong() As ULong
            Dim Total As ULong
            For Each p In Partitions
                Dim FreeBytesAvailable As ULong
                Dim TotalNumberOfBytes As ULong
                Dim TotalNumberOfFreeBytes As ULong
                NativeMethods.GetDiskFreeSpaceEx(p.Letter, FreeBytesAvailable, TotalNumberOfBytes, TotalNumberOfFreeBytes)
                Total += FreeBytesAvailable
            Next
            Return Total
        End Function

        Public Function SizeWriteSpace() As String
            Return OS.SizeStrByteFormat(Size - SizeFreeSpaceULong())
        End Function

        Public Function PartitionLettersArray() As String()
            Dim str As String() = New String(Partitions.Count - 1) {}
            If Partitions.Count <> 0 Then
                For i As Integer = 0 To Partitions.Count - 1
                    str(i) = Partitions(i).Letter
                Next
            End If
            Return str
        End Function

        Public Function PartitionLettersInline() As String
            Dim pStr As String = String.Empty
            If Partitions.Count <> 0 Then
                For Each p In Partitions
                    pStr &= p.Letter & " "
                Next
            End If
            Return pStr.TrimEnd
        End Function

#End Region

    End Class

End Namespace

