﻿Imports System.Management
Imports Sophos_Analyzer.Core.Usb.ChangedEventArgs
Imports Sophos_Analyzer.Win32

Namespace Usb

    Public Class Detector
        Implements IDisposable

#Region " Fields "
        Private insertWatcher As ManagementEventWatcher
#End Region

#Region " Events "
        Public Event DeviceInserted As DeviceInsertedHandler
#End Region

#Region " Enumerations "
        Private Enum PendingDrivers
            Failed = 1
            Ok = 2
        End Enum
#End Region

#Region " Public Delegates "
        Public Delegate Sub DeviceInsertedHandler(sender As Object, eventArgs As ChangedEventArgs)
#End Region

#Region " Constructor "
        Public Sub New()
            EstablishWatchedEvents()
        End Sub
#End Region

#Region " Methods "
        Private Sub EstablishWatchedEvents()
            Try
                Dim weqQuery As New WqlEventQuery() With {.EventClassName = "__InstanceOperationEvent",
                                                          .WithinInterval = New TimeSpan(0, 0, 0, 1),
                                                          .Condition = "TargetInstance ISA 'Win32_DiskDrive'"}
                Dim msScope As New ManagementScope("root\CIMV2")
                msScope.Options.EnablePrivileges = True
                insertWatcher = New ManagementEventWatcher(msScope, weqQuery)
                AddHandler insertWatcher.EventArrived, New EventArrivedEventHandler(AddressOf insertWatcher_EventArrived)
                insertWatcher.Start()
            Catch ex As Exception
                'MsgBox(ex.Message)
            End Try
        End Sub

        Private Sub insertWatcher_EventArrived(sender As Object, e As EventArrivedEventArgs)
            Dim bUSBEvent As Boolean = False

            Dim installing = NativeMethods.CMP_WaitNoPendingInstallEvents(0)
            Dim result As PendingDrivers = PendingDrivers.Failed
            Do
                If installing = NativeConstants.WAIT_TIMEOUT Then

                ElseIf installing = NativeConstants.WAIT_FAILED Then
                    result = PendingDrivers.Failed
                    Exit Do
                ElseIf installing = NativeConstants.WAIT_OBJECT_0 Then
                    result = PendingDrivers.Ok
                    Exit Do
                End If
                Threading.Thread.Sleep(1000)
                installing = NativeMethods.CMP_WaitNoPendingInstallEvents(0)
            Loop
            If result = PendingDrivers.Ok Then
                Try
                    For Each pdData As PropertyData In e.NewEvent.Properties
                        Dim mbo As ManagementBaseObject = CType(pdData.Value, ManagementBaseObject)
                        If mbo IsNot Nothing AndAlso Not mbo("InterfaceType") Is Nothing Then
                            bUSBEvent = If(mbo("InterfaceType").ToString = "USB", True, False)
                            If bUSBEvent Then
                                Dim i% = mbo("Name").ToString().Trim().Substring(mbo("Name").ToString().Trim().Length - 1, 1)

                                Dim hdd As New Disk
                                With hdd
                                    .Index = i
                                    .DeviceID = mbo("DeviceID").ToString().Trim()
                                    .InterfaceType = mbo("InterfaceType").ToString().Trim()
                                    .Model = mbo("Model").ToString().Trim()
                                    .PNPDeviceID = mbo("PNPDeviceID").ToString().Trim()
                                    .SerialNumber = parseSerialFromDeviceID(.PNPDeviceID)
                                    .Size = CLng(mbo("Size"))
                                End With

                                Dim partits As New List(Of Partition)

                                If e.NewEvent.ClassPath.ClassName = "__InstanceCreationEvent" Then
                                    For Each partition As ManagementObject In New ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" & mbo.Properties("DeviceID").Value.ToString & "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get()
                                        For Each disk As ManagementObject In New ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" & partition("DeviceID").ToString & "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get()
                                            'Threading.Thread.Sleep(1000)
                                            Dim part As New Partition
                                            With part
                                                .DiskIndex = i
                                                .Letter = disk("Name")
                                                .TotalSize = disk("Size")
                                                .VolumeLabel = disk("VolumeName")
                                            End With
                                            partits.Add(part)
                                        Next
                                    Next
                                    hdd.Partitions = partits

                                    RaiseEvent DeviceInserted(Me, New ChangedEventArgs() With {
                                         .DeviceState = State.Added,
                                         .Disk = hdd})
                                    Exit For

                                ElseIf e.NewEvent.ClassPath.ClassName = "__InstanceDeletionEvent" Then
                                    RaiseEvent DeviceInserted(Me, New ChangedEventArgs() With {
                                       .DeviceState = State.Removed,
                                       .Disk = hdd})
                                    Exit For
                                    'Else
                                    '    MsgBox(e.NewEvent.ClassPath.ClassName)
                                End If
                                Exit For
                            End If
                        End If
                    Next
                Catch ex As Exception
                    'MsgBox("Detection Failed Exception : " & ex.ToString)
                    DetectionFailed(e)
                End Try
            ElseIf result = PendingDrivers.Failed Then
                'MsgBox("Detection Failed Other !")
                DetectionFailed(e)
            End If
        End Sub

        Private Sub DetectionFailed(e As EventArrivedEventArgs)
            If e.NewEvent.ClassPath.ClassName = "__InstanceCreationEvent" Then
                RaiseEvent DeviceInserted(Me, New ChangedEventArgs() With {
                                   .DeviceState = State.Added, .Disk = Nothing, .Fail = True})
            ElseIf e.NewEvent.ClassPath.ClassName = "__InstanceDeletionEvent" Then
                RaiseEvent DeviceInserted(Me, New ChangedEventArgs() With {
                            .DeviceState = State.Removed, .Disk = Nothing, .Fail = True})
            End If
        End Sub

        Private Function parseSerialFromDeviceID(deviceId As String) As String
            Dim splitDeviceId As String() = deviceId.Split("\"c)
            Dim serialArray As String()
            Dim serial As String
            Dim arrayLen As Integer = splitDeviceId.Length - 1
            serialArray = splitDeviceId(arrayLen).Split("&"c)
            serial = serialArray(0)
            Return serial
        End Function

        Public Sub Dispose() Implements IDisposable.Dispose
            insertWatcher.Stop()
            insertWatcher.Dispose()
        End Sub
#End Region

    End Class
End Namespace