﻿Namespace Usb

    Public Class ChangedEventArgs
        Inherits EventArgs

#Region " Enumerations "
        Public Enum State
            Added = 1
            Removed = 2
        End Enum
#End Region

#Region " Properties "
        Public Property DeviceState As State
        Public Property DeviceName As String
        Public Property Disk As Disk
        Public Property Fail As Boolean
#End Region

    End Class
End Namespace
