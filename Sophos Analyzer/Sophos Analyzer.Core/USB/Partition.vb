﻿Namespace Usb
    Public Class Partition

#Region " Properties "
        Public Property Letter As String
        Public Property TotalSize As Long
        Public Property VolumeLabel As String
        Public Property DiskIndex As Integer
#End Region

    End Class
End Namespace
