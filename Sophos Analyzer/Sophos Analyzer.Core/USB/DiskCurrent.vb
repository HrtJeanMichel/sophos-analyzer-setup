﻿Namespace Usb
    Public Class DiskCurrent
        Inherits Disk

#Region " Constructor "
        Sub New()
            MyBase.New()
        End Sub
#End Region

#Region " Properties "
        Public Property Plugged As Boolean
        Public Property Scanning As Boolean
        Public Property Grabbed As Boolean
#End Region

#Region " Methods "
        Public Sub CleanupProperties()
            Model = String.Empty
            Plugged = False
            Scanning = False
            Grabbed = False
            Partitions.Clear()
            SerialNumber = String.Empty
            DeviceID = String.Empty
            PNPDeviceID = String.Empty
            Index = 0
            InterfaceType = String.Empty
        End Sub

        Public Function HasZeroByte() As Boolean
            Return If(SizeTotalSpace(True) = "0 octet", True, False)
        End Function


#End Region

    End Class
End Namespace

