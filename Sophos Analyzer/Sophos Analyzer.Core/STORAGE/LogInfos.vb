﻿Namespace Storage

    Public Class LogInfos

#Region " Properties "
        Public Property FileName As String
        Public Property Md5 As String
        Public Property ScanDate As String
        Public Property ScanResult As String
        Public Property Action As String
        Public Property ActionTime As String
        Public Property ActionResult As String
        Public Property FromDevice As String
        Public Property ToDevice As String
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub
#End Region

    End Class
End Namespace
