﻿Imports System.IO
Imports System.Xml.Serialization
Imports Sophos_Analyzer.Core.Settings

Namespace Storage

    <Serializable>
    Public Class LogDatabase

#Region " Fields "
        <XmlIgnore()>
        Private m_LogFiles As List(Of LogInfos)
        <XmlIgnore()>
        Private m_LogFileName As String
#End Region

#Region " Properties "
        Public Property UserName As String

        Public Property LogFiles() As List(Of LogInfos)
            Get
                Return m_LogFiles
            End Get
            Set(value As List(Of LogInfos))
                m_LogFiles = value
            End Set
        End Property
#End Region

#Region " Constructors "
        Public Sub New()
        End Sub

        Public Sub New(uName As String)
            UserName = uName
            m_LogFileName = Date.Now.ToString("yyyyMMdd_HH_mm_ss") & "_" & UserName & ".xml"
            If Not Directory.Exists(Paths.SecureFileTransfer) Then
                Directory.CreateDirectory(Paths.SecureFileTransfer)
            End If
            m_LogFiles = New List(Of LogInfos)
        End Sub

        Public Sub ExtractPedSearcher()
            If Not File.Exists(Paths.SecureFileTransfer & "\PedSearcher.exe") Then
                File.WriteAllBytes(Paths.SecureFileTransfer & "\PedSearcher.exe", My.Resources.PedSearcher)
            End If
        End Sub
#End Region

#Region " Methods "
        ''' <summary>
        ''' Enregistre l'état courant de la classe dans un fichier au format XML.
        ''' </summary>
        Public Sub SaveFile()
            Dim serializer As New XmlSerializer(GetType(LogDatabase))
            Dim Save As New StreamWriter(Paths.SecureFileTransfer & "\" & m_LogFileName)
            serializer.Serialize(Save, Me)
            Save.Close()
            Save.Dispose()
        End Sub

        ''' <summary>
        ''' Charge l'état courant du fichier XML.
        ''' </summary>
        ''' <returns>Valeur de type AppSettings</returns>
        Public Shared Function LoadFile(dbName As String) As LogDatabase
            Dim deserializer As New XmlSerializer(GetType(LogDatabase))
            Dim read As New StreamReader(Paths.SecureFileTransfer & "\" & dbName)
            Dim p As LogDatabase = DirectCast(deserializer.Deserialize(read), LogDatabase)
            read.Close()
            read.Dispose()
            Return p
        End Function
#End Region

    End Class
End Namespace
