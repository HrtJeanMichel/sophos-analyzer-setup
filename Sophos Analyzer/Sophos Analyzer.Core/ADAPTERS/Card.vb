﻿Imports System.Management
Imports System.Threading
Imports System.ComponentModel

Namespace Adapters

    Public NotInheritable Class Card

#Region " Fields "
        Private Shared m_AllNetworkAdapter As List(Of Card)
#End Region

#Region " Properties "
        Public Shared ReadOnly Property AllNetworkAdapter As List(Of Card)
            Get
                Return m_AllNetworkAdapter
            End Get
        End Property
        ''' <summary>
        ''' The DeviceID of the NetworkAdapter
        ''' </summary>
        Public Property DeviceId() As Integer

        ''' <summary>
        ''' The ProductName of the NetworkAdapter
        ''' </summary>
        Public Property Name() As String

        ''' <summary>
        ''' The NetEnabled status of the NetworkAdapter
        ''' </summary>
        Public Property NetEnabled() As Integer

        ''' <summary>
        ''' The Net Connection Status Value
        ''' </summary>
        Public Property NetConnectionStatus() As Integer

#End Region

#Region " Shared Constructor "
        Shared Sub New()
            m_AllNetworkAdapter = New List(Of Card)
        End Sub
#End Region

#Region " Constructors "
        Public Sub New(deviceId__1 As Integer, name__2 As String, netEnabled__3 As Integer, netConnectionStatus__4 As Integer)
            DeviceId = deviceId__1
            Name = name__2
            NetEnabled = netEnabled__3
            NetConnectionStatus = netConnectionStatus__4
        End Sub

        Public Sub New(deviceId__1 As Integer)
            Dim crtNetworkAdapter As New ManagementObject()
            Dim strWQuery As String = String.Format("SELECT DeviceID, ProductName, NetConnectionStatus, ConfigManagerErrorCode FROM Win32_NetworkAdapter WHERE DeviceID = {0}", deviceId__1)
            Try
                Dim networkAdapters As ManagementObjectCollection = WMIQuery(strWQuery)

                For Each networkAdapter__2 As ManagementObject In networkAdapters
                    crtNetworkAdapter = networkAdapter__2
                    Exit For
                Next

                DeviceId = deviceId__1
                Name = crtNetworkAdapter("ProductName").ToString()
                ' ConfigManagerErrorCode retourne le code 22 si la carte est désactivée car NetEnabled ne permet pas de le savoir !!
                NetEnabled = If(CInt(crtNetworkAdapter("ConfigManagerErrorCode")) = 22, CInt(NetEnabledStatus.Disabled), CInt(NetEnabledStatus.Enabled))

                NetConnectionStatus = Convert.ToInt32(crtNetworkAdapter("NetConnectionStatus").ToString())
            Catch generatedExceptionName As NullReferenceException
                DeviceId = -1
                Name = String.Empty
                NetEnabled = 0
                NetConnectionStatus = -1
            End Try
        End Sub

#End Region

#Region " Methods "

        Public Function GetNetEnabled() As Integer
            Dim netEnabled As Integer = CInt(NetEnabledStatus.Unknow)
            Dim strWQuery As String = String.Format("SELECT ConfigManagerErrorCode FROM Win32_NetworkAdapter WHERE DeviceID = {0}", DeviceId)
            Try
                Dim networkAdapters As ManagementObjectCollection = WMIQuery(strWQuery)
                For Each networkAdapter As ManagementObject In networkAdapters
                    netEnabled = If(CInt(networkAdapter("ConfigManagerErrorCode")) = 22, CInt(NetEnabledStatus.Disabled), CInt(NetEnabledStatus.Enabled))
                Next
            Catch generatedExceptionName As NullReferenceException
            End Try
            Return netEnabled
        End Function

        Public Function EnableOrDisableNetworkAdapter(strOperation As String) As Integer
            Dim resultEnableDisableNetworkAdapter As Integer = CInt(EnableDisableResult.Unknow)
            Dim crtNetworkAdapter As New ManagementObject()

            Dim strWQuery As String = String.Format("SELECT DeviceID, ProductName, NetEnabled, NetConnectionStatus FROM Win32_NetworkAdapter WHERE DeviceID = {0}", DeviceId)

            Try
                Dim networkAdapters As ManagementObjectCollection = WMIQuery(strWQuery)
                For Each networkAdapter As ManagementObject In networkAdapters
                    crtNetworkAdapter = networkAdapter
                Next

                crtNetworkAdapter.InvokeMethod(strOperation, Nothing)

                Thread.Sleep(500)
                While GetNetEnabled() <> (If((strOperation.Trim() = "Enable"), CInt(NetEnabledStatus.Enabled), CInt(NetEnabledStatus.Disabled)))
                    Thread.Sleep(100)
                End While

                resultEnableDisableNetworkAdapter = CInt(EnableDisableResult.Success)
            Catch generatedExceptionName As NullReferenceException
                resultEnableDisableNetworkAdapter = CInt(EnableDisableResult.Fail)
            End Try

            crtNetworkAdapter.Dispose()

            Return resultEnableDisableNetworkAdapter
        End Function

        Public Shared Sub GetAllNetworkAdapter(Optional ByVal Bgw As BackgroundWorker = Nothing)
            AllNetworkAdapter.Clear()
            'Dim Found As Boolean
            'Dim OneEnabled As Boolean = True
            Dim strWQuery As String = "SELECT DeviceID, ProductName, NetConnectionStatus, ConfigManagerErrorCode FROM Win32_NetworkAdapter WHERE Manufacturer <> 'Microsoft'"

            Dim networkAdapters As ManagementObjectCollection = WMIQuery(strWQuery)
            For Each moNetworkAdapter As ManagementObject In networkAdapters
                Try
                    If Not moNetworkAdapter("ProductName").ToString.ToLower.Contains("virtual") AndAlso
                        Not moNetworkAdapter("ProductName").ToString.ToLower.Contains("virtuelle") Then
                        Dim Interf = New Card(Convert.ToInt32(moNetworkAdapter("DeviceID").ToString()), moNetworkAdapter("ProductName").ToString(), If(CInt(moNetworkAdapter("ConfigManagerErrorCode")) = 22, CInt(NetEnabledStatus.Disabled), CInt(NetEnabledStatus.Enabled)), Convert.ToInt32(moNetworkAdapter("NetConnectionStatus").ToString()))
                        AllNetworkAdapter.Add(Interf)
                        If Not Bgw Is Nothing Then
                            Bgw.ReportProgress(60, Interf)
                            'If Found = False Then
                            '    If Interf.NetEnabled = 1 Then
                            '        OneEnabled = False
                            '        Found = True
                            '    End If
                            'End If
                        End If
                    End If
                Catch generatedExceptionName As NullReferenceException
                End Try
            Next
        End Sub

        Private Shared Function WMIQuery(strwQuery As String) As ManagementObjectCollection
            Dim oReturnCollection As ManagementObjectCollection = Nothing
            Dim oQuery As New ObjectQuery(strwQuery)
            Using oSearcher As New ManagementObjectSearcher(oQuery)
                oReturnCollection = oSearcher.[Get]()
            End Using
            Return oReturnCollection
        End Function

#End Region

    End Class

End Namespace
