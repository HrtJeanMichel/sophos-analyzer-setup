﻿Imports System.ServiceProcess
Imports System.Management

Namespace Services
    Public Class Svc
        Inherits ServiceController

#Region " Properties "
        Public ReadOnly Property Description As String
            Get
                If Installed() Then
                    Dim path As New ManagementPath(("Win32_Service.Name='" & MyBase.ServiceName & "'"))
                    Dim obj As New ManagementObject(path)
                    If (Not obj.Item("Description") Is Nothing) Then
                        Return obj.Item("Description").ToString
                    End If
                End If
                Return Nothing
            End Get
        End Property

        Public Property StartupType As String
            Get
                If (Installed()) Then
                    Dim path As New ManagementPath(("Win32_Service.Name='" & MyBase.ServiceName & "'"))
                    Dim obj As New ManagementObject(path)
                    Return obj.Item("StartMode").ToString
                End If
                Return Nothing
            End Get
            Set(ByVal value As String)
                If (((value <> "Automatic") AndAlso (value <> "Manual")) AndAlso (value <> "Disabled")) Then
                    Throw New Exception("Les valeurs valides ne peuvent être que : Automatic, Manual ou Disabled")
                End If
                If (Installed()) Then
                    Dim path As New ManagementPath(("Win32_Service.Name='" & MyBase.ServiceName & "'"))
                    Dim obj As New ManagementObject(path)
                    Dim args As Object() = New Object() {value}
                    obj.InvokeMethod("ChangeStartMode", args)
                End If
            End Set
        End Property
#End Region

#Region " Constructors "
        Public Sub New()
        End Sub

        Public Sub New(sName As String)
            MyBase.New(sName)
            MyBase.ServiceName = sName
        End Sub
#End Region

#Region " Methods "
        Public Function Disabled() As Boolean
            Return If(Me.StartupType = "Disabled", True, False)
        End Function

        Public Function Running() As Boolean
            Return Me.Status = ServiceControllerStatus.Running
        End Function

        Public Function Stopped() As Boolean
            Return Me.Status = ServiceControllerStatus.Stopped
        End Function

        Public Sub StopAndDisabled()
            If Installed() Then
                If Running() Then
                    Me.Stop()
                    Me.WaitForStatus(ServiceControllerStatus.Stopped)
                    Me.StartupType = "Disabled"
                Else
                    Me.StartupType = "Disabled"
                End If
            End If
        End Sub

        Public Sub StopItNow()
            If Running() Then
                Me.Stop()
                Me.WaitForStatus(ServiceControllerStatus.Stopped)
            End If
        End Sub

        Public Function Installed() As Boolean
            Dim services As ServiceController() = GetServices()
            Return services.Any(Function(f) f.ServiceName = MyBase.ServiceName)
        End Function

#End Region

    End Class
End Namespace


