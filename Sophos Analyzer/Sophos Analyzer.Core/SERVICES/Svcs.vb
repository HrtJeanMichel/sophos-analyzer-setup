﻿Imports System.ComponentModel

Namespace Services
    Public Class Svcs

#Region " Fields "
        Public Shared Names As List(Of String)
        Private Shared m_AllServices As List(Of SvcInfos)
#End Region

#Region " Constructor "
        Shared Sub New()
            Names = New List(Of String) From {"iphlpsvc", "TrkWks", "CscService", "DPS", "WSearch", "wuauserv", "Spooler", "MpsSvc", "ShellHWDetection"}
            m_AllServices = New List(Of SvcInfos)
        End Sub
#End Region

#Region " Properties "
        Public Shared ReadOnly Property AllServices As List(Of SvcInfos)
            Get
                Return m_AllServices
            End Get
        End Property
#End Region

#Region " Methods "

        ''' <summary>
        ''' Routine qui énumère les services selon leur existence, leur état et leur mode d'exécution.
        ''' </summary>
        Public Shared Function GetAllServices(Optional ByVal Bgw As BackgroundWorker = Nothing) As Boolean
            m_AllServices.Clear()
            Dim AllEnabled As Boolean = True
            For Each s In Names
                Using s1 As New Svc(s)
                    If s1.Installed Then
                        Dim svc = New SvcInfos With {.DisplayName = s1.DisplayName, .Name = s1.ServiceName, .Disabled = s1.Stopped AndAlso s1.Disabled}
                        If AllEnabled Then
                            If svc.Disabled Then
                                AllEnabled = False
                            End If
                        End If
                        m_AllServices.Add(svc)
                        If Not Bgw Is Nothing Then
                            Bgw.ReportProgress(62, svc)
                        End If
                    End If
                End Using
            Next
            Return AllEnabled
        End Function

        ''' <summary>
        ''' Routine qui applique l'activation ou la désactivation d'un service.
        ''' </summary>
        Public Shared Sub [Set](Disabled As Boolean)
            If Disabled Then
                For Each s In Names
                    Using s1 As New Svc(s)
                        If s1.Installed Then
                            If s1.Running = True Then
                                s1.StopAndDisabled()
                            Else
                                s1.StartupType = "Disabled"
                            End If
                        End If
                    End Using
                Next
            Else
                For Each s In Names
                    Using s1 As New Svc(s)
                        If s1.Installed Then
                            If s1.Running = False Then
                                If s1.StartupType = "Disabled" Then
                                    s1.StartupType = "Automatic"
                                End If
                                s1.Start()
                            End If
                        End If
                    End Using
                Next
            End If
        End Sub
#End Region

    End Class
End Namespace

