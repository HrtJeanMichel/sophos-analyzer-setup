﻿Imports System.IO

Namespace Settings
    Public NotInheritable Class Paths

#Region " Constants "
        'Current User Reg hive paths
        Public Const UserDatPoliciesExplorer As String = "TempUserDat\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer"
        Public Const UserDatPoliciesSystem As String = "TempUserDat\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
        Public Const UserDatPoliciesNonEnum As String = "TempUserDat\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\NonEnum"
        Public Const UserDatWindowsExplorer As String = "TempUserDat\SOFTWARE\Policies\Microsoft\Windows\Explorer"
        Public Const UserDatWindowsSystem As String = "TempUserDat\SOFTWARE\Policies\Microsoft\Windows\System"
        Public Const UserDatExplorerAdvanced As String = "TempUserDat\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
        Public Const UserDatPoliciesActiveDesktop As String = "TempUserDat\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\ActiveDesktop"
        Public Const UserDatWindowsSidebar As String = "TempUserDat\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Windows\Sidebar"
        Public Const UserDatWSH As String = "TempUserDat\Software\Microsoft\Windows Script Host\Settings"
        Public Const UserDatPoliciesPrograms As String = "TempUserDat\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Programs"

        'System Reg paths
        Public Const PoliciesSystem As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
        Public Const PoliciesExplorer As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer"
        Public Const WinLogon As String = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
        Public Const PoliciesInstaller As String = "SOFTWARE\Policies\Microsoft\Windows\Installer"
        Public Const Power As String = "SYSTEM\CurrentControlSet\Control\Power"

        Public Const ProfilListPathWow6432 As String = "SOFTWARE\WOW6432Node\Microsoft\Windows NT\CurrentVersion\ProfileList\"
        Public Const ProfilListPath As String = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\"

        ' LAUNCHER

#End Region

#Region " Fields "
        Public Shared SystemDrive As String
        Public Shared DriveCleanup As String

        Public Shared SecureFileTransfer As String
        Public Shared SecureFileTransferAvResults As String
#End Region

#Region " Constructor "
        Shared Sub New()
            SystemDrive = Environment.GetEnvironmentVariable("SystemDrive")
            DriveCleanup = Path.Combine(SystemDrive, "DriveCleanup.exe")
            ' LAUNCHER
            SecureFileTransfer = (Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) & "\SophosAnalyzer")
            SecureFileTransferAvResults = (Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) & "\SophosAnalyzer\AvResults")
        End Sub
#End Region

    End Class
End Namespace
