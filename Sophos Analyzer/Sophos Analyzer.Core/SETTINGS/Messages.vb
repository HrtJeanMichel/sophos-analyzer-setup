﻿Namespace Settings
    Public NotInheritable Class Messages

        'CONFIGURATOR

        'Messages de configuration de compte (FrmAccount)
        Public Const AccountNameExists As String = "Ce nom existe déjà !"
        Public Const AccountNameFill As String = "Saisissez un nom de compte"
        Public Const AccountNameValidating As String = "Vous pouvez valider pour continuer"
        Public Const AccountDescription As String = "Compte utilisateur pour Sophos Analyzer"

        'Messages de chargement/déchargement de la ruche NTUSER.DAT
        Public Const RegStartUnload As String = "Déchargement de la ruche ..."
        Public Const RegStartLoad As String = "Chargement de la ruche ..."
        Public Const RegNotLoaded As String = "Ruche non chargée dans le registre !"

        'Messages liés au profile utilisateur
        Public Const ProfilLogoff As String = "Fermeture de la session du compte sélectionné ..."
        Public Const ProfilLoggedOn As String = "La session du compte sélectionné doit être fermée !"
        Public Const ProfilUnLoaded As String = "Profil déchargé du registre"
        Public Const ProfilNotLoaded As String = "Profil non chargé dans le registre !"
        Public Const ProfilDirCreate As String = "Création de l'arborescence du profil ..."
        Public Const ProfilDirCreated As String = "Arborescence du profil créée avec succès"
        Public Const ProfilDirNotCreated As String = "Arborescence du profil non créée !"
        Public Const ProfilFileNotExists As String = "Le fichier de profil n'existe pas !"
        Public Const ProfilLoaded As String = "Chargement terminé avec succès."

        'Messages liés au compte d'utilisateur
        Public Const AccountStartCreate As String = "Compte en cours de création ..."
        Public Const AccountCreated As String = "Compte créé avec succès"
        Public Const AccountNotCreated As String = "Compte non créé !"

        'Message liés à l'application des paramètres
        Public Const ApplyBackground As String = "Application du fond d'écran du bureau ..."
        Public Const ApplyUserStratDesktop As String = "Application des stratégies de sécurité local du bureau ..."
        Public Const ApplyUserStratStartMenu As String = "Application des stratégies de sécurité local du menu démarrer ..."
        Public Const ApplyUserStratExplorer As String = "Application des stratégies de sécurité local de l'explorateur ..."
        Public Const ApplyUserStratSystem As String = "Application des stratégies de sécurité local du système ..."
        Public Const ApplyUserStratTaskbar As String = "Application des stratégies de sécurité local de la barre des tâches ..."
        Public Const ApplyUserStratLauncher As String = "Application des stratégies de démarrage du Launcher ..."
        Public Const ApplyAccountStrat As String = "Application des stratégies du compte ..."
        Public Const ApplyEnabledAdapters As String = "Activation de toutes les cartes réseaux ..."
        Public Const ApplyDisabledAdapters As String = "Désactivation de toutes les cartes réseaux ..."
        Public Const ApplyServices As String = "Modification des services ..."

        'Autres messages
        Public Const StartUserSearch As String = "Recherche de l'utilisateur ..."
        Public Const StartGroupSearch As String = "Recherche du groupe ..."
        Public Const StartAddToUsersGroup As String = "Ajout au groupe : Utilisateurs ..."
        Public Const SidStartDelete As String = "Suppression du SID dans la base de registre ..."

        'Messages liés au chargement des paramètres
        Public Const LoadUserInfos As String = "Chargement des stratégies de sécurité local du compte utilisateur ..."
        Public Const LoadUserStratDesktop As String = "Chargement des stratégies de sécurité local du bureau ..."
        Public Const LoadUserStratStartMenu As String = "Chargement des stratégies de sécurité local du menu démarrer ..."
        Public Const LoadUserStratExplorer As String = "Chargement des stratégies de sécurité local de l'explorateur ..."
        Public Const LoadUserStratLauncher As String = "Chargement des stratégies de démarrage du Launcher ..."
        Public Const LoadUserStratSystem As String = "Chargement des stratégies de sécurité local du système ..."
        Public Const LoadUserStratTaskbar As String = "Chargement des stratégies de sécurité local de la barre des tâches ..."
        Public Const LoadUserBackground As String = "Chargement du fond d'écran du bureau de l'utilisateur ..."
        Public Const LoadInterfaces As String = "Chargement des cartes réseau ..."
        Public Const LoadServices As String = "Chargement des services windows inutiles ..."


        'Messages d'action sur le menu latéral Account
        Public Const AccountOptimized As String = "Les paramètres de sécurité pour ce compte sont optimisés."
        Public Const AccountNotOptimized As String = "Les paramètres de sécurité pour ce compte ne sont pas optimisés !"
        Public Const AccountUserOptimized As String = "Les stratégies de sécurité du compte sélectionné sont optimisées."
        Public Const AccountUserNotOptimized As String = "Les stratégies de sécurité du compte sélectionné ne sont pas optimisées !"
        Public Const AccountWallpaper As String = "Modifier le papier peint de bureau du compte sélectionné."
        Public Const AccountDesktopOptimized As String = "Les stratégies de sécurité de bureau du compte sélectionné sont optimisées."
        Public Const AccountDesktopNotOptimized As String = "Les stratégies de sécurité de bureau du compte sélectionné ne sont pas optimisées."

        '################################################################################################################

        ''MESSAGES DE FENETRE D'INFORMATIONS 

        Public Const ProgramOptimizedForOs As String = "Ce programme est optimisé pour le système d'exploitation suivant : "
        Public Const ProgramOptimizedForOsTitle As String = "Mauvais environnement"
        Public Const ProgramNonDomainExecution As String = "Ce programme ne peut pas être exécuté sur un ordinateur membre d'un domaine !"
        Public Const ProgramNonElevatedExecution As String = "Ce programme ne sera pas exécuté depuis une élévation de privilèges, veuillez l'exécuter depuis un compte membre du groupe ""Administrateurs""!"
        Public Const BeCarefullTitle As String = "Attention"
        Public Const InformationTitle As String = "Information"
        Public Const ReloadAccount As String = "Vous êtes sur le point de recharger les paramètres par défaut du compte sélectionné, voulez-vous continuer ?"
        Public Const TaskIsRunning As String = "Une tâche est en cours d'exécution, veuillez patienter avant de fermer la session !"
        Public Const PleaseCloseSession As String = "Veuillez fermer le programme avant de fermer la session !"
        Public Const SophosNotInstalledTitle As String = "Sophos non installé"
        Public Const SophosNotInstalled As String = "Veuillez installer Sophos Anti-Virus à l'aide du Setup !"
        Public Const SophosAccountNotExistsTitle As String = "Compte Sophos inexistant"
        Public Const SophosAccountNotExists As String = "Veuillez désinstaller puis réinstaller SophosAnalyzer à l'aide du Setup !"
        Public Const SophosAUNotExistsTitle As String = "Compte SophosAU inexistant"
        Public Const SophosAUNotExists As String = "Veuillez désinstaller puis réinstaller Sophos Antivirus à l'aide du Setup !"

    End Class
End Namespace
