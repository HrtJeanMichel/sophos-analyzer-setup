﻿Imports MetroFramework.Controls
Imports Sophos_Analyzer.Configurator.Gui
Imports Sophos_Analyzer.Core.Adapters
Imports Sophos_Analyzer.Core.Services
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Core.Tweaks
Imports Sophos_Analyzer.Helper

Namespace Configurator
    Public NotInheritable Class Manager

#Region " Enumerations "
        Enum Category
            Desktop
            Explorer
            StartMenu
            System
            Taskbar
            Network
            Services
        End Enum

        Enum TaskState
            [Error] = 0
            SuccessUser = 1
            Warning = 2
            AbortUser = 3
        End Enum

        Enum ParameterState
            Disabled = 0
            Enabled = 1
            [Default] = 2
        End Enum
#End Region

#Region " Methods "
        Public Shared Sub [Set](m_parameters As Parameters, cat As Category, ctrl As Control)
            With m_parameters
                Select Case cat
                    Case Category.Desktop
                        If TypeOf (ctrl) Is MetroTextBox Then
                            Dim Txb = TryCast(ctrl, MetroTextBox)
                            Select Case Txb.Name
                                Case "TxbAccountGpoWallpaper"
                                    .Desktop.BackgroundDesktop = Txb.Text
                            End Select
                        End If
                    Case Category.Network
                    Case Category.Services
                End Select
            End With
        End Sub

        Private Shared Function BuildLvi(State As Boolean, EnabledDescription As String, DisabledDescription As String) As MetroListViewItem
            Dim Str As String() = New String(2) {}
            Str(0) = ""
            Str(1) = If(State = False, EnabledDescription, DisabledDescription)
            Dim lvi As New MetroListViewItem(Str)
            lvi.Tag = EnabledDescription & "|" & DisabledDescription
            Dim i As Short = If(State = False, 0, 1)
            lvi.ImageIndex = i
            Return lvi
        End Function

        Public Shared Sub Initialize(Frm As FrmConfigurator, eCase%, eObject As Object)
            Try
                Select Case eCase
                    Case 2
                        Frm.LvAccountGpoDesktop.Items.Add(BuildLvi(eObject, "L'affichage du panneau de configuration est actif !", "L'affichage du panneau de configuration est inactif."))
                    Case 3
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "L'utilisateur peut configurer la barre des tâches !", "L'utilisateur ne peut pas configurer la barre des tâches."))
                    Case 4
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'utilisateur peut accéder à l'option ""Propriétés"" depuis le poste de travail !", "L'utilisateur ne peut pas accéder à l'option ""Propriétés"" depuis le poste de travail."))
                    Case 5
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "L'utilisateur peut accéder aux options des dossiers depuis l'explorateur !", "L'utilisateur ne peut pas accéder aux options des dossiers depuis l'explorateur."))
                    Case 6
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'utilisateur peut accéder à l'éditeur de base de registre !", "L'utilisateur ne peut pas accéder à l'éditeur de base de registre."))
                    Case 7
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'utilisateur peut accéder au gestionnaire des tâches !", "L'utilisateur ne peut pas accéder au gestionnaire des tâches."))
                    Case 8
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'utilisateur peut accéder au panneau de configuration !", "L'utilisateur ne peut pas accéder au panneau de configuration."))
                    Case 9
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'utilisateur peut accéder à l'invite de commande !", "L'utilisateur ne peut pas accéder à l'invite de commande."))
                    Case 10
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "La barre des tâches n'est pas verrouillée !", "La barre des tâches est verrouillée."))
                    Case 11
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "Le clic droit sur la barre des tâches est actif !", "Le clic droit sur la barre des tâches est inactif."))
                    Case 12
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "L'utilisateur peut personnaliser les barres d'outils dans la barre des tâches !", "L'utilisateur ne peut pas personnaliser les barres d'outils dans la barre des tâches."))
                    Case 13
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "L'utilisateur peut pas glisser/déposer des éléments dans le menu démarrer !", "L'utilisateur ne peut pas glisser/déposer des éléments dans le menu démarrer."))
                    Case 14
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "La commande exécuter n'est pas masquée dans le menu démarrer !", "La commande exécuter est masquée dans le menu démarrer."))
                    Case 15
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'utilisateur peut arrêter, redémarrer et mettre en veille l'ordinateur !", "L'utilisateur ne peut pas arrêter, redémarrer et mettre en veille l'ordinateur."))
                    Case 17
                        Frm.LvAccountGpoDesktop.Items.Add(BuildLvi(eObject, "La personnalisation de l'affichage du bureau est active !", "La personnalisation de l'affichage du bureau est inactif."))
                    Case 18
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "L'utilisateur peut paramétrer les barres d'outils depuis l'explorateur !", "L'utilisateur ne peut pas paramétrer les barres d'outils depuis l'explorateur."))
                    Case 19
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "L'utilisateur peut paramétrer les barres d'outils du navigateur (Internet Explorer) !", "L'utilisateur ne peut pas paramétrer les barres d'outils du navigateur (Internet Explorer)."))
                    Case 20
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "L'exécution automatique est active pour tous les lecteurs !", "L'exécution automatique est inactive pour tous les lecteurs."))
                    Case 21
                        Frm.TxbAccountGpoWallpaper.Text = eObject.ToString
                    Case 22
                        Frm.LvAccountGpoDesktop.Items.Add(BuildLvi(eObject, "L'onglet de personnalisation d'arrière-plan du bureau est visible !", "L'onglet de personnalisation d'arrière-plan du bureau est caché."))
                    Case 23
                        Frm.LvAccountGpoDesktop.Items.Add(BuildLvi(eObject, "Les icones du bureau sont visibles !", "Les icones du bureau sont cachées."))
                    Case 24
                        Frm.LvAccountGpoDesktop.Items.Add(BuildLvi(eObject, "La personnalisation d'arrière-plan du bureau est actif !", "La personnalisation d'arrière-plan du bureau est inactif."))
                    Case 25
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Les programmes communs ne sont pas masqués dans le menu démarrer !", "Les programmes communs sont masqués dans le menu démarrer."))
                    Case 26
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Les favoris ne sont pas masqués dans le menu démarrer !", "Les favoris sont masqués dans le menu démarrer."))
                    Case 27
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher aide et support dans le menu démarrer !", "Ne pas afficher aide et support dans le menu démarrer."))
                    Case 28
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "La recherche depuis le menu démarrer est activée !", "La recherche depuis le menu démarrer est désactivée."))
                    Case 29
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher Tous les programmes dans le menu démarrer !", "Ne pas afficher Tous les programmes dans le menu démarrer."))
                    Case 30
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher la liste des programmes récents dans le menu démarrer !", "Ne pas afficher la liste des programmes récents dans le menu démarrer."))
                    Case 31
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher les connexions réseau dans le menu démarrer !", "Ne pas afficher les connexions réseau dans le menu démarrer."))
                    Case 32
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher mes documents récents dans le menu démarrer !", "Ne pas afficher mes documents récents dans le menu démarrer."))
                    Case 33
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher le lien programmes par défaut dans le menu démarrer !", "Ne pas afficher le lien programmes par défaut dans le menu démarrer."))
                    Case 34
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher mes documents dans le menu démarrer !", "Ne pas afficher mes documents dans le menu démarrer."))
                    Case 35
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher ma musique dans le menu démarrer !", "Ne pas afficher ma musique dans le menu démarrer."))
                    Case 36
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher l'icône réseau dans le menu démarrer !", "Ne pas afficher l'icône réseau dans le menu démarrer."))
                    Case 37
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher mes images dans le menu démarrer !", "Ne pas afficher mes images dans le menu démarrer."))
                    Case 38
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "Les icônes de la zone de notifications ne sont pas masqués !", "Les icônes de la zone de notifications sont masqués."))
                    Case 39
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "L'icône du centre de maintenance n'est pas masquée !", "L'icône du centre de maintenance est masquée."))
                    Case 40
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "L'icône de mise en réseau n'est pas masquée !", "L'icône de mise en réseau est masquée."))
                    Case 41
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "L'affichage des infos bulles dans la zone de notifications est actif !", "L'affichage des infos bulles dans la zone de notifications est inactif."))
                    Case 42
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "L'affichage des programmes épinglés à la barre des tâches est actif !", "L'affichage des programmes épinglés à la barre des tâches est inactif."))
                    Case 43
                        Frm.LvAccountGpoDesktop.Items.Add(BuildLvi(eObject, "Le volet des gadgets Windows est actif !", "Le volet des gadgets Windows est inactif."))
                    Case 44
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'utilisateur peut appeler la fenêtre de modification de mot de passe !", "L'utilisateur ne peut pas appeler la fenêtre de modification de mot de passe."))
                    Case 45
                        Frm.LvAccountGpoDesktop.Items.Add(BuildLvi(eObject, "Le clic droit sur le bureau est actif !", "Le clic droit sur le bureau est inactif."))
                    Case 46
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'utilisateur peut utiliser la combinaison des touches Windows + X !", "L'utilisateur ne peut pas utiliser la combinaison des touches Windows + X."))
                    Case 47
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher imprimantes et périphériques dans le menu démarrer !", "Ne pas afficher imprimantes et périphériques dans le menu démarrer."))
                    Case 48
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Afficher le lien du dossier utilisateur dans le menu démarrer !", "Ne pas afficher le lien du dossier utilisateur dans le menu démarrer."))
                    Case 49
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'icône ""Ordinateur"" sur le bureau est visible !", "L'icône ""Ordinateur"" sur le bureau n'est pas visible ."))
                '    Case 50
                '        Frm.ChbDisallowRun.Checked = eObject
                    Case 51
                        Frm.LvAccountGpoTaskbar.Items.Add(BuildLvi(eObject, "L'utilisateur peut prévisualiser le bureau au survol du coin inférieur droit de la barre des tâches !", "L'utilisateur ne peut pas prévisualiser le bureau au survol du coin inférieur droit de la barre des tâches."))
                    Case 52
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "L'utilisateur peut verrouiller l'ordinateur après avoir pressé CTRL + ALT + SUPP !", "L'utilisateur ne peut pas verrouiller l'ordinateur après avoir pressé CTRL + ALT + SUPP."))
                    'Case 53
                    '   Frm.ChbForceClassicLogonScreen.Checked = eObject
                    Case 54
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Le changement rapide d'utilisateur est disponible depuis le menu démarrer !", "Le changement rapide d'utilisateur n'est pas disponible depuis le menu démarrer."))
                '    Case 55

                '    Case 56

                '    Case 57

                '    Case 58

                '    Case 59
                    Case 60
                        Dim Interf = TryCast(eObject, Card)
                        Dim Str As String() = New String(2) {}
                        Str(0) = ""
                        Str(1) = If(Interf.NetEnabled = 1, "La carte réseau """ & Interf.Name & """ n'est pas désactivée !", "La carte réseau """ & Interf.Name & """ est désactivée")
                        Dim lvi As New MetroListViewItem(Str)
                        lvi.Tag = "La carte réseau """ & Interf.Name & """ n'est pas désactivée !|La carte réseau """ & Interf.Name & """ est désactivée"
                        Dim i As Short = If(Interf.NetEnabled = 1, 0, 1)
                        lvi.ImageIndex = i
                        Frm.LvAccountGpoConnexions.Items.Add(lvi)
                    'Case 61

                    Case 62
                        Dim service = TryCast(eObject, SvcInfos)
                        Dim Str As String() = New String(2) {}
                        Str(0) = ""
                        Str(1) = If(service.Disabled = False, "Le service """ & service.DisplayName & " (" & service.Name & ")" & """ n'est pas désactivé !", "Le service """ & service.DisplayName & " (" & service.Name & ")" & """ est désactivé")
                        Dim lvi As New MetroListViewItem(Str)
                        lvi.Tag = "Le service """ & service.DisplayName & " (" & service.Name & ")" & """ n'est pas désactivé !|Le service """ & service.DisplayName & " (" & service.Name & ")" & """ est désactivé"
                        Dim i As Short = If(service.Disabled = False, 0, 1)
                        lvi.ImageIndex = i
                        Frm.LvAccountGpoServices.Items.Add(lvi)
                    Case 63
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "La mise en veille prolongée est activée !", "La mise en veille prolongée est désactivé."))
                    Case 75
                        Frm.LvAccountGpoStartMenu.Items.Add(BuildLvi(eObject, "Rechercher les fichiers dans le menu démarrer !", "Ne pas rechercher les fichiers dans le menu démarrer."))
                    Case 76
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "L'utilisateur peut ajouter une imprimante depuis l'explorateur !", "L'utilisateur ne peut pas ajouter d'imprimante depuis l'explorateur."))
                    Case 73
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "Tous les lecteurs ne sont pas masqués dans l'explorateur !", "Tous les lecteurs sont masqués dans l'explorateur."))
                    Case 77
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "Tous les lecteurs sont accessibles depuis l'explorateur !", "Tous les lecteurs sont inaccessibles depuis l'explorateur."))
                    Case 74

                    Case 78
                        Frm.LvAccountGpoSystem.Items.Add(BuildLvi(eObject, "L'exécution de scripts (VBS, VBE, JS, JSE, WSF, WSH) est autorisée !", "L'exécution de scripts (VBS, VBE, JS, JSE, WSF, WSH) est interdite."))
                '    Case 79

                    'Case 80

                    'Case 81

                    'Case 82

                    'Case 83

                    'Case 84

                    'Case 85

                    'Case 86

                    Case 87
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "Le panneau de configuration des programmes est visible !", "Le panneau de configuration des programmes est masqué."))
                    'Case 88

                    'Case 89

                    'Case 90

                    'Case 91

                    'Case 92

                    'Case 93

                    'Case 94

                    'Case 95

                    'Case 96

                    Case 97

                    Case 98
                        Frm.LvAccountGpoLauncher.Items.Add(BuildLvi(eObject, "La fenêtre d'analyse antivirus ne sera pas affichée à l'ouverture de session du compte sélectionné !", "La fenêtre d'analyse antivirus sera affichée à l'ouverture de session du compte sélectionné."))
                    Case 100
                        Frm.LvAccountGpoAccount.Items.Add(BuildLvi(eObject, "Le compte utilisateur est désactivé !", "Le compte utilisateur est activé."))
                    Case 101
                        Frm.LvAccountGpoAccount.Items.Add(BuildLvi(eObject, "Le compte n'est pas membre du groupe Sophos !", "Le compte est membre du groupe Sophos."))
                    Case 102
                        Frm.LvAccountGpoAccount.Items.Add(BuildLvi(eObject, "Le mot de passe de ce compte peut être changé par l'utilisateur !", "L'utilisateur de ce compte ne peut pas changer son mot de passe."))
                    Case 103
                        Frm.LvAccountGpoAccount.Items.Add(BuildLvi(eObject, "Le mot de passe de ce compte peut arriver à expiration !", "Le mot de passe de ce compte n'expire jamais."))
                    'Case 104

                    'Case 105

                    'Case 107

                    'Case 108

                    Case 109
                        Frm.LvAccountGpoExplorer.Items.Add(BuildLvi(eObject, "Le bouton d'ajout de lecteur réseau est visible depuis la barre de commande de l'explorateur !", "Le bouton d'ajout de lecteur réseau n'est pas visible depuis la barre de commande de l'explorateur."))
                End Select
            Catch ex As Exception
            End Try
        End Sub

        Public Shared Sub LoadProfileEndTask(Frm As FrmConfigurator, eResult As Integer)
            Select Case eResult
                Case TaskState.Error
                    Frm.SpcAccountState.Panel1.Enabled = False
                    Frm.SpcAccountState.Panel2.Enabled = False
                    Frm.LblSecondNotification.Text = ""
                    Frm.SpcMain.Panel1.Enabled = False
                    Frm.TaskOngoing = False
                    Frm.LblMainNotification.BackColor = Color.Red
                Case TaskState.Warning
                    Frm.SpcAccountState.Panel1.Enabled = False
                    Frm.SpcAccountState.Panel2.Enabled = False
                    Frm.LblSecondNotification.Text = ""
                    Frm.SpcMain.Panel1.Enabled = False
                    Frm.TaskOngoing = False
                    Frm.LblMainNotification.BackColor = Color.Orange
                Case TaskState.SuccessUser
                    Frm.SpcAccountState.Panel1.Enabled = True
                    Frm.SpcAccountState.Panel2.Enabled = True
                    Frm.SpcMain.Panel1.Enabled = True
                    Frm.TaskOngoing = False
                    Frm.PgbStatus.Value = 100
                    Frm.LblMainNotification.BackColor = Color.SteelBlue
                    Frm.TglAccountGpoState.Checked = GetGpoState(Frm)
                    Frm.LblSecondNotification.Text = If(Frm.TglAccountGpoState.Checked, Messages.AccountOptimized, Messages.AccountNotOptimized)
                Case TaskState.AbortUser
                    Frm.SpcAccountState.Panel1.Enabled = False
                    Frm.SpcAccountState.Panel2.Enabled = False
                    Frm.SpcMain.Panel1.Enabled = False
                    If Reg.Mounted() Then Reg.Unload()
                    Frm.LblSecondNotification.Text = If(Frm.TglAccountGpoState.Checked, Messages.AccountOptimized, Messages.AccountNotOptimized)
                    Frm.PgbStatus.Value = 0
                    Frm.TaskOngoing = False
                    Frm.LblMainNotification.BackColor = Color.SteelBlue
            End Select
        End Sub

        Private Shared Function GetGpoState(Frm As FrmConfigurator) As Boolean
            Dim undo As Boolean
            Try
                Dim m_LvsGpo = New List(Of MetroListView) From {Frm.LvAccountGpoAccount, Frm.LvAccountGpoDesktop, Frm.LvAccountGpoTaskbar, Frm.LvAccountGpoSystem, Frm.LvAccountGpoStartMenu, Frm.LvAccountGpoExplorer, Frm.LvAccountGpoConnexions, Frm.LvAccountGpoServices, Frm.LvAccountGpoLauncher}
                For Each lv In m_LvsGpo
                    For Each it As MetroListViewItem In lv.Items
                        If it.ImageIndex = 0 Then
                            undo = True
                            Exit For
                        End If
                    Next
                Next
                m_LvsGpo.Clear()
                Return If(undo, False, True)
            Catch ex As Exception
                Frm.LblMainNotification.Text = "Exception non gérée depuis SetGpoState : " & ex.Message
            End Try
            Return False
        End Function

        Public Shared Sub SetGpoState(Frm As FrmConfigurator, Optimized As Boolean)
            Try
                Dim m_LvsGpo = New List(Of MetroListView) From {Frm.LvAccountGpoAccount, Frm.LvAccountGpoDesktop, Frm.LvAccountGpoTaskbar, Frm.LvAccountGpoSystem, Frm.LvAccountGpoStartMenu, Frm.LvAccountGpoExplorer, Frm.LvAccountGpoConnexions, Frm.LvAccountGpoServices, Frm.LvAccountGpoLauncher}
                For Each lv In m_LvsGpo
                    For Each it As MetroListViewItem In lv.Items
                        If it.ImageIndex = If(Optimized, 0, 1) Then
                            it.ImageIndex = If(Optimized, 1, 0)
                            it.SubItems(1).Text = it.Tag.ToString.Split("|")(If(Optimized, 1, 0))
                            it.Data = Nothing
                        End If
                    Next
                Next
                m_LvsGpo.Clear()
            Catch ex As Exception
                Frm.LblMainNotification.Text = "Exception non gérée depuis SetListviews : " & ex.Message
            End Try
        End Sub

        Public Shared Sub SetGpoParametersOptimization(Frm As FrmConfigurator, Optimized As Boolean)
            Frm.Parameters.User.SetBoolProperties(Optimized)
            Frm.Parameters.Services.SetBoolProperty(Optimized)
            Frm.Parameters.Launcher.SetBoolProperty(Optimized)
            Frm.Parameters.Network.SetBoolProperty(Optimized)
            Frm.Parameters.Desktop.SetBoolProperties(Optimized)
            Frm.Parameters.Explorer.SetBoolProperties(Optimized)
            Frm.Parameters.StartMenu.SetBoolProperties(Optimized)
            Frm.Parameters.Systeme.SetBoolProperties(Optimized)
            Frm.Parameters.Taskbar.SetBoolProperties(Optimized)
            SetApplyButton(Frm)
        End Sub

        Public Shared Sub SetGpoListViewItemsClear(Frm As FrmConfigurator)
            Frm.LvAccountGpoAccount.Items.Clear()
            Frm.LvAccountGpoConnexions.Items.Clear()
            Frm.LvAccountGpoDesktop.Items.Clear()
            Frm.LvAccountGpoExplorer.Items.Clear()
            Frm.LvAccountGpoServices.Items.Clear()
            Frm.LvAccountGpoLauncher.Items.Clear()
            Frm.LvAccountGpoStartMenu.Items.Clear()
            Frm.LvAccountGpoSystem.Items.Clear()
            Frm.LvAccountGpoTaskbar.Items.Clear()
        End Sub

        Public Shared Sub SetApplyButton(Frm As FrmConfigurator)
            If Frm.InitParameters.User.EqualsTo(Frm.Parameters.User) And Frm.InitParameters.Desktop.EqualsTo(Frm.Parameters.Desktop) And
                Frm.InitParameters.StartMenu.EqualsTo(Frm.Parameters.StartMenu) And Frm.InitParameters.Explorer.EqualsTo(Frm.Parameters.Explorer) And
                Frm.InitParameters.Taskbar.EqualsTo(Frm.Parameters.Taskbar) And Frm.InitParameters.Systeme.EqualsTo(Frm.Parameters.Systeme) And
                Frm.InitParameters.Network.EqualsTo(Frm.Parameters.Network) And Frm.InitParameters.Services.EqualsTo(Frm.Parameters.Services) And
                Frm.InitParameters.Launcher.EqualsTo(Frm.Parameters.Launcher) Then
                Frm.BtnApply.Enabled = False
            Else
                Frm.BtnApply.Enabled = True
            End If
        End Sub

        Public Shared Sub ApplyProfileEndTask(Frm As FrmConfigurator, eResult As Integer)
            Select Case eResult
                Case TaskState.Error
                    Frm.SpcAccountState.Panel1.Enabled = False
                    Frm.SpcAccountState.Panel2.Enabled = False
                    Frm.LblSecondNotification.Text = ""
                    Frm.SpcMain.Panel1.Enabled = False
                    Frm.TaskOngoing = False
                    Frm.LblMainNotification.BackColor = Color.Red
                Case TaskState.Warning
                    Frm.SpcAccountState.Panel1.Enabled = False
                    Frm.SpcAccountState.Panel2.Enabled = False
                    Frm.LblSecondNotification.Text = ""
                    Frm.SpcMain.Panel1.Enabled = False
                    Frm.TaskOngoing = False
                    Frm.LblMainNotification.BackColor = Color.Orange
                Case TaskState.SuccessUser
                    Frm.SpcAccountState.Panel1.Enabled = True
                    Frm.SpcAccountState.Panel2.Enabled = True
                    Frm.SpcMain.Panel1.Enabled = True
                    Frm.TaskOngoing = False
                    Frm.LblSecondNotification.Text = If(Frm.TglAccountGpoState.Checked, Messages.AccountOptimized, Messages.AccountNotOptimized)
                    Frm.PgbStatus.Value = 100
                    Frm.LblMainNotification.BackColor = Color.SteelBlue
                    Frm.LoadAccountInfos(True)
            End Select
        End Sub
#End Region

    End Class
End Namespace

