﻿Imports MetroFramework.Forms
Imports MetroFramework.Controls
Imports Sophos_Analyzer.Extensions

Namespace Configurator.Gui
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class FrmConfigurator
        Inherits MetroForm

        'Form remplace la méthode Dispose pour nettoyer la liste des composants.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requise par le Concepteur Windows Form
        Private components As System.ComponentModel.IContainer

        'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
        'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
        'Ne la modifiez pas à l'aide de l'éditeur de code.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConfigurator))
            Me.PgbStatus = New MetroFramework.Controls.MetroProgressBar()
            Me.SpcMiddle = New System.Windows.Forms.SplitContainer()
            Me.LblSecondNotification = New System.Windows.Forms.Label()
            Me.TbcMain = New System.Windows.Forms.TabControl()
            Me.TpAccount = New System.Windows.Forms.TabPage()
            Me.SpcAccountInfos = New System.Windows.Forms.SplitContainer()
            Me.TsAccountState = New System.Windows.Forms.ToolStrip()
            Me.TsLblAccountState = New System.Windows.Forms.ToolStripLabel()
            Me.SpcAccountState = New System.Windows.Forms.SplitContainer()
            Me.TlpAccountState = New System.Windows.Forms.TableLayoutPanel()
            Me.LblAccountGpoStates = New System.Windows.Forms.Label()
            Me.TglAccountGpoState = New MetroFramework.Controls.MetroToggle()
            Me.TbcAccountGpo = New MetroFramework.Controls.MetroTabControl()
            Me.TpAccountGpoAccount = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoAccount = New MetroFramework.Controls.MetroListView()
            Me.ClhAccountGpoAccountState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ClhAccountGpoAccountDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ImlParameters = New System.Windows.Forms.ImageList(Me.components)
            Me.TpAccountGpoWallpaper = New MetroFramework.Controls.MetroTabPage()
            Me.PcbAccountGpoWallpaper = New System.Windows.Forms.PictureBox()
            Me.BtnAccountGpoWallpaper = New System.Windows.Forms.Button()
            Me.TxbAccountGpoWallpaper = New MetroFramework.Controls.MetroTextBox()
            Me.LblAccountGpoWallpaper = New MetroFramework.Controls.MetroLabel()
            Me.TpAccountGpoDesktop = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoDesktop = New MetroFramework.Controls.MetroListView()
            Me.ClhAccountGpoDesktopState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ClhAccountGpoDesktopDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.TpAccountGpoStartMenu = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoStartMenu = New MetroFramework.Controls.MetroListView()
            Me.ClhAccountGpoStartMenuState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ClhAccountGpoStartMenuDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.TpAccountGpoExplorer = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoExplorer = New MetroFramework.Controls.MetroListView()
            Me.ClhAccountGpoExplorerState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ClhAccountGpoExplorerDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.TpAccountGpoTaskbar = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoTaskbar = New MetroFramework.Controls.MetroListView()
            Me.ClhAccountGpoTaskbarState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ClhAccountGpoTaskbarDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.TpAccountGpoSystem = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoSystem = New MetroFramework.Controls.MetroListView()
            Me.ClhAccountGpoSystemState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ClhAccountGpoSystemDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.TpAccountGpoConnexions = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoConnexions = New MetroFramework.Controls.MetroListView()
            Me.ClhAccountGpoConnexionsState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ClhAccountGpoConnexionsDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.TpAccountGpoServices = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoServices = New MetroFramework.Controls.MetroListView()
            Me.ClhAccountGpoServicesState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ClhAccountGpoServicesDescription = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.TpAccountGpoLauncher = New MetroFramework.Controls.MetroTabPage()
            Me.LvAccountGpoLauncher = New MetroFramework.Controls.MetroListView()
            Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.TpSettings = New System.Windows.Forms.TabPage()
            Me.SpcAccounts = New System.Windows.Forms.SplitContainer()
            Me.TlpLeftMenu = New System.Windows.Forms.TableLayoutPanel()
            Me.BtnReload = New System.Windows.Forms.Button()
            Me.BtnApply = New System.Windows.Forms.Button()
            Me.BtnAccount = New System.Windows.Forms.Button()
            Me.SpcMain = New System.Windows.Forms.SplitContainer()
            Me.BgwLoadAccounts = New System.ComponentModel.BackgroundWorker()
            Me.BgwLoadProfile = New System.ComponentModel.BackgroundWorker()
            Me.miniToolStrip = New System.Windows.Forms.ToolStrip()
            Me.BgwApply = New System.ComponentModel.BackgroundWorker()
            Me.SpcFrm = New System.Windows.Forms.SplitContainer()
            Me.LblMainNotification = New System.Windows.Forms.Label()
            CType(Me.SpcMiddle, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SpcMiddle.Panel1.SuspendLayout()
            Me.SpcMiddle.Panel2.SuspendLayout()
            Me.SpcMiddle.SuspendLayout()
            Me.TbcMain.SuspendLayout()
            Me.TpAccount.SuspendLayout()
            CType(Me.SpcAccountInfos, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SpcAccountInfos.Panel1.SuspendLayout()
            Me.SpcAccountInfos.Panel2.SuspendLayout()
            Me.SpcAccountInfos.SuspendLayout()
            Me.TsAccountState.SuspendLayout()
            CType(Me.SpcAccountState, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SpcAccountState.Panel1.SuspendLayout()
            Me.SpcAccountState.Panel2.SuspendLayout()
            Me.SpcAccountState.SuspendLayout()
            Me.TlpAccountState.SuspendLayout()
            Me.TbcAccountGpo.SuspendLayout()
            Me.TpAccountGpoAccount.SuspendLayout()
            Me.TpAccountGpoWallpaper.SuspendLayout()
            CType(Me.PcbAccountGpoWallpaper, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.TpAccountGpoDesktop.SuspendLayout()
            Me.TpAccountGpoStartMenu.SuspendLayout()
            Me.TpAccountGpoExplorer.SuspendLayout()
            Me.TpAccountGpoTaskbar.SuspendLayout()
            Me.TpAccountGpoSystem.SuspendLayout()
            Me.TpAccountGpoConnexions.SuspendLayout()
            Me.TpAccountGpoServices.SuspendLayout()
            Me.TpAccountGpoLauncher.SuspendLayout()
            CType(Me.SpcAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SpcAccounts.SuspendLayout()
            Me.TlpLeftMenu.SuspendLayout()
            CType(Me.SpcMain, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SpcMain.Panel1.SuspendLayout()
            Me.SpcMain.Panel2.SuspendLayout()
            Me.SpcMain.SuspendLayout()
            CType(Me.SpcFrm, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SpcFrm.Panel1.SuspendLayout()
            Me.SpcFrm.Panel2.SuspendLayout()
            Me.SpcFrm.SuspendLayout()
            Me.SuspendLayout()
            '
            'PgbStatus
            '
            Me.PgbStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.PgbStatus.Location = New System.Drawing.Point(-1, -4)
            Me.PgbStatus.Name = "PgbStatus"
            Me.PgbStatus.Size = New System.Drawing.Size(985, 10)
            Me.PgbStatus.TabIndex = 2
            '
            'SpcMiddle
            '
            Me.SpcMiddle.BackColor = System.Drawing.Color.Transparent
            Me.SpcMiddle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.SpcMiddle.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SpcMiddle.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
            Me.SpcMiddle.IsSplitterFixed = True
            Me.SpcMiddle.Location = New System.Drawing.Point(0, 0)
            Me.SpcMiddle.Name = "SpcMiddle"
            Me.SpcMiddle.Orientation = System.Windows.Forms.Orientation.Horizontal
            '
            'SpcMiddle.Panel1
            '
            Me.SpcMiddle.Panel1.BackColor = System.Drawing.Color.DodgerBlue
            Me.SpcMiddle.Panel1.Controls.Add(Me.LblSecondNotification)
            '
            'SpcMiddle.Panel2
            '
            Me.SpcMiddle.Panel2.BackColor = System.Drawing.Color.Transparent
            Me.SpcMiddle.Panel2.Controls.Add(Me.TbcMain)
            Me.SpcMiddle.Size = New System.Drawing.Size(860, 570)
            Me.SpcMiddle.SplitterDistance = 28
            Me.SpcMiddle.TabIndex = 7
            Me.SpcMiddle.TabStop = False
            '
            'LblSecondNotification
            '
            Me.LblSecondNotification.BackColor = System.Drawing.SystemColors.MenuHighlight
            Me.LblSecondNotification.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LblSecondNotification.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LblSecondNotification.ForeColor = System.Drawing.Color.White
            Me.LblSecondNotification.Location = New System.Drawing.Point(0, 0)
            Me.LblSecondNotification.Name = "LblSecondNotification"
            Me.LblSecondNotification.Size = New System.Drawing.Size(858, 26)
            Me.LblSecondNotification.TabIndex = 0
            Me.LblSecondNotification.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'TbcMain
            '
            Me.TbcMain.Controls.Add(Me.TpAccount)
            Me.TbcMain.Controls.Add(Me.TpSettings)
            Me.TbcMain.Dock = System.Windows.Forms.DockStyle.Fill
            Me.TbcMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TbcMain.Location = New System.Drawing.Point(0, 0)
            Me.TbcMain.Name = "TbcMain"
            Me.TbcMain.SelectedIndex = 0
            Me.TbcMain.Size = New System.Drawing.Size(858, 536)
            Me.TbcMain.TabIndex = 7
            '
            'TpAccount
            '
            Me.TpAccount.BackColor = System.Drawing.Color.Transparent
            Me.TpAccount.Controls.Add(Me.SpcAccountInfos)
            Me.TpAccount.Location = New System.Drawing.Point(4, 22)
            Me.TpAccount.Name = "TpAccount"
            Me.TpAccount.Padding = New System.Windows.Forms.Padding(3)
            Me.TpAccount.Size = New System.Drawing.Size(850, 510)
            Me.TpAccount.TabIndex = 0
            Me.TpAccount.Text = "TabPage1"
            Me.TpAccount.UseVisualStyleBackColor = True
            '
            'SpcAccountInfos
            '
            Me.SpcAccountInfos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.SpcAccountInfos.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SpcAccountInfos.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
            Me.SpcAccountInfos.IsSplitterFixed = True
            Me.SpcAccountInfos.Location = New System.Drawing.Point(3, 3)
            Me.SpcAccountInfos.Name = "SpcAccountInfos"
            Me.SpcAccountInfos.Orientation = System.Windows.Forms.Orientation.Horizontal
            '
            'SpcAccountInfos.Panel1
            '
            Me.SpcAccountInfos.Panel1.BackColor = System.Drawing.Color.White
            Me.SpcAccountInfos.Panel1.Controls.Add(Me.TsAccountState)
            '
            'SpcAccountInfos.Panel2
            '
            Me.SpcAccountInfos.Panel2.Controls.Add(Me.SpcAccountState)
            Me.SpcAccountInfos.Size = New System.Drawing.Size(844, 504)
            Me.SpcAccountInfos.SplitterDistance = 25
            Me.SpcAccountInfos.TabIndex = 3
            Me.SpcAccountInfos.TabStop = False
            '
            'TsAccountState
            '
            Me.TsAccountState.BackColor = System.Drawing.Color.DodgerBlue
            Me.TsAccountState.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
            Me.TsAccountState.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsLblAccountState})
            Me.TsAccountState.Location = New System.Drawing.Point(0, 0)
            Me.TsAccountState.Name = "TsAccountState"
            Me.TsAccountState.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
            Me.TsAccountState.Size = New System.Drawing.Size(842, 25)
            Me.TsAccountState.TabIndex = 13
            Me.TsAccountState.Text = "ToolStrip1"
            '
            'TsLblAccountState
            '
            Me.TsLblAccountState.BackColor = System.Drawing.Color.Transparent
            Me.TsLblAccountState.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.TsLblAccountState.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TsLblAccountState.ForeColor = System.Drawing.Color.White
            Me.TsLblAccountState.Name = "TsLblAccountState"
            Me.TsLblAccountState.Size = New System.Drawing.Size(204, 22)
            Me.TsLblAccountState.Text = "Etat des paramètres du compte : "
            '
            'SpcAccountState
            '
            Me.SpcAccountState.BackColor = System.Drawing.SystemColors.Control
            Me.SpcAccountState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.SpcAccountState.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SpcAccountState.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
            Me.SpcAccountState.IsSplitterFixed = True
            Me.SpcAccountState.Location = New System.Drawing.Point(0, 0)
            Me.SpcAccountState.Name = "SpcAccountState"
            Me.SpcAccountState.Orientation = System.Windows.Forms.Orientation.Horizontal
            '
            'SpcAccountState.Panel1
            '
            Me.SpcAccountState.Panel1.BackColor = System.Drawing.Color.White
            Me.SpcAccountState.Panel1.Controls.Add(Me.TlpAccountState)
            '
            'SpcAccountState.Panel2
            '
            Me.SpcAccountState.Panel2.Controls.Add(Me.TbcAccountGpo)
            Me.SpcAccountState.Size = New System.Drawing.Size(844, 475)
            Me.SpcAccountState.SplitterDistance = 33
            Me.SpcAccountState.TabIndex = 2
            Me.SpcAccountState.TabStop = False
            '
            'TlpAccountState
            '
            Me.TlpAccountState.ColumnCount = 2
            Me.TlpAccountState.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.192771!))
            Me.TlpAccountState.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.80723!))
            Me.TlpAccountState.Controls.Add(Me.LblAccountGpoStates, 1, 0)
            Me.TlpAccountState.Controls.Add(Me.TglAccountGpoState, 0, 0)
            Me.TlpAccountState.Dock = System.Windows.Forms.DockStyle.Fill
            Me.TlpAccountState.Location = New System.Drawing.Point(0, 0)
            Me.TlpAccountState.Name = "TlpAccountState"
            Me.TlpAccountState.RowCount = 1
            Me.TlpAccountState.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.TlpAccountState.Size = New System.Drawing.Size(842, 31)
            Me.TlpAccountState.TabIndex = 1
            '
            'LblAccountGpoStates
            '
            Me.LblAccountGpoStates.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.LblAccountGpoStates.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LblAccountGpoStates.Location = New System.Drawing.Point(71, 0)
            Me.LblAccountGpoStates.Name = "LblAccountGpoStates"
            Me.LblAccountGpoStates.Size = New System.Drawing.Size(757, 31)
            Me.LblAccountGpoStates.TabIndex = 1
            Me.LblAccountGpoStates.Text = "Les paramètres de sécurité pour ce compte ne sont pas optimisés !"
            Me.LblAccountGpoStates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'TglAccountGpoState
            '
            Me.TglAccountGpoState.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.TglAccountGpoState.AutoSize = True
            Me.TglAccountGpoState.DisplayStatus = False
            Me.TglAccountGpoState.Location = New System.Drawing.Point(3, 3)
            Me.TglAccountGpoState.Name = "TglAccountGpoState"
            Me.TglAccountGpoState.Size = New System.Drawing.Size(50, 25)
            Me.TglAccountGpoState.TabIndex = 0
            Me.TglAccountGpoState.Text = "Off"
            Me.TglAccountGpoState.UseSelectable = True
            '
            'TbcAccountGpo
            '
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoAccount)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoWallpaper)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoDesktop)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoStartMenu)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoExplorer)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoTaskbar)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoSystem)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoConnexions)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoServices)
            Me.TbcAccountGpo.Controls.Add(Me.TpAccountGpoLauncher)
            Me.TbcAccountGpo.Dock = System.Windows.Forms.DockStyle.Fill
            Me.TbcAccountGpo.Location = New System.Drawing.Point(0, 0)
            Me.TbcAccountGpo.Name = "TbcAccountGpo"
            Me.TbcAccountGpo.SelectedIndex = 0
            Me.TbcAccountGpo.Size = New System.Drawing.Size(842, 436)
            Me.TbcAccountGpo.TabIndex = 0
            Me.TbcAccountGpo.TabStop = False
            Me.TbcAccountGpo.UseSelectable = True
            '
            'TpAccountGpoAccount
            '
            Me.TpAccountGpoAccount.Controls.Add(Me.LvAccountGpoAccount)
            Me.TpAccountGpoAccount.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoAccount.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoAccount.HorizontalScrollbarSize = 10
            Me.TpAccountGpoAccount.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoAccount.Name = "TpAccountGpoAccount"
            Me.TpAccountGpoAccount.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoAccount.TabIndex = 0
            Me.TpAccountGpoAccount.Tag = "0"
            Me.TpAccountGpoAccount.Text = "Compte utilisateur"
            Me.TpAccountGpoAccount.VerticalScrollbarBarColor = True
            Me.TpAccountGpoAccount.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoAccount.VerticalScrollbarSize = 10
            '
            'LvAccountGpoAccount
            '
            Me.LvAccountGpoAccount.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoAccount.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoAccount.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountGpoAccountState, Me.ClhAccountGpoAccountDescription})
            Me.LvAccountGpoAccount.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoAccount.FullRowSelect = True
            Me.LvAccountGpoAccount.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoAccount.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoAccount.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoAccount.Name = "LvAccountGpoAccount"
            Me.LvAccountGpoAccount.OwnerDraw = True
            Me.LvAccountGpoAccount.Size = New System.Drawing.Size(795, 392)
            Me.LvAccountGpoAccount.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoAccount.TabIndex = 1
            Me.LvAccountGpoAccount.TabStop = False
            Me.LvAccountGpoAccount.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoAccount.UseSelectable = True
            Me.LvAccountGpoAccount.View = System.Windows.Forms.View.Details
            '
            'ClhAccountGpoAccountState
            '
            Me.ClhAccountGpoAccountState.Text = "Etat"
            Me.ClhAccountGpoAccountState.Width = 39
            '
            'ClhAccountGpoAccountDescription
            '
            Me.ClhAccountGpoAccountDescription.Text = "Description"
            Me.ClhAccountGpoAccountDescription.Width = 670
            '
            'ImlParameters
            '
            Me.ImlParameters.ImageStream = CType(resources.GetObject("ImlParameters.ImageStream"), System.Windows.Forms.ImageListStreamer)
            Me.ImlParameters.TransparentColor = System.Drawing.Color.Transparent
            Me.ImlParameters.Images.SetKeyName(0, "Red16.png")
            Me.ImlParameters.Images.SetKeyName(1, "Green16.png")
            '
            'TpAccountGpoWallpaper
            '
            Me.TpAccountGpoWallpaper.Controls.Add(Me.PcbAccountGpoWallpaper)
            Me.TpAccountGpoWallpaper.Controls.Add(Me.BtnAccountGpoWallpaper)
            Me.TpAccountGpoWallpaper.Controls.Add(Me.TxbAccountGpoWallpaper)
            Me.TpAccountGpoWallpaper.Controls.Add(Me.LblAccountGpoWallpaper)
            Me.TpAccountGpoWallpaper.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoWallpaper.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoWallpaper.HorizontalScrollbarSize = 10
            Me.TpAccountGpoWallpaper.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoWallpaper.Name = "TpAccountGpoWallpaper"
            Me.TpAccountGpoWallpaper.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoWallpaper.TabIndex = 8
            Me.TpAccountGpoWallpaper.Tag = "1"
            Me.TpAccountGpoWallpaper.Text = "Fond d'écran"
            Me.TpAccountGpoWallpaper.VerticalScrollbarBarColor = True
            Me.TpAccountGpoWallpaper.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoWallpaper.VerticalScrollbarSize = 10
            '
            'PcbAccountGpoWallpaper
            '
            Me.PcbAccountGpoWallpaper.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.PcbAccountGpoWallpaper.BackColor = System.Drawing.Color.Transparent
            Me.PcbAccountGpoWallpaper.Location = New System.Drawing.Point(86, 37)
            Me.PcbAccountGpoWallpaper.Name = "PcbAccountGpoWallpaper"
            Me.PcbAccountGpoWallpaper.Size = New System.Drawing.Size(660, 369)
            Me.PcbAccountGpoWallpaper.TabIndex = 5
            Me.PcbAccountGpoWallpaper.TabStop = False
            '
            'BtnAccountGpoWallpaper
            '
            Me.BtnAccountGpoWallpaper.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.BtnAccountGpoWallpaper.BackColor = System.Drawing.Color.DeepSkyBlue
            Me.BtnAccountGpoWallpaper.FlatAppearance.BorderSize = 0
            Me.BtnAccountGpoWallpaper.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.BtnAccountGpoWallpaper.ForeColor = System.Drawing.Color.White
            Me.BtnAccountGpoWallpaper.Location = New System.Drawing.Point(752, 8)
            Me.BtnAccountGpoWallpaper.Name = "BtnAccountGpoWallpaper"
            Me.BtnAccountGpoWallpaper.Size = New System.Drawing.Size(73, 23)
            Me.BtnAccountGpoWallpaper.TabIndex = 4
            Me.BtnAccountGpoWallpaper.TabStop = False
            Me.BtnAccountGpoWallpaper.Tag = "1"
            Me.BtnAccountGpoWallpaper.Text = " ..."
            Me.BtnAccountGpoWallpaper.UseVisualStyleBackColor = False
            '
            'TxbAccountGpoWallpaper
            '
            Me.TxbAccountGpoWallpaper.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            '
            '
            '
            Me.TxbAccountGpoWallpaper.CustomButton.Image = Nothing
            Me.TxbAccountGpoWallpaper.CustomButton.Location = New System.Drawing.Point(638, 1)
            Me.TxbAccountGpoWallpaper.CustomButton.Name = ""
            Me.TxbAccountGpoWallpaper.CustomButton.Size = New System.Drawing.Size(21, 21)
            Me.TxbAccountGpoWallpaper.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
            Me.TxbAccountGpoWallpaper.CustomButton.TabIndex = 1
            Me.TxbAccountGpoWallpaper.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
            Me.TxbAccountGpoWallpaper.CustomButton.UseSelectable = True
            Me.TxbAccountGpoWallpaper.CustomButton.Visible = False
            Me.TxbAccountGpoWallpaper.Lines = New String(-1) {}
            Me.TxbAccountGpoWallpaper.Location = New System.Drawing.Point(86, 8)
            Me.TxbAccountGpoWallpaper.MaxLength = 32767
            Me.TxbAccountGpoWallpaper.Name = "TxbAccountGpoWallpaper"
            Me.TxbAccountGpoWallpaper.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
            Me.TxbAccountGpoWallpaper.ReadOnly = True
            Me.TxbAccountGpoWallpaper.ScrollBars = System.Windows.Forms.ScrollBars.None
            Me.TxbAccountGpoWallpaper.SelectedText = ""
            Me.TxbAccountGpoWallpaper.SelectionLength = 0
            Me.TxbAccountGpoWallpaper.SelectionStart = 0
            Me.TxbAccountGpoWallpaper.ShortcutsEnabled = True
            Me.TxbAccountGpoWallpaper.Size = New System.Drawing.Size(660, 23)
            Me.TxbAccountGpoWallpaper.TabIndex = 3
            Me.TxbAccountGpoWallpaper.TabStop = False
            Me.TxbAccountGpoWallpaper.UseSelectable = True
            Me.TxbAccountGpoWallpaper.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
            Me.TxbAccountGpoWallpaper.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
            '
            'LblAccountGpoWallpaper
            '
            Me.LblAccountGpoWallpaper.AutoSize = True
            Me.LblAccountGpoWallpaper.BackColor = System.Drawing.Color.Transparent
            Me.LblAccountGpoWallpaper.Location = New System.Drawing.Point(15, 8)
            Me.LblAccountGpoWallpaper.Name = "LblAccountGpoWallpaper"
            Me.LblAccountGpoWallpaper.Size = New System.Drawing.Size(65, 19)
            Me.LblAccountGpoWallpaper.TabIndex = 2
            Me.LblAccountGpoWallpaper.Text = "Chemin : "
            '
            'TpAccountGpoDesktop
            '
            Me.TpAccountGpoDesktop.Controls.Add(Me.LvAccountGpoDesktop)
            Me.TpAccountGpoDesktop.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoDesktop.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoDesktop.HorizontalScrollbarSize = 10
            Me.TpAccountGpoDesktop.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoDesktop.Name = "TpAccountGpoDesktop"
            Me.TpAccountGpoDesktop.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoDesktop.TabIndex = 5
            Me.TpAccountGpoDesktop.Tag = "2"
            Me.TpAccountGpoDesktop.Text = "Bureau"
            Me.TpAccountGpoDesktop.VerticalScrollbarBarColor = True
            Me.TpAccountGpoDesktop.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoDesktop.VerticalScrollbarSize = 10
            '
            'LvAccountGpoDesktop
            '
            Me.LvAccountGpoDesktop.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoDesktop.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoDesktop.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountGpoDesktopState, Me.ClhAccountGpoDesktopDescription})
            Me.LvAccountGpoDesktop.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoDesktop.FullRowSelect = True
            Me.LvAccountGpoDesktop.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoDesktop.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoDesktop.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoDesktop.Name = "LvAccountGpoDesktop"
            Me.LvAccountGpoDesktop.OwnerDraw = True
            Me.LvAccountGpoDesktop.Size = New System.Drawing.Size(797, 398)
            Me.LvAccountGpoDesktop.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoDesktop.TabIndex = 2
            Me.LvAccountGpoDesktop.TabStop = False
            Me.LvAccountGpoDesktop.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoDesktop.UseSelectable = True
            Me.LvAccountGpoDesktop.View = System.Windows.Forms.View.Details
            '
            'ClhAccountGpoDesktopState
            '
            Me.ClhAccountGpoDesktopState.Text = "Etat"
            Me.ClhAccountGpoDesktopState.Width = 39
            '
            'ClhAccountGpoDesktopDescription
            '
            Me.ClhAccountGpoDesktopDescription.Text = "Description"
            Me.ClhAccountGpoDesktopDescription.Width = 670
            '
            'TpAccountGpoStartMenu
            '
            Me.TpAccountGpoStartMenu.Controls.Add(Me.LvAccountGpoStartMenu)
            Me.TpAccountGpoStartMenu.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoStartMenu.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoStartMenu.HorizontalScrollbarSize = 10
            Me.TpAccountGpoStartMenu.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoStartMenu.Name = "TpAccountGpoStartMenu"
            Me.TpAccountGpoStartMenu.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoStartMenu.TabIndex = 3
            Me.TpAccountGpoStartMenu.Tag = "3"
            Me.TpAccountGpoStartMenu.Text = "Menu démarrer"
            Me.TpAccountGpoStartMenu.VerticalScrollbarBarColor = True
            Me.TpAccountGpoStartMenu.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoStartMenu.VerticalScrollbarSize = 10
            '
            'LvAccountGpoStartMenu
            '
            Me.LvAccountGpoStartMenu.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoStartMenu.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoStartMenu.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountGpoStartMenuState, Me.ClhAccountGpoStartMenuDescription})
            Me.LvAccountGpoStartMenu.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoStartMenu.FullRowSelect = True
            Me.LvAccountGpoStartMenu.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoStartMenu.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoStartMenu.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoStartMenu.Name = "LvAccountGpoStartMenu"
            Me.LvAccountGpoStartMenu.OwnerDraw = True
            Me.LvAccountGpoStartMenu.Size = New System.Drawing.Size(803, 398)
            Me.LvAccountGpoStartMenu.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoStartMenu.TabIndex = 5
            Me.LvAccountGpoStartMenu.TabStop = False
            Me.LvAccountGpoStartMenu.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoStartMenu.UseSelectable = True
            Me.LvAccountGpoStartMenu.View = System.Windows.Forms.View.Details
            '
            'ClhAccountGpoStartMenuState
            '
            Me.ClhAccountGpoStartMenuState.Text = "Etat"
            Me.ClhAccountGpoStartMenuState.Width = 39
            '
            'ClhAccountGpoStartMenuDescription
            '
            Me.ClhAccountGpoStartMenuDescription.Text = "Description"
            Me.ClhAccountGpoStartMenuDescription.Width = 670
            '
            'TpAccountGpoExplorer
            '
            Me.TpAccountGpoExplorer.Controls.Add(Me.LvAccountGpoExplorer)
            Me.TpAccountGpoExplorer.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoExplorer.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoExplorer.HorizontalScrollbarSize = 10
            Me.TpAccountGpoExplorer.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoExplorer.Name = "TpAccountGpoExplorer"
            Me.TpAccountGpoExplorer.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoExplorer.TabIndex = 4
            Me.TpAccountGpoExplorer.Tag = "4"
            Me.TpAccountGpoExplorer.Text = "Explorateur"
            Me.TpAccountGpoExplorer.VerticalScrollbarBarColor = True
            Me.TpAccountGpoExplorer.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoExplorer.VerticalScrollbarSize = 10
            '
            'LvAccountGpoExplorer
            '
            Me.LvAccountGpoExplorer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoExplorer.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoExplorer.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountGpoExplorerState, Me.ClhAccountGpoExplorerDescription})
            Me.LvAccountGpoExplorer.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoExplorer.FullRowSelect = True
            Me.LvAccountGpoExplorer.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoExplorer.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoExplorer.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoExplorer.Name = "LvAccountGpoExplorer"
            Me.LvAccountGpoExplorer.OwnerDraw = True
            Me.LvAccountGpoExplorer.Size = New System.Drawing.Size(803, 398)
            Me.LvAccountGpoExplorer.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoExplorer.TabIndex = 6
            Me.LvAccountGpoExplorer.TabStop = False
            Me.LvAccountGpoExplorer.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoExplorer.UseSelectable = True
            Me.LvAccountGpoExplorer.View = System.Windows.Forms.View.Details
            '
            'ClhAccountGpoExplorerState
            '
            Me.ClhAccountGpoExplorerState.Text = "Etat"
            Me.ClhAccountGpoExplorerState.Width = 39
            '
            'ClhAccountGpoExplorerDescription
            '
            Me.ClhAccountGpoExplorerDescription.Text = "Description"
            Me.ClhAccountGpoExplorerDescription.Width = 670
            '
            'TpAccountGpoTaskbar
            '
            Me.TpAccountGpoTaskbar.Controls.Add(Me.LvAccountGpoTaskbar)
            Me.TpAccountGpoTaskbar.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoTaskbar.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoTaskbar.HorizontalScrollbarSize = 10
            Me.TpAccountGpoTaskbar.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoTaskbar.Name = "TpAccountGpoTaskbar"
            Me.TpAccountGpoTaskbar.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoTaskbar.TabIndex = 1
            Me.TpAccountGpoTaskbar.Tag = "5"
            Me.TpAccountGpoTaskbar.Text = "Barre des tâches"
            Me.TpAccountGpoTaskbar.VerticalScrollbarBarColor = True
            Me.TpAccountGpoTaskbar.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoTaskbar.VerticalScrollbarSize = 10
            '
            'LvAccountGpoTaskbar
            '
            Me.LvAccountGpoTaskbar.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoTaskbar.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoTaskbar.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountGpoTaskbarState, Me.ClhAccountGpoTaskbarDescription})
            Me.LvAccountGpoTaskbar.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoTaskbar.FullRowSelect = True
            Me.LvAccountGpoTaskbar.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoTaskbar.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoTaskbar.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoTaskbar.Name = "LvAccountGpoTaskbar"
            Me.LvAccountGpoTaskbar.OwnerDraw = True
            Me.LvAccountGpoTaskbar.Size = New System.Drawing.Size(803, 398)
            Me.LvAccountGpoTaskbar.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoTaskbar.TabIndex = 3
            Me.LvAccountGpoTaskbar.TabStop = False
            Me.LvAccountGpoTaskbar.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoTaskbar.UseSelectable = True
            Me.LvAccountGpoTaskbar.View = System.Windows.Forms.View.Details
            '
            'ClhAccountGpoTaskbarState
            '
            Me.ClhAccountGpoTaskbarState.Text = "Etat"
            Me.ClhAccountGpoTaskbarState.Width = 39
            '
            'ClhAccountGpoTaskbarDescription
            '
            Me.ClhAccountGpoTaskbarDescription.Text = "Description"
            Me.ClhAccountGpoTaskbarDescription.Width = 670
            '
            'TpAccountGpoSystem
            '
            Me.TpAccountGpoSystem.Controls.Add(Me.LvAccountGpoSystem)
            Me.TpAccountGpoSystem.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoSystem.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoSystem.HorizontalScrollbarSize = 10
            Me.TpAccountGpoSystem.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoSystem.Name = "TpAccountGpoSystem"
            Me.TpAccountGpoSystem.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoSystem.TabIndex = 2
            Me.TpAccountGpoSystem.Tag = "6"
            Me.TpAccountGpoSystem.Text = "Système"
            Me.TpAccountGpoSystem.VerticalScrollbarBarColor = True
            Me.TpAccountGpoSystem.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoSystem.VerticalScrollbarSize = 10
            '
            'LvAccountGpoSystem
            '
            Me.LvAccountGpoSystem.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoSystem.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoSystem.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountGpoSystemState, Me.ClhAccountGpoSystemDescription})
            Me.LvAccountGpoSystem.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoSystem.FullRowSelect = True
            Me.LvAccountGpoSystem.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoSystem.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoSystem.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoSystem.Name = "LvAccountGpoSystem"
            Me.LvAccountGpoSystem.OwnerDraw = True
            Me.LvAccountGpoSystem.Size = New System.Drawing.Size(803, 398)
            Me.LvAccountGpoSystem.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoSystem.TabIndex = 4
            Me.LvAccountGpoSystem.TabStop = False
            Me.LvAccountGpoSystem.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoSystem.UseSelectable = True
            Me.LvAccountGpoSystem.View = System.Windows.Forms.View.Details
            '
            'ClhAccountGpoSystemState
            '
            Me.ClhAccountGpoSystemState.Text = "Etat"
            Me.ClhAccountGpoSystemState.Width = 39
            '
            'ClhAccountGpoSystemDescription
            '
            Me.ClhAccountGpoSystemDescription.Text = "Description"
            Me.ClhAccountGpoSystemDescription.Width = 670
            '
            'TpAccountGpoConnexions
            '
            Me.TpAccountGpoConnexions.Controls.Add(Me.LvAccountGpoConnexions)
            Me.TpAccountGpoConnexions.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoConnexions.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoConnexions.HorizontalScrollbarSize = 10
            Me.TpAccountGpoConnexions.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoConnexions.Name = "TpAccountGpoConnexions"
            Me.TpAccountGpoConnexions.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoConnexions.TabIndex = 6
            Me.TpAccountGpoConnexions.Tag = "7"
            Me.TpAccountGpoConnexions.Text = "Connexions"
            Me.TpAccountGpoConnexions.VerticalScrollbarBarColor = True
            Me.TpAccountGpoConnexions.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoConnexions.VerticalScrollbarSize = 0
            '
            'LvAccountGpoConnexions
            '
            Me.LvAccountGpoConnexions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoConnexions.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoConnexions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountGpoConnexionsState, Me.ClhAccountGpoConnexionsDescription})
            Me.LvAccountGpoConnexions.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoConnexions.FullRowSelect = True
            Me.LvAccountGpoConnexions.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoConnexions.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoConnexions.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoConnexions.Name = "LvAccountGpoConnexions"
            Me.LvAccountGpoConnexions.OwnerDraw = True
            Me.LvAccountGpoConnexions.Size = New System.Drawing.Size(804, 395)
            Me.LvAccountGpoConnexions.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoConnexions.TabIndex = 7
            Me.LvAccountGpoConnexions.TabStop = False
            Me.LvAccountGpoConnexions.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoConnexions.UseSelectable = True
            Me.LvAccountGpoConnexions.View = System.Windows.Forms.View.Details
            '
            'ClhAccountGpoConnexionsState
            '
            Me.ClhAccountGpoConnexionsState.Text = "Etat"
            Me.ClhAccountGpoConnexionsState.Width = 39
            '
            'ClhAccountGpoConnexionsDescription
            '
            Me.ClhAccountGpoConnexionsDescription.Text = "Description"
            Me.ClhAccountGpoConnexionsDescription.Width = 670
            '
            'TpAccountGpoServices
            '
            Me.TpAccountGpoServices.Controls.Add(Me.LvAccountGpoServices)
            Me.TpAccountGpoServices.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoServices.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoServices.HorizontalScrollbarSize = 10
            Me.TpAccountGpoServices.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoServices.Name = "TpAccountGpoServices"
            Me.TpAccountGpoServices.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoServices.TabIndex = 7
            Me.TpAccountGpoServices.Tag = "8"
            Me.TpAccountGpoServices.Text = "Services"
            Me.TpAccountGpoServices.VerticalScrollbarBarColor = True
            Me.TpAccountGpoServices.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoServices.VerticalScrollbarSize = 10
            '
            'LvAccountGpoServices
            '
            Me.LvAccountGpoServices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoServices.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoServices.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountGpoServicesState, Me.ClhAccountGpoServicesDescription})
            Me.LvAccountGpoServices.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoServices.FullRowSelect = True
            Me.LvAccountGpoServices.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoServices.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoServices.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoServices.Name = "LvAccountGpoServices"
            Me.LvAccountGpoServices.OwnerDraw = True
            Me.LvAccountGpoServices.Size = New System.Drawing.Size(803, 395)
            Me.LvAccountGpoServices.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoServices.TabIndex = 8
            Me.LvAccountGpoServices.TabStop = False
            Me.LvAccountGpoServices.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoServices.UseSelectable = True
            Me.LvAccountGpoServices.View = System.Windows.Forms.View.Details
            '
            'ClhAccountGpoServicesState
            '
            Me.ClhAccountGpoServicesState.Text = "Etat"
            Me.ClhAccountGpoServicesState.Width = 39
            '
            'ClhAccountGpoServicesDescription
            '
            Me.ClhAccountGpoServicesDescription.Text = "Description"
            Me.ClhAccountGpoServicesDescription.Width = 670
            '
            'TpAccountGpoLauncher
            '
            Me.TpAccountGpoLauncher.Controls.Add(Me.LvAccountGpoLauncher)
            Me.TpAccountGpoLauncher.HorizontalScrollbarBarColor = True
            Me.TpAccountGpoLauncher.HorizontalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoLauncher.HorizontalScrollbarSize = 10
            Me.TpAccountGpoLauncher.Location = New System.Drawing.Point(4, 38)
            Me.TpAccountGpoLauncher.Name = "TpAccountGpoLauncher"
            Me.TpAccountGpoLauncher.Size = New System.Drawing.Size(834, 394)
            Me.TpAccountGpoLauncher.TabIndex = 9
            Me.TpAccountGpoLauncher.Text = "Launcher"
            Me.TpAccountGpoLauncher.VerticalScrollbarBarColor = True
            Me.TpAccountGpoLauncher.VerticalScrollbarHighlightOnWheel = False
            Me.TpAccountGpoLauncher.VerticalScrollbarSize = 10
            '
            'LvAccountGpoLauncher
            '
            Me.LvAccountGpoLauncher.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LvAccountGpoLauncher.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.LvAccountGpoLauncher.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
            Me.LvAccountGpoLauncher.Font = New System.Drawing.Font("Segoe UI", 12.0!)
            Me.LvAccountGpoLauncher.FullRowSelect = True
            Me.LvAccountGpoLauncher.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
            Me.LvAccountGpoLauncher.LargeImageList = Me.ImlParameters
            Me.LvAccountGpoLauncher.Location = New System.Drawing.Point(15, 8)
            Me.LvAccountGpoLauncher.Name = "LvAccountGpoLauncher"
            Me.LvAccountGpoLauncher.OwnerDraw = True
            Me.LvAccountGpoLauncher.Size = New System.Drawing.Size(803, 395)
            Me.LvAccountGpoLauncher.SmallImageList = Me.ImlParameters
            Me.LvAccountGpoLauncher.TabIndex = 9
            Me.LvAccountGpoLauncher.TabStop = False
            Me.LvAccountGpoLauncher.UseCompatibleStateImageBehavior = False
            Me.LvAccountGpoLauncher.UseSelectable = True
            Me.LvAccountGpoLauncher.View = System.Windows.Forms.View.Details
            '
            'ColumnHeader1
            '
            Me.ColumnHeader1.Text = "Etat"
            Me.ColumnHeader1.Width = 39
            '
            'ColumnHeader2
            '
            Me.ColumnHeader2.Text = "Description"
            Me.ColumnHeader2.Width = 670
            '
            'TpSettings
            '
            Me.TpSettings.Location = New System.Drawing.Point(4, 22)
            Me.TpSettings.Name = "TpSettings"
            Me.TpSettings.Padding = New System.Windows.Forms.Padding(3)
            Me.TpSettings.Size = New System.Drawing.Size(850, 510)
            Me.TpSettings.TabIndex = 3
            Me.TpSettings.Text = "TabPage5"
            Me.TpSettings.UseVisualStyleBackColor = True
            '
            'SpcAccounts
            '
            Me.SpcAccounts.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SpcAccounts.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
            Me.SpcAccounts.IsSplitterFixed = True
            Me.SpcAccounts.Location = New System.Drawing.Point(0, 0)
            Me.SpcAccounts.Name = "SpcAccounts"
            Me.SpcAccounts.Orientation = System.Windows.Forms.Orientation.Horizontal
            '
            'SpcAccounts.Panel1
            '
            Me.SpcAccounts.Panel1.BackColor = System.Drawing.Color.White
            '
            'SpcAccounts.Panel2
            '
            Me.SpcAccounts.Panel2.BackColor = System.Drawing.Color.White
            Me.SpcAccounts.Size = New System.Drawing.Size(177, 386)
            Me.SpcAccounts.SplitterDistance = 25
            Me.SpcAccounts.TabIndex = 9
            '
            'TlpLeftMenu
            '
            Me.TlpLeftMenu.BackColor = System.Drawing.SystemColors.MenuHighlight
            Me.TlpLeftMenu.ColumnCount = 1
            Me.TlpLeftMenu.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
            Me.TlpLeftMenu.Controls.Add(Me.BtnReload, 0, 5)
            Me.TlpLeftMenu.Controls.Add(Me.BtnApply, 0, 6)
            Me.TlpLeftMenu.Controls.Add(Me.BtnAccount, 0, 0)
            Me.TlpLeftMenu.Dock = System.Windows.Forms.DockStyle.Fill
            Me.TlpLeftMenu.Location = New System.Drawing.Point(0, 0)
            Me.TlpLeftMenu.Name = "TlpLeftMenu"
            Me.TlpLeftMenu.RowCount = 7
            Me.TlpLeftMenu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28716!))
            Me.TlpLeftMenu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28717!))
            Me.TlpLeftMenu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28716!))
            Me.TlpLeftMenu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28252!))
            Me.TlpLeftMenu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28252!))
            Me.TlpLeftMenu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28531!))
            Me.TlpLeftMenu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28816!))
            Me.TlpLeftMenu.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
            Me.TlpLeftMenu.Size = New System.Drawing.Size(80, 570)
            Me.TlpLeftMenu.TabIndex = 0
            '
            'BtnReload
            '
            Me.BtnReload.BackColor = System.Drawing.SystemColors.MenuHighlight
            Me.BtnReload.Dock = System.Windows.Forms.DockStyle.Fill
            Me.BtnReload.FlatAppearance.BorderSize = 0
            Me.BtnReload.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.BtnReload.ForeColor = System.Drawing.Color.White
            Me.BtnReload.Image = Global.Sophos_Analyzer.My.Resources.Resources.Refresh48
            Me.BtnReload.Location = New System.Drawing.Point(3, 408)
            Me.BtnReload.Name = "BtnReload"
            Me.BtnReload.Size = New System.Drawing.Size(74, 75)
            Me.BtnReload.TabIndex = 3
            Me.BtnReload.Tag = "4"
            Me.BtnReload.UseVisualStyleBackColor = False
            '
            'BtnApply
            '
            Me.BtnApply.BackColor = System.Drawing.SystemColors.MenuHighlight
            Me.BtnApply.Dock = System.Windows.Forms.DockStyle.Fill
            Me.BtnApply.Enabled = False
            Me.BtnApply.FlatAppearance.BorderSize = 0
            Me.BtnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.BtnApply.ForeColor = System.Drawing.Color.White
            Me.BtnApply.Image = Global.Sophos_Analyzer.My.Resources.Resources.Valid48
            Me.BtnApply.Location = New System.Drawing.Point(3, 489)
            Me.BtnApply.Name = "BtnApply"
            Me.BtnApply.Size = New System.Drawing.Size(74, 78)
            Me.BtnApply.TabIndex = 4
            Me.BtnApply.Tag = "5"
            Me.BtnApply.UseVisualStyleBackColor = False
            '
            'BtnAccount
            '
            Me.BtnAccount.BackColor = System.Drawing.SystemColors.MenuHighlight
            Me.BtnAccount.Dock = System.Windows.Forms.DockStyle.Fill
            Me.BtnAccount.FlatAppearance.BorderColor = System.Drawing.Color.White
            Me.BtnAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.BtnAccount.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.BtnAccount.ForeColor = System.Drawing.Color.White
            Me.BtnAccount.Image = Global.Sophos_Analyzer.My.Resources.Resources.Account48
            Me.BtnAccount.Location = New System.Drawing.Point(3, 3)
            Me.BtnAccount.Name = "BtnAccount"
            Me.BtnAccount.Size = New System.Drawing.Size(74, 75)
            Me.BtnAccount.TabIndex = 0
            Me.BtnAccount.Tag = "0"
            Me.BtnAccount.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            Me.BtnAccount.UseVisualStyleBackColor = False
            '
            'SpcMain
            '
            Me.SpcMain.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SpcMain.IsSplitterFixed = True
            Me.SpcMain.Location = New System.Drawing.Point(0, 0)
            Me.SpcMain.Name = "SpcMain"
            '
            'SpcMain.Panel1
            '
            Me.SpcMain.Panel1.Controls.Add(Me.TlpLeftMenu)
            '
            'SpcMain.Panel2
            '
            Me.SpcMain.Panel2.BackColor = System.Drawing.Color.Transparent
            Me.SpcMain.Panel2.Controls.Add(Me.SpcMiddle)
            Me.SpcMain.Size = New System.Drawing.Size(944, 570)
            Me.SpcMain.SplitterDistance = 80
            Me.SpcMain.TabIndex = 1
            Me.SpcMain.TabStop = False
            '
            'BgwLoadAccounts
            '
            Me.BgwLoadAccounts.WorkerReportsProgress = True
            '
            'BgwLoadProfile
            '
            Me.BgwLoadProfile.WorkerReportsProgress = True
            Me.BgwLoadProfile.WorkerSupportsCancellation = True
            '
            'miniToolStrip
            '
            Me.miniToolStrip.AutoSize = False
            Me.miniToolStrip.BackColor = System.Drawing.Color.DodgerBlue
            Me.miniToolStrip.CanOverflow = False
            Me.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None
            Me.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
            Me.miniToolStrip.Location = New System.Drawing.Point(250, 3)
            Me.miniToolStrip.Name = "miniToolStrip"
            Me.miniToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
            Me.miniToolStrip.Size = New System.Drawing.Size(830, 25)
            Me.miniToolStrip.TabIndex = 15
            '
            'BgwApply
            '
            Me.BgwApply.WorkerReportsProgress = True
            Me.BgwApply.WorkerSupportsCancellation = True
            '
            'SpcFrm
            '
            Me.SpcFrm.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SpcFrm.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
            Me.SpcFrm.IsSplitterFixed = True
            Me.SpcFrm.Location = New System.Drawing.Point(20, 60)
            Me.SpcFrm.Name = "SpcFrm"
            Me.SpcFrm.Orientation = System.Windows.Forms.Orientation.Horizontal
            '
            'SpcFrm.Panel1
            '
            Me.SpcFrm.Panel1.Controls.Add(Me.LblMainNotification)
            '
            'SpcFrm.Panel2
            '
            Me.SpcFrm.Panel2.Controls.Add(Me.SpcMain)
            Me.SpcFrm.Size = New System.Drawing.Size(944, 604)
            Me.SpcFrm.SplitterDistance = 30
            Me.SpcFrm.TabIndex = 3
            '
            'LblMainNotification
            '
            Me.LblMainNotification.BackColor = System.Drawing.Color.SteelBlue
            Me.LblMainNotification.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LblMainNotification.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LblMainNotification.ForeColor = System.Drawing.Color.White
            Me.LblMainNotification.Location = New System.Drawing.Point(0, 0)
            Me.LblMainNotification.Name = "LblMainNotification"
            Me.LblMainNotification.Size = New System.Drawing.Size(944, 30)
            Me.LblMainNotification.TabIndex = 1
            Me.LblMainNotification.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'FrmConfigurator
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle
            Me.ClientSize = New System.Drawing.Size(984, 684)
            Me.Controls.Add(Me.SpcFrm)
            Me.Controls.Add(Me.PgbStatus)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Name = "FrmConfigurator"
            Me.Resizable = False
            Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
            Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
            Me.Text = "Sophos Analyzer Configurator"
            Me.SpcMiddle.Panel1.ResumeLayout(False)
            Me.SpcMiddle.Panel2.ResumeLayout(False)
            CType(Me.SpcMiddle, System.ComponentModel.ISupportInitialize).EndInit()
            Me.SpcMiddle.ResumeLayout(False)
            Me.TbcMain.ResumeLayout(False)
            Me.TpAccount.ResumeLayout(False)
            Me.SpcAccountInfos.Panel1.ResumeLayout(False)
            Me.SpcAccountInfos.Panel1.PerformLayout()
            Me.SpcAccountInfos.Panel2.ResumeLayout(False)
            CType(Me.SpcAccountInfos, System.ComponentModel.ISupportInitialize).EndInit()
            Me.SpcAccountInfos.ResumeLayout(False)
            Me.TsAccountState.ResumeLayout(False)
            Me.TsAccountState.PerformLayout()
            Me.SpcAccountState.Panel1.ResumeLayout(False)
            Me.SpcAccountState.Panel2.ResumeLayout(False)
            CType(Me.SpcAccountState, System.ComponentModel.ISupportInitialize).EndInit()
            Me.SpcAccountState.ResumeLayout(False)
            Me.TlpAccountState.ResumeLayout(False)
            Me.TlpAccountState.PerformLayout()
            Me.TbcAccountGpo.ResumeLayout(False)
            Me.TpAccountGpoAccount.ResumeLayout(False)
            Me.TpAccountGpoWallpaper.ResumeLayout(False)
            Me.TpAccountGpoWallpaper.PerformLayout()
            CType(Me.PcbAccountGpoWallpaper, System.ComponentModel.ISupportInitialize).EndInit()
            Me.TpAccountGpoDesktop.ResumeLayout(False)
            Me.TpAccountGpoStartMenu.ResumeLayout(False)
            Me.TpAccountGpoExplorer.ResumeLayout(False)
            Me.TpAccountGpoTaskbar.ResumeLayout(False)
            Me.TpAccountGpoSystem.ResumeLayout(False)
            Me.TpAccountGpoConnexions.ResumeLayout(False)
            Me.TpAccountGpoServices.ResumeLayout(False)
            Me.TpAccountGpoLauncher.ResumeLayout(False)
            CType(Me.SpcAccounts, System.ComponentModel.ISupportInitialize).EndInit()
            Me.SpcAccounts.ResumeLayout(False)
            Me.TlpLeftMenu.ResumeLayout(False)
            Me.SpcMain.Panel1.ResumeLayout(False)
            Me.SpcMain.Panel2.ResumeLayout(False)
            CType(Me.SpcMain, System.ComponentModel.ISupportInitialize).EndInit()
            Me.SpcMain.ResumeLayout(False)
            Me.SpcFrm.Panel1.ResumeLayout(False)
            Me.SpcFrm.Panel2.ResumeLayout(False)
            CType(Me.SpcFrm, System.ComponentModel.ISupportInitialize).EndInit()
            Me.SpcFrm.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents PgbStatus As MetroProgressBar
        Friend WithEvents SpcMiddle As SplitContainer
        Friend WithEvents LblSecondNotification As Label
        Friend WithEvents TlpLeftMenu As TableLayoutPanel
        Friend WithEvents BtnAccount As Button
        Friend WithEvents BtnApply As Button
        Friend WithEvents SpcMain As SplitContainer
        Friend WithEvents BgwLoadAccounts As System.ComponentModel.BackgroundWorker
        Friend WithEvents TbcMain As TabControl
        Friend WithEvents TpAccount As TabPage
        Friend WithEvents SpcAccounts As SplitContainer
        Friend WithEvents TpSettings As TabPage
        Friend WithEvents BgwLoadProfile As System.ComponentModel.BackgroundWorker
        Friend WithEvents ImlParameters As ImageList
        Friend WithEvents SpcAccountInfos As SplitContainer
        Friend WithEvents TsAccountState As ToolStrip
        Friend WithEvents TsLblAccountState As ToolStripLabel
        Friend WithEvents SpcAccountState As SplitContainer
        Friend WithEvents LblAccountGpoStates As Label
        Friend WithEvents TglAccountGpoState As MetroToggle
        Friend WithEvents TbcAccountGpo As MetroTabControl
        Friend WithEvents TpAccountGpoAccount As MetroTabPage
        Friend WithEvents LvAccountGpoAccount As MetroListView
        Friend WithEvents ClhAccountGpoAccountState As ColumnHeader
        Friend WithEvents ClhAccountGpoAccountDescription As ColumnHeader
        Friend WithEvents TpAccountGpoDesktop As MetroTabPage
        Friend WithEvents LvAccountGpoDesktop As MetroListView
        Friend WithEvents ClhAccountGpoDesktopState As ColumnHeader
        Friend WithEvents ClhAccountGpoDesktopDescription As ColumnHeader
        Friend WithEvents TpAccountGpoTaskbar As MetroTabPage
        Friend WithEvents LvAccountGpoTaskbar As MetroListView
        Friend WithEvents ClhAccountGpoTaskbarState As ColumnHeader
        Friend WithEvents ClhAccountGpoTaskbarDescription As ColumnHeader
        Friend WithEvents TpAccountGpoSystem As MetroTabPage
        Friend WithEvents LvAccountGpoSystem As MetroListView
        Friend WithEvents ClhAccountGpoSystemState As ColumnHeader
        Friend WithEvents ClhAccountGpoSystemDescription As ColumnHeader
        Friend WithEvents TpAccountGpoStartMenu As MetroTabPage
        Friend WithEvents LvAccountGpoStartMenu As MetroListView
        Friend WithEvents ClhAccountGpoStartMenuState As ColumnHeader
        Friend WithEvents ClhAccountGpoStartMenuDescription As ColumnHeader
        Friend WithEvents TpAccountGpoExplorer As MetroTabPage
        Friend WithEvents LvAccountGpoExplorer As MetroListView
        Friend WithEvents ClhAccountGpoExplorerState As ColumnHeader
        Friend WithEvents ClhAccountGpoExplorerDescription As ColumnHeader
        Friend WithEvents TpAccountGpoConnexions As MetroTabPage
        Friend WithEvents LvAccountGpoConnexions As MetroListView
        Friend WithEvents ClhAccountGpoConnexionsState As ColumnHeader
        Friend WithEvents ClhAccountGpoConnexionsDescription As ColumnHeader
        Friend WithEvents TpAccountGpoServices As MetroTabPage
        Friend WithEvents LvAccountGpoServices As MetroListView
        Friend WithEvents ClhAccountGpoServicesState As ColumnHeader
        Friend WithEvents ClhAccountGpoServicesDescription As ColumnHeader
        Friend WithEvents TpAccountGpoWallpaper As MetroTabPage
        Friend WithEvents PcbAccountGpoWallpaper As PictureBox
        Friend WithEvents BtnAccountGpoWallpaper As Button
        Friend WithEvents TxbAccountGpoWallpaper As MetroTextBox
        Friend WithEvents LblAccountGpoWallpaper As MetroLabel
        Friend WithEvents BtnReload As Button
        Friend WithEvents TlpAccountState As TableLayoutPanel
        Friend WithEvents miniToolStrip As ToolStrip
        Friend WithEvents BgwApply As System.ComponentModel.BackgroundWorker
        Friend WithEvents SpcFrm As SplitContainer
        Friend WithEvents LblMainNotification As Label
        Friend WithEvents TpAccountGpoLauncher As MetroTabPage
        Friend WithEvents LvAccountGpoLauncher As MetroListView
        Friend WithEvents ColumnHeader1 As ColumnHeader
        Friend WithEvents ColumnHeader2 As ColumnHeader
    End Class
End Namespace