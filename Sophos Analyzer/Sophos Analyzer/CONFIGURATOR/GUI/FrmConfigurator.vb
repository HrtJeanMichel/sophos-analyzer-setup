﻿Imports System.IO
Imports System.ComponentModel
Imports MetroFramework.Forms
Imports MetroFramework.Controls
Imports Sophos_Analyzer.Core.Usb
Imports Sophos_Analyzer.Core.Tweaks
Imports Sophos_Analyzer.Helper
Imports Sophos_Analyzer.Helper.FrmInfos
Imports Sophos_Analyzer.Core.Settings
Imports System.Threading

Namespace Configurator.Gui
    Public Class FrmConfigurator
        Inherits MetroForm

#Region " Fields "
        Private m_SelectedBtn As Button
        Private m_Random As Random
        Private m_Detector As Detector
        Private m_MimeTypes As MimeTypes

        Friend UserInfos As UserInfos
        Friend UserAccountName As String
        Friend TaskOngoing As Boolean

        Friend Parameters As Parameters

        Friend InitParameters As Parameters

        Private LoadingProfil As Boolean
        Private SavingProfil As Boolean
#End Region

#Region " Enumerations "
        Enum TaskState
            [Error] = 0
            SuccessUser = 1
            Warning = 2
            AbortUser = 3
        End Enum

        Public Enum ThreadState
            MainMessage = -1
            Progress
        End Enum
#End Region

#Region " Constructor "
        Public Sub New(UserI As UserInfos)
            InitializeComponent()
            '783; 371
            PcbAccountGpoWallpaper.Size = New Size(648, 342)
            LvAccountGpoAccount.Size = New Size(785, 371)
            LvAccountGpoConnexions.Size = New Size(785, 371)
            LvAccountGpoDesktop.Size = New Size(785, 371)
            LvAccountGpoExplorer.Size = New Size(785, 371)
            LvAccountGpoServices.Size = New Size(785, 371)
            LvAccountGpoLauncher.Size = New Size(785, 371)
            LvAccountGpoStartMenu.Size = New Size(785, 371)
            LvAccountGpoSystem.Size = New Size(785, 371)
            LvAccountGpoTaskbar.Size = New Size(785, 371)

            UserInfos = UserI
            UserAccountName = UserI.Name
            TsLblAccountState.Text &= UserI.Name
            HideMainMenuTabs()
            m_SelectedBtn = BtnAccount
            BtnAccount.Text = UserI.Name
            m_Random = New Random
            m_MimeTypes = New MimeTypes
        End Sub
#End Region

        Private Sub FrmConfigurator_Shown(sender As Object, e As EventArgs) Handles Me.Shown
            LoadAccountInfos()
        End Sub

        Public Sub LoadAccountInfos(Optional ByVal Reinit As Boolean = False)
            Try
                If Not BgwLoadProfile.IsBusy Then
                    LoadingProfil = True
                    BtnApply.Enabled = False
                    TaskOngoing = True
                    TpAccount.Focus()
                    ActiveControl = TpAccount
                    TbcAccountGpo.SelectedTab = TpAccountGpoAccount
                    If Reinit Then
                        UpdateBorder(BtnAccount)
                        TbcMain.SelectedIndex = 0
                        TbcMain.TabPages(0).Focus()
                        ActiveControl = TbcMain.TabPages(0)
                    End If
                    SpcAccountState.Panel1.Enabled = False
                    SpcAccountState.Panel2.Enabled = False

                    SpcMain.Panel1.Enabled = False
                    Manager.SetGpoListViewItemsClear(Me)

                    Parameters = New Parameters(BgwLoadProfile)

                    InitParameters = New Parameters

                    UserInfos = New UserInfos(UserAccountName, True)

                    BgwLoadProfile.RunWorkerAsync(UserInfos)
                End If
            Catch ex As Exception
                LblMainNotification.Text = "Exception non gérée depuis LoadAccountInfos :  " & ex.Message
            End Try
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque le programme est en cours de fermeture.
        ''' </summary>
        ''' <remarks>
        ''' Si une tache est en cours alors on annule la fermeture du programme ou de la session.
        ''' </remarks>
        Private Sub FrmConfigurator_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
            If e.CloseReason.Equals(CloseReason.UserClosing) Then
                If TaskOngoing Then e.Cancel = True
            ElseIf e.CloseReason.Equals(CloseReason.WindowsShutDown) Then
                If TaskOngoing Then
                    OS.ShutdownCancel()
                    Using infos As New FrmInfos(Messages.BeCarefullTitle, Messages.TaskIsRunning, ButtonTypes.OK, MessageTypes.Warning)
                        infos.ShowDialog()
                    End Using
                Else
                    If Reg.Mounted() Then
                        OS.ShutdownCancel()
                        Using infos As New FrmInfos(Messages.BeCarefullTitle, Messages.PleaseCloseSession, ButtonTypes.OK, MessageTypes.Warning)
                            infos.ShowDialog()
                        End Using
                    End If
                End If
            End If
        End Sub

        Private Sub FrmConfigurator_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
            Dim Loaded As Boolean = True
            Do
                Loaded = Reg.Mounted()
                If Loaded Then
                    Reg.Unload()
                    Thread.Sleep(1000)
                Else
                    Exit Do
                End If
            Loop
        End Sub

        Private Sub HideMainMenuTabs()
            With TbcMain
                .SuspendLayout()
                .SizeMode = TabSizeMode.Fixed
                .ItemSize = New Size(0, 1)
                .Appearance = TabAppearance.FlatButtons
                .ResumeLayout()
            End With
        End Sub

        ''' <summary>
        ''' Charge toutes les informations du profil sélectionné (NTUSER.DAT est chargé dans TempUserDat).
        ''' </summary>
        Private Sub BgwLoadProfile_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwLoadProfile.DoWork
            Try
                Dim Account As UserInfos = TryCast(e.Argument, UserInfos)

                With Parameters
                    If Not Directory.Exists(Account.ProfileImagePath) Then
                        If Account.Generate(BgwLoadProfile) Then
                            BgwLoadProfile.ReportProgress(ThreadState.MainMessage, Messages.ProfilDirCreated)
                        Else
                            BgwLoadProfile.ReportProgress(ThreadState.MainMessage, "Erreur depuis BgwLoadProfile_DoWork : " & Messages.ProfilDirNotCreated)
                            e.Result = TaskState.Warning
                            Exit Sub
                        End If
                    End If

                    If File.Exists(Account.NtUserDatFilePath) Then
                        If Account.IsLoggedIn Then
                            BgwLoadProfile.ReportProgress(ThreadState.MainMessage, Messages.ProfilLogoff)
                            If Account.LogoffSession() = False Then
                                BgwLoadProfile.ReportProgress(ThreadState.MainMessage, "Attention : " & Messages.ProfilLoggedOn)
                                e.Result = TaskState.Warning
                                Exit Sub
                            End If
                        End If

                        If Reg.Mounted() Then
                            BgwLoadProfile.ReportProgress(ThreadState.MainMessage, Messages.RegStartUnload)
                            Reg.Unload()
                        End If
                        BgwLoadProfile.ReportProgress(ThreadState.MainMessage, Messages.RegStartLoad)

                        If Reg.Load(Account.NtUserDatFilePath) Then
                            If Reg.Mounted() Then
                                'Affiche les informations du compte utilisateur
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 1)
                                .User.Load(Account)
                                InitParameters.User.Load(Account)

                                '################################################# Stratégies Utilisateur
                                'Affiche l'état des stratégies locales de sécurité du bureau
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 2)
                                .Desktop.Load()
                                InitParameters.Desktop.Load()

                                'Affiche l'état des stratégies locales de sécurité de la barre des tâches
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 3)
                                .Taskbar.Load()
                                InitParameters.Taskbar.Load()

                                'Affiche l'état des stratégies locales de sécurité système
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 4)
                                .Systeme.Load()
                                InitParameters.Systeme.Load()

                                'Affiche l'état des stratégies locales de sécurité du menu démarrer
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 5)
                                .StartMenu.Load()
                                InitParameters.StartMenu.Load()

                                'Affiche l'état des stratégies locales de sécurité de l'explorateur
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 6)
                                .Explorer.Load()
                                InitParameters.Explorer.Load()

                                ''################################################# Stratégies Ordinateur
                                'Affiche les informations des cartes réseau
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 7)
                                .Network.Load()
                                InitParameters.Network.Load()

                                'Affiche les informations des services
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 8)
                                .Services.Load()
                                InitParameters.Services.Load()

                                'Affiche les informations sur le Launcher
                                BgwLoadProfile.ReportProgress(ThreadState.Progress, 9)
                                .Launcher.Load(Account.SID)
                                InitParameters.Launcher.Load(Account.SID)

                                If BgwLoadProfile.CancellationPending Then
                                    BgwLoadProfile.ReportProgress(ThreadState.MainMessage, Messages.ProfilUnLoaded)
                                    e.Result = TaskState.AbortUser
                                    Exit Sub
                                Else
                                    'Analyse des paramètres terminée !
                                    BgwLoadProfile.ReportProgress(ThreadState.MainMessage, Messages.ProfilLoaded)
                                    e.Result = TaskState.SuccessUser
                                End If
                            Else
                                BgwLoadProfile.ReportProgress(ThreadState.MainMessage, "Erreur depuis BgwLoadProfile_DoWork : " & Messages.ProfilNotLoaded)
                                e.Result = TaskState.Warning
                            End If
                        Else
                            BgwLoadProfile.ReportProgress(ThreadState.MainMessage, "Erreur depuis BgwLoadProfile_DoWork : " & Messages.RegNotLoaded)
                            e.Result = TaskState.Warning
                        End If
                    Else
                        BgwLoadProfile.ReportProgress(ThreadState.MainMessage, "Erreur depuis BgwLoadProfile_DoWork : " & Messages.ProfilFileNotExists)
                        e.Result = TaskState.Warning
                    End If
                End With
            Catch ex As Exception
                BgwLoadProfile.ReportProgress(ThreadState.MainMessage, "Exception non gérée depuis BgwLoadProfile_DoWork : " & ex.Message)
                e.Result = TaskState.Error
            End Try
        End Sub

        ''' <summary>
        ''' Affiche toutes les informations du profil sélectionné (NTUSER.DAT est chargé dans TempUserDat).
        ''' </summary>
        Private Sub BgwLoadProfile_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwLoadProfile.ProgressChanged
            Try
                If e.ProgressPercentage = -1 Then
                    LblMainNotification.Text = e.UserState.ToString
                ElseIf e.ProgressPercentage = 0 Then
                    PgbStatus.Value = CInt(e.UserState * 100) / 9
                Else
                    Manager.Initialize(Me, e.ProgressPercentage, e.UserState)
                End If
            Catch ex As Exception
                LblMainNotification.Text = "Exception non gérée depuis BgwLoadProfile_ProgressChanged :  " & ex.Message
            End Try
        End Sub

        ''' <summary>
        ''' Le chargement des informations du profil sélectionné est terminé, la ruche de profil reste chargée dans le registre pour les futures modifications.
        ''' </summary>
        Private Sub BgwLoadProfile_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwLoadProfile.RunWorkerCompleted
            Manager.LoadProfileEndTask(Me, e.Result)
            LoadingProfil = False
        End Sub

        Private Sub TglAccountGpoState_CheckedChanged(sender As Object, e As EventArgs) Handles TglAccountGpoState.CheckedChanged
            LblAccountGpoStates.Text = If(TglAccountGpoState.Checked, Messages.AccountOptimized, Messages.AccountNotOptimized)
            If LoadingProfil = False Then
                LblSecondNotification.Text = If(TglAccountGpoState.Checked, Messages.AccountOptimized, Messages.AccountNotOptimized)
            End If
            LblMainNotification.BackColor = Color.SteelBlue
        End Sub

        Private Sub LblAccountGpoStates_Click(sender As Object, e As EventArgs) Handles LblAccountGpoStates.Click
            TglAccountGpoState.Checked = Not TglAccountGpoState.Checked
            Manager.SetGpoState(Me, TglAccountGpoState.Checked)
            Manager.SetGpoParametersOptimization(Me, TglAccountGpoState.Checked)
        End Sub

        Private Sub TglAccountGpoState_Click(sender As Object, e As EventArgs) Handles TglAccountGpoState.Click
            Dim checked = TglAccountGpoState.Checked
            Manager.SetGpoState(Me, checked)
            Manager.SetGpoParametersOptimization(Me, checked)
        End Sub

        Private Sub Btns_Click(sender As Object, e As EventArgs) Handles BtnAccount.Click, BtnReload.Click, BtnApply.Click
            SelectTab(TryCast(sender, Button))
        End Sub

        Private Sub UpdateBorder(Btn As Button)
            m_SelectedBtn.FlatAppearance.BorderColor = Nothing
            m_SelectedBtn.FlatAppearance.BorderSize = 0
            Btn.FlatAppearance.BorderColor = Color.White
            Btn.FlatAppearance.BorderSize = 1
            m_SelectedBtn = Btn
            m_SelectedBtn.Refresh()
        End Sub

        Private Sub SelectTab(BtnTab As Button)
            Try
                Select Case BtnTab.Tag
                    Case 0
                        UpdateBorder(BtnTab)
                        If LoadingProfil = False Then
                            LblSecondNotification.Text = If(TglAccountGpoState.Checked, Messages.AccountOptimized, Messages.AccountNotOptimized)
                        End If

                        TbcMain.SelectedIndex = 0
                        TbcMain.TabPages(0).Focus()
                        ActiveControl = TbcMain.TabPages(0)

                        TpAccount.Focus()
                        ActiveControl = TpAccount
                        TbcAccountGpo.SelectedTab = TpAccountGpoAccount
                    Case 1
                        UpdateBorder(BtnTab)
                        TbcMain.SelectedIndex = 1
                        TbcMain.TabPages(1).Focus()
                        ActiveControl = TbcMain.TabPages(1)
                    Case 2
                        UpdateBorder(BtnTab)
                        TbcMain.SelectedIndex = 2
                        TbcMain.TabPages(2).Focus()
                        ActiveControl = TbcMain.TabPages(2)
                    Case 3
                        UpdateBorder(BtnTab)
                        TbcMain.SelectedIndex = 3
                        TbcMain.TabPages(3).Focus()
                        ActiveControl = TbcMain.TabPages(3)
                    Case 4
                        Using infos As New FrmInfos(Messages.InformationTitle, Messages.ReloadAccount, ButtonTypes.YesNo, MessageTypes.Info)
                            If infos.ShowDialog = DialogResult.OK Then
                                LoadAccountInfos(True)
                            End If
                        End Using
                    Case 5
                        If Not BgwApply.IsBusy Then
                            UpdateBorder(BtnTab)
                            TbcMain.SelectedIndex = 0
                            TbcMain.TabPages(0).Focus()
                            ActiveControl = TbcMain.TabPages(0)

                            SpcAccountState.Panel1.Enabled = False
                            SpcAccountState.Panel2.Enabled = False

                            SpcMain.Panel1.Enabled = False
                            TaskOngoing = True

                            BgwApply.RunWorkerAsync(Parameters)
                        End If
                End Select
            Catch ex As Exception
                LblMainNotification.Text = "Exception non gérée depuis SelectTab : " & ex.Message
            End Try
        End Sub

        ''' <summary>
        ''' Sauvegarde toutes les informations du profil sélectionné (NTUSER.DAT est chargé dans TempUserDat).
        ''' 
        ''' </summary>
        Private Sub BgwApply_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwApply.DoWork
            Try
                Dim params = TryCast(e.Argument, Parameters)
                With params
                    .Bgw = BgwApply
                    If InitParameters.User.EqualsTo(.User) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 1)
                        .User.Save()
                    End If
                    If InitParameters.Desktop.EqualsTo(.Desktop) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 2)
                        .Desktop.Save()
                    End If
                    If InitParameters.Explorer.EqualsTo(.Explorer) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 3)
                        .Explorer.Save()
                    End If
                    If InitParameters.Network.EqualsTo(.Network) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 4)
                        .Network.Save()
                    End If
                    If InitParameters.StartMenu.EqualsTo(.StartMenu) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 5)
                        .StartMenu.Save()
                    End If
                    If InitParameters.Systeme.EqualsTo(.Systeme) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 6)
                        .Systeme.Save()
                    End If
                    If InitParameters.Taskbar.EqualsTo(.Taskbar) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 7)
                        .Taskbar.Save()
                    End If
                    If InitParameters.Services.EqualsTo(.Services) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 8)
                        .Services.Save()
                    End If
                    If InitParameters.Launcher.EqualsTo(.Launcher) = False Then
                        BgwApply.ReportProgress(ThreadState.Progress, 9)
                        .Launcher.Save(params.User.SID)
                    End If
                End With
                e.Result = TaskState.SuccessUser
            Catch ex As Exception
                BgwApply.ReportProgress(ThreadState.MainMessage, "Exception non gérée depuis BgwApply_DoWork : " & ex.Message)
                e.Result = TaskState.Error
            End Try
        End Sub

        Private Sub BgwApply_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwApply.ProgressChanged
            If e.ProgressPercentage = -1 Then
                LblMainNotification.Text = e.UserState.ToString
            ElseIf e.ProgressPercentage = 0 Then
                PgbStatus.Value = CInt(e.UserState * 100) / 9
            End If
        End Sub

        Private Sub BgwApply_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwApply.RunWorkerCompleted
            Manager.ApplyProfileEndTask(Me, e.Result)
        End Sub

        Private Sub TxbAccountGpoWallpaper_TextChanged(sender As Object, e As EventArgs) Handles TxbAccountGpoWallpaper.TextChanged
            If File.Exists(TxbAccountGpoWallpaper.Text) Then
                PcbAccountGpoWallpaper.Tag = Image.FromFile(TxbAccountGpoWallpaper.Text)
                Pictures.AutosizeImgByPath(TxbAccountGpoWallpaper.Text, PcbAccountGpoWallpaper, PictureBoxSizeMode.CenterImage)
            End If
        End Sub

        Private Sub PcbAccountGpoWallpaper_SizeChanged(sender As Object, e As EventArgs) Handles PcbAccountGpoWallpaper.SizeChanged
            Pictures.AutosizeImgByBitmap(PcbAccountGpoWallpaper.Tag, PcbAccountGpoWallpaper, PictureBoxSizeMode.CenterImage)
        End Sub

        Private Sub FrmMain_ClientSizeChanged(sender As Object, e As EventArgs) Handles LvAccountGpoAccount.SizeChanged, LvAccountGpoDesktop.SizeChanged, LvAccountGpoTaskbar.SizeChanged, LvAccountGpoSystem.SizeChanged, LvAccountGpoStartMenu.SizeChanged, LvAccountGpoExplorer.SizeChanged, LvAccountGpoConnexions.SizeChanged, LvAccountGpoServices.SizeChanged, LvAccountGpoLauncher.SizeChanged
            Dim lv As MetroListView = TryCast(sender, MetroListView)
            Dim oldTotalWidth As Integer = 0
            For Each column As ColumnHeader In lv.Columns
                oldTotalWidth += column.Width
            Next
            Dim newTotalWidth As Integer = lv.ClientSize.Width
            Dim growthFactor As Double = newTotalWidth / oldTotalWidth
            For Each column As ColumnHeader In lv.Columns
                column.Width = Convert.ToInt32(Math.Round(column.Width * growthFactor))
            Next
        End Sub

    End Class
End Namespace