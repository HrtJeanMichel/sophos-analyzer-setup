﻿Imports System.IO
Imports Sophos_Analyzer.Helper

Namespace Launcher.Sophos
    Friend NotInheritable Class Settings

#Region " Fields "
        Private Shared workDirUcheck$ = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) & "\SophosAnalyzer"
        Friend Shared driveCleanup$ = Environment.GetEnvironmentVariable("SystemDrive") & "\DriveCleanup.exe"
#End Region

#Region " Constants "
        Public Const DeviceRipped As String = "Votre périphérique USB a été arraché, le scan a été annulé !" & vbNewLine & "Veuillez insérer votre support."
        Public Const RemoveDevice As String = "Veuillez retirer votre périphérique USB"
        Public Const InsertDevice As String = "Veuillez insérer votre périphérique USB"
        Public Const CleanupDevices As String = "Nettoyage des fichiers de pilotes en cours ..."
        Public Const ScanInProgress As String = "Veuillez patienter durant l'analyse"
        Public Const DeviceClean As String = "Aucune menace détectée."
        Public Const DeviceInfect As String = "Menace(s) détectée(s)."
        Public Const ContactAdmin As String = "Contactez l'administrateur SSI !"
        Public Const WarningSavCli As String = "Un problème est survenu."
        Public Const ErrorDetected As String = "Erreur rencontrée."
        Public Const ExecutionInterrupted As String = "Exécution interrompue."
        Public Const ScanInterrupted As String = "Le scan a été interrompu." & vbNewLine & " Veuillez retirer votre support !"
        Public Const SavCliNotExists As String = "Le fichier ""sav32cli.exe"" est manquant."
        Public Const ProtectedArchive As String = "Archive protégée par mot de passe."
        Public Const IntegrityCheckWarn As String = "Echec de vérification d'intégrité."
        Public Const Clean As String = "Clean"
        Public Const Infected As String = "Infected"
        Public Const Errors As String = "Errors"
#End Region

#Region " Methods "
        Public Shared Function GetWorkDirUcheck() As String
            If Not Directory.Exists(workDirUcheck) Then Directory.CreateDirectory(workDirUcheck)
            Return workDirUcheck
        End Function

        Public Shared Function GetScanAVFilePath() As String
            Return OS.GetProgramFilesDir() & "\" & "Sophos\Sophos Anti-Virus\sav32cli.exe"
        End Function

        Public Shared Function GetSophosLastUpdate() As String
            Dim path = OS.GetProgramFilesDir() & "\" & "Sophos\Sophos Anti-Virus\"
            Dim FilteredFiles As New SortedDictionary(Of String, String)
            Dim files = Directory.GetFiles(path, "*.ide", SearchOption.TopDirectoryOnly)

            If files.Count <> 0 Then
                For Each f In files
                    FilteredFiles.Add(f, File.GetCreationTime(f).ToString("yyyyMMdd"))
                Next
                Dim last = FilteredFiles.OrderByDescending(Function(f) f.Value).FirstOrDefault
                Return "Mis à jour le " & FormatDate(last.Value)
            End If
            Return " "
        End Function

        Private Shared Function FormatDate(DateStr As String) As String
            Dim InfoDateYear = DateStr.Substring(0, 4)
            Dim InfoDateMonth = DateStr.Substring(4).Remove(2)
            Dim InfoDateDay = DateStr.Substring(6)
            Return InfoDateDay & "/" & InfoDateMonth & "/" & InfoDateYear
        End Function
#End Region

    End Class
End Namespace
