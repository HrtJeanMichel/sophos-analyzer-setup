﻿Imports Sophos_Analyzer.Core.Services

Namespace Launcher.Sophos
    Friend NotInheritable Class Resident

#Region " Methods "
        Public Shared Sub DisableOnScanDemand(Disable As Boolean)
            If Disable Then
                Try
                    Using s1 As New Svc("SAVService")
                        If s1.Installed Then
                            If s1.Running = True Then
                                s1.StopAndDisabled()
                            Else
                                s1.StartupType = "Disabled"
                            End If
                        End If
                    End Using
                Catch ex As InvalidOperationException
                    'MsgBox(ex.ToString)
                End Try
            Else
                Try
                    Using s1 As New Svc("SAVService")
                        If s1.Installed Then
                            If s1.Running = False Then
                                If s1.StartupType = "Disabled" Then
                                    s1.StartupType = "Automatic"
                                End If
                                s1.Start()
                            End If
                        End If
                    End Using
                Catch ex As InvalidOperationException
                End Try
            End If
        End Sub
#End Region

    End Class
End Namespace