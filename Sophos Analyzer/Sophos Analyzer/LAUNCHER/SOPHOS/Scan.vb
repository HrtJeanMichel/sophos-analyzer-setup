﻿Imports System.IO
Imports System.Text
Imports Microsoft.Win32
Imports Sophos_Analyzer.Helper
Imports Sophos_Analyzer.Helper.Cmd
Imports Sophos_Analyzer.Launcher.Gui

Namespace Launcher.Sophos
    Public Class Scan
        Implements IDisposable

#Region " Fields "
        Private m_ScanArgs As String = "-di -nb -nc -all -eec -rec -remove -no-stop-scan -f -tnef -mime -oe -archive -nmbr -nmem -nreg -cleanup -pua"
        'Public Shared ScanArgs As String = "-di -nb -nc -all -eec -rec -remove -cleanup -sc -no-stop-scan -f -tnef -mime -oe -pua -suspicious -archive -nmbr -nmem -nreg"
        Private m_CmdStr As String
        Private m_UcheckLogFilePath$ = String.Empty
        Private m_LogsExists As Boolean
        Private m_CmdErrors As Boolean
        Private m_MalwaresDetected As Boolean
        Private m_ErrorsDetected As Integer
        Private m_FrmMain As FrmLauncher

        Friend CmdSav32Cli As Cmd
        Friend CmdAborted As Boolean
        Friend ScanAborted As Boolean
        Friend VirusDetected As Integer
#End Region

#Region " Delegates "
        Private Delegate Sub UpdateScanDelegate(Tup As Tuple(Of Boolean, Integer))
        Private Delegate Sub UpdateCmdDelegate(Str As String)
#End Region

#Region " Constructor "
        Public Sub New(frmMain As FrmLauncher)
            m_FrmMain = frmMain
        End Sub
#End Region

#Region " Methods "
        ''' <summary>
        ''' Cette procédure lance le scan antivirus.
        ''' </summary>
        ''' <remarks>
        ''' On vérifie que le fichier SavCli.exe existe dans le chemin indiqué.
        ''' On mets à jour l'interface.
        ''' On lance le processus.
        ''' </remarks>
        Public Sub ScanNow()
            If File.Exists(Settings.GetScanAVFilePath) Then
                If CmdSav32Cli Is Nothing Then
                    m_FrmMain.ControlBox = True
                    m_FrmMain.Invoke(New UpdateScanDelegate(AddressOf UpdateScan), Tuple.Create(True, 0))
                    Start()
                End If
            Else
                m_FrmMain.PnlUserInput.Visible = False
                m_FrmMain.LblMain.Visible = True
                m_FrmMain.PbxMain.Image = My.Resources.yellow
                m_FrmMain.LblMain.Text = Settings.SavCliNotExists & vbNewLine & Settings.ContactAdmin
                m_FrmMain.ClearInfos()
            End If
        End Sub

        ''' <summary>
        ''' On lance le scan antivirus.
        ''' </summary>
        Private Sub [Start]()
            VirusDetected = 0
            m_MalwaresDetected = False
            m_ErrorsDetected = 0
            m_CmdStr = String.Empty
            m_UcheckLogFilePath = Settings.GetWorkDirUcheck & "\" & Date.Now.ToString("yyyyMMdd_HH_mm_ss") & "_" & m_FrmMain.TxbUserInput.Text & ".txt"
            CmdSav32Cli = New Cmd(Settings.GetScanAVFilePath(), m_ScanArgs & " " & m_FrmMain.CurrentDisk.PartitionLettersInline)

            AddHandler CmdSav32Cli.Aborted, AddressOf CmdSav32Cli_Aborted
            AddHandler CmdSav32Cli.Exited, AddressOf CmdSav32Cli_Exited
            AddHandler CmdSav32Cli.OutputLine, AddressOf CmdSav32Cli_OutputLine
            CmdSav32Cli.Command = Settings.GetScanAVFilePath()
            CmdSav32Cli.Arguments = m_ScanArgs & " " & m_FrmMain.CurrentDisk.PartitionLettersInline
            CmdSav32Cli.UseComSpec = False
            CmdSav32Cli.Start()

            If Not m_FrmMain.BgwUpdateScan.IsBusy Then m_FrmMain.BgwUpdateScan.RunWorkerAsync()
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque le scan antivirus est interrompu.
        ''' </summary>
        Private Sub CmdSav32Cli_Aborted(sender As Object, e As AbortedEventArgs)
            CmdAborted = True
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque le processus d'analyse est terminé.
        ''' </summary>
        Private Sub CmdSav32Cli_Exited(ender As Object, e As ExitedEventArgs)
            HookTaskbar.ShowTaskbarAndStartButton()
            HookKey.Stop()
            m_FrmMain.Invoke(New UpdateScanDelegate(AddressOf UpdateScan), Tuple.Create(False, e.ExitCode))
            CmdSav32Cli.Close()
            CmdSav32Cli = Nothing
        End Sub

        ''' <summary>
        ''' Cette procédure mets à jour l'interface.
        ''' </summary>
        ''' <remarks>
        ''' Si la fenêtre des fichiers logs est ouverte alors on la ferme.
        ''' On cache la bare des tâches.
        ''' On mets à jour l'interface en incrémentant l'animation de progression.
        ''' </remarks>
        Private Sub UpdateScan(Tup As Tuple(Of Boolean, Integer))
            Try
                If Tup.Item1 Then
                    m_FrmMain.CloseFrmLogs()
                    HookTaskbar.HideTaskbarAndStartButton()
                    HookKey.Start()
                    m_FrmMain.BtnAllLogs.Visible = False
                    m_FrmMain.BtnCurrentLogs.Visible = False
                    m_FrmMain.PnlUserInput.Visible = False
                    m_FrmMain.LblMain.Visible = True
                    m_FrmMain.CurrentDisk.Scanning = True
                    m_FrmMain.LblMain.Text = Settings.ScanInProgress
                ElseIf Tup.Item1 = False Then
                    m_FrmMain.ControlBox = False
                    m_FrmMain.LblMain.Text = Settings.ScanInProgress
                    Dim exitCode = 0
                    If ScanAborted Then
                        exitCode = 2000
                    Else
                        If m_CmdErrors Then
                            exitCode = 36
                            m_CmdErrors = False
                        Else
                            exitCode = If(CmdAborted, -1, Tup.Item2)
                        End If
                    End If
                    m_FrmMain.LblMain.Text = returnStr(exitCode)
                    VirusDetected = 0
                    m_FrmMain.PbxMain.Invalidate()
                    m_FrmMain.CurrentDisk.Scanning = False
                    m_FrmMain.ClearInfos()

                    Select Case exitCode
                        Case -1, 1, 2000
                            m_FrmMain.BtnCurrentLogs.Visible = If(exitCode = 2000, Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt").Count <> 0, False)
                            m_FrmMain.BtnAllLogs.Visible = Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt").Count <> 0
                            Exit Select
                        Case Else
                            WriteLog(exitCode)
                            m_LogsExists = Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt").Count <> 0
                            m_FrmMain.BtnCurrentLogs.Visible = m_LogsExists
                            m_FrmMain.BtnAllLogs.Visible = m_LogsExists
                            Exit Select
                    End Select

                    'Useful life of Sophos Anti-Virus has been exceeded

                    If m_MalwaresDetected Then
                        HookWindow.CloseLibrariesWindow()
                        m_MalwaresDetected = False
                    End If
                    CmdAborted = False
                    ScanAborted = False
                End If
            Catch ex As Exception
                'MsgBox("UpdateScan Error : " & ex.ToString)
            End Try

        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsqu'une ligne est saisie.
        ''' </summary>
        Private Sub CmdSav32Cli_OutputLine(ender As Object, e As OutputLineEventArgs)
            If e.Text.Contains(">>> Virus ") AndAlso e.Text.Contains("found in file HK") Then
            ElseIf e.Text.Contains("Registry value ""HK") Then
                If e.Text.EndsWith("has been cleaned up.") Then
                    Dim Key = ParseBetween(e.Text, "Registry value """, """", "Registry value """.Length)
                    ApplyValue(Key)
                End If
            ElseIf e.Text.Contains("Registry key ""HK") Then
                If e.Text.EndsWith("has been cleaned up.") Then
                    'Dim Key = Utils.ParseBetween(e.Text, "Registry key """, """", "Registry key """.Length)
                End If
            ElseIf e.Text.Contains("Could not check ") AndAlso e.Text.Contains("virus scan failed") Then
                CmdSav32Cli.Abort()
            ElseIf e.Text.ToLower.Contains("could not open ""c:\boot\bcd") Then
                m_CmdErrors = True
                CmdSav32Cli.Abort()
            ElseIf e.Text.Contains("File ""C:\Windows\explorer.exe""") Then
                m_MalwaresDetected = True
            ElseIf e.Text.Contains("viruses were discovered.") OrElse e.Text.Contains("virus was discovered.") Then
            Else
                If e.Text.StartsWith(">>> ") Then
                    If Not m_CmdStr.Contains(e.Text) Then
                        VirusDetected += 1
                    End If
                End If
                If e.Text.StartsWith("Could not check ") Then
                    If Not m_CmdStr.Contains(e.Text) Then
                        m_ErrorsDetected += 1
                    End If
                End If
                m_CmdStr &= e.Text & vbNewLine
                m_FrmMain.Invoke(New UpdateCmdDelegate(AddressOf UpdateCmd), e.Text)
            End If
        End Sub

        Private Sub UpdateCmd(Str As String)
            If VirusDetected > 0 Then
                m_FrmMain.PbxMain.Image = My.Resources.red
                m_FrmMain.PbxMain.Invalidate()
            End If
        End Sub

        Private Function returnStr(ExitedCode As Integer) As String
            Select Case ExitedCode
                Case 0
                    m_FrmMain.PbxMain.Image = My.Resources.green
                    Return Settings.DeviceClean & vbNewLine & Settings.RemoveDevice
                    Exit Select
                Case 3, 20, 24, 28
                    m_FrmMain.PbxMain.Image = My.Resources.red
                    Return VirusDetected.ToString & " " & If(VirusDetected > 1, "menaces détectées.", "menace détectée.") & vbNewLine & Settings.ContactAdmin
                    Exit Select
                Case -1, 1
                    m_FrmMain.PbxMain.Image = My.Resources.yellow
                    Return Settings.DeviceRipped
                    Exit Select
                Case 8
                    m_FrmMain.PbxMain.Image = My.Resources.yellow
                    Return m_ErrorsDetected.ToString & " " & If(m_ErrorsDetected > 1, "erreurs détectées.", "erreur détectée.") & vbNewLine & Settings.ContactAdmin
                    Exit Select
                Case 16
                    m_FrmMain.PbxMain.Image = My.Resources.yellow
                    Return Settings.ProtectedArchive & vbNewLine & Settings.ContactAdmin
                    Exit Select
                Case 32
                    m_FrmMain.PbxMain.Image = My.Resources.yellow
                    Return Settings.IntegrityCheckWarn & vbNewLine & Settings.ContactAdmin
                    Exit Select
                Case 2, 36
                    m_FrmMain.PbxMain.Image = My.Resources.yellow
                    Return Settings.ErrorDetected & vbNewLine & Settings.ContactAdmin
                    Exit Select
                Case 40
                    m_FrmMain.PbxMain.Image = My.Resources.yellow
                    Return Settings.ExecutionInterrupted & vbNewLine & Settings.ContactAdmin
                    Exit Select
                Case 2000
                    m_FrmMain.PbxMain.Image = My.Resources.yellow
                    Return Settings.ScanInterrupted
                    Exit Select
            End Select

            m_FrmMain.PbxMain.Image = My.Resources.yellow
            Return Settings.WarningSavCli & vbNewLine & Settings.ContactAdmin
        End Function

        Private Sub ApplyValue(key As String)
            If key.StartsWith("HKU") Then
                Dim subKey As String = key.Replace("HKU\", String.Empty)
                Dim keySplited() = subKey.Split("\")
                Dim value = keySplited(keySplited.Length - 1)
                subKey = subKey.Replace(keySplited(0) & "\", String.Empty).Replace("\" & value, String.Empty)
                Try
                    Registry.CurrentUser.CreateSubKey(subKey).SetValue(value, 1, RegistryValueKind.DWord)
                Catch ex As Exception
                End Try
                'ElseIf key.StartsWith("HKLM") Then
                '    Dim subKey As String = key.Replace("HKLM\", String.Empty)
                '    Dim keySplited() = subKey.Split("\")
                '    Dim value = keySplited(keySplited.Length - 1)
                '    subKey = subKey.Replace("\" & value, String.Empty)

                '    Select Case value
                '        Case "ConsentPromptBehaviorAdmin", "ConsentPromptBehaviorUser"
                '            Try
                '                Registry.LocalMachine.CreateSubKey(subKey).SetValue(value, 0, RegistryValueKind.DWord)
                '            Catch ex As Exception
                '            End Try
                '    End Select
            End If
        End Sub

        Private Sub WriteLog(ExitCode As Integer)
            Dim stb As New StringBuilder
            stb.AppendLine("MODELE : " & m_FrmMain.CurrentDisk.Model)
            stb.AppendLine("CAPACITE : " & m_FrmMain.CurrentDisk.SizeTotalSpace(True))
            stb.AppendLine("PARTITIONS : " & m_FrmMain.CurrentDisk.Partitions.Count.ToString)
            stb.AppendLine("NUMERO DE SERIE : " & m_FrmMain.CurrentDisk.SerialNumber)
            Dim result As String = String.Empty
            Select Case ExitCode
                Case 0
                    result = Settings.Clean
                Case 3, 20, 24, 28
                    result = Settings.Infected
                Case 2, 8, 16, 32, 36, 40
                    result = Settings.Errors
            End Select
            stb.AppendLine("RESULTAT : " & result)
            File.WriteAllText(m_UcheckLogFilePath, stb.ToString & vbNewLine & m_CmdStr)
        End Sub

        ''' <summary>
        ''' On retourne une chaine contenu entre deux balises ou chaines définies en argument.
        ''' </summary>
        Private Function ParseBetween(Source$, Before$, After$, Offset%) As String
            If String.IsNullOrEmpty(Source) Then
                Return String.Empty
            End If
            If Source.Contains(Before) = True Then
                Dim Result$ = Source.Substring(Source.IndexOf(Before) + Offset)
                If Result.Contains(After) = True Then
                    If Not String.IsNullOrEmpty(After) Then
                        Result = Result.Substring(0, Result.IndexOf(After))
                    End If
                End If
                Return Result
            Else
                Return String.Empty
            End If
        End Function

#Region "IDisposable Support"
        Private disposedValue As Boolean

        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    If Not CmdSav32Cli Is Nothing Then
                        CmdSav32Cli.Close()
                    End If
                End If
                m_CmdStr = String.Empty
                m_UcheckLogFilePath$ = String.Empty
                m_LogsExists = False
                m_CmdErrors = False
                m_MalwaresDetected = False
                m_ErrorsDetected = 0
            End If
            disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
        End Sub
#End Region

#End Region

    End Class
End Namespace
