﻿Imports System.DirectoryServices.AccountManagement
Imports Sophos_Analyzer.Helper

Namespace Launcher.Sophos
    Friend NotInheritable Class Prerequisites

#Region " Methods "
        Public Shared Function SophosAutonomeExists() As Boolean
            Dim userAutonome As Boolean = False
            Dim userAdmin As Boolean = False
            Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                Using user As New UserPrincipal(ctx)
                    user.Name = "*"
                    Using ps As New PrincipalSearcher()
                        ps.QueryFilter = user
                        Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll()
                        For Each p As Principal In result
                            If p.Name.ToLower = "sophos_autonome" Then
                                userAdmin = p.GetGroups.Any(Function(f) f.Name.ToLower = If(OS.GetInstalledUICulture = 1036, "administrateurs", "administrators"))
                                userAutonome = p.GetGroups.Any(Function(f) f.Name.ToLower = "sophosadministrator")
                            End If
                        Next
                    End Using
                End Using
            End Using
            Return userAutonome And userAdmin
        End Function

        Public Shared Function SophosAutoUpdateExists() As Boolean
            Dim UserUpdate As Boolean = False
            Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                Using user As New UserPrincipal(ctx)
                    user.Name = "*"
                    Using ps As New PrincipalSearcher()
                        ps.QueryFilter = user
                        Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll()
                        For Each p As Principal In result
                            If p.Name.StartsWith("SophosSAU") Then
                                UserUpdate = (p.GetGroups.Count = 0)
                            End If
                        Next
                    End Using
                End Using
            End Using
            Return UserUpdate
        End Function

        Public Shared Function SophosInstalledFromDirectories() As Boolean
            Dim SophosUpdate = IO.File.Exists(OS.GetProgramFilesDir & "\Sophos\AutoUpdate\ALMon.exe")
            Dim SophosAV = IO.File.Exists(OS.GetProgramFilesDir & "\Sophos\Sophos Anti-Virus\VirusDetection.dll")
            Return If(SophosUpdate And SophosAV, True, False)
        End Function
#End Region

    End Class
End Namespace
