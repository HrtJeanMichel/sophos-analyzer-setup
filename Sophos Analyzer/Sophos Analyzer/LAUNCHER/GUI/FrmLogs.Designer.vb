﻿Imports Sophos_Analyzer.Extensions

Namespace Launcher.Gui
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class FrmLogs
        Inherits System.Windows.Forms.Form

        'Form remplace la méthode Dispose pour nettoyer la liste des composants.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requise par le Concepteur Windows Form
        Private components As System.ComponentModel.IContainer

        'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
        'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
        'Ne la modifiez pas à l'aide de l'éditeur de code.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLogs))
            Me.SpcLogs = New System.Windows.Forms.SplitContainer()
            Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
            Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
            Me.TsBtnExportLogs = New System.Windows.Forms.ToolStripButton()
            Me.TxbLogs = New System.Windows.Forms.TextBox()
            Me.BgwLoadLogs = New System.ComponentModel.BackgroundWorker()
            Me.LvLogs = New ListViewEx()
            Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
            Me.SpcLogs.Panel1.SuspendLayout()
            Me.SpcLogs.Panel2.SuspendLayout()
            Me.SpcLogs.SuspendLayout()
            Me.SplitContainer1.Panel1.SuspendLayout()
            Me.SplitContainer1.Panel2.SuspendLayout()
            Me.SplitContainer1.SuspendLayout()
            Me.ToolStrip1.SuspendLayout()
            Me.SuspendLayout()
            '
            'SpcLogs
            '
            Me.SpcLogs.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SpcLogs.Location = New System.Drawing.Point(0, 0)
            Me.SpcLogs.Name = "SpcLogs"
            Me.SpcLogs.Orientation = System.Windows.Forms.Orientation.Horizontal
            '
            'SpcLogs.Panel1
            '
            Me.SpcLogs.Panel1.Controls.Add(Me.SplitContainer1)
            '
            'SpcLogs.Panel2
            '
            Me.SpcLogs.Panel2.Controls.Add(Me.TxbLogs)
            Me.SpcLogs.Size = New System.Drawing.Size(841, 497)
            Me.SpcLogs.SplitterDistance = 353
            Me.SpcLogs.TabIndex = 0
            '
            'SplitContainer1
            '
            Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
            Me.SplitContainer1.IsSplitterFixed = True
            Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
            Me.SplitContainer1.Name = "SplitContainer1"
            Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
            '
            'SplitContainer1.Panel1
            '
            Me.SplitContainer1.Panel1.Controls.Add(Me.ToolStrip1)
            '
            'SplitContainer1.Panel2
            '
            Me.SplitContainer1.Panel2.Controls.Add(Me.LvLogs)
            Me.SplitContainer1.Size = New System.Drawing.Size(841, 353)
            Me.SplitContainer1.SplitterDistance = 25
            Me.SplitContainer1.TabIndex = 4
            '
            'ToolStrip1
            '
            Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
            Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsBtnExportLogs})
            Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
            Me.ToolStrip1.Name = "ToolStrip1"
            Me.ToolStrip1.Size = New System.Drawing.Size(841, 25)
            Me.ToolStrip1.TabIndex = 0
            '
            'TsBtnExportLogs
            '
            Me.TsBtnExportLogs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
            Me.TsBtnExportLogs.Enabled = False
            Me.TsBtnExportLogs.Image = CType(resources.GetObject("TsBtnExportLogs.Image"), System.Drawing.Image)
            Me.TsBtnExportLogs.ImageTransparentColor = System.Drawing.Color.Magenta
            Me.TsBtnExportLogs.Name = "TsBtnExportLogs"
            Me.TsBtnExportLogs.Size = New System.Drawing.Size(187, 22)
            Me.TsBtnExportLogs.Text = "Exporter tous les fichiers journaux"
            '
            'TxbLogs
            '
            Me.TxbLogs.BackColor = System.Drawing.Color.Black
            Me.TxbLogs.Dock = System.Windows.Forms.DockStyle.Fill
            Me.TxbLogs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TxbLogs.ForeColor = System.Drawing.Color.White
            Me.TxbLogs.Location = New System.Drawing.Point(0, 0)
            Me.TxbLogs.Multiline = True
            Me.TxbLogs.Name = "TxbLogs"
            Me.TxbLogs.ReadOnly = True
            Me.TxbLogs.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.TxbLogs.Size = New System.Drawing.Size(841, 140)
            Me.TxbLogs.TabIndex = 0
            '
            'BgwLoadLogs
            '
            Me.BgwLoadLogs.WorkerReportsProgress = True
            Me.BgwLoadLogs.WorkerSupportsCancellation = True
            '
            'LvLogs
            '
            Me.LvLogs.AllowColumnReorder = True
            Me.LvLogs.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader5, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader6})
            Me.LvLogs.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LvLogs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LvLogs.ForeColor = System.Drawing.SystemColors.WindowText
            Me.LvLogs.FullRowSelect = True
            Me.LvLogs.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
            Me.LvLogs.Location = New System.Drawing.Point(0, 0)
            Me.LvLogs.MultiSelect = False
            Me.LvLogs.Name = "LvLogs"
            Me.LvLogs.Size = New System.Drawing.Size(841, 324)
            Me.LvLogs.Sorting = System.Windows.Forms.SortOrder.Ascending
            Me.LvLogs.TabIndex = 4
            Me.LvLogs.UseCompatibleStateImageBehavior = False
            Me.LvLogs.View = System.Windows.Forms.View.Details
            '
            'ColumnHeader1
            '
            Me.ColumnHeader1.Text = "Heure"
            Me.ColumnHeader1.Width = 69
            '
            'ColumnHeader5
            '
            Me.ColumnHeader5.Text = "Utilisateur"
            Me.ColumnHeader5.Width = 169
            '
            'ColumnHeader2
            '
            Me.ColumnHeader2.Text = "Modèle"
            Me.ColumnHeader2.Width = 272
            '
            'ColumnHeader3
            '
            Me.ColumnHeader3.Text = "Capacité"
            Me.ColumnHeader3.Width = 67
            '
            'ColumnHeader4
            '
            Me.ColumnHeader4.Text = "N° de série"
            Me.ColumnHeader4.Width = 174
            '
            'ColumnHeader6
            '
            Me.ColumnHeader6.Text = "Résultat"
            Me.ColumnHeader6.Width = 79
            '
            'FrmLogs
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(841, 497)
            Me.Controls.Add(Me.SpcLogs)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Name = "FrmLogs"
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Fichiers Log"
            Me.SpcLogs.Panel1.ResumeLayout(False)
            Me.SpcLogs.Panel2.ResumeLayout(False)
            Me.SpcLogs.Panel2.PerformLayout()
            Me.SpcLogs.ResumeLayout(False)
            Me.SplitContainer1.Panel1.ResumeLayout(False)
            Me.SplitContainer1.Panel1.PerformLayout()
            Me.SplitContainer1.Panel2.ResumeLayout(False)
            Me.SplitContainer1.ResumeLayout(False)
            Me.ToolStrip1.ResumeLayout(False)
            Me.ToolStrip1.PerformLayout()
            Me.ResumeLayout(False)

        End Sub

        Friend WithEvents SpcLogs As SplitContainer
        Friend WithEvents TxbLogs As TextBox
        Friend WithEvents SplitContainer1 As SplitContainer
        Friend WithEvents ToolStrip1 As ToolStrip
        Friend WithEvents TsBtnExportLogs As ToolStripButton
        Friend WithEvents LvLogs As ListViewEx
        Friend WithEvents ColumnHeader1 As ColumnHeader
        Friend WithEvents ColumnHeader5 As ColumnHeader
        Friend WithEvents ColumnHeader2 As ColumnHeader
        Friend WithEvents ColumnHeader3 As ColumnHeader
        Friend WithEvents ColumnHeader4 As ColumnHeader
        Friend WithEvents ColumnHeader6 As ColumnHeader
        Friend WithEvents BgwLoadLogs As System.ComponentModel.BackgroundWorker
    End Class
End Namespace
