﻿Imports System.IO
Imports System.ComponentModel
Imports System.Threading
Imports Microsoft.Win32
Imports Sophos_Analyzer.Core.Usb.ChangedEventArgs
Imports Sophos_Analyzer.Helper
Imports Sophos_Analyzer.Core.Usb
Imports Sophos_Analyzer.Win32
Imports Sophos_Analyzer.Extensions
Imports Sophos_Analyzer.Core.Usb.Detector
Imports Sophos_Analyzer.Launcher.Sophos
Imports Sophos_Analyzer.Helper.Cmd

Namespace Launcher.Gui
    Public Class FrmLauncher
        Inherits FrmGlass

#Region " Fields "
        Private m_DriveID As String = String.Empty
        Private m_DeviceDetect As Detector
        Private m_FrmLogs As FrmLogs
        Private m_Scan As Scan

        Friend CurrentDisk As DiskCurrent
#End Region

#Region " Delegates "
        Private Delegate Sub UpdateUIDelegate(stateType As State)
        Private Delegate Sub UpdateFailedUIDelegate(stateType As State)
        Private Delegate Sub UpdateDriveCleanupUIDelegate(Text As String)
#End Region

#Region " Constructor "
        Public Sub New()
            InitializeComponent()
            Unmovable = True
            LblMain.Text = "Veuillez patientez ..."
            Text = Settings.GetSophosLastUpdate()
            AddHandler SystemEvents.SessionEnding, New SessionEndingEventHandler(AddressOf SystemEvents_SessionEnding)
            CurrentDisk = New DiskCurrent
            m_DeviceDetect = New Detector
            m_Scan = New Scan(Me)
        End Sub
#End Region

#Region " Methods "
        ''' <summary>
        ''' Cet évènement se déclenche lorsque le programme s'ouvre.
        ''' </summary>
        ''' <remarks>
        ''' On vérifie que l'OS est Dwm Api compatible puis on applique si possible l'effet de transparence Aero.
        ''' </remarks>
        Private Sub FrmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            If IsAeroGlassStyleSupported() Then
                PbxMain.BackColor = TransparencyColor
                PbxAv.BackColor = TransparencyColor
                ExtendFrameEnabled = True
                BlurBehindWindowEnabled = False
            End If
            BtnAllLogs.Visible = Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt").Count <> 0
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque la fenêtre du programme est affichée.
        ''' </summary>
        ''' <remarks>
        ''' On s'abonne à l'évènement de détection/retrait de périphérique USB.
        ''' On mets à jour le message.
        ''' </remarks>
        Private Sub FrmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
            HookTaskbar.HideTaskbarDesktopButton()
            HookSearch.CloseStartMenuSearch()
            HookKey.Start()
            AddHandler m_DeviceDetect.DeviceInserted, New DeviceInsertedHandler(AddressOf EventArrived)
            LblMain.Text = Settings.InsertDevice
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque le programme est en cours de fermeture.
        ''' </summary>
        ''' <remarks>
        ''' On empêche bien évidement la fermeture.
        ''' Si le scan est en cours alors on l'interrompt.
        ''' </remarks>
        Private Sub FrmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
            If CurrentDisk.Scanning Then
                e.Cancel = True
                If m_Scan.CmdSav32Cli IsNot Nothing Then
                    m_Scan.ScanAborted = True
                    m_Scan.CmdSav32Cli.Abort()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque la session se ferme.
        ''' </summary>
        ''' <remarks>
        ''' On arrête le service SAVService, on modifie les paramètres de l'agent résident pour ne pas qu'il soit en conflit avec SAVCli.exe (outil en ligne de commande Sophos), on redémarre le service SAVService.
        ''' </remarks>
        Private Sub SystemEvents_SessionEnding(sender As Object, e As SessionEndingEventArgs)
            e.Cancel = True
            Resident.DisableOnScanDemand(False)
            e.Cancel = False
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsqu'un périphérique USB est inséré ou retiré.
        ''' </summary>
        ''' <remarks>
        ''' On vérifie que le périphérique inséré corresponds à celui qui fera l'objet d'un retrait et on mets à jour les messages de l'interface.
        ''' On vérifie également que le pilote de périphérique est bien installé sinon on supprime les traces d'installation résiduelles !
        ''' </remarks>
        Private Sub EventArrived(sender As Object, e As ChangedEventArgs)
            Select Case e.DeviceState
                Case State.Added
                    If e.Fail AndAlso CurrentDisk.Scanning = False Then
                        Me.Invoke(New UpdateFailedUIDelegate(AddressOf UpdateFailedUI), e.DeviceState)
                    Else
                        If m_DriveID = "" Then
                            If CurrentDisk.Plugged = False Then
                                CurrentDisk.Plugged = True
                                CurrentDisk.Partitions = e.Disk.Partitions
                                CurrentDisk.Model = e.Disk.Model
                                CurrentDisk.SerialNumber = e.Disk.SerialNumber
                                CurrentDisk.Size = e.Disk.Size
                                m_DriveID = CurrentDisk.Model & CurrentDisk.SerialNumber
                                Me.Invoke(If(CurrentDisk.HasZeroByte, New UpdateFailedUIDelegate(AddressOf UpdateFailedUI), New UpdateUIDelegate(AddressOf UpdateUI)), e.DeviceState)
                            End If
                        End If
                    End If
                Case State.Removed
                    If e.Fail AndAlso CurrentDisk.Scanning = False Then
                        Me.Invoke(New UpdateFailedUIDelegate(AddressOf UpdateFailedUI), e.DeviceState)
                    Else
                        If CurrentDisk.Scanning Then m_Scan.CmdAborted = True
                        If CurrentDisk.Plugged = True AndAlso m_DriveID = e.Disk.Model & e.Disk.SerialNumber Then
                            CurrentDisk.Plugged = False
                            Me.Invoke(If(CurrentDisk.HasZeroByte, New UpdateFailedUIDelegate(AddressOf UpdateFailedUI), New UpdateUIDelegate(AddressOf UpdateUI)), e.DeviceState)
                        End If
                    End If
            End Select
        End Sub

        ''' <summary>
        ''' Cette procédure se déclenche lorsqu'un périphérique USB est inséré ou retiré.
        ''' </summary>
        ''' <remarks>
        ''' Si la fenêtre des fichiers logs est ouverte alors on la ferme.
        ''' On mets à jour les messages de l'interface.
        ''' </remarks>
        Private Sub UpdateUI(StateType As State)
            CloseFrmLogs()
            Select Case StateType
                Case State.Added
                    PnlUserInput.Visible = True
                    If LblMain.Text = Settings.DeviceRipped Then
                        LblMain.Visible = False
                        ClearInfos()
                        BtnCurrentLogs.Visible = Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt").Count <> 0
                        PbxMain.Image = My.Resources.green
                    ElseIf LblMain.Text = Settings.InsertDevice Then
                        BtnCurrentLogs.Visible = Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt").Count <> 0
                    ElseIf Me.LblMain.Text = Settings.CleanupDevices Then
                        Exit Sub
                    Else
                        BtnCurrentLogs.Visible = False
                    End If

                    TxbUserInput.Focus()
                    ActiveControl = TxbUserInput

                    Me.Text = If(CurrentDisk.Partitions.Count > 1, "Lecteurs", "Lecteur") & " : " & CurrentDisk.PartitionLettersInline &
                    " | Nom : " & CurrentDisk.Model & " | Capacité : " & CurrentDisk.SizeTotalSpace(True) &
                    " | Espace libre : " & CurrentDisk.SizeFreeSpace &
                    " | s/n : " & CurrentDisk.SerialNumber
                Case State.Removed
                    m_Scan.VirusDetected = 0
                    PbxMain.Invalidate()
                    If LblMain.Visible = True Then
                        Select Case LblMain.Text
                            Case Settings.RemoveDevice,
                             Settings.DeviceClean & vbNewLine & Settings.RemoveDevice,
                             Settings.ProtectedArchive & vbNewLine & Settings.ContactAdmin,
                             Settings.ExecutionInterrupted & vbNewLine & Settings.ContactAdmin,
                             Settings.IntegrityCheckWarn & vbNewLine & Settings.ContactAdmin,
                             Settings.ErrorDetected & vbNewLine & Settings.ContactAdmin,
                             Settings.SavCliNotExists & vbNewLine & Settings.ContactAdmin,
                             Settings.WarningSavCli & vbNewLine & Settings.ContactAdmin,
                             Settings.ScanInterrupted
                                InsertDevice()
                                Exit Select
                            Case Else
                                If LblMain.Text.Contains(Settings.ScanInProgress) AndAlso CurrentDisk.Scanning Then
                                    LblMain.Text = Settings.DeviceRipped
                                    BtnCurrentLogs.Visible = False
                                    If Not m_Scan.CmdSav32Cli Is Nothing Then m_Scan.CmdSav32Cli.Abort()
                                ElseIf Not LblMain.Text.Contains(Settings.DeviceClean) AndAlso (LblMain.Text.Contains("menace détectée.") OrElse LblMain.Text.Contains("menaces détectées.")) Then
                                    InsertDevice()
                                ElseIf LblMain.Text.Contains("erreur détectée") OrElse LblMain.Text.Contains("erreurs détectées.") Then
                                    InsertDevice()
                                End If
                                Exit Select
                        End Select
                        m_DriveID = ""
                        PnlUserInput.Visible = False
                    Else
                        CurrentDisk.Grabbed = True
                        PnlUserInput.Visible = False
                        LblMain.Visible = True
                        m_DriveID = ""
                        InsertDevice()
                    End If
                    ClearInfos()
                    Me.Text = Settings.GetSophosLastUpdate()
            End Select
        End Sub

        ''' <summary>
        ''' Cette procédure se déclenche lorsqu'un pilote de périphérique USB n'a pas été correctement installé.
        ''' </summary>
        ''' <remarks>
        ''' On supprime l'installation résiduelle des pilotes de périphériques.
        ''' </remarks>
        Private Sub UpdateFailedUI(stateType As State)
            Select Case stateType
                Case State.Added
                    Me.LblMain.Text = Settings.RemoveDevice
                Case State.Removed
                    Me.LblMain.Text = Settings.CleanupDevices
                    If Not File.Exists(Settings.driveCleanup) Then
                        File.WriteAllBytes(Settings.driveCleanup, If(OS.is64Bits, My.Resources.DriveCleanup64, My.Resources.DriveCleanup86))
                    End If
                    Dim DriveCleanup = New Cmd(Settings.driveCleanup)
                    AddHandler DriveCleanup.Exited, AddressOf DriveCleanup_Exited
                    DriveCleanup.Command = Settings.driveCleanup
                    DriveCleanup.UseComSpec = False
                    DriveCleanup.Start()
            End Select
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque les pilotes de périphérique USB résiduels ont été effacés.
        ''' </summary>
        ''' <remarks>
        ''' On mets à jour les messages de l'interface.
        ''' </remarks>
        Private Sub DriveCleanup_Exited(sender As Object, e As ExitedEventArgs)
            Dim dcSender As Cmd = TryCast(sender, Cmd)
            dcSender.Close()
            dcSender = Nothing
            Me.Invoke(New UpdateDriveCleanupUIDelegate(AddressOf UpdateDriveCleanupUI), Settings.InsertDevice)
        End Sub

        Private Sub UpdateDriveCleanupUI(text As String)
            LblMain.Text = text
            CurrentDisk.CleanupProperties()
            m_DriveID = String.Empty
        End Sub

        Friend Sub CloseFrmLogs()
            If Not m_FrmLogs Is Nothing Then
                m_FrmLogs.Close()
                m_FrmLogs.Dispose()
            End If
        End Sub

        Private Sub InsertDevice()
            LblMain.Text = Settings.InsertDevice
            BtnCurrentLogs.Visible = False
            PbxMain.Image = My.Resources.green
            CurrentDisk.Plugged = False
            m_DriveID = String.Empty
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsqu'un caractère non autorisé a été saisi dans le champs texte (Grade/Nom).
        ''' </summary>
        ''' <remarks>
        ''' On passe la propriété Enabled du bouton de validation à True ou False.
        ''' </remarks>
        Private Sub TxbUserInput_CharsUnauthorized(sender As Object, e As TextBoxExEventArgs) Handles TxbUserInput.CharsUnauthorized
            BtnOkUserInput.Enabled = e.CharsAllowed
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque la propriété Visible du panel de saisi d'information utilisateur (Grade/Nom) passe à True ou False.
        ''' </summary>
        ''' <remarks>
        ''' On affiche/Cache le Panel information
        ''' On applique l'effet de transparence selon le système d'exploitation. 
        ''' </remarks>
        Private Sub PnlUserInput_VisibleChanged(sender As Object, e As EventArgs) Handles PnlUserInput.VisibleChanged
            Dim pnlVisible As Boolean = PnlUserInput.Visible
            LblMain.Visible = Not pnlVisible
            If IsAeroGlassStyleSupported() Then
                GlassMargins = If(pnlVisible = True, New NativeStruct.MARGINS(259, 259, 26, 26), New NativeStruct.MARGINS(150, 150, 9, 9))
                Invalidate()
            End If
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque l'utilisateur presse la touche entrée après avoir correctement renseigné le champs texte (Grade/Nom).
        ''' </summary>
        ''' <remarks>
        ''' On lance l'analyse antivirus
        ''' </remarks>
        Private Sub TxbUserInput_KeyDown(sender As Object, e As KeyEventArgs) Handles TxbUserInput.KeyDown
            If (e.KeyCode = Keys.Enter) AndAlso BtnOkUserInput.Enabled = True Then
                m_Scan.ScanNow()
            End If
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lors du clique sur le bouton de validation de remplissage du champs texte (Grade/Nom).
        ''' </summary>
        ''' <remarks>
        ''' On lance l'analyse antivirus
        ''' </remarks>
        Private Sub BtnOkUserInput_Click(sender As Object, e As EventArgs) Handles BtnOkUserInput.Click
            m_Scan.ScanNow()
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lors du clique sur le bouton d'annulation.
        ''' </summary>
        ''' <remarks>
        ''' On mets à jour l'interface.
        ''' </remarks>
        Private Sub BtnCancelUserInput_Click(sender As Object, e As EventArgs) Handles BtnCancelUserInput.Click
            Me.Text = Settings.GetSophosLastUpdate
            LblMain.Visible = True
            PnlUserInput.Visible = False
            CurrentDisk.Grabbed = False
            LblMain.Text = Settings.RemoveDevice
        End Sub

        ''' <summary>
        ''' On efface le champs texte (Grade/Nom).
        ''' </summary>
        Friend Sub ClearInfos()
            TxbUserInput.Text = String.Empty
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque l'utilisateur affiche la fenêtre des fichiers journaux.
        ''' </summary>
        Private Sub BtnLogs_Click(sender As Object, e As EventArgs) Handles BtnAllLogs.Click, BtnCurrentLogs.Click
            Dim btn As Button = TryCast(sender, Button)
            m_FrmLogs = New FrmLogs(CurrentDisk, btn.Name)
            m_FrmLogs.ShowDialog()
            TxbUserInput.Focus()
            ActiveControl = TxbUserInput
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque la méthode Invalidate de la PictureBox est appelée.
        ''' Si le nombre de détection est supérieur à 0 alors on écrit sur la zone de dessin.
        ''' </summary>
        Private Sub PbxMain_Paint(sender As Object, e As PaintEventArgs) Handles PbxMain.Paint
            If m_Scan.VirusDetected > 0 Then
                e.Graphics.DrawString(m_Scan.VirusDetected.ToString, New Font("Segoe UI", 18, FontStyle.Regular), Brushes.White, 0, 82)
            End If
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque l'état visble du bouton des fichiers logs est modifié.
        ''' Si l'état passe à True alors on mets à jours sa propriété Text et on y écrit le nombre de fichiers journaux.
        ''' </summary>
        Private Sub BtnAllLogs_VisibleChanged(sender As Object, e As EventArgs) Handles BtnAllLogs.VisibleChanged
            If BtnAllLogs.Visible = True Then
                BtnAllLogs.Text = "Tous les Logs (" & Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt").Count.ToString & ")"
            End If
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque l'analyse est lancée.
        ''' Ca permet d'afficher une animation sur l'image Sophos de la fenêtre principale, la lettre "S" passe de la couleur rouge à la couleur blanche jusqu'à ce que le scan soit terminé ou annulé.
        ''' </summary>
        Private Sub BgwUpdateScan_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwUpdateScan.DoWork
            Dim b As Boolean
            Do While m_Scan.CmdSav32Cli.IsRunning
                BgwUpdateScan.ReportProgress(0, b)
                Thread.Sleep(300)
                b = Not b
            Loop
            e.Result = 100
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche pour mettre à jour l'interface de l'animation lors du scan en dehors du thread appelant.
        ''' </summary>
        Private Sub BgwUpdateScan_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwUpdateScan.ProgressChanged
            PbxAv.Image = If(CType(e.UserState, Boolean) = True, My.Resources.UCheckSophosRed, My.Resources.UCheckSophos)
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque le scan est terminé ou annulé.
        ''' L'image de l'animation revient à son état d'origine
        ''' </summary>
        Private Sub BgwUpdateScan_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwUpdateScan.RunWorkerCompleted
            PbxAv.Image = My.Resources.UCheckSophos
        End Sub
#End Region

    End Class
End Namespace