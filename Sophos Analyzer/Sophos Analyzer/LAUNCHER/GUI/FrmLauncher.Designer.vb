﻿Imports Sophos_Analyzer.Extensions

Namespace Launcher.Gui
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class FrmLauncher
        Inherits FrmGlass
        'Inherits System.Windows.Forms.Form

        'Form remplace la méthode Dispose pour nettoyer la liste des composants.
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requise par le Concepteur Windows Form
        Private components As System.ComponentModel.IContainer

        'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
        'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
        'Ne la modifiez pas à l'aide de l'éditeur de code.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLauncher))
            Me.LblMain = New System.Windows.Forms.Label()
            Me.PnlUserInput = New System.Windows.Forms.Panel()
            Me.TxbUserInput = New TextBoxEx()
            Me.LblUserInput = New System.Windows.Forms.Label()
            Me.TblPnlUserInput = New System.Windows.Forms.TableLayoutPanel()
            Me.BtnOkUserInput = New System.Windows.Forms.Button()
            Me.BtnCancelUserInput = New System.Windows.Forms.Button()
            Me.BtnAllLogs = New System.Windows.Forms.Button()
            Me.BtnCurrentLogs = New System.Windows.Forms.Button()
            Me.PbxAv = New System.Windows.Forms.PictureBox()
            Me.PbxMain = New System.Windows.Forms.PictureBox()
            Me.BgwUpdateScan = New System.ComponentModel.BackgroundWorker()
            Me.PnlUserInput.SuspendLayout()
            Me.TblPnlUserInput.SuspendLayout()
            CType(Me.PbxAv, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PbxMain, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LblMain
            '
            Me.LblMain.Font = New System.Drawing.Font("Calibri", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LblMain.Location = New System.Drawing.Point(150, 9)
            Me.LblMain.Name = "LblMain"
            Me.LblMain.Size = New System.Drawing.Size(560, 168)
            Me.LblMain.TabIndex = 1
            Me.LblMain.Text = "Veuillez insérer votre périphérique USB"
            Me.LblMain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'PnlUserInput
            '
            Me.PnlUserInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.PnlUserInput.Controls.Add(Me.TxbUserInput)
            Me.PnlUserInput.Controls.Add(Me.LblUserInput)
            Me.PnlUserInput.Controls.Add(Me.TblPnlUserInput)
            Me.PnlUserInput.Location = New System.Drawing.Point(258, 25)
            Me.PnlUserInput.Name = "PnlUserInput"
            Me.PnlUserInput.Size = New System.Drawing.Size(344, 137)
            Me.PnlUserInput.TabIndex = 3
            Me.PnlUserInput.Visible = False
            '
            'TxbUserInput
            '
            Me.TxbUserInput.Location = New System.Drawing.Point(23, 62)
            Me.TxbUserInput.Name = "TxbUserInput"
            Me.TxbUserInput.Size = New System.Drawing.Size(281, 20)
            Me.TxbUserInput.TabIndex = 2
            Me.TxbUserInput.UseRegularExpression = True
            Me.TxbUserInput.UseRegularExpressionErrorMessage = "Caractères spéciaux interdits ! (1 à 50 caractères)"
            Me.TxbUserInput.UseRegularExpressionPattern = "^[a-zA-Z0-9ëË\s]{1,50}$"
            '
            'LblUserInput
            '
            Me.LblUserInput.Font = New System.Drawing.Font("Calibri", 13.0!, System.Drawing.FontStyle.Bold)
            Me.LblUserInput.Location = New System.Drawing.Point(14, 24)
            Me.LblUserInput.Name = "LblUserInput"
            Me.LblUserInput.Size = New System.Drawing.Size(315, 29)
            Me.LblUserInput.TabIndex = 4
            Me.LblUserInput.Text = "Veuillez saisir votre grade et votre nom :"
            '
            'TblPnlUserInput
            '
            Me.TblPnlUserInput.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TblPnlUserInput.ColumnCount = 2
            Me.TblPnlUserInput.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.TblPnlUserInput.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.TblPnlUserInput.Controls.Add(Me.BtnOkUserInput, 0, 0)
            Me.TblPnlUserInput.Controls.Add(Me.BtnCancelUserInput, 1, 0)
            Me.TblPnlUserInput.Location = New System.Drawing.Point(174, 95)
            Me.TblPnlUserInput.Name = "TblPnlUserInput"
            Me.TblPnlUserInput.RowCount = 1
            Me.TblPnlUserInput.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.TblPnlUserInput.Size = New System.Drawing.Size(146, 29)
            Me.TblPnlUserInput.TabIndex = 3
            '
            'BtnOkUserInput
            '
            Me.BtnOkUserInput.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.BtnOkUserInput.Enabled = False
            Me.BtnOkUserInput.Location = New System.Drawing.Point(3, 3)
            Me.BtnOkUserInput.Name = "BtnOkUserInput"
            Me.BtnOkUserInput.Size = New System.Drawing.Size(67, 23)
            Me.BtnOkUserInput.TabIndex = 1
            Me.BtnOkUserInput.Text = "OK"
            '
            'BtnCancelUserInput
            '
            Me.BtnCancelUserInput.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.BtnCancelUserInput.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.BtnCancelUserInput.Location = New System.Drawing.Point(76, 3)
            Me.BtnCancelUserInput.Name = "BtnCancelUserInput"
            Me.BtnCancelUserInput.Size = New System.Drawing.Size(67, 23)
            Me.BtnCancelUserInput.TabIndex = 2
            Me.BtnCancelUserInput.Text = "Annuler"
            '
            'BtnAllLogs
            '
            Me.BtnAllLogs.Location = New System.Drawing.Point(731, 146)
            Me.BtnAllLogs.Name = "BtnAllLogs"
            Me.BtnAllLogs.Size = New System.Drawing.Size(108, 23)
            Me.BtnAllLogs.TabIndex = 4
            Me.BtnAllLogs.Text = "Tous les Logs"
            Me.BtnAllLogs.UseVisualStyleBackColor = True
            '
            'BtnCurrentLogs
            '
            Me.BtnCurrentLogs.Location = New System.Drawing.Point(21, 146)
            Me.BtnCurrentLogs.Name = "BtnCurrentLogs"
            Me.BtnCurrentLogs.Size = New System.Drawing.Size(108, 23)
            Me.BtnCurrentLogs.TabIndex = 5
            Me.BtnCurrentLogs.Text = "Logs du support"
            Me.BtnCurrentLogs.UseVisualStyleBackColor = True
            Me.BtnCurrentLogs.Visible = False
            '
            'PbxAv
            '
            Me.PbxAv.Image = My.Resources.Resources.UCheckSophos
            Me.PbxAv.Location = New System.Drawing.Point(731, 26)
            Me.PbxAv.Name = "PbxAv"
            Me.PbxAv.Size = New System.Drawing.Size(108, 114)
            Me.PbxAv.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
            Me.PbxAv.TabIndex = 2
            Me.PbxAv.TabStop = False
            '
            'PbxMain
            '
            Me.PbxMain.Image = CType(resources.GetObject("PbxMain.Image"), System.Drawing.Image)
            Me.PbxMain.Location = New System.Drawing.Point(21, 26)
            Me.PbxMain.Name = "PbxMain"
            Me.PbxMain.Size = New System.Drawing.Size(108, 114)
            Me.PbxMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
            Me.PbxMain.TabIndex = 0
            Me.PbxMain.TabStop = False
            '
            'BgwUpdateScan
            '
            Me.BgwUpdateScan.WorkerReportsProgress = True
            Me.BgwUpdateScan.WorkerSupportsCancellation = True
            '
            'FrmMain
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(860, 187)
            Me.Controls.Add(Me.BtnCurrentLogs)
            Me.Controls.Add(Me.BtnAllLogs)
            Me.Controls.Add(Me.PnlUserInput)
            Me.Controls.Add(Me.PbxAv)
            Me.Controls.Add(Me.LblMain)
            Me.Controls.Add(Me.PbxMain)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Name = "FrmMain"
            Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = " "
            Me.PnlUserInput.ResumeLayout(False)
            Me.PnlUserInput.PerformLayout()
            Me.TblPnlUserInput.ResumeLayout(False)
            CType(Me.PbxAv, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PbxMain, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents PbxMain As System.Windows.Forms.PictureBox
        Friend WithEvents LblMain As System.Windows.Forms.Label
        Friend WithEvents PbxAv As System.Windows.Forms.PictureBox
        Friend WithEvents PnlUserInput As System.Windows.Forms.Panel
        Friend WithEvents TxbUserInput As TextBoxEx
        Friend WithEvents LblUserInput As System.Windows.Forms.Label
        Friend WithEvents TblPnlUserInput As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents BtnOkUserInput As System.Windows.Forms.Button
        Friend WithEvents BtnCancelUserInput As System.Windows.Forms.Button
        Friend WithEvents BtnAllLogs As Button
        Friend WithEvents BtnCurrentLogs As Button
        Friend WithEvents BgwUpdateScan As System.ComponentModel.BackgroundWorker
    End Class
End Namespace
