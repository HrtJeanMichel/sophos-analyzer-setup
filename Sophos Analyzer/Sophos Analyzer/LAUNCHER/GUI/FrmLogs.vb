﻿Imports System.IO
Imports System.ComponentModel
Imports Sophos_Analyzer.Core.Usb
Imports Sophos_Analyzer.Extensions
Imports Sophos_Analyzer.Launcher.Sophos

Namespace Launcher.Gui
    Public Class FrmLogs

#Region " Fields "
        Private m_DiskCurr As DiskCurrent
        Private m_BtnName As String
        Private m_UsbPath As String
        Private m_Loading As Boolean
#End Region

#Region " Constructor "
        Sub New(DiskCurr As DiskCurrent, BtnName As String)
            InitializeComponent()
            m_DiskCurr = DiskCurr
            m_BtnName = BtnName
            Text = If(BtnName = "BtnCurrentLogs", "Fichiers Log du périphérique USB connecté", "Fichiers Log de toutes les analyses")
            If Not BgwLoadLogs.IsBusy Then
                m_Loading = True
                BgwLoadLogs.RunWorkerAsync(Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt"))
            End If
        End Sub
#End Region

#Region " Methods "
        Private Sub FrmLogs_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
            If m_Loading Then
                e.Cancel = True
            End If
        End Sub

        Private Function FormatDate(DateStr As String) As String
            Dim InfoDateYear = DateStr.Substring(0, 4)
            Dim InfoDateMonth = DateStr.Substring(4).Remove(2)
            Dim InfoDateDay = DateStr.Substring(6)
            Return InfoDateYear & "/" & InfoDateMonth & "/" & InfoDateDay
        End Function

        Private Sub AddDeviceInfos(FilePath As String, InfoName As String, InfoDate As String, InfoHour As String)
            Try
                Dim lvi As New ListViewItem(InfoHour)
                lvi.Text = InfoHour
                lvi.SubItems.Add(InfoName)
                Dim txt As String = String.Empty
                Using sr As StreamReader = New StreamReader(FilePath)
                    Do While sr.Peek() >= 0
                        Dim line = sr.ReadLine()
                        txt &= line & vbNewLine
                        If line.Contains("MODELE :") Then
                            lvi.SubItems.Add(line.Split(":")(1).Trim(" "))
                        ElseIf line.Contains("CAPACITE :") Then
                            lvi.SubItems.Add(line.Split(":")(1).Trim(" "))
                        ElseIf line.Contains("NUMERO DE SERIE :") Then
                            lvi.SubItems.Add(line.Split(":")(1).Trim(" "))
                        ElseIf line.Contains("RESULTAT :") Then
                            Dim res = line.Split(":")(1).Trim(" ")
                            Select Case res
                                Case Settings.Clean
                                    lvi.ForeColor = Color.Green
                                    lvi.SubItems.Add("Sain")
                                Case Settings.Infected
                                    lvi.ForeColor = Color.Red
                                    lvi.SubItems.Add("Infecté")
                                Case Settings.Errors
                                    lvi.ForeColor = Color.Orange
                                    lvi.SubItems.Add("Erreurs")
                            End Select
                        End If
                    Loop
                End Using
                lvi.Tag = txt

                If m_BtnName = "BtnCurrentLogs" Then
                    If m_DiskCurr.Plugged Then
                        If lvi.SubItems(2).Text = m_DiskCurr.Model AndAlso
                        lvi.SubItems(3).Text = m_DiskCurr.SizeTotalSpace(True) AndAlso
                        lvi.SubItems(4).Text = m_DiskCurr.SerialNumber Then
                            BgwLoadLogs.ReportProgress(0, Tuple.Create(lvi, InfoDate))
                        End If
                    End If
                Else
                    BgwLoadLogs.ReportProgress(0, Tuple.Create(lvi, InfoDate))
                End If
            Catch ex As Exception
            End Try
        End Sub

        Private Sub LoadLvGroups(lvi As ListViewItem, InfoDate As String)
            Try
                If LvLogs.Groups.Cast(Of ListViewGroup).Any(Function(f) f.Header = InfoDate) Then
                    lvi.Group = LvLogs.Groups.Item(InfoDate)
                    LvLogs.Items.Add(lvi)
                Else
                    Dim gUser As New ListViewGroup(InfoDate, InfoDate)
                    gUser.HeaderAlignment = HorizontalAlignment.Center
                    LvLogs.Groups.Add(gUser)
                    lvi.Group = gUser
                    LvLogs.Items.Add(lvi)
                End If
            Catch ex As Exception
            End Try
        End Sub

        Private Sub LvLogs_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles LvLogs.ItemSelectionChanged
            If e.IsSelected Then
                TxbLogs.Text = e.Item.Tag.ToString
                EndOfLog()
            Else
                TxbLogs.Text = String.Empty
            End If
        End Sub

        Private Sub FrmLogs_ClientSizeChanged(sender As Object, e As EventArgs) Handles Me.ClientSizeChanged
            Dim oldTotalWidth As Integer = 0
            For Each column As ColumnHeader In LvLogs.Columns
                oldTotalWidth += column.Width
            Next
            Dim newTotalWidth As Integer = LvLogs.ClientSize.Width
            Dim growthFactor As Double = newTotalWidth / oldTotalWidth
            For Each column As ColumnHeader In LvLogs.Columns
                column.Width = Convert.ToInt32(Math.Round(column.Width * growthFactor))
            Next
        End Sub

        Private Sub EndOfLog()
            LvLogs.EnsureVisible(LvLogs.Items.IndexOf(LvLogs.SelectedItems(0)))
            TxbLogs.SelectionStart = TxbLogs.Text.Length
            TxbLogs.ScrollToCaret()
        End Sub

        Private Sub TsBtnExportLogs_Click(sender As Object, e As EventArgs) Handles TsBtnExportLogs.Click
            Try
                Dim dir = Date.Now.ToString("yyyyMMdd_HHmmss") & "_SOPHOS_ISOLEE_LOGS"
                Directory.CreateDirectory(m_UsbPath & "\" & dir)
                Dim files = Directory.GetFiles(Settings.GetWorkDirUcheck, "*.txt")
                For Each f In files
                    Dim fi As New FileInfo(f)
                    File.Copy(f, m_UsbPath & "\" & dir & "\" & fi.Name)
                Next
                Dim exported = String.Format(" fichier{0} exporté{0} sur le lecteur ", If(files.Count <> 0, "s", String.Empty))
                MessageBox.Show(files.Count.ToString & exported & m_UsbPath, "Export de logs", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
            End Try
        End Sub

        Private Sub BgwLoadLogs_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwLoadLogs.DoWork
            Try
                For Each f In TryCast(e.Argument, String())
                    Dim fi As New FileInfo(f)
                    Dim FileName = Path.GetFileNameWithoutExtension(fi.FullName)
                    If FileName.Contains("_") Then
                        Dim Infos() = FileName.Split("_")
                        If Infos.Count = 5 Then
                            Dim InfoDate = FormatDate(Infos(0))
                            Dim InfoHour = Infos(1) & ":" & Infos(2) & ":" & Infos(3)
                            Dim InfoName = Infos(4)
                            AddDeviceInfos(f, InfoName, InfoDate, InfoHour)
                        End If
                    End If
                Next
            Catch ex As Exception
            End Try
        End Sub

        Private Sub BgwLoadLogs_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwLoadLogs.ProgressChanged
            Dim tups = TryCast(e.UserState, Tuple(Of ListViewItem, String))
            LoadLvGroups(tups.Item1, tups.Item2)
        End Sub

        Private Sub BgwLoadLogs_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwLoadLogs.RunWorkerCompleted
            Try
                If LvLogs.Items.Count <> 0 Then
                    Dim it = LvLogs.Groups(LvLogs.Groups.Count - 1).Items.OfType(Of ListViewItem).Last
                    it.Selected = True
                    If m_DiskCurr.Plugged Then
                        TsBtnExportLogs.Enabled = True
                        m_UsbPath = If(m_DiskCurr.PartitionLettersInline.Contains(" "), m_DiskCurr.PartitionLettersInline.Split(" ")(0), m_DiskCurr.PartitionLettersInline)
                        TsBtnExportLogs.Enabled = If(m_DiskCurr.Partitions.Count <> 0, True, False)
                        If TsBtnExportLogs.Enabled = True Then
                            TsBtnExportLogs.Text &= " sur " & m_UsbPath
                        End If
                    Else
                        TsBtnExportLogs.Enabled = False
                    End If
                Else
                    TsBtnExportLogs.Enabled = False
                End If
                CType(LvLogs, ListViewExGroupSorter).SortGroups(False)
                If LvLogs.Items.Count <> 0 Then EndOfLog()
                m_Loading = False
            Catch ex As Exception
            End Try
        End Sub
#End Region

    End Class
End Namespace