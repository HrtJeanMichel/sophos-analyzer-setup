﻿Imports Sophos_Analyzer.Helper
Imports Sophos_Analyzer.Win32

Namespace Launcher.Gui
    Public Class FrmGlass
        Inherits Form

#Region " Designer "
        'Form remplace la méthode Dispose pour nettoyer la liste des composants.
        <DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requise par le Concepteur Windows Form
        Private components As System.ComponentModel.IContainer

        <DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.SuspendLayout()
            '
            'FrmGlass
            '
            Me.AutoScaleDimensions = New SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = AutoScaleMode.Font
            Me.ClientSize = New Size(653, 23)
            Me.ControlBox = False
            Me.DoubleBuffered = True
            Me.FormBorderStyle = FormBorderStyle.FixedToolWindow
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "FrmGlass"
            Me.ShowIcon = False
            Me.ShowInTaskbar = False
            Me.ResumeLayout(False)

        End Sub
#End Region

#Region " Fields "
        Private m_BlurRegion As Region
#End Region

#Region " Properties "
        Public Property ExtendFrameEnabled() As Boolean
        Public Property BlurBehindWindowEnabled() As Boolean
        Public Property GlassMargins As NativeStruct.MARGINS
        Public Property MarginRegion As Region
        Public Property Unmovable As Boolean

        ''' <summary>
        ''' La region où l'effet blur sera appliqué.
        ''' </summary>
        Public Property BlurRegion() As Region
            Get
                Return m_BlurRegion
            End Get
            Set
                If m_BlurRegion IsNot Nothing Then
                    m_BlurRegion.Dispose()
                End If
                m_BlurRegion = Value
            End Set
        End Property

        Public Property TransparencyColor As Color = Color.FromArgb(255, 221, 220, 220)
#End Region

#Region " Constructor "
        Public Sub New()
            InitializeComponent()
        End Sub

#End Region

#Region " Methods "
        Protected Overrides Sub WndProc(ByRef message As Message)
            Select Case message.Msg
                Case NativeConstants.WM_SYSCOMMAND
                    If Unmovable Then
                        Dim command% = message.WParam.ToInt32() And &HFFF0
                        If command = NativeConstants.SC_MOVE Then
                            Return
                        End If
                    End If
                    Exit Select
            End Select
            MyBase.WndProc(message)
        End Sub

        ''' <summary>
        ''' Cet évènement se déclenche lorsque le contrôle est redessiné (en appelant la méthode Invalidate).
        ''' </summary>
        ''' <remarks>
        ''' On gère la définition (et l'exclusion) de la zone de transparence ainsi que l'effet Blur.
        ''' </remarks>
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            If IsAeroGlassStyleSupported() Then
                If (ExtendFrameEnabled OrElse BlurBehindWindowEnabled) Then
                    TransparencyKey = TransparencyColor
                    Using transparentBrush As Brush = New SolidBrush(TransparencyColor)
                        If ExtendFrameEnabled Then
                            Dim glassMargs = GlassMargins
                            NativeMethods.DwmExtendFrameIntoClientArea(Me.Handle, glassMargs)
                            MarginRegion = New Region(Me.ClientRectangle)
                            If GlassMargins.IsNegativeOrOverride(Me.ClientSize) Then
                                e.Graphics.FillRegion(transparentBrush, MarginRegion)
                            Else
                                MarginRegion.Exclude(New Rectangle(GlassMargins.cxLeftWidth, GlassMargins.cyTopHeight, Me.ClientSize.Width - GlassMargins.cxLeftWidth - GlassMargins.cxRightWidth, Me.ClientSize.Height - GlassMargins.cyTopHeight - GlassMargins.cyBottomHeight))
                                e.Graphics.FillRegion(transparentBrush, MarginRegion)
                            End If
                        Else
                            Dim glassMargs = New NativeStruct.MARGINS(-1)
                            NativeMethods.DwmExtendFrameIntoClientArea(Me.Handle, glassMargs)
                        End If
                        If BlurBehindWindowEnabled Then
                            ResetDwmBlurBehind(True, e.Graphics)
                            e.Graphics.FillRegion(transparentBrush, If(BlurRegion IsNot Nothing, BlurRegion, Me.ClientRectangle))
                        Else
                            ResetDwmBlurBehind(False, Nothing)
                        End If
                    End Using
                End If
            End If
        End Sub

        ''' <summary>
        ''' Active ou désactive l'effet Blur.
        ''' </summary>
        Public Sub ResetDwmBlurBehind(enable As Boolean, graphics As Graphics)
            Try
                Dim bbh As New NativeMethods.DWM_BLURBEHIND()
                If enable Then
                    bbh.dwFlags = NativeMethods.DWM_BLURBEHIND.DWM_BB_ENABLE
                    bbh.fEnable = True
                    bbh.hRegionBlur = If(BlurRegion IsNot Nothing, BlurRegion.GetHrgn(graphics), IntPtr.Zero)
                Else
                    bbh.dwFlags = NativeMethods.DWM_BLURBEHIND.DWM_BB_ENABLE Or NativeMethods.DWM_BLURBEHIND.DWM_BB_BLURREGION
                    bbh.fEnable = False
                    bbh.hRegionBlur = IntPtr.Zero
                End If
                NativeMethods.DwmEnableBlurBehindWindow(Me.Handle, bbh)
            Catch
            End Try
        End Sub

        ''' <summary>
        ''' Cette procédure permet de savoir si la composition DWM est activée sur le système d'exploitation.
        ''' </summary>
        Public Shared Function IsAeroGlassStyleSupported() As Boolean
            Dim isDWMEnable As Boolean = False
            Try
                If OS.isCorrectOS Then NativeMethods.DwmIsCompositionEnabled(isDWMEnable)
            Catch
            End Try
            Return isDWMEnable
        End Function

        Protected Overloads Sub Dispose()
            MyBase.Dispose(Disposing)
            If MarginRegion IsNot Nothing Then
                MarginRegion.Dispose()
            End If

            If m_BlurRegion IsNot Nothing Then
                m_BlurRegion.Dispose()
            End If
        End Sub

#End Region

    End Class
End Namespace