﻿Imports System.Threading
Imports Sophos_Analyzer.Helper
Imports Sophos_Analyzer.Helper.FrmInfos
Imports Sophos_Analyzer.Configurator.Gui
Imports Sophos_Analyzer.Core.Settings
Imports Sophos_Analyzer.Core.Tweaks
Imports Sophos_Analyzer.Launcher.Gui
Imports Sophos_Analyzer.Launcher.Sophos

Friend Class Program

#Region " Events "
    Private Shared WithEvents DLLDomain As AppDomain = AppDomain.CurrentDomain
#End Region

#Region " Constructor "
    Shared Sub New()
        AddHandler DLLDomain.AssemblyResolve, AddressOf DLL_AssemblyResolve
    End Sub
#End Region

#Region " Methods "
    <STAThread()>
    Public Shared Sub Main(Args As String())
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Dim instanceCountOne As Boolean = False
        Using mtex As Mutex = New Mutex(True, Application.ProductName, instanceCountOne)
            If instanceCountOne Then
                If (Args.Length = 0) Then
                    If OS.CurrentIsAdministrator Then
                        If OS.isCorrectOS Then
                            If OS.IsComputerInDomain Then
                                Using infos As New FrmInfos(Messages.BeCarefullTitle, Messages.ProgramNonDomainExecution, ButtonTypes.OK, MessageTypes.Warning)
                                    infos.ShowDialog()
                                End Using
                            Else
                                If OS.ProcessIsRunning("Sophos Analyzer Setup") Then
                                    Using infos As New FrmInfos("Exécution annulée", "Sophos Analyzer ne sera pas lancé puisque ""Sophos Analyzer Setup"" est en cours d'exécuton !", ButtonTypes.OK, MessageTypes.Warning)
                                        infos.ShowDialog()
                                    End Using
                                Else
                                    Dim SophosProductsInstalled As Boolean = Prerequisites.SophosInstalledFromDirectories
                                    If SophosProductsInstalled Then
                                        Dim Account = New UserInfos("sophos_autonome", False)
                                        If Account.UserExists(String.Empty) Then
                                            Dim SophosUpdateInstalled As Boolean = Prerequisites.SophosAutoUpdateExists
                                            If SophosUpdateInstalled Then
                                                Application.Run(New FrmConfigurator(Account))
                                            Else
                                                Using infos As New FrmInfos(Messages.SophosAUNotExistsTitle, Messages.SophosAUNotExists, ButtonTypes.OK, MessageTypes.Warning, True)
                                                    infos.ShowDialog()
                                                End Using
                                            End If
                                        Else
                                            Account.DeleteAccountProfilByName(Account.SID, Account.ProfileImagePath)
                                            Using infos As New FrmInfos(Messages.SophosAccountNotExistsTitle, Messages.SophosAccountNotExists, ButtonTypes.OK, MessageTypes.Warning, True)
                                                infos.ShowDialog()
                                            End Using
                                        End If
                                    Else
                                        Using infos As New FrmInfos(Messages.SophosNotInstalledTitle, Messages.SophosNotInstalled, ButtonTypes.OK, MessageTypes.Warning, True)
                                            infos.ShowDialog()
                                        End Using
                                    End If
                                End If
                                mtex.ReleaseMutex()
                            End If
                        Else
                            Using infos As New FrmInfos(Messages.ProgramOptimizedForOsTitle, Messages.ProgramOptimizedForOs & vbNewLine & "- " & OS.GetOsInline, ButtonTypes.OK, MessageTypes.Warning)
                                infos.ShowDialog()
                            End Using
                        End If
                    Else
                        Using infos As New FrmInfos(Messages.BeCarefullTitle, Messages.ProgramNonElevatedExecution, ButtonTypes.OK, MessageTypes.Warning)
                            infos.ShowDialog()
                        End Using
                    End If
                Else
                    Dim cmd As New Cmdline(Args)
                    If (cmd.Item("launcher") Is Nothing) Then
                    Else
                        Dim cValue = cmd("launcher")
                        If String.IsNullOrEmpty(cValue) Then
                        Else
                            If OS.isCorrectOS Then
                                If OS.IsComputerInDomain Then
                                    Using infos As New FrmInfos(Messages.BeCarefullTitle, Messages.ProgramNonDomainExecution, ButtonTypes.OK, MessageTypes.Warning)
                                        infos.ShowDialog()
                                    End Using
                                    mtex.ReleaseMutex()
                                Else
                                    Dim SophosProductsInstalled As Boolean = Prerequisites.SophosInstalledFromDirectories
                                    If SophosProductsInstalled Then
                                        Dim Account = New UserInfos("sophos_autonome", False)
                                        If Account.UserExists(String.Empty) Then
                                            Dim SophosUpdateInstalled As Boolean = Prerequisites.SophosAutoUpdateExists
                                            If SophosUpdateInstalled Then
                                                Application.Run(New FrmLauncher())
                                            Else
                                                Using infos As New FrmInfos(Messages.SophosAUNotExistsTitle, Messages.SophosAUNotExists, ButtonTypes.OK, MessageTypes.Warning, True)
                                                    infos.ShowDialog()
                                                End Using
                                            End If
                                        Else
                                            Account.DeleteAccountProfilByName(Account.SID, Account.ProfileImagePath)
                                            Using infos As New FrmInfos(Messages.SophosAccountNotExistsTitle, Messages.SophosAccountNotExists, ButtonTypes.OK, MessageTypes.Warning, True)
                                                infos.ShowDialog()
                                            End Using
                                        End If
                                    Else
                                        Using infos As New FrmInfos(Messages.SophosNotInstalledTitle, Messages.SophosNotInstalled, ButtonTypes.OK, MessageTypes.Warning, True)
                                            infos.ShowDialog()
                                        End Using
                                    End If
                                    mtex.ReleaseMutex()
                                End If
                            Else
                                Using infos As New FrmInfos(Messages.ProgramOptimizedForOsTitle, Messages.ProgramOptimizedForOs & vbNewLine & "- " & OS.GetOsInline, ButtonTypes.OK, MessageTypes.Warning)
                                    infos.ShowDialog()
                                End Using
                                mtex.ReleaseMutex()
                            End If
                        End If
                    End If
                End If
            End If
        End Using
    End Sub

    Private Shared Function DLL_AssemblyResolve(sender As Object, args As ResolveEventArgs) As Reflection.Assembly
        If args.Name.Contains("Interop.TaskScheduler") Then
            Return Reflection.Assembly.Load(My.Resources.taskschd)
        Else
            Return Nothing
        End If
    End Function
#End Region

End Class

