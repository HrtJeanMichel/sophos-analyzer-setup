﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace MetroFramework.Controls
{
    public partial class MetroListViewItem : ListViewItem
    {
        private object _data;

        public object Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public MetroListViewItem(String[] Str) : base(Str)
        {
           
        }

    }
}
