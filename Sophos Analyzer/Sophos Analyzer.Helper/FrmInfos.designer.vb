﻿Imports System.Windows.Forms
Imports MetroFramework.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmInfos
    Inherits MetroForm

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmInfos))
        Me.LblInfosContent = New MetroFramework.Controls.MetroLabel()
        Me.TlpInfosBottom = New System.Windows.Forms.TableLayoutPanel()
        Me.BtnInfosCancel = New System.Windows.Forms.Button()
        Me.BtnInfosValid = New System.Windows.Forms.Button()
        Me.TlpInfosBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblInfosContent
        '
        Me.LblInfosContent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblInfosContent.Location = New System.Drawing.Point(23, 60)
        Me.LblInfosContent.Name = "LblInfosContent"
        Me.LblInfosContent.Size = New System.Drawing.Size(431, 86)
        Me.LblInfosContent.TabIndex = 2
        Me.LblInfosContent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblInfosContent.WrapToLine = True
        '
        'TlpInfosBottom
        '
        Me.TlpInfosBottom.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TlpInfosBottom.ColumnCount = 2
        Me.TlpInfosBottom.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TlpInfosBottom.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TlpInfosBottom.Controls.Add(Me.BtnInfosCancel, 1, 0)
        Me.TlpInfosBottom.Controls.Add(Me.BtnInfosValid, 0, 0)
        Me.TlpInfosBottom.Location = New System.Drawing.Point(308, 159)
        Me.TlpInfosBottom.Name = "TlpInfosBottom"
        Me.TlpInfosBottom.RowCount = 1
        Me.TlpInfosBottom.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TlpInfosBottom.Size = New System.Drawing.Size(146, 29)
        Me.TlpInfosBottom.TabIndex = 3
        '
        'BtnInfosCancel
        '
        Me.BtnInfosCancel.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.BtnInfosCancel.FlatAppearance.BorderSize = 0
        Me.BtnInfosCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnInfosCancel.ForeColor = System.Drawing.Color.White
        Me.BtnInfosCancel.Location = New System.Drawing.Point(76, 3)
        Me.BtnInfosCancel.Name = "BtnInfosCancel"
        Me.BtnInfosCancel.Size = New System.Drawing.Size(67, 23)
        Me.BtnInfosCancel.TabIndex = 7
        Me.BtnInfosCancel.Tag = "1"
        Me.BtnInfosCancel.Text = "Non"
        Me.BtnInfosCancel.UseVisualStyleBackColor = False
        '
        'BtnInfosValid
        '
        Me.BtnInfosValid.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.BtnInfosValid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BtnInfosValid.FlatAppearance.BorderSize = 0
        Me.BtnInfosValid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnInfosValid.ForeColor = System.Drawing.Color.White
        Me.BtnInfosValid.Location = New System.Drawing.Point(3, 3)
        Me.BtnInfosValid.Name = "BtnInfosValid"
        Me.BtnInfosValid.Size = New System.Drawing.Size(67, 23)
        Me.BtnInfosValid.TabIndex = 6
        Me.BtnInfosValid.Tag = "1"
        Me.BtnInfosValid.Text = "Oui"
        Me.BtnInfosValid.UseVisualStyleBackColor = False
        '
        'FrmInfos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle
        Me.ClientSize = New System.Drawing.Size(477, 203)
        Me.Controls.Add(Me.TlpInfosBottom)
        Me.Controls.Add(Me.LblInfosContent)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmInfos"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Information"
        Me.TlpInfosBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblInfosContent As MetroFramework.Controls.MetroLabel
    Friend WithEvents TlpInfosBottom As TableLayoutPanel
    Friend WithEvents BtnInfosCancel As Button
    Friend WithEvents BtnInfosValid As Button
End Class