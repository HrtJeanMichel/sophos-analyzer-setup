﻿Imports System.Collections.Specialized
Imports System.Text.RegularExpressions
Imports System.Reflection

''' <summary>
'''     Original C# project by GriffonRL from                    : http://www.codeproject.com/Articles/3111/C-NET-Command-Line-Arguments-Parser
'''     This is the vbnet version converted for my personal use !
''' </summary>
<DefaultMember("Item")>
Public Class Cmdline

#Region " Fields "
    Private Parameters As StringDictionary = New StringDictionary
#End Region

#Region " Properties "
    ' Retrieve a parameter value if it exists 
    ' (overriding C# indexer property)
    Default Public ReadOnly Property Item(ByVal Param$) As String
        Get
            Return Me.Parameters.Item(Param)
        End Get
    End Property
#End Region

#Region " Constructor "
    Public Sub New(ByVal Args As String())
        Dim Spliter As New Regex("^-{1,2}|^/|=|:", (RegexOptions.Compiled Or RegexOptions.IgnoreCase))
        Dim Remover As New Regex("^['""]?(.*?)['""]?$", (RegexOptions.Compiled Or RegexOptions.IgnoreCase))
        Dim Parameter As String = Nothing
        ' Valid parameters forms:
        ' {-,/,--}param{ ,=,:}((",')value(",'))
        ' Examples: 
        ' -param1 value1 --param2 /param3:"Test-:-work" 
        '   /param4=happy -param5 '--=nice=--'
        For Each Txt In Args
            ' Look for new parameters (-,/ or --) and a
            ' possible enclosed value (=,:)
            Dim Parts As String() = Spliter.Split(Txt, 3)
            Select Case Parts.Length
                    ' Found a value (for the last parameter 
                    ' found (space separator))
                Case 1
                    If (Not Parameter Is Nothing) Then
                        If Not Me.Parameters.ContainsKey(Parameter) Then
                            Parts(0) = Remover.Replace(Parts(0), "$1")
                            Me.Parameters.Add(Parameter, Parts(0))
                        End If
                        Parameter = Nothing
                    End If
                    ' else Error: no parameter waiting for a value (skipped)
                    Exit Select
                        ' Found just a parameter
                Case 2
                    ' The last parameter is still waiting. 
                    ' With no value, set it to true.
                    If ((Not Parameter Is Nothing) AndAlso Not Me.Parameters.ContainsKey(Parameter)) Then
                        Me.Parameters.Add(Parameter, "true")
                    End If
                    Parameter = Parts(1)
                    Exit Select
                        ' Parameter with enclosed value
                Case 3
                    ' The last parameter is still waiting. 
                    ' With no value, set it to true.
                    If ((Not Parameter Is Nothing) AndAlso Not Me.Parameters.ContainsKey(Parameter)) Then
                        Me.Parameters.Add(Parameter, "true")
                    End If
                    Parameter = Parts(1)
                    ' Remove possible enclosing characters (",')
                    If Not Me.Parameters.ContainsKey(Parameter) Then
                        Parts(2) = Remover.Replace(Parts(2), "$1")
                        Me.Parameters.Add(Parameter, Parts(2))
                    End If
                    Parameter = Nothing
                    Exit Select
                    ' In case a parameter is still waiting
            End Select
        Next
        If ((Not Parameter Is Nothing) AndAlso Not Me.Parameters.ContainsKey(Parameter)) Then
            Me.Parameters.Add(Parameter, "true")
        End If
    End Sub
#End Region

End Class


