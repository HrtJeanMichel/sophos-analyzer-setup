﻿Imports Microsoft.Win32

Public NotInheritable Class Reg

#Region " Fields "
    Private Shared m_TempUserDat As String = "TempUserDat"
#End Region

#Region " DWORD "
    Public Class BINARY
            Public Shared Function GetValue(ByVal SubKey As String, ByVal ValueName As String) As Byte()
                Try
                    Return Registry.LocalMachine.OpenSubKey(SubKey).GetValue(ValueName)
                Catch exception1 As Exception
                End Try
                Return Nothing
            End Function
        End Class

    Public Class DWORD

        Public Shared Function GetValue(ByVal SubKey As String, ByVal ValueName As String, Optional ByVal DefaultValue As Integer = -2) As Integer
            Try
                Return CInt(Registry.LocalMachine.OpenSubKey(SubKey).GetValue(ValueName, DefaultValue))
            Catch exception1 As Exception
            End Try
            Return DefaultValue
        End Function

        Public Shared Sub SetValue(ByVal SubKey As String, ByVal ValueName As String, ByVal IntValue As String, Optional ByVal DefaultValue_EqualToDelete As Integer = -2)
            Try
                If (IntValue = DefaultValue_EqualToDelete) Then
                    Registry.LocalMachine.OpenSubKey(SubKey, True).DeleteValue(ValueName, False)
                Else
                    Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, IntValue, RegistryValueKind.DWord)
                End If
            Catch ex As Exception
            End Try
        End Sub

        Public Shared Function RegIsSeted(ByVal SubKey As String, ByVal ValueName As String, Optional ByVal KeepValueName As Boolean = False) As Boolean
            If KeepValueName Then
                Dim flag As Boolean
                Try
                    Dim key As RegistryKey = Registry.LocalMachine.OpenSubKey(SubKey)
                    Dim str As String
                    For Each str In key.GetValueNames
                        If ((str.ToLower = ValueName.ToLower) AndAlso (CInt(key.GetValue(ValueName)) = 1)) Then
                            flag = True
                            Exit For
                        End If
                    Next
                Catch ex As Exception
                    flag = False
                End Try
                Return flag
            Else
                Try
                    If (CInt(Registry.LocalMachine.OpenSubKey(SubKey).GetValue(ValueName)) = 1) Then
                        Return True
                    End If
                Catch ex As Exception
                    Return False
                End Try
            End If
            Return False
        End Function

        Public Shared Sub SetBlnValue(ByVal SubKey As String, ByVal ValueName As String, ByVal value As Boolean, Optional ByVal KeepValueName As Boolean = False)
            Try
                If KeepValueName Then
                    Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, If(value, 1, 0), RegistryValueKind.DWord)
                Else
                    If value Then
                        Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, 1, RegistryValueKind.DWord)
                    Else
                        Registry.LocalMachine.OpenSubKey(SubKey, True).DeleteValue(ValueName, False)
                    End If
                End If
            Catch ex As Exception
            End Try
        End Sub
    End Class

#End Region

#Region " SZ "
    Public Class SZ

        Public Shared Sub SetBlnValue(ByVal SubKey As String, ByVal ValueName As String, ByVal strTrueValue As String, ByVal value As Boolean)
            Try
                If value Then
                    Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, strTrueValue, RegistryValueKind.String)
                Else
                    Registry.LocalMachine.OpenSubKey(SubKey, True).DeleteValue(ValueName, False)
                End If
            Catch ex As Exception
            End Try
        End Sub

        Public Shared Function GetValue(ByVal SubKey As String, ByVal ValueName As String, Optional ByVal DefaultValue As String = "") As String
            Try
                Dim obj = Registry.LocalMachine.OpenSubKey(SubKey)
                If Not obj Is Nothing Then
                    Return obj.GetValue(ValueName, DefaultValue).ToString
                End If
                Return DefaultValue
            Catch ex As Exception
            End Try
            Return DefaultValue
        End Function

        Public Shared Sub SetValue(ByVal SubKey As String, ByVal ValueName As String, ByVal strValue As String, Optional ByVal Expand As Boolean = False)
            Try
                Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, strValue, If(Expand, RegistryValueKind.ExpandString, RegistryValueKind.String))
            Catch ex As Exception
            End Try
        End Sub

    End Class

#End Region

#Region " MULTI SZ "
    Public Class MULTI_SZ

        Public Shared Sub SetBlnValue(MainRK As RegistryKey, SubKey As String, ValueName As String, strTrueValue As String, value As Boolean)
            Try
                If value Then
                    Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, strTrueValue, RegistryValueKind.String)
                Else
                    Registry.LocalMachine.OpenSubKey(SubKey, True).DeleteValue(ValueName, False)
                End If
            Catch ex As Exception
            End Try
        End Sub

        Public Shared Function GetValue(SubKey As String, ValueName As String, Optional ByVal DefaultValue As String() = Nothing) As String()
            Try
                Return Registry.LocalMachine.OpenSubKey(SubKey).GetValue(ValueName, DefaultValue)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            Return DefaultValue
        End Function

        Public Shared Sub SetValue(SubKey As String, ValueName As String, strValue As String)
            Try
                Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, strValue, RegistryValueKind.String)
            Catch ex As Exception
            End Try
        End Sub

    End Class
#End Region

#Region " Methods "
    ''' <summary>
    ''' Vérifie si la sous clé TempUserDat existe dans la ruche HKLM du registre puis retourne une valeur de type booléen.
    ''' </summary>
    ''' <returns>Valeur de type Boolean</returns>
    Public Shared Function Mounted() As Boolean
        Dim registryKey1 As RegistryKey = Nothing
        Dim b1 As Boolean = False
        Try
            registryKey1 = Registry.LocalMachine.OpenSubKey(m_TempUserDat, RegistryKeyPermissionCheck.ReadSubTree)
            Dim object2 As Object = registryKey1.GetValue("(Default)")
            b1 = True
        Catch ex As Exception
            b1 = False
        Finally
            If Not registryKey1 Is Nothing Then
                registryKey1.Close()
                registryKey1.Dispose()
            End If
        End Try
        Return b1
    End Function

    ''' <summary>
    ''' Charge une ruche de registre dans HKLM\TempUserDat puis retourne True ou False.
    ''' </summary>
    ''' <param name="fPath">Chemin du fichier</param>
    ''' <returns>Valeur de type Boolean</returns>
    Public Shared Function Load(fPath As String) As Boolean
        Return Utils.StartCommand(Environment.ExpandEnvironmentVariables("%WinDir%") & "\System32\Cmd.exe", " /c REG LOAD HKLM\" & m_TempUserDat & " " & """" & fPath & """", True)
    End Function

    ''' <summary>
    ''' Décharge la ruche de registre HKLM\TempUserDat puis retourne True ou False.
    ''' </summary>
    ''' <returns>Valeur de type Boolean</returns>
    Public Shared Function Unload() As Boolean
        Return Utils.StartCommand(Environment.ExpandEnvironmentVariables("%WinDir%") & "\System32\Cmd.exe", " /c REG UNLOAD HKLM\" & m_TempUserDat, True)
    End Function

    Public Shared Function ExistsKey(SubKey As String) As Boolean
        Return Registry.LocalMachine.OpenSubKey(SubKey, True) IsNot Nothing
    End Function

    Public Shared Sub DeleteSubKey(SubKey As String)
        If ExistsKey(SubKey) Then
            Registry.LocalMachine.DeleteSubKeyTree(SubKey)
        End If
    End Sub
#End Region

End Class



