﻿Imports System.DirectoryServices.AccountManagement
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Security.Cryptography
Imports System.Text
Imports Sophos_Analyzer.Win32
Imports System.Windows.Forms

Public NotInheritable Class OS

#Region " Fields "
    Private Shared m_WinVersions As String() = New String() {"Windows 7", "Windows 10"}
    Private Shared m_ProgramFiles As String = "C:\Program Files"
    Private Shared m_ProgramFiles32 As String = "C:\Program Files (x86)"
    Public Shared SystemDrive As String = Environment.GetEnvironmentVariable("SystemDrive")
#End Region

#Region " Methods "
    ''' <summary>
    ''' Renvoie la version du système d'exploitation (Windows Vista, Windows 7, Windows 8, Windows 8.1, Windows 10, ...
    ''' </summary>
    ''' <returns>Valeur de type String</returns>
    Public Shared Function GetVersion() As String
        Dim oSVersion As OperatingSystem = Environment.OSVersion
        Select Case oSVersion.Platform.ToString
            Case "Win32Windows"
                Select Case oSVersion.Version.Minor
                    Case 0
                        Return "Windows 95"
                    Case 10
                        If (oSVersion.Version.Revision.ToString = "2222A") Then
                            Return "Windows 98 Second Edition"
                        End If
                        Return "Windows 98"
                    Case 90
                        Return "Windows Me"
                End Select
                Exit Select
            Case "Win32NT"
                Select Case oSVersion.Version.Major
                    Case 3
                        Return "Windows NT 3.51"
                    Case 4
                        Return "Windows NT 4.0"
                    Case 5
                        Select Case oSVersion.Version.Minor
                            Case 0
                                Return "Windows 2000"
                            Case 1
                                Return "Windows XP"
                            Case 2
                                If My.Computer.Info.OSFullName.Contains("R2") Then
                                    Return "Windows Server 2003 R2"
                                Else
                                    Return "Windows Server 2003"
                                End If
                        End Select
                        Exit Select
                    Case 6
                        Select Case oSVersion.Version.Minor
                            Case 0
                                If My.Computer.Info.OSFullName.Contains("Serv") Then
                                    Return "Windows Server 2008"
                                ElseIf My.Computer.Info.OSFullName.Contains("Vista") Then
                                    Return "Windows Vista"
                                ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                    Return "Windows Embedded"
                                End If
                            Case 1
                                If My.Computer.Info.OSFullName.Contains("R2") Then
                                    Return "Windows Server 2008 R2"
                                ElseIf My.Computer.Info.OSFullName.Contains("7") Then
                                    Return "Windows 7"
                                ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                    Return "Windows Embedded"
                                End If
                            Case 2
                                If My.Computer.Info.OSFullName.Contains("Serv") Then
                                    Return "Windows Server 2012"
                                ElseIf My.Computer.Info.OSFullName.Contains("8") Then
                                    Return "Windows 8"
                                ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                    Return "Windows Embedded"
                                ElseIf My.Computer.Info.OSFullName.Contains("10") Then
                                    Return "Windows 10"
                                End If
                            Case 3
                                If My.Computer.Info.OSFullName.Contains("Serv") Then
                                    Return "Windows Server 2012 R2"
                                ElseIf My.Computer.Info.OSFullName.Contains("8.1") Then
                                    Return "Windows 8.1"
                                ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                    Return "Windows Embedded"
                                ElseIf My.Computer.Info.OSFullName.Contains("10") Then
                                    Return "Windows 10"
                                End If
                            Case 4
                                If My.Computer.Info.OSFullName.Contains("10") Then
                                    Return "Windows 10"
                                ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                    Return "Windows Embedded"
                                End If
                        End Select
                    Case 10
                        If My.Computer.Info.OSFullName.Contains("10") Then
                            Return "Windows 10"
                        ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                            Return "Windows Embedded"
                        End If
                        Exit Select
                End Select
                Exit Select
        End Select
        Return "Système d'exploitation inconnu !"
    End Function

    ''' <summary>
    ''' Vérifie si le système d'exploitation est conforme aux exigences.
    ''' </summary>
    ''' <returns>Valeur de type Boolean</returns>
    ''' <remarks>
    ''' Compatible Windows Vista et 7 uniquement
    ''' </remarks>
    Public Shared Function isCorrectOS() As Boolean
        Return m_WinVersions.Contains(GetVersion)
    End Function

    Public Shared Function GetOsInline() As String
        Return String.Join(vbNewLine & "- ", m_WinVersions)
    End Function

    Public Shared Function GetProgramFilesDir() As String
        If Directory.Exists(m_ProgramFiles32) Then
            Return m_ProgramFiles32
        End If
        Return m_ProgramFiles
    End Function

    '''' <summary>
    '''' On détermine l'architecture du système d'exploitation en se basant sur l'existence du chemin "Program Files x86".
    '''' </summary>
    Public Shared Function is64Bits() As Boolean
        Return Directory.Exists(m_ProgramFiles32)
    End Function

    '''' <summary>
    '''' On tue un processus qui est en cours d'exécution avec son nom passé en paramètre.
    '''' </summary>
    Public Shared Sub KillProcess(ProcName As String)
        Try
            For Each ObjPro As Process In Process.GetProcessesByName(ProcName)
                NativeMethods.TerminateProcess(ObjPro.Handle, 1)
                Do Until ObjPro.HasExited = True
                    Application.DoEvents()
                Loop
            Next
        Catch ex As Exception
        End Try
    End Sub

    '''' <summary>
    '''' On vérifie si un processus est en cours d'exécution avec son nom passé en paramètre.
    '''' </summary>
    Public Shared Function ProcessIsRunning(ProcName As String) As Boolean
        Try
            For Each ObjPro As Process In Process.GetProcesses
                If ObjPro.ProcessName.Contains(ProcName) Then
                    Return True
                End If
            Next
        Catch ex As Exception
        End Try
        Return False
    End Function

    Public Shared Function MD5Hash(input As String) As String
        Dim hash As StringBuilder = New StringBuilder()
        Dim md5provider As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
        Dim bytes As Byte() = md5provider.ComputeHash(New UTF8Encoding().GetBytes(input))
        For i As Integer = 0 To bytes.Length - 1
            hash.Append(bytes(i).ToString("x2"))
        Next
        Return hash.ToString()
    End Function

    '''' <summary>
    '''' On retourne la taille formatée par l'OS avec le suffixe (Eo, Po, To, Go, Mo, Ko, octets).
    '''' </summary>
    Public Shared Function SizeStrByteFormat(Size As ULong) As String
        Dim sb As New StringBuilder(20)
        NativeMethods.StrFormatByteSize(Size, sb, sb.Capacity)
        Return sb.ToString()
    End Function

    '''' <summary>
    '''' On retourne la valeur de la culture de langue utilisée par l'OS.
    '''' </summary>
    Public Shared Function GetInstalledUICulture() As Integer
        If CultureInfo.InstalledUICulture.ToString.ToLower = "fr-fr" Then
            Return 1036
        End If
        Return 1033
    End Function

    '' Return the short file name for a long file name.
    'Public Shared Function ShortFileName(long_name As String) As String
    '    Dim name_chars As Char() = New Char(1023) {}
    '    Dim length As Long = NativeMethods.GetShortPathName(long_name, name_chars, name_chars.Length)

    '    Dim short_name As New String(name_chars)
    '    Return short_name.Substring(0, CInt(length))
    'End Function

    ''' <summary>
    ''' Détermine si l'utilisateur exécutant la session appartient au groupe administrateurs.
    ''' </summary>
    ''' <returns>Valeur de type Boolean</returns>
    Public Shared Function CurrentIsAdministrator() As Boolean
        Try
            Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                Using user As New UserPrincipal(ctx)
                    user.Name = "*"
                    Using ps As New PrincipalSearcher()
                        ps.QueryFilter = user
                        Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll
                        For Each p As Principal In result
                            Using up As UserPrincipal = DirectCast(p, UserPrincipal)
                                If up.Name = GetUsernameBySessionId() Then
                                    For Each g In up.GetGroups
                                        If g.Name.ToLower = "administrateurs" OrElse g.Name.ToLower = "administrators" Then
                                            Return True
                                        End If
                                    Next
                                End If
                            End Using
                        Next
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    ''' <summary>
    ''' Retourne le nom de l'utilisateur exécutant la session en cours.
    ''' </summary>
    ''' <param name="CheckDomain">permet de préfixer le nom d'utilisateur par le nom de domaine ou le nom de l'ordinateur</param>
    ''' <returns>Valeur de type String</returns>
    Public Shared Function GetUsernameBySessionId(Optional ByVal checkDomain As Boolean = False) As String
        Dim buff As IntPtr
        Dim strLength As Integer
        Dim userName As String = "SYSTEM"
        Dim sessionId = NativeMethods.WTSGetActiveConsoleSessionId()
        If NativeMethods.WTSQuerySessionInformation(IntPtr.Zero, sessionId, NativeEnum.WTS_INFO_CLASS.WTSUserName, buff, strLength) AndAlso strLength > 1 Then
            userName = Marshal.PtrToStringAnsi(buff)
            NativeMethods.WTSFreeMemory(buff)
            If checkDomain Then
                If NativeMethods.WTSQuerySessionInformation(IntPtr.Zero, sessionId, NativeEnum.WTS_INFO_CLASS.WTSDomainName, buff, strLength) AndAlso strLength > 1 Then
                    userName = Marshal.PtrToStringAnsi(buff) & "\" & userName
                    NativeMethods.WTSFreeMemory(buff)
                End If
            End If
        End If
        Return userName
    End Function

    ''' <summary>
    '''On vérifie si l'ordinateur est membre d'un domaine.
    ''' </summary>
    Public Shared Function IsComputerInDomain() As Boolean
        Try
            Dim status As NativeEnum.NetJoinStatus = NativeEnum.NetJoinStatus.NetSetupUnknownStatus
            Dim pDomain As IntPtr = IntPtr.Zero
            Dim result As Integer = NativeMethods.NetGetJoinInformation(Nothing, pDomain, status)

            If (pDomain <> IntPtr.Zero) Then
                NativeMethods.NetApiBufferFree(pDomain)
            End If

            If (result = 0) Then
                Return If(status = NativeEnum.NetJoinStatus.NetSetupDomainName, True, False)
            Else
                Throw New Exception("Pas d'informations de domaine !")
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    '''On annule la tentative d'extinction de l'ordinateur.
    ''' </summary>
    Public Shared Sub ShutdownCancel()
        NativeMethods.CancelShutdown()
    End Sub
#End Region

End Class
