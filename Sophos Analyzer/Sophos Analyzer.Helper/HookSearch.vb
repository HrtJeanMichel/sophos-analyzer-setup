﻿Imports System.Text
Imports System.Threading
Imports Sophos_Analyzer.Win32

Public NotInheritable Class HookSearch

    '''' <summary>
    '''' Désactive le Handle de la fenêtre de zone de recherche du menu démarrer. 
    '''' C'est ponctuel, durant la session uniquement.
    '''' </summary>
    Public Shared Sub CloseStartMenuSearch()

        Dim processExists As Boolean = False
        Thread.Sleep(2000)
        Do
            processExists = Process.GetProcessesByName("explorer").Count >= 1
            If processExists = False Then
                Thread.Sleep(100)
            Else
                Exit Do
            End If
        Loop

        If processExists Then
            Dim startmenu As Integer = 0

            For Each hnd In EnumerateProcessWindowHandles(Process.GetProcessesByName("explorer").First().Id)
                Dim mess As New StringBuilder(1000)
                NativeMethods.SendMessage(hnd, NativeConstants.WM_GETTEXT, mess.Capacity, mess)
                If mess.ToString.ToLower.Contains(If(OS.GetInstalledUICulture = 1036, "menu démarrer", "start menu")) Then
                    startmenu = hnd.ToInt32
                    Exit For
                End If
            Next

            If Not startmenu = 0 Then
                Dim childwindow = 0
                Do
                    childwindow = NativeMethods.FindWindowEx(startmenu, 0, "Desktop OpenBox Host", 0)
                    If childwindow = 0 Then
                        Thread.Sleep(100)
                    Else
                        Exit Do
                    End If
                Loop

                If Not childwindow = 0 Then
                    Dim childSearch = 0
                    Do
                        childSearch = NativeMethods.FindWindowEx(childwindow, 0, "Search Box", 0)
                        If childSearch = 0 Then
                            Thread.Sleep(100)
                        Else
                            Exit Do
                        End If
                    Loop

                    If Not childSearch = 0 Then
                        NativeMethods.PostMessage(childSearch, NativeConstants.WM_CLOSE, 0, 0)
                    End If
                End If
            End If

        End If

    End Sub

    Private Shared Function EnumerateProcessWindowHandles(processId As Integer) As IEnumerable(Of IntPtr)
        Dim hnds = New List(Of IntPtr)()

        For Each Thr As ProcessThread In Process.GetProcessById(processId).Threads
            NativeMethods.EnumThreadWindows(Thr.Id, Function(hWnd, lParam)
                                                        hnds.Add(hWnd)
                                                        Return True
                                                    End Function, IntPtr.Zero)
        Next

        Return hnds
    End Function

End Class
