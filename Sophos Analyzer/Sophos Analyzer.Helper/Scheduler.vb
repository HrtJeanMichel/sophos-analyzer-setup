﻿Imports System.Security.Principal
Imports TaskScheduler

Public Class Scheduler

#Region " Fields "
        Private objScheduler As TaskScheduler.TaskScheduler
        Private objTaskDef As ITaskDefinition
        Private objAction As IExecAction
#End Region

#Region " Methods "
    Public Sub CreateTask(sid As String, tName$, PgPath As String)
        Try
            objScheduler = New TaskScheduler.TaskScheduler()
            objScheduler.Connect()
            'Setting Task Definition
            SetTaskDefinition(tName)
            'Setting Task Action Information
            SetActionInfo(PgPath, sid)
            'Setting Task Trigger
            Dim Trigger = DirectCast(SetTrigger(), ILogonTrigger)
            'Trigger.Type =
            Dim Si = New SecurityIdentifier(sid)
            Dim na = Si.Translate(GetType(NTAccount))
            Trigger.UserId = na.ToString
            Trigger.Enabled = True

            'Getting the roort folder
            Dim root As ITaskFolder = objScheduler.GetFolder("\")
            'Registering the task, if the task is already exist then it will be updated
            root.RegisterTaskDefinition(tName, objTaskDef, _TASK_CREATION.TASK_CREATE_OR_UPDATE, sid, Nothing, _TASK_LOGON_TYPE.TASK_LOGON_INTERACTIVE_TOKEN, "")
            'root.RegisterTaskDefinition(tName, objTaskDef, _TASK_CREATION.TASK_CREATE_OR_UPDATE, sid, Nothing, _TASK_LOGON_TYPE.TASK_LOGON_INTERACTIVE_TOKEN, "")
            'To execute the task immediately calling Run()
            'Dim runtask As IRunningTask = regTask.Run(Nothing)
        Catch ex As Exception
            'MessageBox.Show("Erreur tache planifiée non créée : " & ex.Message)
        End Try
    End Sub

    'Setting Task Definition
    Private Sub SetTaskDefinition(tName As String)
        Try
            objTaskDef = objScheduler.NewTask(0)
            'Registration Info for task
            'Name of the task Author
            objTaskDef.RegistrationInfo.Author = OS.GetUsernameBySessionId(True)
            'Description of the task 
            objTaskDef.RegistrationInfo.Description = "Exécution du programme " & tName & " avec élévation de privilèges."
            'Registration date of the task 
            objTaskDef.RegistrationInfo.Date = Today.ToString("yyyy-MM-ddTHH:mm:ss")
            'Execute with highest privileges
            objTaskDef.Principal.RunLevel = _TASK_RUNLEVEL.TASK_RUNLEVEL_HIGHEST
            '############################ Settings for task
            'Optimize for Operating System (Windows 7, Server 2008R2)
            objTaskDef.Settings.Compatibility = GetOptimizedValue()
            'Thread Priority
            objTaskDef.Settings.Priority = 0
            'Enabling the task
            objTaskDef.Settings.Enabled = True
            'To hide/show the task
            objTaskDef.Settings.Hidden = False

            objTaskDef.Settings.DisallowStartIfOnBatteries = False
            objTaskDef.Settings.StopIfGoingOnBatteries = False

            'Specifying no need of network connection
            objTaskDef.Settings.RunOnlyIfNetworkAvailable = False
        Catch ex As Exception
            MsgBox("SetTaskDefinition : " & ex.ToString)
        End Try
    End Sub

    Private Function SetTrigger() As ITrigger
        Return objTaskDef.Triggers.Create(_TASK_TRIGGER_TYPE2.TASK_TRIGGER_LOGON)
    End Function

    Private Function GetOptimizedValue() As Integer
        Dim osv = OS.GetVersion
        Dim iVal% = 3
        Select Case osv
            Case "Windows Vista"
                iVal = 2
            Case "Windows Embedded", "Windows 7"
                iVal = 3
            Case "Windows 8", "Windows 8.1", "Windows 10"
                iVal = 4
        End Select
        Return iVal
    End Function

    'Setting Task Action Information
    Private Sub SetActionInfo(pPath$, sid As String)
        Try
            'Action information based on exe- TASK_ACTION_EXEC
            objAction = DirectCast(objTaskDef.Actions.Create(_TASK_ACTION_TYPE.TASK_ACTION_EXEC), IExecAction)
            'Action ID
            objAction.Id = "Launcher_" & sid
            'Set the path of the exe file to execute
            objAction.Path = Chr(34) & pPath & Chr(34)
            'Set Args
            objAction.Arguments = "-launcher:yes"
        Catch ex As Exception
            'MessageBox.Show("SetActionInfo : " & ex.ToString)
        End Try
    End Sub

    Public Sub deleteTask(tName$)
        Try
            Dim objScheduler As New TaskScheduler.TaskScheduler()
            'Connect to Task Scheduler
            objScheduler.Connect()
            'Get working dir
            Dim containingFolder As ITaskFolder = objScheduler.GetFolder("\")

            For Each t As IRegisteredTask In containingFolder.GetTasks(0)
                'MsgBox(t.Name)
                If t.Name = tName Then
                    'Deleting the task
                    containingFolder.DeleteTask(tName, 0)
                End If
            Next
        Catch ex As Exception
            'MessageBox.Show("deleteTask : " & ex.ToString)
        End Try
    End Sub

    Public Function ExistsTask(tName$) As Boolean
        Try
            Dim objScheduler As New TaskScheduler.TaskScheduler()
            'Connect to Task Scheduler
            objScheduler.Connect()
            'Get working dir
            Dim containingFolder As ITaskFolder = objScheduler.GetFolder("\")
            'checking task
            For Each t As IRegisteredTask In containingFolder.GetTasks(0)
                'MsgBox(t.Name)
                If t.Name = tName Then
                    Return True
                End If
            Next
        Catch ex As Exception
            'MessageBox.Show("ExistsTask : " & ex.ToString)
        End Try
        Return False
    End Function

#End Region

End Class