﻿Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Runtime.InteropServices

Public Class MimeTypes

#Region " Fields "
    Private m_knownTypes As List(Of String)
    Private m_mimeTypes As Dictionary(Of String, String)
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeMimeTypeLists()
    End Sub
#End Region

#Region " Methods "
    Public Function IsWord(fileName As FileInfo) As Boolean
        Try
            Dim contentType As String = ""
            If Path.HasExtension(fileName.FullName) Then
                Dim extension As String = Path.GetExtension(fileName.FullName)
                Dim extensionWithoutDot As String = extension.Remove(0, 1).ToLower()
                m_mimeTypes.TryGetValue(extensionWithoutDot, contentType)
                If contentType = "application/msword" AndAlso ScanFileForMimeType(fileName.FullName) = "application/x-zip-compressed" Then
                    Return True
                ElseIf contentType = "application/msword" AndAlso ScanFileForMimeType(fileName.FullName) = "application/octet-stream" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Return False
    End Function

    Public Function IsPdf(fileName As FileInfo) As Boolean
        Dim contentType As String = ""
        If Path.HasExtension(fileName.FullName) Then
            Dim extension As String = Path.GetExtension(fileName.FullName)
            Dim extensionWithoutDot As String = extension.Remove(0, 1).ToLower()
            m_mimeTypes.TryGetValue(extensionWithoutDot, contentType)
            Return If(contentType = "application/pdf" AndAlso ScanFileForMimeType(fileName.FullName) = "application/pdf", True, False)
        End If
        Return False
    End Function

    Public Function IsPpt(fileName As FileInfo) As Boolean
        Dim contentType As String = ""
        If Path.HasExtension(fileName.FullName) Then
            Dim extension As String = Path.GetExtension(fileName.FullName)
            Dim extensionWithoutDot As String = extension.Remove(0, 1).ToLower()
            m_mimeTypes.TryGetValue(extensionWithoutDot, contentType)
            Return If(contentType = "application/vnd.ms-powerpoint" OrElse contentType = "application/mspowerpoint", True, False)
        End If
        Return False
    End Function

    Private Function GetGuidID(ByVal filepath$) As Guid
        Dim imagedata As Byte() = File.ReadAllBytes(filepath)
        Dim id As Guid
        Using ms As New MemoryStream(imagedata)
            Using img As Image = Image.FromStream(ms)
                id = img.RawFormat.Guid
            End Using
        End Using
        Return id
    End Function

    Public Function IsBitmap(fileName As String) As Boolean
        Dim GuidId = GetGuidID(fileName)
        If GuidId = ImageFormat.Bmp.Guid OrElse GuidId = ImageFormat.MemoryBmp.Guid Then
            Return True
        End If
        Return False
    End Function

    Public Function IsGif(fileName As String) As Boolean
        Dim GuidId = GetGuidID(fileName)
        If GuidId = ImageFormat.Gif.Guid Then
            Return True
        End If
        Return False
    End Function

    Public Function IsPng(fileName As String) As Boolean
        Dim GuidId = GetGuidID(fileName)
        If GuidId = ImageFormat.Png.Guid Then
            Return True
        End If
        Return False
    End Function

    Public Function IsJpeg(fileName As String) As Boolean
        Dim GuidId = GetGuidID(fileName)
        If GuidId = ImageFormat.Jpeg.Guid OrElse GuidId = ImageFormat.Exif.Guid Then
            Return True
        End If
        Return False
    End Function

    Public Function IsTiff(fileName As String) As Boolean
        Dim GuidId = GetGuidID(fileName)
        If GuidId = ImageFormat.Tiff.Guid Then
            Return True
        End If
        Return False
    End Function

    Public Function IsText(fileName As String) As Boolean
        Dim contentType As String = ""
        If Path.HasExtension(fileName) Then
            Dim extension As String = Path.GetExtension(fileName)
            Dim extensionWithoutDot As String = extension.Remove(0, 1).ToLower()
            m_mimeTypes.TryGetValue(extensionWithoutDot, contentType)
            Return If(contentType = "text/plain" AndAlso ScanFileForMimeType(fileName) = "text/plain", True, False)
        End If
        Return False
    End Function

    Public Function IsHtml(fileName As FileInfo) As Boolean
        Dim contentType As String = ""
        If Path.HasExtension(fileName.FullName) Then
            Dim extension As String = Path.GetExtension(fileName.FullName)
            Dim extensionWithoutDot As String = extension.Remove(0, 1).ToLower()
            m_mimeTypes.TryGetValue(extensionWithoutDot, contentType)
            Return If(contentType = "text/html" AndAlso ScanFileForMimeType(fileName.FullName) = "text/html", True, False)
        End If
        Return False
    End Function

    Public Function IsRar(fileName As String) As Boolean
        Dim contentType As String = ""
        If Path.HasExtension(fileName) Then
            Dim extension As String = Path.GetExtension(fileName)
            Dim extensionWithoutDot As String = extension.Remove(0, 1).ToLower()
            m_mimeTypes.TryGetValue(extensionWithoutDot, contentType)
            Dim RAR As Byte() = {82, 97, 114, 33, 26, 7, 0}
            Dim FileBytes = File.ReadAllBytes(fileName)
            Return If(contentType = "application/x-rar-compressed" AndAlso FileBytes.Take(7).SequenceEqual(RAR), True, False)
        End If
        Return False
    End Function

    <DllImport("urlmon.dll", CharSet:=CharSet.Auto)>
    Private Shared Function FindMimeFromData(pBC As UInteger, <MarshalAs(UnmanagedType.LPStr)> pwzUrl As String, <MarshalAs(UnmanagedType.LPArray)> pBuffer As Byte(), cbSize As UInteger, <MarshalAs(UnmanagedType.LPStr)> pwzMimeProposed As String, dwMimeFlags As UInteger,
    ByRef ppwzMimeOut As UInteger, dwReserverd As UInteger) As UInteger
    End Function

    Public Function ScanFileForMimeType(fileName As String) As String
        Dim fs As New FileStream(fileName, FileMode.Open)

        Try
            Dim buffer As Byte() = New Byte(255) {}

            Dim readLength As Integer = Convert.ToInt32(Math.Min(256, fs.Length))
            fs.Read(buffer, 0, readLength)

            'Using fs As New FileStream(fileName, FileMode.Open)
            '    Dim readLength As Integer = Convert.ToInt32(Math.Min(256, fs.Length))
            '    fs.Read(buffer, 0, readLength)
            'End Using

            Dim mimeType As UInteger = Nothing
            FindMimeFromData(0, Nothing, buffer, 256, Nothing, 0, mimeType, 0)
            Dim mimeTypePtr As New IntPtr(mimeType)
            Dim mime As String = Marshal.PtrToStringUni(mimeTypePtr)
            Marshal.FreeCoTaskMem(mimeTypePtr)
            Marshal.FreeHGlobal(mimeTypePtr)
            If String.IsNullOrEmpty(mime) Then
                mime = "application/octet-stream"
            End If
            Return mime
        Catch ex As Exception
            'MsgBox(ex.ToString)
            Return "application/octet-stream"
        Finally
            fs.Close()
            fs.Dispose()
        End Try
    End Function

    Private Sub InitializeMimeTypeLists()
        m_knownTypes = New List(Of String)() From {
        "text/plain",
        "text/html",
        "text/xml",
        "text/richtext",
        "text/scriptlet",
        "audio/x-aiff",
        "audio/basic",
        "audio/mid",
        "audio/wav",
        "image/gif",
        "image/jpeg",
        "image/pjpeg",
        "image/png",
        "image/x-png",
        "image/tiff",
        "image/bmp",
        "image/x-xbitmap",
        "image/x-jg",
        "image/x-emf",
        "image/x-wmf",
        "video/avi",
        "video/mpeg",
        "application/octet-stream",
        "application/postscript",
        "application/base64",
        "application/macbinhex40",
        "application/pdf",
        "application/xml",
        "application/atom+xml",
        "application/rss+xml",
        "application/x-compressed",
        "application/x-rar-compressed",
        "application/x-zip-compressed",
        "application/x-gzip-compressed",
        "application/java",
        "application/x-msdownload"
    }

        m_mimeTypes = New Dictionary(Of String, String)()
        m_mimeTypes.Add("3dm", "x-world/x-3dmf")
        m_mimeTypes.Add("3dmf", "x-world/x-3dmf")
        m_mimeTypes.Add("a", "application/octet-stream")
        m_mimeTypes.Add("aab", "application/x-authorware-bin")
        m_mimeTypes.Add("aam", "application/x-authorware-map")
        m_mimeTypes.Add("aas", "application/x-authorware-seg")
        m_mimeTypes.Add("abc", "text/vnd.abc")
        m_mimeTypes.Add("acgi", "text/html")
        m_mimeTypes.Add("afl", "video/animaflex")
        m_mimeTypes.Add("ai", "application/postscript")
        m_mimeTypes.Add("aif", "audio/aiff")
        m_mimeTypes.Add("aifc", "audio/aiff")
        m_mimeTypes.Add("aiff", "audio/aiff")
        m_mimeTypes.Add("aim", "application/x-aim")
        m_mimeTypes.Add("aip", "text/x-audiosoft-intra")
        m_mimeTypes.Add("ani", "application/x-navi-animation")
        m_mimeTypes.Add("aos", "application/x-nokia-9000-communicator-add-on-software")
        m_mimeTypes.Add("aps", "application/mime")
        m_mimeTypes.Add("arc", "application/octet-stream")
        m_mimeTypes.Add("arj", "application/arj")
        m_mimeTypes.Add("art", "image/x-jg")
        m_mimeTypes.Add("asf", "video/x-ms-asf")
        m_mimeTypes.Add("asm", "text/x-asm")
        m_mimeTypes.Add("asp", "text/asp")
        m_mimeTypes.Add("asx", "application/x-mplayer2")
        m_mimeTypes.Add("au", "audio/basic")
        m_mimeTypes.Add("avi", "video/avi")
        m_mimeTypes.Add("avs", "video/avs-video")
        m_mimeTypes.Add("bcpio", "application/x-bcpio")
        m_mimeTypes.Add("bin", "application/octet-stream")
        m_mimeTypes.Add("bm", "image/bmp")
        m_mimeTypes.Add("bmp", "image/bmp")
        m_mimeTypes.Add("boo", "application/book")
        m_mimeTypes.Add("book", "application/book")
        m_mimeTypes.Add("boz", "application/x-bzip2")
        m_mimeTypes.Add("bsh", "application/x-bsh")
        m_mimeTypes.Add("bz", "application/x-bzip")
        m_mimeTypes.Add("bz2", "application/x-bzip2")
        m_mimeTypes.Add("c", "text/plain")
        m_mimeTypes.Add("c++", "text/plain")
        m_mimeTypes.Add("cat", "application/vnd.ms-pki.seccat")
        m_mimeTypes.Add("cc", "text/plain")
        m_mimeTypes.Add("ccad", "application/clariscad")
        m_mimeTypes.Add("cco", "application/x-cocoa")
        m_mimeTypes.Add("cdf", "application/cdf")
        m_mimeTypes.Add("cer", "application/pkix-cert")
        m_mimeTypes.Add("cha", "application/x-chat")
        m_mimeTypes.Add("chat", "application/x-chat")
        m_mimeTypes.Add("class", "application/java")
        m_mimeTypes.Add("com", "application/octet-stream")
        m_mimeTypes.Add("conf", "text/plain")
        m_mimeTypes.Add("cpio", "application/x-cpio")
        m_mimeTypes.Add("cpp", "text/x-c")
        m_mimeTypes.Add("cpt", "application/x-cpt")
        m_mimeTypes.Add("crl", "application/pkcs-crl")
        m_mimeTypes.Add("css", "text/css")
        m_mimeTypes.Add("def", "text/plain")
        m_mimeTypes.Add("der", "application/x-x509-ca-cert")
        m_mimeTypes.Add("dif", "video/x-dv")
        m_mimeTypes.Add("dir", "application/x-director")
        m_mimeTypes.Add("dl", "video/dl")
        m_mimeTypes.Add("doc", "application/msword")
        m_mimeTypes.Add("docx", "application/msword")
        m_mimeTypes.Add("docm", "application/msword")
        m_mimeTypes.Add("dot", "application/msword")
        m_mimeTypes.Add("dp", "application/commonground")
        m_mimeTypes.Add("drw", "application/drafting")
        m_mimeTypes.Add("dump", "application/octet-stream")
        m_mimeTypes.Add("dv", "video/x-dv")
        m_mimeTypes.Add("dvi", "application/x-dvi")
        m_mimeTypes.Add("dwf", "drawing/x-dwf (old)")
        m_mimeTypes.Add("dwg", "application/acad")
        m_mimeTypes.Add("dxf", "application/dxf")
        m_mimeTypes.Add("eps", "application/postscript")
        m_mimeTypes.Add("es", "application/x-esrehber")
        m_mimeTypes.Add("etx", "text/x-setext")
        m_mimeTypes.Add("evy", "application/envoy")
        m_mimeTypes.Add("exe", "application/octet-stream")
        m_mimeTypes.Add("f", "text/plain")
        m_mimeTypes.Add("f90", "text/x-fortran")
        m_mimeTypes.Add("fdf", "application/vnd.fdf")
        m_mimeTypes.Add("fif", "image/fif")
        m_mimeTypes.Add("fli", "video/fli")
        m_mimeTypes.Add("flv", "video/x-flv")
        m_mimeTypes.Add("for", "text/x-fortran")
        m_mimeTypes.Add("fpx", "image/vnd.fpx")
        m_mimeTypes.Add("g", "text/plain")
        m_mimeTypes.Add("g3", "image/g3fax")
        m_mimeTypes.Add("gif", "image/gif")
        m_mimeTypes.Add("gl", "video/gl")
        m_mimeTypes.Add("gsd", "audio/x-gsm")
        m_mimeTypes.Add("gtar", "application/x-gtar")
        m_mimeTypes.Add("gz", "application/x-compressed")
        m_mimeTypes.Add("h", "text/plain")
        m_mimeTypes.Add("help", "application/x-helpfile")
        m_mimeTypes.Add("hgl", "application/vnd.hp-hpgl")
        m_mimeTypes.Add("hh", "text/plain")
        m_mimeTypes.Add("hlp", "application/x-winhelp")
        m_mimeTypes.Add("htc", "text/x-component")
        m_mimeTypes.Add("htm", "text/html")
        m_mimeTypes.Add("html", "text/html")
        m_mimeTypes.Add("htmls", "text/html")
        m_mimeTypes.Add("htt", "text/webviewhtml")
        m_mimeTypes.Add("htx", "text/html")
        m_mimeTypes.Add("ice", "x-conference/x-cooltalk")
        m_mimeTypes.Add("ico", "image/x-icon")
        m_mimeTypes.Add("idc", "text/plain")
        m_mimeTypes.Add("ief", "image/ief")
        m_mimeTypes.Add("iefs", "image/ief")
        m_mimeTypes.Add("iges", "application/iges")
        m_mimeTypes.Add("igs", "application/iges")
        m_mimeTypes.Add("ima", "application/x-ima")
        m_mimeTypes.Add("imap", "application/x-httpd-imap")
        m_mimeTypes.Add("inf", "application/inf")
        m_mimeTypes.Add("ins", "application/x-internett-signup")
        m_mimeTypes.Add("ip", "application/x-ip2")
        m_mimeTypes.Add("isu", "video/x-isvideo")
        m_mimeTypes.Add("it", "audio/it")
        m_mimeTypes.Add("iv", "application/x-inventor")
        m_mimeTypes.Add("ivr", "i-world/i-vrml")
        m_mimeTypes.Add("ivy", "application/x-livescreen")
        m_mimeTypes.Add("jam", "audio/x-jam")
        m_mimeTypes.Add("jav", "text/plain")
        m_mimeTypes.Add("java", "text/plain")
        m_mimeTypes.Add("jcm", "application/x-java-commerce")
        m_mimeTypes.Add("jfif", "image/jpeg")
        m_mimeTypes.Add("jfif-tbnl", "image/jpeg")
        m_mimeTypes.Add("jpe", "image/jpeg")
        m_mimeTypes.Add("jpeg", "image/jpeg")
        m_mimeTypes.Add("jpg", "image/jpeg")
        m_mimeTypes.Add("jps", "image/x-jps")
        m_mimeTypes.Add("js", "application/x-javascript")
        m_mimeTypes.Add("jut", "image/jutvision")
        m_mimeTypes.Add("kar", "audio/midi")
        m_mimeTypes.Add("ksh", "application/x-ksh")
        m_mimeTypes.Add("la", "audio/nspaudio")
        m_mimeTypes.Add("lam", "audio/x-liveaudio")
        m_mimeTypes.Add("latex", "application/x-latex")
        m_mimeTypes.Add("lha", "application/lha")
        m_mimeTypes.Add("lhx", "application/octet-stream")
        m_mimeTypes.Add("list", "text/plain")
        m_mimeTypes.Add("lma", "audio/nspaudio")
        m_mimeTypes.Add("log", "text/plain")
        m_mimeTypes.Add("lsp", "application/x-lisp")
        m_mimeTypes.Add("lst", "text/plain")
        m_mimeTypes.Add("lsx", "text/x-la-asf")
        m_mimeTypes.Add("ltx", "application/x-latex")
        m_mimeTypes.Add("lzh", "application/octet-stream")
        m_mimeTypes.Add("lzx", "application/lzx")
        m_mimeTypes.Add("m", "text/plain")
        m_mimeTypes.Add("m1v", "video/mpeg")
        m_mimeTypes.Add("m2a", "audio/mpeg")
        m_mimeTypes.Add("m2v", "video/mpeg")
        m_mimeTypes.Add("m3u", "audio/x-mpequrl")
        m_mimeTypes.Add("man", "application/x-troff-man")
        m_mimeTypes.Add("map", "application/x-navimap")
        m_mimeTypes.Add("mar", "text/plain")
        m_mimeTypes.Add("mbd", "application/mbedlet")
        m_mimeTypes.Add("mc$", "application/x-magic-cap-package-1.0")
        m_mimeTypes.Add("mcd", "application/mcad")
        m_mimeTypes.Add("mcf", "image/vasa")
        m_mimeTypes.Add("mcp", "application/netmc")
        m_mimeTypes.Add("me", "application/x-troff-me")
        m_mimeTypes.Add("mht", "message/rfc822")
        m_mimeTypes.Add("mhtml", "message/rfc822")
        m_mimeTypes.Add("mid", "audio/midi")
        m_mimeTypes.Add("midi", "audio/midi")
        m_mimeTypes.Add("mif", "application/x-frame")
        m_mimeTypes.Add("mime", "message/rfc822")
        m_mimeTypes.Add("mjf", "audio/x-vnd.audioexplosion.mjuicemediafile")
        m_mimeTypes.Add("mjpg", "video/x-motion-jpeg")
        m_mimeTypes.Add("mm", "application/base64")
        m_mimeTypes.Add("mme", "application/base64")
        m_mimeTypes.Add("mod", "audio/mod")
        m_mimeTypes.Add("moov", "video/quicktime")
        m_mimeTypes.Add("mov", "video/quicktime")
        m_mimeTypes.Add("movie", "video/x-sgi-movie")
        m_mimeTypes.Add("mp2", "audio/mpeg")
        m_mimeTypes.Add("mp3", "audio/mpeg3")
        m_mimeTypes.Add("mpa", "audio/mpeg")
        m_mimeTypes.Add("mpc", "application/x-project")
        m_mimeTypes.Add("mpe", "video/mpeg")
        m_mimeTypes.Add("mpeg", "video/mpeg")
        m_mimeTypes.Add("mpg", "video/mpeg")
        m_mimeTypes.Add("mpga", "audio/mpeg")
        m_mimeTypes.Add("mpp", "application/vnd.ms-project")
        m_mimeTypes.Add("mpt", "application/x-project")
        m_mimeTypes.Add("mpv", "application/x-project")
        m_mimeTypes.Add("mpx", "application/x-project")
        m_mimeTypes.Add("mrc", "application/marc")
        m_mimeTypes.Add("ms", "application/x-troff-ms")
        m_mimeTypes.Add("mv", "video/x-sgi-movie")
        m_mimeTypes.Add("my", "audio/make")
        m_mimeTypes.Add("mzz", "application/x-vnd.audioexplosion.mzz")
        m_mimeTypes.Add("nap", "image/naplps")
        m_mimeTypes.Add("naplps", "image/naplps")
        m_mimeTypes.Add("nc", "application/x-netcdf")
        m_mimeTypes.Add("ncm", "application/vnd.nokia.configuration-message")
        m_mimeTypes.Add("nif", "image/x-niff")
        m_mimeTypes.Add("niff", "image/x-niff")
        m_mimeTypes.Add("nix", "application/x-mix-transfer")
        m_mimeTypes.Add("nsc", "application/x-conference")
        m_mimeTypes.Add("nvd", "application/x-navidoc")
        m_mimeTypes.Add("o", "application/octet-stream")
        m_mimeTypes.Add("oda", "application/oda")
        m_mimeTypes.Add("omc", "application/x-omc")
        m_mimeTypes.Add("omcd", "application/x-omcdatamaker")
        m_mimeTypes.Add("omcr", "application/x-omcregerator")
        m_mimeTypes.Add("p", "text/x-pascal")
        m_mimeTypes.Add("p10", "application/pkcs10")
        m_mimeTypes.Add("p12", "application/pkcs-12")
        m_mimeTypes.Add("p7a", "application/x-pkcs7-signature")
        m_mimeTypes.Add("p7c", "application/pkcs7-mime")
        m_mimeTypes.Add("pas", "text/pascal")
        m_mimeTypes.Add("pbm", "image/x-portable-bitmap")
        m_mimeTypes.Add("pcl", "application/vnd.hp-pcl")
        m_mimeTypes.Add("pct", "image/x-pict")
        m_mimeTypes.Add("pcx", "image/x-pcx")
        m_mimeTypes.Add("pdf", "application/pdf")
        m_mimeTypes.Add("pfunk", "audio/make")
        m_mimeTypes.Add("pgm", "image/x-portable-graymap")
        m_mimeTypes.Add("pic", "image/pict")
        m_mimeTypes.Add("pict", "image/pict")
        m_mimeTypes.Add("pkg", "application/x-newton-compatible-pkg")
        m_mimeTypes.Add("pko", "application/vnd.ms-pki.pko")
        m_mimeTypes.Add("pl", "text/plain")
        m_mimeTypes.Add("plx", "application/x-pixclscript")
        m_mimeTypes.Add("pm", "image/x-xpixmap")
        m_mimeTypes.Add("png", "image/png")
        m_mimeTypes.Add("pnm", "application/x-portable-anymap")
        m_mimeTypes.Add("pot", "application/mspowerpoint")
        m_mimeTypes.Add("pov", "model/x-pov")
        m_mimeTypes.Add("ppa", "application/vnd.ms-powerpoint")
        m_mimeTypes.Add("ppm", "image/x-portable-pixmap")
        m_mimeTypes.Add("pps", "application/mspowerpoint")
        m_mimeTypes.Add("ppt", "application/mspowerpoint")
        m_mimeTypes.Add("pptx", "application/mspowerpoint")
        m_mimeTypes.Add("ppz", "application/mspowerpoint")
        m_mimeTypes.Add("pre", "application/x-freelance")
        m_mimeTypes.Add("prt", "application/pro_eng")
        m_mimeTypes.Add("ps", "application/postscript")
        m_mimeTypes.Add("psd", "application/octet-stream")
        m_mimeTypes.Add("pvu", "paleovu/x-pv")
        m_mimeTypes.Add("pwz", "application/vnd.ms-powerpoint")
        m_mimeTypes.Add("py", "text/x-script.phyton")
        m_mimeTypes.Add("pyc", "applicaiton/x-bytecode.python")
        m_mimeTypes.Add("qcp", "audio/vnd.qcelp")
        m_mimeTypes.Add("qd3", "x-world/x-3dmf")
        m_mimeTypes.Add("qd3d", "x-world/x-3dmf")
        m_mimeTypes.Add("qif", "image/x-quicktime")
        m_mimeTypes.Add("qt", "video/quicktime")
        m_mimeTypes.Add("qtc", "video/x-qtc")
        m_mimeTypes.Add("qti", "image/x-quicktime")
        m_mimeTypes.Add("qtif", "image/x-quicktime")
        m_mimeTypes.Add("ra", "audio/x-pn-realaudio")
        m_mimeTypes.Add("ram", "audio/x-pn-realaudio")
        m_mimeTypes.Add("rar", "application/x-rar-compressed")
        m_mimeTypes.Add("ras", "application/x-cmu-raster")
        m_mimeTypes.Add("rast", "image/cmu-raster")
        m_mimeTypes.Add("rexx", "text/x-script.rexx")
        m_mimeTypes.Add("rf", "image/vnd.rn-realflash")
        m_mimeTypes.Add("rgb", "image/x-rgb")
        m_mimeTypes.Add("rm", "application/vnd.rn-realmedia")
        m_mimeTypes.Add("rmi", "audio/mid")
        m_mimeTypes.Add("rmm", "audio/x-pn-realaudio")
        m_mimeTypes.Add("rmp", "audio/x-pn-realaudio")
        m_mimeTypes.Add("rng", "application/ringing-tones")
        m_mimeTypes.Add("rnx", "application/vnd.rn-realplayer")
        m_mimeTypes.Add("roff", "application/x-troff")
        m_mimeTypes.Add("rp", "image/vnd.rn-realpix")
        m_mimeTypes.Add("rpm", "audio/x-pn-realaudio-plugin")
        m_mimeTypes.Add("rt", "text/richtext")
        m_mimeTypes.Add("rtf", "text/richtext")
        m_mimeTypes.Add("rtx", "application/rtf")
        m_mimeTypes.Add("rv", "video/vnd.rn-realvideo")
        m_mimeTypes.Add("s", "text/x-asm")
        m_mimeTypes.Add("s3m", "audio/s3m")
        m_mimeTypes.Add("saveme", "application/octet-stream")
        m_mimeTypes.Add("sbk", "application/x-tbook")
        m_mimeTypes.Add("scm", "application/x-lotusscreencam")
        m_mimeTypes.Add("sdml", "text/plain")
        m_mimeTypes.Add("sdp", "application/sdp")
        m_mimeTypes.Add("sdr", "application/sounder")
        m_mimeTypes.Add("sea", "application/sea")
        m_mimeTypes.Add("set", "application/set")
        m_mimeTypes.Add("sgm", "text/sgml")
        m_mimeTypes.Add("sgml", "text/sgml")
        m_mimeTypes.Add("sh", "application/x-bsh")
        m_mimeTypes.Add("shtml", "text/html")
        m_mimeTypes.Add("sid", "audio/x-psid")
        m_mimeTypes.Add("sit", "application/x-sit")
        m_mimeTypes.Add("skd", "application/x-koan")
        m_mimeTypes.Add("skm", "application/x-koan")
        m_mimeTypes.Add("skp", "application/x-koan")
        m_mimeTypes.Add("skt", "application/x-koan")
        m_mimeTypes.Add("sl", "application/x-seelogo")
        m_mimeTypes.Add("smi", "application/smil")
        m_mimeTypes.Add("smil", "application/smil")
        m_mimeTypes.Add("snd", "audio/basic")
        m_mimeTypes.Add("sol", "application/solids")
        m_mimeTypes.Add("spc", "application/x-pkcs7-certificates")
        m_mimeTypes.Add("spl", "application/futuresplash")
        m_mimeTypes.Add("spr", "application/x-sprite")
        m_mimeTypes.Add("sprite", "application/x-sprite")
        m_mimeTypes.Add("src", "application/x-wais-source")
        m_mimeTypes.Add("ssi", "text/x-server-parsed-html")
        m_mimeTypes.Add("ssm", "application/streamingmedia")
        m_mimeTypes.Add("sst", "application/vnd.ms-pki.certstore")
        m_mimeTypes.Add("step", "application/step")
        m_mimeTypes.Add("stl", "application/sla")
        m_mimeTypes.Add("stp", "application/step")
        m_mimeTypes.Add("sv4cpio", "application/x-sv4cpio")
        m_mimeTypes.Add("sv4crc", "application/x-sv4crc")
        m_mimeTypes.Add("svf", "image/vnd.dwg")
        m_mimeTypes.Add("svr", "application/x-world")
        m_mimeTypes.Add("swf", "application/x-shockwave-flash")
        m_mimeTypes.Add("t", "application/x-troff")
        m_mimeTypes.Add("talk", "text/x-speech")
        m_mimeTypes.Add("tar", "application/x-tar")
        m_mimeTypes.Add("tbk", "application/toolbook")
        m_mimeTypes.Add("tcl", "application/x-tcl")
        m_mimeTypes.Add("tcsh", "text/x-script.tcsh")
        m_mimeTypes.Add("tex", "application/x-tex")
        m_mimeTypes.Add("texi", "application/x-texinfo")
        m_mimeTypes.Add("texinfo", "application/x-texinfo")
        m_mimeTypes.Add("text", "text/plain")
        m_mimeTypes.Add("tgz", "application/x-compressed")
        m_mimeTypes.Add("tif", "image/tiff")
        m_mimeTypes.Add("tr", "application/x-troff")
        m_mimeTypes.Add("tsi", "audio/tsp-audio")
        m_mimeTypes.Add("tsp", "audio/tsplayer")
        m_mimeTypes.Add("tsv", "text/tab-separated-values")
        m_mimeTypes.Add("turbot", "image/florian")
        m_mimeTypes.Add("txt", "text/plain")
        m_mimeTypes.Add("uil", "text/x-uil")
        m_mimeTypes.Add("uni", "text/uri-list")
        m_mimeTypes.Add("unis", "text/uri-list")
        m_mimeTypes.Add("unv", "application/i-deas")
        m_mimeTypes.Add("uri", "text/uri-list")
        m_mimeTypes.Add("uris", "text/uri-list")
        m_mimeTypes.Add("ustar", "application/x-ustar")
        m_mimeTypes.Add("uu", "application/octet-stream")
        m_mimeTypes.Add("vcd", "application/x-cdlink")
        m_mimeTypes.Add("vcs", "text/x-vcalendar")
        m_mimeTypes.Add("vda", "application/vda")
        m_mimeTypes.Add("vdo", "video/vdo")
        m_mimeTypes.Add("vew", "application/groupwise")
        m_mimeTypes.Add("viv", "video/vivo")
        m_mimeTypes.Add("vivo", "video/vivo")
        m_mimeTypes.Add("vmd", "application/vocaltec-media-desc")
        m_mimeTypes.Add("vmf", "application/vocaltec-media-file")
        m_mimeTypes.Add("voc", "audio/voc")
        m_mimeTypes.Add("vos", "video/vosaic")
        m_mimeTypes.Add("vox", "audio/voxware")
        m_mimeTypes.Add("vqe", "audio/x-twinvq-plugin")
        m_mimeTypes.Add("vqf", "audio/x-twinvq")
        m_mimeTypes.Add("vql", "audio/x-twinvq-plugin")
        m_mimeTypes.Add("vrml", "application/x-vrml")
        m_mimeTypes.Add("vrt", "x-world/x-vrt")
        m_mimeTypes.Add("vsd", "application/x-visio")
        m_mimeTypes.Add("vst", "application/x-visio")
        m_mimeTypes.Add("vsw", "application/x-visio")
        m_mimeTypes.Add("w60", "application/wordperfect6.0")
        m_mimeTypes.Add("w61", "application/wordperfect6.1")
        m_mimeTypes.Add("w6w", "application/msword")
        m_mimeTypes.Add("wav", "audio/wav")
        m_mimeTypes.Add("wb1", "application/x-qpro")
        m_mimeTypes.Add("wbmp", "image/vnd.wap.wbmp")
        m_mimeTypes.Add("web", "application/vnd.xara")
        m_mimeTypes.Add("wiz", "application/msword")
        m_mimeTypes.Add("wk1", "application/x-123")
        m_mimeTypes.Add("wmf", "windows/metafile")
        m_mimeTypes.Add("wml", "text/vnd.wap.wml")
        m_mimeTypes.Add("wmlc", "application/vnd.wap.wmlc")
        m_mimeTypes.Add("wmls", "text/vnd.wap.wmlscript")
        m_mimeTypes.Add("wmlsc", "application/vnd.wap.wmlscriptc")
        m_mimeTypes.Add("word", "application/msword")
        m_mimeTypes.Add("wp", "application/wordperfect")
        m_mimeTypes.Add("wp5", "application/wordperfect")
        m_mimeTypes.Add("wp6", "application/wordperfect")
        m_mimeTypes.Add("wpd", "application/wordperfect")
        m_mimeTypes.Add("wq1", "application/x-lotus")
        m_mimeTypes.Add("wri", "application/mswrite")
        m_mimeTypes.Add("wrl", "application/x-world")
        m_mimeTypes.Add("wrz", "model/vrml")
        m_mimeTypes.Add("wsc", "text/scriplet")
        m_mimeTypes.Add("wsrc", "application/x-wais-source")
        m_mimeTypes.Add("wtk", "application/x-wintalk")
        m_mimeTypes.Add("xbm", "image/x-xbitmap")
        m_mimeTypes.Add("xdr", "video/x-amt-demorun")
        m_mimeTypes.Add("xgz", "xgl/drawing")
        m_mimeTypes.Add("xif", "image/vnd.xiff")
        m_mimeTypes.Add("xl", "application/excel")
        m_mimeTypes.Add("xla", "application/excel")
        m_mimeTypes.Add("xlb", "application/excel")
        m_mimeTypes.Add("xlc", "application/excel")
        m_mimeTypes.Add("xld", "application/excel")
        m_mimeTypes.Add("xlk", "application/excel")
        m_mimeTypes.Add("xll", "application/excel")
        m_mimeTypes.Add("xlm", "application/excel")
        m_mimeTypes.Add("xls", "application/excel")
        m_mimeTypes.Add("xlsx", "application/excel")
        m_mimeTypes.Add("xlt", "application/excel")
        m_mimeTypes.Add("xlv", "application/excel")
        m_mimeTypes.Add("xlw", "application/excel")
        m_mimeTypes.Add("xm", "audio/xm")
        m_mimeTypes.Add("xml", "text/xml")
        m_mimeTypes.Add("xmz", "xgl/movie")
        m_mimeTypes.Add("xpix", "application/x-vnd.ls-xpix")
        m_mimeTypes.Add("xpm", "image/x-xpixmap")
        m_mimeTypes.Add("x-png", "image/png")
        m_mimeTypes.Add("xsr", "video/x-amt-showrun")
        m_mimeTypes.Add("xwd", "image/x-xwd")
        m_mimeTypes.Add("xyz", "chemical/x-pdb")
        m_mimeTypes.Add("z", "application/x-compress")
        m_mimeTypes.Add("zip", "application/x-compressed")
        m_mimeTypes.Add("zoo", "application/octet-stream")
        m_mimeTypes.Add("zsh", "text/x-script.zsh")
    End Sub
#End Region

End Class