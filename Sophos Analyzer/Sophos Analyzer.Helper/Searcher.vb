﻿Imports System.IO
Imports System.Runtime.InteropServices

Public Class Searcher

#Region " Fields "
    Private Shared INVALID_HANDLE_VALUE As New IntPtr(-1)
#End Region

#Region " Delegates "
    Public Delegate Function FileCallbacks(path As FileInformations) As Boolean
#End Region

#Region " Methods "

    Public Class FileInformations
        Public Path As String
        Public ModifiedDate As Date
        Public CreatedDate As Date
    End Class

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)>
    Public Structure WIN32_FIND_DATAW
        Public dwFileAttributes As FileAttributes
        Friend ftCreationTime As ComTypes.FILETIME
        Friend ftLastAccessTime As ComTypes.FILETIME
        Friend ftLastWriteTime As ComTypes.FILETIME
        Public nFileSizeHigh As Integer
        Public nFileSizeLow As Integer
        Public dwReserved0 As Integer
        Public dwReserved1 As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=260)>
        Public cFileName As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=14)>
        Public cAlternateFileName As String

        Public Function HasBitFlag(Of T As IConvertible)(other As T) As Boolean
            Dim eFlag = Convert.ToUInt64(dwFileAttributes)
            Dim otherFlag = Convert.ToUInt64(other)
            Return ((eFlag And otherFlag) = otherFlag)
        End Function
    End Structure

    <DllImport("kernel32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Public Shared Function FindFirstFileW(lpFileName As String, <Out> ByRef lpFindFileData As WIN32_FIND_DATAW) As IntPtr
    End Function

    <DllImport("kernel32.dll", CharSet:=CharSet.Unicode)>
    Public Shared Function FindNextFile(hFindFile As IntPtr, <Out> ByRef lpFindFileData As WIN32_FIND_DATAW) As Boolean
    End Function

    <DllImport("kernel32.dll")>
    Public Shared Function FindClose(hFindFile As IntPtr) As Boolean
    End Function

    Public Shared Function RecursiveScan(directory As String, fPath As FileCallbacks) As Boolean
        If String.IsNullOrEmpty(directory) Then Throw New ArgumentNullException(NameOf(directory))
        If fPath Is Nothing Then Throw New ArgumentNullException(NameOf(fPath))

        Dim INVALID_HANDLE_VALUE As IntPtr = New IntPtr(-1)
        Dim findData As WIN32_FIND_DATAW = Nothing
        Dim findHandle As IntPtr = INVALID_HANDLE_VALUE
        Try
            findHandle = FindFirstFileW(directory & "\*", findData)
            If findHandle <> INVALID_HANDLE_VALUE Then
                Dim TotalFiles As Integer = 0
                Do
                    If findData.cFileName = "." OrElse findData.cFileName = ".." Then Continue Do
                    Dim fullpath As String = directory & (If(directory.EndsWith("\"), "", "\")) + findData.cFileName
                    Dim isDir As Boolean
                    If findData.HasBitFlag(FileAttributes.Directory) AndAlso Not findData.HasBitFlag(FileAttributes.ReparsePoint) Then
                        isDir = True
                        If RecursiveScan(fullpath, fPath) = False Then
                            Return False
                        End If
                    End If

                    If isDir = False Then
                        If Not fPath Is Nothing Then
                            Dim fileInfos = New FileInformations() With {.CreatedDate = ToDateTime(findData.ftCreationTime), .ModifiedDate = ToDateTime(findData.ftLastWriteTime), .Path = fullpath}
                            If fPath(fileInfos) = False Then
                                Return False
                            End If
                        Else
                            Return False
                        End If
                    End If
                Loop While FindNextFile(findHandle, findData)
            End If

            Return True
        Catch ex As Exception
            Return False
        Finally
            If findHandle <> INVALID_HANDLE_VALUE Then FindClose(findHandle)
        End Try
    End Function

    Private Shared Function ToDateTime(time As ComTypes.FILETIME) As Date
        Dim high = CLng(time.dwHighDateTime)
        Dim low = CLng(time.dwLowDateTime)
        Dim fileTime = CLng((high << 32) + low)
        Return Date.FromFileTimeUtc(fileTime)
    End Function
#End Region

End Class
