﻿Imports System.Text
Imports Sophos_Analyzer.Win32

Public NotInheritable Class HookWindow

#Region " Methods "
    Public Shared Sub CloseLibrariesWindow()
        For Each hnd In EnumerateProcessWindowHandles(Process.GetProcessesByName("explorer").First().Id)
            Dim mess As New StringBuilder(1000)
            NativeMethods.SendMessage(hnd, NativeConstants.WM_GETTEXT, mess.Capacity, mess)
            If mess.ToString.ToLower.Contains("Libraries") OrElse mess.ToString.Contains("Bibliothèques") Then
                NativeMethods.PostMessage(hnd, NativeConstants.WM_CLOSE, 0, 0)
            End If
        Next
    End Sub

    Private Shared Function EnumerateProcessWindowHandles(processId As Integer) As IEnumerable(Of IntPtr)
        Dim hnds = New List(Of IntPtr)()

        For Each Thr As ProcessThread In Process.GetProcessById(processId).Threads
            NativeMethods.EnumThreadWindows(Thr.Id, Function(hWnd, lParam)
                                                        hnds.Add(hWnd)
                                                        Return True
                                                    End Function, IntPtr.Zero)
        Next

        Return hnds
    End Function
#End Region

End Class
