﻿Imports System.Drawing
Imports MetroFramework.Controls

Public NotInheritable Class Randomizer

#Region " Fields "
    Private Shared m_alphabeticChars As Char()() = {"0123456789abcdefghijklmnopqrstuvwxyzれづれなるまゝに日暮らし硯にむかひて心にうりゆくよな事を、こはかとなく書きつくればあやうこそものぐるほけれ。αβγδεζηθικλµνξοπρστυϕχψω☹☺☻☼☽☾☿♀♁♂♔♕♖♗♘♙♚♛♜♝♞♟♠♡♢♣♤♥♦♧♩♪♫♬♭♮♯♻♼♿⚐⚑⚒⚓⚔⚕⚖⚠⚢⚣⚤⚥⚦⚧⚨⚩⛄⛅⚽✔✓✕✖✗✘✝✞✟❗❓❤☸".ToCharArray()}
    Private Shared m_Numerics As Char()() = {"0123456789".ToCharArray()}
#End Region

#Region " Methods "
    ''' <summary>
    ''' Génère un mot de passe pseudo-aléatoire avec une longueur définie de 5 à 20 caractères
    ''' </summary>
    Private Shared Function GenerateKey(length%, Optional ByVal NumericChars As Boolean = False) As String
        Dim csp As New Security.Cryptography.RNGCryptoServiceProvider()

        Dim allchars = If(NumericChars, m_Numerics.Take(1).SelectMany(Function(c) c).ToArray(), m_alphabeticChars.Take(1).SelectMany(Function(c) c).ToArray())
        Dim bytes = New Byte(allchars.Length - 1) {}
        csp.GetBytes(bytes)

        For i% = 0 To allchars.Length - 1
            Dim tmp = allchars(i)
            allchars(i) = allchars(bytes(i) Mod allchars.Length)
            allchars(bytes(i) Mod allchars.Length) = tmp
        Next
        Array.Resize(bytes, length)
        Dim result = New Char(length - 1) {}
        While True
            csp.GetBytes(bytes)
            For i% = 0 To length - 1
                result(i) = allchars(bytes(i) Mod allchars.Length)
            Next
            If Char.IsWhiteSpace(result(0)) OrElse Char.IsWhiteSpace(result((length - 1) Mod length)) Then
                Continue While
            End If

            Dim testResult As New String(result)
            If 0 <> If(NumericChars, m_Numerics.Take(1).Count(Function(c) testResult.IndexOfAny(c) < 0), m_alphabeticChars.Take(1).Count(Function(c) testResult.IndexOfAny(c) < 0)) Then
                Continue While
            End If

            Return testResult
        End While
        Return If(NumericChars, m_Numerics.Take(1).ToString, m_alphabeticChars.Take(1).ToString)
    End Function

    ''' <summary>
    ''' On génère un mot de passe pseudo-aléatoire et on l'affiche dans le champs .Text d'une MetroTextBox.
    ''' </summary>
    Public Shared Sub GenerateOperatorPassword(Txb As MetroTextBox)
        Txb.Text = GenerateKey(4, True)
    End Sub

    ''' <summary>
    ''' On retourne une couleur générée aléatoirement.
    ''' </summary>
    Public Shared Function GetColor(randomGen As Random) As Color
        Dim names As KnownColor() = DirectCast(System.Enum.GetValues(GetType(KnownColor)), KnownColor())
        Dim randomColorName As KnownColor = names(randomGen.[Next](names.Length))
        Return Color.FromKnownColor(randomColorName)
    End Function

#End Region

End Class
