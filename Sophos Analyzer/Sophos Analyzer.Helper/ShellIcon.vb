﻿Imports System.Drawing
Imports System.Runtime.InteropServices

''' <summary>
''' Provides static methods to read system icons for both folders and files.
''' </summary>
''' <example>
''' <code>IconReader.GetFileIcon("c:\\general.xls");</code>
''' </example>
Public NotInheritable Class ShellIcon
    Private Sub New()
    End Sub

    ''' <summary>
    ''' Returns an icon for a given file - indicated by the name parameter.
    ''' </summary>
    ''' <param name="name">Pathname for file.</param>
    ''' <param name="size">Large or small</param>
    ''' <param name="linkOverlay">Whether to include the link icon</param>
    ''' <returns>System.Drawing.Icon</returns>
    Public Shared Function GetFileIcon(name As String, size As IconSize, linkOverlay As Boolean) As Icon
        Dim shfi As New Shell32.SHFILEINFO()
        Dim flags As UInteger = Shell32.SHGFI_ICON Or Shell32.SHGFI_USEFILEATTRIBUTES

        If True = linkOverlay Then
            flags = flags Or Shell32.SHGFI_LINKOVERLAY
        End If

        ' Check the size specified for return. 

        If IconSize.Small = size Then
            flags = flags Or Shell32.SHGFI_SMALLICON
        Else
            flags = flags Or Shell32.SHGFI_LARGEICON
        End If

        Shell32.SHGetFileInfo(name, Shell32.FILE_ATTRIBUTE_NORMAL, shfi, CUInt(Marshal.SizeOf(shfi)), flags)

        ' Copy (clone) the returned icon to a new object, thus allowing us to clean-up properly
        Dim icon As Icon = DirectCast(Icon.FromHandle(shfi.hIcon).Clone(), Icon)
        User32.DestroyIcon(shfi.hIcon)
        ' Cleanup
        Return icon
    End Function

    ''' <summary>
    ''' Used to access system folder icons.
    ''' </summary>
    ''' <param name="size">Specify large or small icons.</param>
    ''' <param name="folderType__1">Specify open or closed FolderType.</param>
    ''' <returns>System.Drawing.Icon</returns>
    Public Shared Function GetFolderIcon(size As IconSize, folderType__1 As FolderType) As Icon
        ' Need to add size check, although errors generated at present!
        Dim flags As UInteger = Shell32.SHGFI_ICON Or Shell32.SHGFI_USEFILEATTRIBUTES

        If FolderType.Open = folderType__1 Then
            flags = flags Or Shell32.SHGFI_OPENICON
        End If

        If IconSize.Small = size Then
            flags = flags Or Shell32.SHGFI_SMALLICON
        Else
            flags = flags Or Shell32.SHGFI_LARGEICON
        End If

        ' Get the folder icon
        Dim shfi As New Shell32.SHFILEINFO()
        Shell32.SHGetFileInfo(System.Environment.CurrentDirectory, Shell32.FILE_ATTRIBUTE_DIRECTORY, shfi, CUInt(Marshal.SizeOf(shfi)), flags)

        Icon.FromHandle(shfi.hIcon)
        ' Load the icon from an HICON handle
        ' Now clone the icon, so that it can be successfully stored in an ImageList
        Dim ic As Icon = DirectCast(Icon.FromHandle(shfi.hIcon).Clone(), Icon)

        User32.DestroyIcon(shfi.hIcon)
        ' Cleanup
        Return ic
    End Function
End Class

''' <summary>
''' Options to specify the size of icons to return.
''' </summary>
Public Enum IconSize
        ''' <summary>
        ''' Specify large icon - 32 pixels by 32 pixels.
        ''' </summary>
        Large = 0
        ''' <summary>
        ''' Specify small icon - 16 pixels by 16 pixels.
        ''' </summary>
        Small = 1
    End Enum

    ''' <summary>
    ''' Options to specify whether folders should be in the open or closed state.
    ''' </summary>
    Public Enum FolderType
        ''' <summary>
        ''' Specify closed folder.
        ''' </summary>
        Closed = 0
        ''' <summary>
        ''' Specify Open folder.
        ''' </summary>
        Open = 1
    End Enum

''' <summary>
''' Wraps necessary Shell32.dll structures and functions required to retrieve Icon Handles using SHGetFileInfo. Code
''' courtesy of MSDN Cold Rooster Consulting case study.
''' </summary>
Public NotInheritable Class Shell32
        Private Sub New()
        End Sub

        Public Const MAX_PATH As Integer = 256

        <StructLayout(LayoutKind.Sequential)>
        Public Structure SHITEMID
            Public cb As UShort
            <MarshalAs(UnmanagedType.LPArray)>
            Public abID As Byte()
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure ITEMIDLIST
            Public mkid As SHITEMID
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure BROWSEINFO
            Public hwndOwner As IntPtr
            Public pidlRoot As IntPtr
            Public pszDisplayName As IntPtr
            <MarshalAs(UnmanagedType.LPTStr)>
            Public lpszTitle As String
            Public ulFlags As UInteger
            Public lpfn As IntPtr
            Public lParam As Integer
            Public iImage As IntPtr
        End Structure

        ' Browsing for directory.
        Public Const BIF_RETURNONLYFSDIRS As UInteger = &H1
        Public Const BIF_DONTGOBELOWDOMAIN As UInteger = &H2
        Public Const BIF_STATUSTEXT As UInteger = &H4
        Public Const BIF_RETURNFSANCESTORS As UInteger = &H8
        Public Const BIF_EDITBOX As UInteger = &H10
        Public Const BIF_VALIDATE As UInteger = &H20
        Public Const BIF_NEWDIALOGSTYLE As UInteger = &H40
        Public Const BIF_USENEWUI As UInteger = (BIF_NEWDIALOGSTYLE Or BIF_EDITBOX)
        Public Const BIF_BROWSEINCLUDEURLS As UInteger = &H80
        Public Const BIF_BROWSEFORCOMPUTER As UInteger = &H1000
        Public Const BIF_BROWSEFORPRINTER As UInteger = &H2000
        Public Const BIF_BROWSEINCLUDEFILES As UInteger = &H4000
        Public Const BIF_SHAREABLE As UInteger = &H8000

        <StructLayout(LayoutKind.Sequential)>
        Public Structure SHFILEINFO
            Public Const NAMESIZE As Integer = 80
            Public hIcon As IntPtr
            Public iIcon As Integer
            Public dwAttributes As UInteger
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PATH)>
            Public szDisplayName As String
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=NAMESIZE)>
            Public szTypeName As String
        End Structure

        Public Const SHGFI_ICON As UInteger = &H100
        ' get icon
        Public Const SHGFI_DISPLAYNAME As UInteger = &H200
        ' get display name
        Public Const SHGFI_TYPENAME As UInteger = &H400
        ' get type name
        Public Const SHGFI_ATTRIBUTES As UInteger = &H800
        ' get attributes
        Public Const SHGFI_ICONLOCATION As UInteger = &H1000
        ' get icon location
        Public Const SHGFI_EXETYPE As UInteger = &H2000
        ' return exe type
        Public Const SHGFI_SYSICONINDEX As UInteger = &H4000
        ' get system icon index
        Public Const SHGFI_LINKOVERLAY As UInteger = &H8000
        ' put a link overlay on icon
        Public Const SHGFI_SELECTED As UInteger = &H10000
        ' show icon in selected state
        Public Const SHGFI_ATTR_SPECIFIED As UInteger = &H20000
        ' get only specified attributes
        Public Const SHGFI_LARGEICON As UInteger = &H0
        ' get large icon
        Public Const SHGFI_SMALLICON As UInteger = &H1
        ' get small icon
        Public Const SHGFI_OPENICON As UInteger = &H2
        ' get open icon
        Public Const SHGFI_SHELLICONSIZE As UInteger = &H4
        ' get shell size icon
        Public Const SHGFI_PIDL As UInteger = &H8
        ' pszPath is a pidl
        Public Const SHGFI_USEFILEATTRIBUTES As UInteger = &H10
        ' use passed dwFileAttribute
        Public Const SHGFI_ADDOVERLAYS As UInteger = &H20
        ' apply the appropriate overlays
        Public Const SHGFI_OVERLAYINDEX As UInteger = &H40
        ' Get the index of the overlay
        Public Const FILE_ATTRIBUTE_DIRECTORY As UInteger = &H10
        Public Const FILE_ATTRIBUTE_NORMAL As UInteger = &H80

        <DllImport("Shell32.dll")>
        Public Shared Function SHGetFileInfo(pszPath As String, dwFileAttributes As UInteger, ByRef psfi As SHFILEINFO, cbFileInfo As UInteger, uFlags As UInteger) As IntPtr
        End Function
    End Class

''' <summary>
''' Wraps necessary functions imported from User32.dll. Code courtesy of MSDN Cold Rooster Consulting example.
''' </summary>
Public Class User32
    ''' <summary>
    ''' Provides access to function required to delete handle. This method is used internally
    ''' and is not required to be called separately.
    ''' </summary>
    ''' <param name="hIcon">Pointer to icon handle.</param>
    ''' <returns>N/A</returns>
    <DllImport("User32.dll")>
    Public Shared Function DestroyIcon(hIcon As IntPtr) As Integer
    End Function
End Class

'0   SI_UNKNOWN	Unknown File Type
'1   SI_DEF_DOCUMENT	Default document
'2   SI_DEF_APPLICATION	Default application
'3   SI_FOLDER_CLOSED	Closed folder
'4   SI_FOLDER_OPEN	Open folder
'5   SI_FLOPPY_514	5 1/4 floppy
'6   SI_FLOPPY_35	3 1/2 floppy
'7   SI_REMOVABLE	Removable drive
'8   SI_HDD	Hard disk drive
'9   SI_NETWORKDRIVE	Network drive
'10  SI_NETWORKDRIVE_DISCONNECTED	network drive offline
'11  SI_CDROM	CD drive
'12  SI_RAMDISK	RAM disk
'13  SI_NETWORK	Entire network
'14      ?
'15  SI_MYCOMPUTER	My Computer
'16  SI_PRINTMANAGER	Printer Manager
'17  SI_NETWORK_NEIGHBORHOOD	Network Neighborhood
'18  SI_NETWORK_WORKGROUP	Network Workgroup
'19  SI_STARTMENU_PROGRAMS	Start Menu Programs
'20  SI_STARTMENU_DOCUMENTS	Start Menu Documents
'21  SI_STARTMENU_SETTINGS	Start Menu Settings
'22  SI_STARTMENU_FIND	Start Menu Find
'23  SI_STARTMENU_HELP	Start Menu Help
'24  SI_STARTMENU_RUN	Start Menu Run
'25  SI_STARTMENU_SUSPEND	Start Menu Suspend
'26  SI_STARTMENU_DOCKING	Start Menu Docking
'27  SI_STARTMENU_SHUTDOWN	Start Menu Shutdown
'28  SI_SHARE	Sharing overlay (hand)
'29  SI_SHORTCUT	Shortcut overlay (small arrow)
'30  SI_PRINTER_DEFAULT	Default printer overlay (small tick)
'31  SI_RECYCLEBIN_EMPTY	Recycle bin empty
'32  SI_RECYCLEBIN_FULL	Recycle bin full
'33  SI_DUN	Dial-up Network Folder
'34  SI_DESKTOP	Desktop
'35  SI_CONTROLPANEL	Control Panel
'36  SI_PROGRAMGROUPS	Program Group
'37  SI_PRINTER	Printer
'38  SI_FONT	Font Folder
'39  SI_TASKBAR	Taskbar
'40  SI_AUDIO_CD	Audio CD
'41      ?
'42      ?
'43  SI_FAVORITES	IE favorites
'44  SI_LOGOFF	Start Menu Logoff
'45      ?
'46      ?
'47  SI_LOCK	Lock
'48  SI_HIBERNATE	Hibernate

