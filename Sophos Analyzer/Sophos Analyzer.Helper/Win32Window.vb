﻿Imports System.Windows.Forms

Public Class Win32Window
        Implements IWin32Window

#Region " Fields "
    Private handle As IntPtr
#End Region

#Region " Properties "
    Private ReadOnly Property IWin32Window_Handle() As IntPtr Implements IWin32Window.Handle
        Get
            Return handle
        End Get
    End Property
#End Region

#Region " Constructor "
    Public Sub New(window As IWin32Window)
        Me.handle = window.Handle
    End Sub
#End Region

End Class