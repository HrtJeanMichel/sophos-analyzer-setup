﻿Imports System.IO
Imports System.Text
Imports System.Windows.Forms
Imports System.Xml.Serialization

Public NotInheritable Class Utils

#Region " Methods "
    ''' <summary>
    ''' Lance l'exécution d'un processus selon les paramètres fournis en argument puis renvoie une valeur booléenne pour déterminer ou non la bonne exécution de la commande.
    ''' </summary>
    ''' <param name="fPath">Chemin du fichier</param>
    ''' <param name="Arguments">Arguments de la ligne de commande si nécessaire, par défaut la valeur est initilisée à String.Empty</param>
    ''' <param name="Wait">Permet de définir si on doit attendre la fin du processus avant de continuer à exécuter les autres instructions. Par défaut la valeur est initilisée à True</param>
    ''' <returns>Valeur de type Boolean</returns>
    Public Shared Function StartCommand(fPath As String, Optional ByVal Arguments As String = "", Optional ByVal Wait As Boolean = True) As Boolean
        If File.Exists(fPath) Then
            Dim objProcess As Process = Nothing
            Try
                objProcess = New Process()
                With objProcess
                    .StartInfo.Verb = "runas"
                    .StartInfo.FileName = fPath
                    .StartInfo.Arguments = Arguments
                    .StartInfo.WindowStyle = ProcessWindowStyle.Hidden
                    Application.DoEvents()
                    .Start()
                    If Wait = True Then
                        Do Until .HasExited
                            Application.DoEvents()
                        Loop
                        Return True
                    End If
                End With
                Return True
            Catch
                Return False
            Finally
                If Not objProcess Is Nothing Then
                    objProcess.Dispose()
                End If
            End Try
        Else
            Return False
        End If
    End Function

    '''' <summary>
    '''' On retourne en type String le contenu serializé d'un objet.
    '''' </summary>
    Public Shared Function Serialize(Of T)(obj As Object) As String
        Dim serializer1 As New XmlSerializer(GetType(T))
        Dim str1 As String = String.Empty
        Using MS = New MemoryStream()
            serializer1.Serialize(MS, obj)
            str1 = Encoding.Default.GetString(MS.ToArray)
        End Using
        Return str1
    End Function

    '''' <summary>
    '''' On ferme toutes les instances de fenêtre ouvertes de la classe FrmInfos
    '''' </summary>
    Public Shared Sub CloseAllFrmInfos()
        For Each db In Application.OpenForms.Cast(Of Control).Where(Function(f) TypeOf f Is FrmInfos)
            Dim ctrl = TryCast(db, FrmInfos)
            ctrl.Close()
        Next
    End Sub


#End Region

End Class