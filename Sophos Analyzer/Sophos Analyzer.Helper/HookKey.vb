﻿Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports Sophos_Analyzer.Win32

Public NotInheritable Class HookKey

#Region " Fields "
    Private Shared m_proc As NativeDelegates.LowLevelKeyboardProc = AddressOf HookCapture
    Private Shared m_hookID As IntPtr = IntPtr.Zero
#End Region

#Region " Methods "
    Public Shared Sub [Start]()
        m_hookID = SetHook(m_proc)
    End Sub

    Public Shared Sub [Stop]()
        NativeMethods.UnhookWindowsHookEx(m_hookID)
    End Sub

    Private Shared Function SetHook(proc As NativeDelegates.LowLevelKeyboardProc) As IntPtr
        Using curProcess As Process = Process.GetCurrentProcess()
            Using curModule As ProcessModule = curProcess.MainModule
                Return NativeMethods.SetWindowsHookEx(NativeConstants.WH_KEY, proc, NativeMethods.GetModuleHandle(curModule.ModuleName), 0)
            End Using
        End Using
    End Function

    Private Shared Function HookCapture(nCode As Integer, wParam As IntPtr, lParam As IntPtr) As IntPtr
        If nCode >= 0 Then
            Dim hookStruct As NativeStruct.KBDLLHOOKSTRUCT = CType(Marshal.PtrToStructure(lParam, GetType(NativeStruct.KBDLLHOOKSTRUCT)), NativeStruct.KBDLLHOOKSTRUCT)
            If hookStruct.key = Keys.RWin OrElse hookStruct.key = Keys.LWin Then
                Return CType(1, IntPtr)
            ElseIf hookStruct.key = Keys.Control Then
                Return CType(1, IntPtr)
            ElseIf hookStruct.key = Keys.LControlKey Then
                Return CType(1, IntPtr)
            ElseIf hookStruct.key = Keys.RControlKey Then
                Return CType(1, IntPtr)
            ElseIf hookStruct.key = Keys.Alt Then
                Return CType(1, IntPtr)
            ElseIf hookStruct.key = Keys.Tab Then
                Return CType(1, IntPtr)
            ElseIf hookStruct.key = Keys.F1 Then
                Return CType(1, IntPtr)
            ElseIf hookStruct.key = Keys.F3 Then
                Return CType(1, IntPtr)
            ElseIf hookStruct.key = Keys.F4 Then
                Return CType(1, IntPtr)
            End If
        End If
        Return NativeMethods.CallNextHookEx(m_hookID, nCode, wParam, lParam)
    End Function
#End Region

End Class
