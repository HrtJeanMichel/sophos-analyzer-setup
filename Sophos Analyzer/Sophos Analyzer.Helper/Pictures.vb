﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms

Public NotInheritable Class Pictures

#Region " Methods "
    ''' <summary>
    ''' Redimensionne une image passée en paramétre selon son chemin, puis affecte l'image dans un composant PictureBox passé en paramètre.
    ''' </summary>
    ''' <param name="ImagePath">Chemin du fichier</param>
    ''' <param name="Pbx">Composant PictureBox auquel sera affecté l'image redimensionnée</param>
    ''' <param name="pSizeMode">Paramètre optionnel de redimensionnement d'image. Par défaut : CenterImage</param>
    Public Shared Sub AutosizeImgByPath(ImagePath As String, Pbx As PictureBox, Optional ByVal pSizeMode As PictureBoxSizeMode = PictureBoxSizeMode.CenterImage)
        Try
            Pbx.Image = Nothing
            Pbx.SizeMode = pSizeMode
            If IO.File.Exists(ImagePath) Then
                Pbx.Image = GetAutoSize(Bitmap.FromFile(ImagePath), Pbx.Size)
            Else
                Pbx.Image = Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Public Shared Sub AutosizeImgByBitmap(Image As Bitmap, Pbx As PictureBox, Optional ByVal pSizeMode As PictureBoxSizeMode = PictureBoxSizeMode.CenterImage)
        Try
            Pbx.Image = Nothing
            Pbx.SizeMode = pSizeMode
            If Not Image Is Nothing Then
                Pbx.Image = FixedSize(Image, Pbx.Size.Width, Pbx.Size.Height, True)
            Else
                Pbx.Image = Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Public Shared Function FixedSize(image As Image, Width As Integer, Height As Integer, needToFill As Boolean) As Image
        Dim sourceWidth As Integer = image.Width
        Dim sourceHeight As Integer = image.Height
        Dim sourceX As Integer = 0
        Dim sourceY As Integer = 0
        Dim destX As Double = 0
        Dim destY As Double = 0

        Dim nScale As Double = 0
        Dim nScaleW As Double = 0
        Dim nScaleH As Double = 0

        nScaleW = (CDbl(Width) / CDbl(sourceWidth))
        nScaleH = (CDbl(Height) / CDbl(sourceHeight))
        If Not needToFill Then
            nScale = Math.Min(nScaleH, nScaleW)
        Else
            nScale = Math.Max(nScaleH, nScaleW)
            destY = (Height - sourceHeight * nScale) / 2
            destX = (Width - sourceWidth * nScale) / 2
        End If

        If nScale > 1 Then
            nScale = 1
        End If

        Dim destWidth As Integer = CInt(Math.Round(sourceWidth * nScale))
        Dim destHeight As Integer = CInt(Math.Round(sourceHeight * nScale))

        Dim bmPhoto As Bitmap = Nothing
        Try
            bmPhoto = New Bitmap(destWidth + CInt(Math.Round(2 * destX)), destHeight + CInt(Math.Round(2 * destY)))
        Catch ex As Exception
            Throw New ApplicationException(String.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}", destWidth, destX, destHeight, destY, Width,
            Height), ex)
        End Try
        Using grPhoto As Graphics = Graphics.FromImage(bmPhoto)
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic
            grPhoto.CompositingQuality = CompositingQuality.HighQuality
            grPhoto.SmoothingMode = SmoothingMode.HighQuality

            Dim ToRect As Rectangle = New Rectangle(CInt(Math.Round(destX)), CInt(Math.Round(destY)), destWidth, destHeight)
            Dim FromRect As Rectangle = New Rectangle(sourceX, sourceY, sourceWidth, sourceHeight)

            grPhoto.DrawImage(image, ToRect, FromRect, GraphicsUnit.Pixel)

            Return bmPhoto
        End Using
    End Function

    ''' <summary>
    ''' Redimensionne une image passée en paramétre selon son chemin, puis affecte l'image dans un composant PictureBox passé en paramètre.
    ''' </summary>
    ''' <param name="ImagePath">Chemin du fichier</param>
    ''' <param name="Pbx">Composant PictureBox auquel sera affecté l'image redimensionnée</param>
    ''' <param name="pSizeMode">Paramètre optionnel de redimensionnement d'image. Par défaut : CenterImage</param>
    Public Shared Sub AutosizeImg(ImagePath As String, Pbx As PictureBox, Optional ByVal pSizeMode As PictureBoxSizeMode = PictureBoxSizeMode.CenterImage)
        Try
            Pbx.Image = Nothing
            Pbx.SizeMode = pSizeMode
            If IO.File.Exists(ImagePath) Then
                Pbx.Image = GetAutoSize(Bitmap.FromFile(ImagePath), Pbx.Size)
            Else
                Pbx.Image = Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    ''' <summary>
    ''' Redimensionne une image selon une taille prédéfinie passée en paramétre.
    ''' </summary>
    ''' <param name="Img">Image au format Bitmap</param>
    ''' <param name="siz">Dimensons pour la future 'image</param>
    ''' <returns>Valeur de type Image</returns>
    Private Shared Function GetAutoSize(Img As Bitmap, siz As Size) As Image
        Dim imgOrg As Bitmap
        Dim imgShow As Bitmap
        Dim g As Graphics
        Dim divideBy, divideByH, divideByW As Double
        imgOrg = Img
        imgShow = imgOrg
        Try
            divideByW = imgOrg.Width / siz.Width
            divideByH = imgOrg.Height / siz.Height
            If divideByW > 1 Or divideByH > 1 Then
                If divideByW > divideByH Then
                    divideBy = divideByW
                Else
                    divideBy = divideByH
                End If
                imgShow = New Bitmap(CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy))
                imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                g = Graphics.FromImage(imgShow)
                g.SmoothingMode = SmoothingMode.HighQuality
                g.InterpolationMode = InterpolationMode.HighQualityBicubic
                g.DrawImage(imgOrg, New Rectangle(0, 0, CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy)), 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                g.Dispose()
            Else
                imgShow = New Bitmap(imgOrg.Width, imgOrg.Height)
                imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                g = Graphics.FromImage(imgShow)
                g.SmoothingMode = SmoothingMode.HighQuality
                g.InterpolationMode = InterpolationMode.HighQualityBicubic
                g.DrawImage(imgOrg, New Rectangle(0, 0, imgOrg.Width, imgOrg.Height), 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                g.Dispose()
            End If
            imgOrg.Dispose()
        Catch ex As Exception
        End Try

        Return imgShow
    End Function

#End Region

End Class
