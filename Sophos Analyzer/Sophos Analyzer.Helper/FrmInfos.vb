﻿Imports System.Drawing
Imports System.Windows.Forms

Public Class FrmInfos

    Public Enum ButtonTypes
        OK
        YesNo
    End Enum

    Public Enum MessageTypes
        Warning
        Info
    End Enum


#Region " Constructor "
    Public Sub New(Title As String, ContentText As String, ButtonT As ButtonTypes, messageT As MessageTypes, Optional ByVal SmallSize As Boolean = False)
        InitializeComponent()
        Size = If(SmallSize, New Size(434, 146), Me.Size)
        Text = Title
        LblInfosContent.Text = ContentText
        BtnInfosValid.BackColor = If(messageT = MessageTypes.Warning, Color.Orange, Color.DeepSkyBlue)
        BtnInfosCancel.BackColor = If(messageT = MessageTypes.Warning, Color.Orange, Color.DeepSkyBlue)
        Style = If(messageT = MessageTypes.Warning, MetroFramework.MetroColorStyle.Orange, MetroFramework.MetroColorStyle.Blue)
        If ButtonT = ButtonTypes.OK Then
            BtnInfosValid.Visible = False
            BtnInfosCancel.Text = "OK"
        End If
    End Sub
#End Region

#Region " Methods "
    Private Sub OK_Button_Click(sender As Object, ByVal e As EventArgs) Handles BtnInfosValid.Click
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(sender As Object, ByVal e As EventArgs) Handles BtnInfosCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

#End Region

End Class