﻿Imports System.Threading
Imports Sophos_Analyzer.Win32

Public NotInheritable Class HookTaskbar

#Region " Properties "
#End Region

#Region " Taskbar Methods "
    Public Shared Sub ShowTaskbarAndStartButton()
        Taskbar(NativeConstants.SW_SHOW)
        StartButton(NativeConstants.SW_SHOW)
    End Sub

    Public Shared Sub HideTaskbarAndStartButton()
        Taskbar(NativeConstants.SW_HIDE)
        StartButton(NativeConstants.SW_HIDE)
    End Sub
#End Region

#Region " Methods "
    Private Shared Sub Taskbar(state As Integer)
        Dim hdn As Integer = 0
        Do
            hdn = NativeMethods.FindWindow("Shell_TrayWnd", String.Empty)
            If hdn = 0 Then
                Thread.Sleep(100)
            Else
                Exit Do
            End If
        Loop

        If Not hdn = 0 Then
            NativeMethods.ShowWindow(hdn, state)
        End If
    End Sub

    Private Shared Sub StartButton(state As Integer)
        Dim hdn As Integer = 0
        Do
            hdn = NativeMethods.GetDesktopWindow()
            If hdn = 0 Then
                Thread.Sleep(100)
            Else
                Exit Do
            End If
        Loop

        If Not hdn = 0 Then
            Dim hdn1 As Integer = 0
            Do
                hdn1 = NativeMethods.FindWindowEx(hdn, 0, "button", 0)
                If hdn1 = 0 Then
                    Thread.Sleep(100)
                Else
                    Exit Do
                End If
            Loop

            If Not hdn1 = 0 Then
                NativeMethods.ShowWindow(hdn1, state)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Masque le bouton d'accès rapide du bureau à droite de la barre de tâche.
    ''' </summary>
    Public Shared Sub HideTaskbarDesktopButton()
        DesktopButton(NativeConstants.SW_HIDE)
    End Sub

    ''' <summary>
    ''' Affiche le bouton d'accès rapide du bureau à droite de la barre de tâche.
    ''' </summary>
    Public Shared Sub ShowTaskbarDesktopButton()
        DesktopButton(NativeConstants.SW_SHOW)
    End Sub

    Private Shared Sub DesktopButton(State As Integer)
        'On temporise, cela permet d'attendre que le bureau soit entièrement chargé. 
        'Cela retourne un Handle <> 0 pour pouvoir exécuter le masquage ou l'affichage du bouton d'accès rapide au bureau.
        Dim hdn As Integer = 0
        Do
            hdn = NativeMethods.FindWindow("Shell_TrayWnd", String.Empty)
            If hdn = 0 Then
                Thread.Sleep(100)
            Else
                Exit Do
            End If
        Loop

        If Not hdn = 0 Then
            Dim hdn1 As Integer = 0
            Do
                hdn1 = NativeMethods.FindWindowEx(hdn, 0, "TrayNotifyWnd", String.Empty)
                If hdn1 = 0 Then
                    Thread.Sleep(100)
                Else
                    Exit Do
                End If
            Loop

            If Not hdn1 = 0 Then
                Dim hdn2 As Integer = 0
                Do
                    hdn2 = NativeMethods.FindWindowEx(hdn1, 0, "TrayShowDesktopButtonWClass", String.Empty)
                    If hdn2 = 0 Then
                        Thread.Sleep(100)
                    Else
                        Exit Do
                    End If
                Loop

                If Not hdn2 = 0 Then
                    NativeMethods.ShowWindow(hdn2, State)
                End If
            End If
        End If
    End Sub
#End Region

End Class
