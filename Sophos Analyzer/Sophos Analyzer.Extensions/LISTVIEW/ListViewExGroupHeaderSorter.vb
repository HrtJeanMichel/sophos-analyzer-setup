﻿Imports System.Windows.Forms

Public Class ListViewExGroupHeaderSorter
    Implements IComparer(Of ListViewGroup)

#Region " Fields "
    Private _ascending As Boolean = True
#End Region

#Region " Constructor "
    Public Sub New(ascending As Boolean)
        _ascending = ascending
    End Sub
#End Region

#Region "IComparer<ListViewGroup> Members"

    Private Function IComparer_Compare(x As ListViewGroup, y As ListViewGroup) As Integer Implements IComparer(Of ListViewGroup).Compare
        If _ascending Then
            Return String.Compare(DirectCast(x, ListViewGroup).Header, DirectCast(y, ListViewGroup).Header)
        Else
            Return String.Compare(DirectCast(y, ListViewGroup).Header, DirectCast(x, ListViewGroup).Header)
        End If
    End Function

#End Region

End Class