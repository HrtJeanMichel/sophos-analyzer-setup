﻿Imports System.ComponentModel
Imports System.Windows.Forms
Imports Sophos_Analyzer.Win32

Public Class ListViewEx
    Inherits ListView

#Region " Fields "
    Private m_elv As Boolean = False
    Private components As Container = Nothing
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeComponent()
        FullRowSelect = True
        View = View.Details
        AllowColumnReorder = True
        DoubleBuffered = True
        NativeMethods.SendMessage(Me.Handle, NativeConstants.WM_CHANGEUISTATE, MakeLong(NativeConstants.UIS_SET, NativeConstants.UISF_HIDEFOCUS), 0)
    End Sub
#End Region

#Region " Designer "

    Private Sub InitializeComponent()
        components = New Container()
    End Sub

    Protected Overrides Sub WndProc(ByRef msg As Message)
        Select Case msg.Msg
            Case NativeConstants.WM_PAINT
                '################# ADDED : LISTVIEW NT6 THEME ################
                If Not m_elv Then
                    SetWindowTheme()
                    m_elv = True
                End If
                Exit Select
        End Select

        MyBase.WndProc(msg)
    End Sub

    '''	<summary>
    '''	Clean up any resources being used.
    '''	</summary>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
#End Region

#Region " Methods "
    Private Sub SetWindowTheme()
        NativeMethods.SetWindowTheme(Me.Handle, "explorer", Nothing)
        NativeMethods.SendMessage(Me.Handle, NativeConstants.LVM_SETEXTENDEDLISTVIEWSTYLE, NativeConstants.LVS_EX_DOUBLEBUFFER, NativeConstants.LVS_EX_DOUBLEBUFFER)
    End Sub

    Private Function MakeLong(wLow As Integer, wHigh As Integer) As Integer
        Dim low As Integer = CInt(IntLoWord(wLow))
        Dim high As Short = IntLoWord(wHigh)
        Dim product As Integer = &H10000 * CInt(high)
        Dim mkLong As Integer = CInt(low Or product)
        Return mkLong
    End Function

    Private Function IntLoWord(word As Integer) As Short
        Return CShort(word And Short.MaxValue)
    End Function
#End Region

End Class
