﻿Imports MetroFramework.Controls

Public Class ListViewMetro
    Inherits MetroListView

#Region " Fields "
    Public Shadows Items As ListViewItemCollectionEvents
#End Region

#Region " Delegates "
    Public Delegate Sub ListViewItemDelegate(item As MetroListViewItem)
    Public Delegate Sub ListViewItemRangeDelegate(item As MetroListViewItem())
    Public Delegate Sub ListViewRemoveDelegate(item As MetroListViewItem)
    Public Delegate Sub ListViewRemoveAtDelegate(index As Integer, item As MetroListViewItem)
    Public Delegate Sub ListViewItemsClearDelegate()
#End Region

#Region " Events "
    Public Event Item_Added As ListViewItemDelegate
    Public Event ItemRangeAdded As ListViewItemRangeDelegate
    Public Event ItemRemoved As ListViewRemoveDelegate
    Public Event ItemRemovedAt As ListViewRemoveAtDelegate
    Public Event ItemChanged(sender As Object, e As EventArgs)
    Public Event ItemsClear As ListViewItemsClearDelegate
#End Region

#Region " Constructors "
    Public Sub New()
        MyBase.New()
        Items = New ListViewItemCollectionEvents(Me)
        AddHandler Items.ItemChanged, AddressOf ListViewItemCollectionChanged
    End Sub
#End Region

#Region " Methods "
    Private Sub ListViewEx_HandleCreated(sender As Object, e As EventArgs) Handles Me.HandleCreated
        RaiseEvent ItemChanged(Me, EventArgs.Empty)
    End Sub

    Private Sub ListViewItemCollectionChanged(sender As Object, e As EventArgs)
        RaiseEvent ItemChanged(Me, EventArgs.Empty)
    End Sub

    Private Sub AddedItem(lvi As MetroListViewItem)
        RaiseEvent Item_Added(lvi)
    End Sub

    Private Sub AddedItemRange(lvi As MetroListViewItem())
        RaiseEvent ItemRangeAdded(lvi)
    End Sub
    Private Overloads Sub RemovedItem(lvi As MetroListViewItem)
        RaiseEvent ItemRemoved(lvi)
    End Sub
    Private Overloads Sub RemovedItem(index As Integer, item As MetroListViewItem)
        RaiseEvent ItemRemovedAt(index, item)
    End Sub
#End Region


    Public Class ListViewItemCollectionEvents
        Inherits ListViewItemCollection

        Dim parent As ListViewMetro

        Public Event ItemChanged(sender As Object, e As EventArgs)

        Sub New(owner As ListViewMetro)
            MyBase.New(owner)
            parent = owner
        End Sub

        Public Shadows Sub Add(Lvi As MetroListViewItem)
            MyBase.Add(Lvi)
            parent.AddedItem(Lvi)
            RaiseEvent ItemChanged(Nothing, Nothing)
        End Sub

        Public Shadows Sub AddRange(Lvi As MetroListViewItem())
            MyBase.AddRange(Lvi)
            parent.AddedItemRange(Lvi)
            RaiseEvent ItemChanged(Nothing, Nothing)
        End Sub
        Public Shadows Sub Remove(Lvi As MetroListViewItem)
            MyBase.Remove(Lvi)
            parent.RemovedItem(Lvi)
            RaiseEvent ItemChanged(Nothing, Nothing)
        End Sub

        Public Shadows Sub RemoveAt(index As Integer)
            Dim lvi As MetroListViewItem = Me.Item(index)
            MyBase.RemoveAt(index)
            parent.RemovedItem(index, lvi)
            RaiseEvent ItemChanged(Nothing, Nothing)
        End Sub

    End Class

End Class

