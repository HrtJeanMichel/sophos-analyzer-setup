﻿Imports System.Windows.Forms

Public Class ListViewExGroupSorter
    Friend _listview As ListViewEx

    Public Shared Operator =(listview As ListViewEx, sorter As ListViewExGroupSorter) As Boolean
        Return listview Is sorter._listview
    End Operator

    Public Shared Operator <>(listview As ListViewEx, sorter As ListViewExGroupSorter) As Boolean
        Return listview IsNot sorter._listview
    End Operator

    Public Shared Widening Operator CType(sorter As ListViewExGroupSorter) As ListView
        Return sorter._listview
    End Operator

    Public Shared Widening Operator CType(listview As ListView) As ListViewExGroupSorter
        Return New ListViewExGroupSorter(listview)
    End Operator

    Friend Sub New(listview As ListViewEx)
        _listview = listview
    End Sub

    Public Sub SortGroups(ascending As Boolean)
        _listview.BeginUpdate()
        Dim lvgs As New List(Of ListViewGroup)()
        For Each lvg As ListViewGroup In _listview.Groups
            lvgs.Add(lvg)
        Next
        _listview.Groups.Clear()
        lvgs.Sort(New ListViewExGroupHeaderSorter(ascending))
        _listview.Groups.AddRange(lvgs.ToArray())
        _listview.EndUpdate()
    End Sub

#Region " Overridden Methods"

    Public Overrides Function Equals(obj As Object) As Boolean
        Return _listview.Equals(obj)
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return _listview.GetHashCode()
    End Function

    Public Overrides Function ToString() As String
        Return _listview.ToString()
    End Function

#End Region

End Class