﻿
Public Class NativeDelegates

#Region " Helpers.HookKey "
    Public Delegate Function LowLevelKeyboardProc(nCode As Integer, wParam As IntPtr, lParam As IntPtr) As IntPtr
#End Region

#Region " Helpers.HookWindow "
    Public Delegate Function EnumThreadDelegate(hWnd As IntPtr, lParam As IntPtr) As Boolean
#End Region

End Class
