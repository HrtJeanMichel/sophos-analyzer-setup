﻿Imports System.Runtime.InteropServices
Imports System.Text

Public NotInheritable Class NativeMethods

#Region " Helpers.OS "
    Public Declare Function TerminateProcess Lib "kernel32" (hProcess As IntPtr, uExitCode As UInteger) As Integer

    Public Declare Auto Function NetGetJoinInformation Lib "Netapi32.dll" (server As String, ByRef IntPtr As IntPtr, ByRef status As NativeEnum.NetJoinStatus) As Integer
    Public Declare Auto Function NetApiBufferFree Lib "Netapi32.dll" (Buffer As IntPtr) As Integer

    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function CancelShutdown() As Integer
    End Function
#End Region

#Region " Helpers.Taskbar "
    <DllImport("user32.dll")>
    Public Shared Function FindWindow(className As String, windowText As String) As Integer
    End Function

    <DllImport("user32.dll")>
    Public Shared Function ShowWindow(hwnd As Integer, command As Integer) As Integer
    End Function

    <DllImport("user32.dll")>
    Public Shared Function FindWindowEx(parentHandle As Integer, childAfter As Integer, className As String, windowTitle As Integer) As Integer
    End Function

    <DllImport("user32.dll")>
    Public Shared Function GetDesktopWindow() As Integer
    End Function
#End Region

#Region " Helpers.HookDesktop "
    <DllImport("user32.dll", EntryPoint:="FindWindowExA", CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True)>
    Public Shared Function FindWindowEx(ByVal hWnd1 As Integer, ByVal hWnd2 As Integer, <MarshalAs(UnmanagedType.VBByRefStr)> ByRef lpsz1 As String, <MarshalAs(UnmanagedType.VBByRefStr)> ByRef lpsz2 As String) As Integer
    End Function
#End Region

#Region " Helpers.HookKey "
    <DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Public Shared Function SetWindowsHookEx(idHook As Integer, lpfn As NativeDelegates.LowLevelKeyboardProc, hMod As IntPtr, dwThreadId As UInteger) As IntPtr
    End Function

    <DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Public Shared Function UnhookWindowsHookEx(hhk As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Public Shared Function CallNextHookEx(hhk As IntPtr, nCode As Integer, wParam As IntPtr, lParam As IntPtr) As IntPtr
    End Function

    <DllImport("kernel32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Public Shared Function GetModuleHandle(lpModuleName As String) As IntPtr
    End Function
#End Region

#Region " Helpers.HookWindow "

    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Public Shared Function SendMessage(hWnd As IntPtr, Msg As UInteger, wParam As Integer, lParam As StringBuilder) As IntPtr
    End Function

    <DllImport("user32.Dll")>
    Public Shared Function PostMessage(hWnd As IntPtr, msg As Integer, wParam As Integer, lParam As Integer) As Integer
    End Function

    <DllImport("user32.dll")>
    Public Shared Function EnumThreadWindows(dwThreadId As Integer, lpfn As NativeDelegates.EnumThreadDelegate, lParam As IntPtr) As Boolean
    End Function
#End Region

#Region " Helpers.Wallpaper "
    <DllImport("user32.dll", CharSet:=CharSet.Unicode)>
    Public Shared Function SystemParametersInfo(uAction As Integer, uParam As Integer, lpvParam As String, fuWinIni As Integer) As Integer
    End Function
#End Region

#Region " Extensions.ListViewEx "
    <DllImport("user32.dll")>
    Public Shared Function SendMessage(hWnd As IntPtr, msg As Integer, wPar As IntPtr, lPar As IntPtr) As IntPtr
    End Function

    '<DllImport("uxtheme.dll", CharSet:=CharSet.Unicode)>
    'Public Shared Function SetWindowTheme(hWnd As IntPtr, pszSubAppName As String, pszSubIdList As String) As Integer
    'End Function

    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Public Shared Function SendMessage(hWnd As IntPtr, Msg As Integer, wParam As Integer, lParam As Integer) As IntPtr
    End Function
#End Region

#Region " Extensions.TreeviewEx "

    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Public Shared Function SendMessage(hWnd As IntPtr, Msg As UInteger, wParam As IntPtr, lParam As IntPtr) As IntPtr
    End Function

    <DllImport("uxtheme.dll", ExactSpelling:=True, CharSet:=CharSet.Unicode)>
    Public Shared Function SetWindowTheme(hWnd As IntPtr, pszSubAppName As String, pszSubIdList As String) As Integer
    End Function

#End Region

#Region " Core.Watcher.Disk "
    <DllImport("Shlwapi.dll", CharSet:=CharSet.Auto)>
    Public Shared Function StrFormatByteSize(fileSize As Long, buffer As StringBuilder, bufferSize As Integer) As Long
    End Function

    <DllImport("kernel32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Public Shared Function GetDiskFreeSpaceEx(lpDirectoryName As String, ByRef lpFreeBytesAvailable As ULong, ByRef lpTotalNumberOfBytes As ULong, ByRef lpTotalNumberOfFreeBytes As ULong) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
#End Region

#Region " Core.Watcher.Detector "
    <DllImport("setupapi.dll", SetLastError:=True, EntryPoint:="CMP_WaitNoPendingInstallEvents", CharSet:=CharSet.Auto)>
    Public Shared Function CMP_WaitNoPendingInstallEvents(TimeOut As UInteger) As UInteger
    End Function
    '<DllImport("cfgmgr32.dll", SetLastError:=True, EntryPoint:="CMP_WaitNoPendingInstallEvents", CharSet:=CharSet.Auto)>
    'Public Shared Function CMP_WaitNoPendingInstallEvents(TimeOut As UInteger) As UInteger
    'End Function
#End Region

#Region " Core.Configurator.Account.Profile "
    <DllImport("userenv.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Public Shared Function CreateProfile(<MarshalAs(UnmanagedType.LPWStr)> pszUserSid As String, <MarshalAs(UnmanagedType.LPWStr)> pszUserName As String, <Out, MarshalAs(UnmanagedType.LPWStr)> pszProfilePath As StringBuilder, cchProfilePath As UInteger) As Integer
    End Function

    <DllImport("userenv.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Public Shared Function DeleteProfile(<MarshalAs(UnmanagedType.LPWStr)> lpSidString As String, <MarshalAs(UnmanagedType.LPWStr)> lpProfilePath As String, <MarshalAs(UnmanagedType.LPWStr)> lpComputerName As String) As Integer
    End Function
#End Region

#Region " Core.Configurator.Account.User "
    <DllImport("shell32.dll", EntryPoint:="#261", CharSet:=CharSet.Unicode, PreserveSig:=False)>
    Public Shared Sub GetUserTilePath(username As String, whatever As Long, picpath As StringBuilder, maxLength As Integer)
    End Sub
#End Region

#Region " Core.Configurator.Account.Users "
    <DllImport("wtsapi32.dll", SetLastError:=True)>
    Public Shared Function WTSLogoffSession(hServer As IntPtr, SessionId As Integer, bWait As Boolean) As Boolean
    End Function

    <DllImport("kernel32.dll")>
    Public Shared Function WTSGetActiveConsoleSessionId() As UInteger
    End Function

    <DllImport("Wtsapi32.dll")>
    Public Shared Function WTSQuerySessionInformation(hServer As IntPtr, sessionId As UInteger, wtsInfoClass As NativeEnum.WTS_INFO_CLASS, ByRef ppBuffer As IntPtr, ByRef pBytesReturned As UInteger) As Boolean
    End Function

    <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Public Shared Function WTSOpenServer(ByVal pServerName As String) As IntPtr
    End Function

    <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Public Shared Sub WTSCloseServer(ByVal hServer As IntPtr)
    End Sub

    '<DllImport("wtsapi32.dll", SetLastError:=True)>
    'Public Shared Function WTSEnumerateSessions(hServer As IntPtr, <MarshalAs(UnmanagedType.U4)> Reserved As Integer, <MarshalAs(UnmanagedType.U4)> Version As Integer, ByRef ppSessionInfo As IntPtr, <MarshalAs(UnmanagedType.U4)> ByRef pCount As Integer) As Integer
    'End Function

    <DllImport("wtsapi32.dll", BestFitMapping:=True, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Auto, EntryPoint:="WTSEnumerateSessions", SetLastError:=True, ThrowOnUnmappableChar:=True)>
    Public Shared Function WTSEnumerateSessions(ByVal hServer As IntPtr, <MarshalAs(UnmanagedType.U4)> ByVal Reserved As Integer, <MarshalAs(UnmanagedType.U4)> ByVal Version As Integer, ByRef ppSessionInfo As IntPtr, <MarshalAs(UnmanagedType.U4)> ByRef pCount As Integer) As Integer
    End Function

    <DllImport("wtsapi32.dll")>
    Public Shared Sub WTSFreeMemory(pMemory As IntPtr)
    End Sub

#End Region

    '#Region " Core.Configurator.scan "
    '    <DllImport("kernel32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    '    Public Shared Function GetShortPathName(lpszLongPath As String, lpszShortPath As Char(), cchBuffer As Integer) As UInteger
    '    End Function
    '#End Region

    '#Region " ClamAv "
    '    <DllImport("kernel32.dll")>
    '    Public Shared Function LoadLibrary(dllName As String) As IntPtr
    '    End Function

    '    <DllImport("kernel32.dll")>
    '    Public Shared Function GetProcAddress(hModule As IntPtr, funcName As String) As IntPtr
    '    End Function

    '    <DllImport("kernel32.dll")>
    '    Public Shared Function FreeLibrary(hModule As IntPtr) As Boolean
    '    End Function
    '#End Region

#Region " UCheckSophos.FrmGlass "
    ''' <summary>
    ''' Obtain a value that indicates whether Desktop Window Manager
    ''' (DWM) composition is enabled. 
    ''' </summary>
    <DllImport("dwmapi.dll", CharSet:=CharSet.Auto, PreserveSig:=False, SetLastError:=True)>
    Public Shared Sub DwmIsCompositionEnabled(ByRef pfEnable As Boolean)
    End Sub
    ''' <summary>
    ''' Extend the window frame into the client area.
    ''' </summary>
    <DllImport("dwmapi.dll", CharSet:=CharSet.Auto, PreserveSig:=False, SetLastError:=True)>
    Public Shared Sub DwmExtendFrameIntoClientArea(hWnd As IntPtr, <[In]> ByRef margins As NativeStruct.MARGINS)
    End Sub
    ''' <summary>
    ''' Enable the blur effect on a specified window.
    ''' </summary>
    <DllImport("dwmapi.dll", CharSet:=CharSet.Auto, PreserveSig:=False, SetLastError:=True)>
    Public Shared Sub DwmEnableBlurBehindWindow(hWnd As IntPtr, pBlurBehind As DWM_BLURBEHIND)
    End Sub

    ''' <summary>
    ''' Specify Desktop Window Manager (DWM) blur-behind properties. 
    ''' </summary>
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
    Public Class DWM_BLURBEHIND
        ' Indicate the members of this structure have been set.
        Public dwFlags As UInteger

        ' The flag specify  whether the subsequent compositions of the window
        ' blurring the content behind it or not.
        <MarshalAs(UnmanagedType.Bool)>
        Public fEnable As Boolean

        ' The region where the glass style will be applied.
        Public hRegionBlur As IntPtr

        ' Whether the windows color should be transited to match the maximized 
        ' windows or not.
        <MarshalAs(UnmanagedType.Bool)>
        Public fTransitionOnMaximized As Boolean

        ' Flags used to indicate the  members contain valid information.
        Public Const DWM_BB_ENABLE As UInteger = &H1
        Public Const DWM_BB_BLURREGION As UInteger = &H2
        Public Const DWM_BB_TRANSITIONONMAXIMIZED As UInteger = &H4
    End Class

#End Region

End Class
