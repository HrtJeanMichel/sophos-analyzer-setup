﻿Imports System.Drawing
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Public NotInheritable Class NativeStruct

#Region " Helpers.HookKey "
    <StructLayout(LayoutKind.Sequential)>
    Public Structure KBDLLHOOKSTRUCT
        Public key As Keys
        Public scanCode As Integer
        Public flags As Integer
        Public time As Integer
        Public extra As IntPtr
    End Structure
#End Region

#Region " Core.Configurator.Account.Users "
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
    Public Structure WTS_SESSION_INFO
        Dim SessionID As Integer 'DWORD integer
        Dim pWinStationName As String ' integer LPTSTR - Pointer to a null-terminated string containing the name of the WinStation for this session
        Dim State As NativeEnum.WTS_CONNECTSTATE_CLASS
    End Structure

    Public Structure strSessionsInfo
        Dim SessionID As Integer
        Dim ConnectionState As String
        Dim SessionUserName As String
    End Structure
#End Region

#Region " UCheckSophos.FrmGlass"
    ''' <summary>
    ''' The point of MARGINS structure that describes the margins to use when
    ''' extending the frame into the client area.
    ''' </summary>
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
    Public Structure MARGINS
        ' Width of the left border that retains its size.
        Public cxLeftWidth As Integer

        ' Width of the right border that retains its size.
        Public cxRightWidth As Integer

        ' Height of the top border that retains its size.
        Public cyTopHeight As Integer

        ' Height of the bottom border that retains its size.
        Public cyBottomHeight As Integer

        Public Sub New(margin As Integer)
            cxLeftWidth = margin
            cxRightWidth = margin
            cyTopHeight = margin
            cyBottomHeight = margin
        End Sub

        Public Sub New(leftWidth As Integer, rightWidth As Integer, topHeight As Integer, bottomHeight As Integer)
            cxLeftWidth = leftWidth
            cxRightWidth = rightWidth
            cyTopHeight = topHeight
            cyBottomHeight = bottomHeight
        End Sub

        ''' <summary>
        ''' Determine whether there is a negative value, or the value is valid
        ''' for a Form.
        ''' </summary>
        Public Function IsNegativeOrOverride(formClientSize As Size) As Boolean
            Return cxLeftWidth < 0 OrElse cxRightWidth < 0 OrElse cyBottomHeight < 0 OrElse cyTopHeight < 0 OrElse (cxLeftWidth + cxRightWidth) > formClientSize.Width OrElse (cyTopHeight + cyBottomHeight) > formClientSize.Height
        End Function
    End Structure
#End Region
End Class
