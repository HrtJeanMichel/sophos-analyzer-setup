﻿
Public NotInheritable Class NativeConstants

#Region " Helper.HookKey "
    Public Const WH_KEY As Integer = 13
#End Region

#Region " Helper.HookWindow "
    Public Const WM_CLOSE As Integer = &H10
    Public Const WM_GETTEXT As UInteger = &HD
#End Region

#Region " Helper.HookTaskbar "
    Public Const SW_HIDE As Integer = 0
    Public Const SW_SHOW As Integer = 1
#End Region

#Region " Core.Tweaks.Desktop "
    Public Const SPI_GETDESKWALLPAPER As Integer = &H73
    Public Const MAX_PATH As Integer = 260
#End Region

#Region " Core.Usb.Detector "
    Public Const WAIT_OBJECT_0 As UInteger = &H0
    Public Const WAIT_TIMEOUT As UInteger = &H102
    Public Const WAIT_FAILED As UInteger = &HFFFFFFFFUI
#End Region

#Region " Extensions.ListViewEx "
    Public Const LVM_FIRST As Integer = &H1000
    Public Const LVM_SETEXTENDEDLISTVIEWSTYLE As Integer = LVM_FIRST + 54
    Public Const LVS_EX_DOUBLEBUFFER As Integer = 65536
    Public Const WM_PAINT As Integer = 15

    Public Const WM_CHANGEUISTATE As Integer = &H127
    Public Const UIS_SET As Integer = 1
    Public Const UISF_HIDEFOCUS As Integer = &H1
#End Region

#Region " UCheckSophos.FrmGlass "
    Public Const DTT_COMPOSITED As Integer = 8192
    Public Const DTT_GLOWSIZE As Integer = 2048
    Public Const DTT_TEXTCOLOR As Integer = 1
    Public Const SRCCOPY As Integer = &HCC0020
    Public Const WM_SYSCOMMAND As Integer = &H112
    Public Const SC_MOVE As Integer = &HF010
#End Region
End Class
