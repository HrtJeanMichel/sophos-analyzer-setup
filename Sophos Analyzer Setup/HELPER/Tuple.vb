﻿Namespace Helper
    Public Class Tuple(Of T1)
        Public Sub New(item1__1 As T1)
            Item1 = item1__1
        End Sub

        Public Property Item1() As T1
            Get
                Return m_Item1
            End Get
            Set
                m_Item1 = Value
            End Set
        End Property
        Private m_Item1 As T1
    End Class

    Public Class Tuple(Of T1, T2)
        Inherits Tuple(Of T1)
        Public Sub New(item1 As T1, item2__1 As T2)
            MyBase.New(item1)
            Item2 = item2__1
        End Sub

        Public Property Item2() As T2
            Get
                Return m_Item2
            End Get
            Set
                m_Item2 = Value
            End Set
        End Property
        Private m_Item2 As T2
    End Class

    Public NotInheritable Class Tuple
        Private Sub New()
        End Sub
        Public Shared Function Create(Of T1)(item1 As T1) As Tuple(Of T1)
            Return New Tuple(Of T1)(item1)
        End Function

        Public Shared Function Create(Of T1, T2)(item1 As T1, item2 As T2) As Tuple(Of T1, T2)
            Return New Tuple(Of T1, T2)(item1, item2)
        End Function
    End Class
End Namespace