﻿Imports Microsoft.Win32

Namespace Helper

    Public NotInheritable Class Reg

#Region " Fields "
        Private Shared TempUserDat As String = "TempUserDat"
#End Region

#Region " Methods "
        ''' <summary>
        ''' Vérifie si la sous clé TempUserDat existe dans la ruche HKLM du registre puis retourne une valeur de type booléen.
        ''' </summary>
        ''' <returns>Valeur de type Boolean</returns>
        Public Shared Function Mounted() As Boolean
            Dim registryKey1 As RegistryKey = Nothing
            Dim b1 As Boolean = False
            Try
                registryKey1 = Registry.LocalMachine.OpenSubKey(TempUserDat, RegistryKeyPermissionCheck.ReadSubTree)
                Dim object2 As Object = registryKey1.GetValue("(Default)")
                b1 = True
                registryKey1.Close()
            Catch exception1 As Exception
                b1 = False
            End Try
            Return b1
        End Function

        ''' <summary>
        ''' Décharge la ruche de registre HKLM\TempUserDat puis retourne True ou False.
        ''' </summary>
        ''' <returns>Valeur de type Boolean</returns>
        Public Shared Function Unload() As Boolean
            Return Utils.StartCommand(Environment.ExpandEnvironmentVariables("%WinDir%") & "\System32\Cmd.exe", " /c REG UNLOAD HKLM\" & TempUserDat, True)
        End Function
#End Region

        Public Class DWORD

            Public Shared Sub SetValue(ByVal SubKey As String, ByVal ValueName As String, ByVal IntValue As String, Optional ByVal DefaultValue_EqualToDelete As Integer = -2)
                Try
                    If (IntValue = DefaultValue_EqualToDelete) Then
                        Registry.LocalMachine.OpenSubKey(SubKey, True).DeleteValue(ValueName, False)
                    Else
                        Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, IntValue, RegistryValueKind.DWord)
                    End If
                Catch ex As Exception
                End Try
            End Sub

            Public Shared Sub SetBlnValue(ByVal SubKey As String, ByVal ValueName As String, ByVal value As Boolean, Optional ByVal KeepValueName As Boolean = False)
                Try
                    If KeepValueName Then
                        Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, If(value, 1, 0), RegistryValueKind.DWord)
                    Else
                        If value Then
                            Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, 1, RegistryValueKind.DWord)
                        Else
                            Registry.LocalMachine.OpenSubKey(SubKey, True).DeleteValue(ValueName, False)
                        End If
                    End If
                Catch ex As Exception
                End Try
            End Sub
        End Class

#Region " SZ "
        Public Class SZ
            Public Shared Function IsSeted(ByVal SubKey As String, ByVal ValueName As String, ByVal strTrueValue As String) As Boolean
                Dim flag As Boolean
                Try
                    If (Registry.LocalMachine.OpenSubKey(SubKey).GetValue(ValueName).ToString.ToLower = strTrueValue.ToLower) Then
                        Return True
                    End If
                    flag = False
                Catch ex As Exception
                    flag = False
                End Try
                Return flag
            End Function

            Public Shared Sub SetBlnValue(ByVal SubKey As String, ByVal ValueName As String, ByVal strTrueValue As String, ByVal value As Boolean)
                Try
                    If value Then
                        Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, strTrueValue, RegistryValueKind.String)
                    Else
                        Registry.LocalMachine.OpenSubKey(SubKey, True).DeleteValue(ValueName, False)
                    End If
                Catch ex As Exception
                End Try
            End Sub

            Public Shared Function GetValue(ByVal SubKey As String, ByVal ValueName As String, Optional ByVal DefaultValue As String = "") As String
                Try
                    Dim obj = Registry.LocalMachine.OpenSubKey(SubKey)
                    If Not obj Is Nothing Then
                        Return obj.GetValue(ValueName, DefaultValue).ToString
                    End If
                    Return DefaultValue
                Catch ex As Exception
                End Try
                Return DefaultValue
            End Function

            Public Shared Sub SetValue(ByVal SubKey As String, ByVal ValueName As String, ByVal strValue As String, Optional ByVal Expand As Boolean = False)
                Try
                    Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, strValue, If(Expand, RegistryValueKind.ExpandString, RegistryValueKind.String))
                Catch ex As Exception
                End Try
            End Sub

        End Class

#End Region

#Region " MULTI SZ "
        Public Class MULTI_SZ
            Public Shared Function IsSeted(MainRK As RegistryKey, SubKey As String, ValueName As String, strTrueValue As String) As Boolean
                Dim flag As Boolean
                Try
                    If (Registry.LocalMachine.OpenSubKey(SubKey).GetValue(ValueName).ToString.ToLower = strTrueValue.ToLower) Then
                        Return True
                    End If
                    flag = False
                Catch ex As Exception
                    flag = False
                End Try
                Return flag
            End Function

            Public Shared Sub SetBlnValue(MainRK As RegistryKey, SubKey As String, ValueName As String, strTrueValue As String, value As Boolean)
                Try
                    If value Then
                        Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, strTrueValue, RegistryValueKind.String)
                    Else
                        Registry.LocalMachine.OpenSubKey(SubKey, True).DeleteValue(ValueName, False)
                    End If
                Catch ex As Exception
                End Try
            End Sub

            Public Shared Function GetValue(SubKey As String, ValueName As String, Optional ByVal DefaultValue As String() = Nothing) As String()
                Try
                    Return Registry.LocalMachine.OpenSubKey(SubKey).GetValue(ValueName, DefaultValue)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                Return DefaultValue
            End Function

            Public Shared Sub SetValue(SubKey As String, ValueName As String, strValue As String)
                Try
                    Registry.LocalMachine.CreateSubKey(SubKey).SetValue(ValueName, strValue, RegistryValueKind.String)
                Catch ex As Exception
                End Try
            End Sub

        End Class
#End Region

#Region " EXISTS METHODS "
        Public Shared Function ExistsValue(SubKey As String, Value As String) As Boolean
            Try
                Dim objectValue As Object = Registry.LocalMachine.OpenSubKey(SubKey, False)
                If (objectValue Is Nothing) Then
                    Return False
                Else
                    Dim objectValue0 As Object = Registry.LocalMachine.OpenSubKey(SubKey, False).GetValue(Value)
                    If (objectValue0 IsNot Nothing) Then
                        Return True
                    End If
                End If
            Catch ex As Exception
                'MsgBox(ex.Message)
            End Try
            Return False
        End Function

        Public Shared Function ExistsKey(SubKey As String) As Boolean
            Return Registry.LocalMachine.OpenSubKey(SubKey, True) IsNot Nothing
        End Function

        Public Shared Function CreateSubKey(SubKey As String) As RegistryKey
            Return Registry.LocalMachine.CreateSubKey(SubKey, RegistryKeyPermissionCheck.ReadWriteSubTree)
        End Function

        Public Shared Sub DeleteSubKey(SubKey As String)
            If ExistsKey(SubKey) Then
                Registry.LocalMachine.DeleteSubKeyTree(SubKey)
            End If
        End Sub

        Public Shared Function ReadSubKey(SubKey As String) As RegistryKey
            Return Registry.LocalMachine.OpenSubKey(SubKey, True)
        End Function
#End Region

    End Class

End Namespace


