﻿Imports System.Globalization
Imports System.IO
Imports IWshRuntimeLibrary
Imports System.DirectoryServices.AccountManagement
Imports Sophos_Analyzer_Setup.Win32
Imports System.ServiceProcess
Imports TaskScheduler
Imports System.Runtime.InteropServices

Namespace Helper
    Public Class Utils

#Region " Fields "
        Private Shared m_WinVersions As String() = New String() {"Windows 7"}
        Private Shared m_ProgramFiles As String = "C:\Program Files"
        Private Shared m_ProgramFiles32 As String = "C:\Program Files (x86)"
#End Region

#Region " Methods "
        ''' <summary>
        ''' Détermine si l'utilisateur exécutant la session appartient au groupe administrateurs.
        ''' </summary>
        ''' <returns>Valeur de type Boolean</returns>
        Public Shared Function CurrentIsAdministrator() As Boolean
            Try
                Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                    Using user As New UserPrincipal(ctx)
                        user.Name = "*"
                        Using ps As New PrincipalSearcher()
                            ps.QueryFilter = user
                            Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll
                            For Each p As Principal In result
                                Using up As UserPrincipal = DirectCast(p, UserPrincipal)
                                    If up.Name = GetUsernameBySessionId(False) Then
                                        For Each g In up.GetGroups
                                            If g.Name.ToLower = "administrateurs" OrElse g.Name.ToLower = "administrators" Then
                                                Return True
                                            End If
                                        Next
                                    End If
                                End Using
                            Next
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                Return False
            End Try
            Return False
        End Function

        Public Shared Function IsComputerInDomain() As Boolean
            Try
                Dim status As NativeEnum.NetJoinStatus = NativeEnum.NetJoinStatus.NetSetupUnknownStatus
                Dim pDomain As IntPtr = IntPtr.Zero
                Dim result As Integer = NativeMethods.NetGetJoinInformation(Nothing, pDomain, status)

                If (pDomain <> IntPtr.Zero) Then
                    NativeMethods.NetApiBufferFree(pDomain)
                End If

                If (result = 0) Then
                    Return If(status = NativeEnum.NetJoinStatus.NetSetupDomainName, True, False)
                Else
                    Throw New Exception("Pas d'informations de domaine !")
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Sub DeleteDirectoryAndContent(BaseDirectory As String)
            Try
                If Directory.Exists(BaseDirectory) Then
                    Dim stackA = New Stack(Of DirectoryInfo)()
                    stackA.Push(New DirectoryInfo(BaseDirectory))
                    Dim stackB = New Stack(Of DirectoryInfo)()
                    While stackA.Any()
                        Dim dir = stackA.Pop()
                        For Each file In dir.GetFiles()
                            file.IsReadOnly = False
                            file.Delete()
                        Next

                        For Each subDir In dir.GetDirectories()
                            stackA.Push(subDir)
                            stackB.Push(subDir)
                        Next
                    End While

                    While stackB.Any()
                        stackB.Pop().Delete()
                    End While

                    If IO.Directory.Exists(BaseDirectory) Then
                        IO.Directory.Delete(BaseDirectory)
                    End If
                End If
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        End Sub

        ''' <summary>
        ''' Vérifie si le système d'exploitation est conforme aux exigences.
        ''' </summary>
        ''' <returns>Valeur de type Boolean</returns>
        ''' <remarks>
        ''' Compatible Windows 7 uniquement
        ''' </remarks>
        Public Shared Function isCorrectOS() As Boolean
            Return m_WinVersions.Contains(GetVersion)
        End Function

        Public Shared Function GetOsInline() As String
            Return String.Join(vbNewLine & "- ", m_WinVersions)
        End Function

        Public Shared Function GetProgramFilesDir() As String
            If Directory.Exists(m_ProgramFiles32) Then
                Return m_ProgramFiles32
            End If
            Return m_ProgramFiles
        End Function

        Public Shared Function GetShutdownExeFilePath() As String
            Return If(is64Bits(), "C:\Windows\SysWOW64\", "C:\Windows\System32\") & "shutdown.exe"

        End Function

        Public Shared Function GetMsiExecExeFilePath() As String
            Return If(is64Bits(), "C:\Windows\SysWOW64\", "C:\Windows\System32\") & "msiexec.exe"
        End Function

        ''' <summary>
        ''' Renvoie la version du système d'exploitation (Windows Vista, Windows 7, Windows 8, Windows 8.1, Windows 10, ...
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function GetVersion() As String
            Dim oSVersion As OperatingSystem = Environment.OSVersion
            Select Case oSVersion.Platform.ToString
                Case "Win32Windows"
                    Select Case oSVersion.Version.Minor
                        Case 0
                            Return "Windows 95"
                        Case 10
                            If (oSVersion.Version.Revision.ToString = "2222A") Then
                                Return "Windows 98 Second Edition"
                            End If
                            Return "Windows 98"
                        Case 90
                            Return "Windows Me"
                    End Select
                    Exit Select
                Case "Win32NT"
                    Select Case oSVersion.Version.Major
                        Case 3
                            Return "Windows NT 3.51"
                        Case 4
                            Return "Windows NT 4.0"
                        Case 5
                            Select Case oSVersion.Version.Minor
                                Case 0
                                    Return "Windows 2000"
                                Case 1
                                    Return "Windows XP"
                                Case 2
                                    If My.Computer.Info.OSFullName.Contains("R2") Then
                                        Return "Windows Server 2003 R2"
                                    Else
                                        Return "Windows Server 2003"
                                    End If
                            End Select
                            Exit Select
                        Case 6
                            Select Case oSVersion.Version.Minor
                                Case 0
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2008"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Vista") Then
                                        Return "Windows Vista"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                                Case 1
                                    If My.Computer.Info.OSFullName.Contains("R2") Then
                                        Return "Windows Server 2008 R2"
                                    ElseIf My.Computer.Info.OSFullName.Contains("7") Then
                                        Return "Windows 7"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                                Case 2
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2012"
                                    ElseIf My.Computer.Info.OSFullName.Contains("8") Then
                                        Return "Windows 8"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    ElseIf My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    End If
                                Case 3
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2012 R2"
                                    ElseIf My.Computer.Info.OSFullName.Contains("8.1") Then
                                        Return "Windows 8.1"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    ElseIf My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    End If
                                Case 4
                                    If My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                            End Select
                        Case 10
                            If My.Computer.Info.OSFullName.Contains("10") Then
                                Return "Windows 10"
                            ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                Return "Windows Embedded"
                            End If
                            Exit Select
                    End Select
                    Exit Select
            End Select
            Return "Système d'exploitation inconnu !"
        End Function

        Public Shared Sub KillProcess(ProcName As String)
            For Each ObjPro As Process In Process.GetProcessesByName(ProcName)
                ObjPro.Kill()
                ObjPro.WaitForExit()
            Next
        End Sub

        Public Shared Function ProcessIsRunning(ProcName As String) As Boolean
            Return Process.GetProcessesByName(ProcName).Count > 0
        End Function

        '''' <summary>
        '''' On détermine l'architecture du système d'exploitation en se basant sur l'existence du chemin "Program Files x86".
        '''' </summary>
        Public Shared Function is64Bits() As Boolean
            Return Directory.Exists(m_ProgramFiles32)
        End Function

        Public Shared Function StopTheService(SvcName As String) As Boolean
            If ServiceController.GetServices.Any(Function(s) s.ServiceName = SvcName) Then
                Using Svc = New ServiceController(SvcName)
                    If Svc.Status = ServiceControllerStatus.Running Then
                        Try
                            Svc.Stop()
                            Svc.WaitForStatus(ServiceControllerStatus.Stopped)
                            Return True
                        Catch ex As Exception
                            Return False
                        End Try
                        Return False
                    Else
                        Return True
                    End If
                End Using
            Else
                Return True
            End If
        End Function

        ''' <summary>
        ''' Lance l'exécution d'un processus selon les paramètres fournis en argument puis renvoie une valeur booléenne pour déterminer ou non la bonne exécution de la commande.
        ''' </summary>
        ''' <param name="fPath">Chemin du fichier</param>
        ''' <param name="Arguments">Arguments de la ligne de commande si nécessaire, par défaut la valeur est initilisée à String.Empty</param>
        ''' <param name="Wait">Permet de définir si on doit attendre la fin du processus avant de continuer à exécuter les autres instructions. Par défaut la valeur est initilisée à True</param>
        ''' <returns>Valeur de type Boolean</returns>
        Public Shared Function StartCommand(fPath As String, Optional ByVal Arguments As String = "", Optional ByVal Wait As Boolean = True) As Boolean
            If IO.File.Exists(fPath) Then
                Dim objProcess As Process = Nothing
                Try
                    objProcess = New Process()
                    With objProcess
                        .StartInfo.FileName = fPath
                        .StartInfo.Arguments = Arguments
                        .StartInfo.WindowStyle = ProcessWindowStyle.Hidden
                        Application.DoEvents()
                        .Start()
                        If Wait = True Then
                            Do Until .HasExited
                                Application.DoEvents()
                            Loop
                            Return True
                        End If
                    End With
                    Return True
                Catch
                    Return False
                Finally
                    If Not objProcess Is Nothing Then
                        objProcess.Dispose()
                    End If
                End Try
            Else
                Return False
            End If
        End Function

        Public Shared Function GetInstalledUICulture() As Integer
            If CultureInfo.InstalledUICulture.ToString.ToLower = "fr-fr" Then
                Return 1036
            End If
            Return 1033
        End Function

        Public Shared Sub CreateShortcut(fullPathToLink As String, fullPathToTargetExe As String, WorkingDirectory As String, description As String)
            Dim shell = New WshShell()
            Dim link = CType(shell.CreateShortcut(fullPathToLink), IWshShortcut)
            link.IconLocation = fullPathToTargetExe
            link.TargetPath = fullPathToTargetExe
            link.Description = description
            link.WorkingDirectory = WorkingDirectory
            link.Save()
        End Sub

        Public Shared Sub ShutdownCancel()
            NativeMethods.CancelShutdown()
        End Sub

        Public Shared Function SophosInstalledFromDirectories() As Boolean
            Dim SophosUpdate = IO.File.Exists(GetProgramFilesDir() & "\Sophos\AutoUpdate\ALMon.exe")
            Dim SophosAV = IO.File.Exists(GetProgramFilesDir() & "\Sophos\Sophos Anti-Virus\VirusDetection.dll")
            Return If(SophosUpdate And SophosAV, True, False)
        End Function

        Public Shared Function SophosSavExists() As Boolean
            Return IO.File.Exists("C:\maj_sophos\sav\crt\version.txt")
        End Function

        Public Shared Sub deleteTask(tName$)
            Try
                Dim objScheduler As New TaskScheduler.TaskScheduler()
                objScheduler.Connect()
                Dim containingFolder As ITaskFolder = objScheduler.GetFolder("\")
                For Each t As IRegisteredTask In containingFolder.GetTasks(0)
                    If t.Name = tName Then
                        containingFolder.DeleteTask(tName, 0)
                    End If
                Next
            Catch ex As Exception
            End Try
        End Sub

        ''' <summary>
        ''' Retourne le nom de l'utilisateur exécutant la session en cours.
        ''' </summary>
        ''' <param name="CheckDomain">permet de préfixer le nom d'utilisateur par le nom de domaine ou le nom de l'ordinateur</param>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function GetUsernameBySessionId(Optional ByVal checkDomain As Boolean = False) As String
            Dim buff As IntPtr
            Dim strLength As Integer
            Dim userName As String = "SYSTEM"
            Dim sessionId = NativeMethods.WTSGetActiveConsoleSessionId()
            If NativeMethods.WTSQuerySessionInformation(IntPtr.Zero, sessionId, NativeEnum.WTS_INFO_CLASS.WTSUserName, buff, strLength) AndAlso strLength > 1 Then
                userName = Marshal.PtrToStringAnsi(buff)
                NativeMethods.WTSFreeMemory(buff)
                If checkDomain Then
                    If NativeMethods.WTSQuerySessionInformation(IntPtr.Zero, sessionId, NativeEnum.WTS_INFO_CLASS.WTSDomainName, buff, strLength) AndAlso strLength > 1 Then
                        userName = Marshal.PtrToStringAnsi(buff) & "\" & userName
                        NativeMethods.WTSFreeMemory(buff)
                    End If
                End If
            End If
            Return userName
        End Function
#End Region

    End Class
End Namespace
