﻿Namespace Engine
    Public MustInherit Class Task

#Region " Properties "
        Protected Friend Property InitState As InitStates
        Public MustOverride ReadOnly Property InstallMessage As String
        Public MustOverride ReadOnly Property UninstallMessage As String
        Public MustOverride ReadOnly Property Name As String
        Public MustOverride ReadOnly Property Description As String
#End Region

#Region " Events "
        Public Event OnInitEvent(Sender As Object, InitInfos As InitInfos)
#End Region

#Region " Constructors "
        Sub New()
        End Sub
#End Region

#Region " Methods "
        Protected Overridable Sub OnInit(Sender As Object, InitInfos As InitInfos)
            RaiseEvent OnInitEvent(Sender, InitInfos)
        End Sub

        Public Overridable Sub Init()
        End Sub

        Public Overridable Sub Apply(ForceUninstall As Boolean)
        End Sub

        Public Overrides Function ToString() As String
            Return Me.Name
        End Function
#End Region

    End Class
End Namespace
