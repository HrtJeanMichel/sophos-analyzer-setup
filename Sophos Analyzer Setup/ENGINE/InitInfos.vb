﻿Namespace Engine
    Public Class InitInfos

#Region " Properties "
        Public Property TaskName As TaskNames
        Public Property State As InitStates
        Public Property Description As String
        Public Property ActionMessage As String
        Public Property StaticMessage As String
#End Region

    End Class
End Namespace
