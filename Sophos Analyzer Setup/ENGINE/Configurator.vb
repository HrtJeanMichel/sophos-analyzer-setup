﻿Imports System.ComponentModel

Namespace Engine
    Public Class Configurator

#Region " Properties "
        Protected Friend Property Tasks As List(Of Task)
        Protected Friend Property Bgw As BackgroundWorker
#End Region

#Region " Events "
        Public Event InitStatusEvent(Sender As Object, InitStates As InitInfos)
#End Region

#Region " Constructor "
        Sub New(Bgw As BackgroundWorker)
            Me.Tasks = New List(Of Task)
            Me.Bgw = Bgw
        End Sub
#End Region

#Region " Methods "
        Public Sub Initialize()
            For Each task As Task In Me.Tasks
                AddHandler task.OnInitEvent, AddressOf InitStatus
                task.Init()
            Next
        End Sub

        Public Sub Apply(ForceUninstall As Boolean)
            Dim tasksFilteredLessInvalid = Me.Tasks.Where(Function(f) f.InitState <> InitStates.Invalid)
            Dim tasksFiltered = If(ForceUninstall, tasksFilteredLessInvalid.Where(Function(f) f.InitState <> InitStates.Nearly),
                                                    tasksFilteredLessInvalid.Where(Function(f) f.InitState = InitStates.Nearly))
            Dim TaskCount As Short = tasksFiltered.Count
            Dim iCount As Integer = 0
            For Each task As Task In tasksFiltered
                Bgw.ReportProgress((iCount / TaskCount) * 100, If(ForceUninstall, task.UninstallMessage, If(task.InitState = InitStates.Nearly, task.InstallMessage, task.UninstallMessage)))
                task.Apply(ForceUninstall)
                Bgw.ReportProgress(200, task)
                iCount += 1
            Next
        End Sub

        Protected Friend Sub Load(Task As Task)
            If (Not Me.HasTask(Task.GetType)) Then Me.Tasks.Insert(0, Task)
        End Sub

        Private Sub InitStatus(Sender As Object, InitStates As InitInfos)
            RaiseEvent InitStatusEvent(Sender, InitStates)
        End Sub

        Protected Friend Function HasTask(Type As Type) As Boolean
            Return Me.Tasks.Any AndAlso Me.Tasks.Any(Function(task As Task) task.GetType Is Type)
        End Function

        Protected Friend Function HasTask(Of T)() As Boolean
            Return Me.Tasks.Any AndAlso Me.Tasks.Any(Function(task As Task) TypeOf task Is T)
        End Function

        Protected Friend Function GetTask(Of T)() As T
            If (Me.HasTask(Of T)()) Then Return Me.Tasks.Where(Function(task As Task) TypeOf task Is T).Cast(Of T)().FirstOrDefault()
        End Function
#End Region

    End Class
End Namespace
