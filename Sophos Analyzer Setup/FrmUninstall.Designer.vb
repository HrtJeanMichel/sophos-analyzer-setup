﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUninstall
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TlpMain = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.LblMain = New System.Windows.Forms.Label()
        Me.BgwUninstall = New System.ComponentModel.BackgroundWorker()
        Me.PcbMain = New System.Windows.Forms.PictureBox()
        Me.PgbMain = New System.Windows.Forms.ProgressBar()
        Me.TlpMain.SuspendLayout()
        CType(Me.PcbMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TlpMain
        '
        Me.TlpMain.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TlpMain.ColumnCount = 2
        Me.TlpMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TlpMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TlpMain.Controls.Add(Me.OK_Button, 0, 0)
        Me.TlpMain.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TlpMain.Location = New System.Drawing.Point(334, 101)
        Me.TlpMain.Name = "TlpMain"
        Me.TlpMain.RowCount = 1
        Me.TlpMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TlpMain.Size = New System.Drawing.Size(146, 29)
        Me.TlpMain.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "Oui"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Non"
        '
        'LblMain
        '
        Me.LblMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblMain.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMain.Location = New System.Drawing.Point(91, 30)
        Me.LblMain.Name = "LblMain"
        Me.LblMain.Size = New System.Drawing.Size(369, 57)
        Me.LblMain.TabIndex = 1
        Me.LblMain.Text = "Etes-vous sûr de vouloir supprimer Sophos Analyzer ainsi que le compte Windows ""s" &
    "ophos_autonome"" ?"
        Me.LblMain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BgwUninstall
        '
        Me.BgwUninstall.WorkerReportsProgress = True
        Me.BgwUninstall.WorkerSupportsCancellation = True
        '
        'PcbMain
        '
        Me.PcbMain.Image = Global.Sophos_Analyzer_Setup.My.Resources.Resources.Setup
        Me.PcbMain.Location = New System.Drawing.Point(20, 28)
        Me.PcbMain.Name = "PcbMain"
        Me.PcbMain.Size = New System.Drawing.Size(64, 64)
        Me.PcbMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbMain.TabIndex = 2
        Me.PcbMain.TabStop = False
        '
        'PgbMain
        '
        Me.PgbMain.Location = New System.Drawing.Point(-1, 133)
        Me.PgbMain.Name = "PgbMain"
        Me.PgbMain.Size = New System.Drawing.Size(493, 13)
        Me.PgbMain.TabIndex = 3
        Me.PgbMain.Visible = False
        '
        'FrmUninstall
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(492, 146)
        Me.Controls.Add(Me.PcbMain)
        Me.Controls.Add(Me.LblMain)
        Me.Controls.Add(Me.TlpMain)
        Me.Controls.Add(Me.PgbMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmUninstall"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Désinstallation de Sophos Analyzer"
        Me.TlpMain.ResumeLayout(False)
        CType(Me.PcbMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TlpMain As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents LblMain As Label
    Friend WithEvents PcbMain As PictureBox
    Friend WithEvents BgwUninstall As System.ComponentModel.BackgroundWorker
    Friend WithEvents PgbMain As ProgressBar
End Class
