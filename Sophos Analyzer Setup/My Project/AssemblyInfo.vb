﻿Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Vérifiez les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("Sophos Analyzer Setup")>
<Assembly: AssemblyDescription("Installeur et configurateur de Sophos Analyzer et Sophos Endpoint Security and Control")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("Sophos Analyzer Setup")>
<Assembly: AssemblyCopyright("Copyright © JmHrt 2018")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("a0d7dea4-e735-430d-b68b-1085d63cb993")>

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.0.0")>
<Assembly: AssemblyFileVersion("3.0.0.0")>
