﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.SpcMain = New System.Windows.Forms.SplitContainer()
        Me.PcbMain = New System.Windows.Forms.PictureBox()
        Me.SpcMiddle = New System.Windows.Forms.SplitContainer()
        Me.GbxSophosSav = New System.Windows.Forms.GroupBox()
        Me.PcbSophosSavInfos = New System.Windows.Forms.PictureBox()
        Me.LblSophosSav = New System.Windows.Forms.Label()
        Me.PcbSophosSav = New System.Windows.Forms.PictureBox()
        Me.TxbSophosSav = New System.Windows.Forms.TextBox()
        Me.GbxFramework = New System.Windows.Forms.GroupBox()
        Me.PcbFrameworkInfos = New System.Windows.Forms.PictureBox()
        Me.LblFramework = New System.Windows.Forms.Label()
        Me.PcbFramework = New System.Windows.Forms.PictureBox()
        Me.TxbFramework = New System.Windows.Forms.TextBox()
        Me.GbxSophos = New System.Windows.Forms.GroupBox()
        Me.PcbSophosInfos = New System.Windows.Forms.PictureBox()
        Me.LblSophos = New System.Windows.Forms.Label()
        Me.PcbSophos = New System.Windows.Forms.PictureBox()
        Me.TxbSophos = New System.Windows.Forms.TextBox()
        Me.GbxInstallDir = New System.Windows.Forms.GroupBox()
        Me.PcbAnalyzerInfos = New System.Windows.Forms.PictureBox()
        Me.LblInstallDir = New System.Windows.Forms.Label()
        Me.PcbInstallDir = New System.Windows.Forms.PictureBox()
        Me.TxbInstallDir = New System.Windows.Forms.TextBox()
        Me.GgxAccount = New System.Windows.Forms.GroupBox()
        Me.PcbAccountInfos = New System.Windows.Forms.PictureBox()
        Me.LblAccount = New System.Windows.Forms.Label()
        Me.PcbAccount = New System.Windows.Forms.PictureBox()
        Me.TxbAccount = New System.Windows.Forms.TextBox()
        Me.SpcBottom = New System.Windows.Forms.SplitContainer()
        Me.BtnInstall = New System.Windows.Forms.Button()
        Me.LblNotification = New System.Windows.Forms.Label()
        Me.PgbInstall = New System.Windows.Forms.ProgressBar()
        Me.BgwInstall = New System.ComponentModel.BackgroundWorker()
        Me.TtInfos = New System.Windows.Forms.ToolTip(Me.components)
        Me.SpcMain.Panel1.SuspendLayout()
        Me.SpcMain.Panel2.SuspendLayout()
        Me.SpcMain.SuspendLayout()
        CType(Me.PcbMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SpcMiddle.Panel1.SuspendLayout()
        Me.SpcMiddle.Panel2.SuspendLayout()
        Me.SpcMiddle.SuspendLayout()
        Me.GbxSophosSav.SuspendLayout()
        CType(Me.PcbSophosSavInfos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PcbSophosSav, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxFramework.SuspendLayout()
        CType(Me.PcbFrameworkInfos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PcbFramework, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxSophos.SuspendLayout()
        CType(Me.PcbSophosInfos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PcbSophos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxInstallDir.SuspendLayout()
        CType(Me.PcbAnalyzerInfos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PcbInstallDir, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GgxAccount.SuspendLayout()
        CType(Me.PcbAccountInfos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PcbAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SpcBottom.Panel1.SuspendLayout()
        Me.SpcBottom.Panel2.SuspendLayout()
        Me.SpcBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpcMain
        '
        Me.SpcMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SpcMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SpcMain.IsSplitterFixed = True
        Me.SpcMain.Location = New System.Drawing.Point(0, 0)
        Me.SpcMain.Name = "SpcMain"
        Me.SpcMain.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SpcMain.Panel1
        '
        Me.SpcMain.Panel1.Controls.Add(Me.PcbMain)
        '
        'SpcMain.Panel2
        '
        Me.SpcMain.Panel2.Controls.Add(Me.SpcMiddle)
        Me.SpcMain.Size = New System.Drawing.Size(638, 602)
        Me.SpcMain.SplitterDistance = 77
        Me.SpcMain.SplitterWidth = 5
        Me.SpcMain.TabIndex = 0
        '
        'PcbMain
        '
        Me.PcbMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PcbMain.Image = CType(resources.GetObject("PcbMain.Image"), System.Drawing.Image)
        Me.PcbMain.Location = New System.Drawing.Point(0, 0)
        Me.PcbMain.Name = "PcbMain"
        Me.PcbMain.Size = New System.Drawing.Size(638, 77)
        Me.PcbMain.TabIndex = 0
        Me.PcbMain.TabStop = False
        '
        'SpcMiddle
        '
        Me.SpcMiddle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SpcMiddle.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SpcMiddle.IsSplitterFixed = True
        Me.SpcMiddle.Location = New System.Drawing.Point(0, 0)
        Me.SpcMiddle.Name = "SpcMiddle"
        Me.SpcMiddle.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SpcMiddle.Panel1
        '
        Me.SpcMiddle.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.SpcMiddle.Panel1.Controls.Add(Me.GbxSophosSav)
        Me.SpcMiddle.Panel1.Controls.Add(Me.GbxFramework)
        Me.SpcMiddle.Panel1.Controls.Add(Me.GbxSophos)
        Me.SpcMiddle.Panel1.Controls.Add(Me.GbxInstallDir)
        Me.SpcMiddle.Panel1.Controls.Add(Me.GgxAccount)
        '
        'SpcMiddle.Panel2
        '
        Me.SpcMiddle.Panel2.Controls.Add(Me.SpcBottom)
        Me.SpcMiddle.Size = New System.Drawing.Size(638, 520)
        Me.SpcMiddle.SplitterDistance = 454
        Me.SpcMiddle.SplitterWidth = 5
        Me.SpcMiddle.TabIndex = 1
        '
        'GbxSophosSav
        '
        Me.GbxSophosSav.Controls.Add(Me.PcbSophosSavInfos)
        Me.GbxSophosSav.Controls.Add(Me.LblSophosSav)
        Me.GbxSophosSav.Controls.Add(Me.PcbSophosSav)
        Me.GbxSophosSav.Controls.Add(Me.TxbSophosSav)
        Me.GbxSophosSav.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GbxSophosSav.Location = New System.Drawing.Point(12, 273)
        Me.GbxSophosSav.Name = "GbxSophosSav"
        Me.GbxSophosSav.Size = New System.Drawing.Size(614, 82)
        Me.GbxSophosSav.TabIndex = 6
        Me.GbxSophosSav.TabStop = False
        Me.GbxSophosSav.Text = "Installation de Sophos Sav (Mise à jour)"
        '
        'PcbSophosSavInfos
        '
        Me.PcbSophosSavInfos.Image = Global.Sophos_Analyzer_Setup.My.Resources.Resources.Infos
        Me.PcbSophosSavInfos.Location = New System.Drawing.Point(577, 29)
        Me.PcbSophosSavInfos.Name = "PcbSophosSavInfos"
        Me.PcbSophosSavInfos.Size = New System.Drawing.Size(23, 23)
        Me.PcbSophosSavInfos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbSophosSavInfos.TabIndex = 7
        Me.PcbSophosSavInfos.TabStop = False
        '
        'LblSophosSav
        '
        Me.LblSophosSav.AutoSize = True
        Me.LblSophosSav.Location = New System.Drawing.Point(46, 58)
        Me.LblSophosSav.Name = "LblSophosSav"
        Me.LblSophosSav.Size = New System.Drawing.Size(0, 15)
        Me.LblSophosSav.TabIndex = 5
        '
        'PcbSophosSav
        '
        Me.PcbSophosSav.Location = New System.Drawing.Point(14, 29)
        Me.PcbSophosSav.Name = "PcbSophosSav"
        Me.PcbSophosSav.Size = New System.Drawing.Size(23, 23)
        Me.PcbSophosSav.TabIndex = 4
        Me.PcbSophosSav.TabStop = False
        '
        'TxbSophosSav
        '
        Me.TxbSophosSav.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TxbSophosSav.Location = New System.Drawing.Point(43, 29)
        Me.TxbSophosSav.Name = "TxbSophosSav"
        Me.TxbSophosSav.ReadOnly = True
        Me.TxbSophosSav.Size = New System.Drawing.Size(528, 23)
        Me.TxbSophosSav.TabIndex = 1
        Me.TxbSophosSav.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GbxFramework
        '
        Me.GbxFramework.Controls.Add(Me.PcbFrameworkInfos)
        Me.GbxFramework.Controls.Add(Me.LblFramework)
        Me.GbxFramework.Controls.Add(Me.PcbFramework)
        Me.GbxFramework.Controls.Add(Me.TxbFramework)
        Me.GbxFramework.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GbxFramework.Location = New System.Drawing.Point(12, 9)
        Me.GbxFramework.Name = "GbxFramework"
        Me.GbxFramework.Size = New System.Drawing.Size(614, 82)
        Me.GbxFramework.TabIndex = 5
        Me.GbxFramework.TabStop = False
        Me.GbxFramework.Text = "Framework compatible (requis : v4 minimum)"
        '
        'PcbFrameworkInfos
        '
        Me.PcbFrameworkInfos.Image = Global.Sophos_Analyzer_Setup.My.Resources.Resources.Infos
        Me.PcbFrameworkInfos.Location = New System.Drawing.Point(577, 29)
        Me.PcbFrameworkInfos.Name = "PcbFrameworkInfos"
        Me.PcbFrameworkInfos.Size = New System.Drawing.Size(23, 23)
        Me.PcbFrameworkInfos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbFrameworkInfos.TabIndex = 8
        Me.PcbFrameworkInfos.TabStop = False
        '
        'LblFramework
        '
        Me.LblFramework.AutoSize = True
        Me.LblFramework.Location = New System.Drawing.Point(46, 58)
        Me.LblFramework.Name = "LblFramework"
        Me.LblFramework.Size = New System.Drawing.Size(0, 15)
        Me.LblFramework.TabIndex = 4
        '
        'PcbFramework
        '
        Me.PcbFramework.Location = New System.Drawing.Point(14, 29)
        Me.PcbFramework.Name = "PcbFramework"
        Me.PcbFramework.Size = New System.Drawing.Size(23, 23)
        Me.PcbFramework.TabIndex = 1
        Me.PcbFramework.TabStop = False
        '
        'TxbFramework
        '
        Me.TxbFramework.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TxbFramework.Location = New System.Drawing.Point(43, 29)
        Me.TxbFramework.Name = "TxbFramework"
        Me.TxbFramework.ReadOnly = True
        Me.TxbFramework.Size = New System.Drawing.Size(528, 23)
        Me.TxbFramework.TabIndex = 0
        Me.TxbFramework.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GbxSophos
        '
        Me.GbxSophos.Controls.Add(Me.PcbSophosInfos)
        Me.GbxSophos.Controls.Add(Me.LblSophos)
        Me.GbxSophos.Controls.Add(Me.PcbSophos)
        Me.GbxSophos.Controls.Add(Me.TxbSophos)
        Me.GbxSophos.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GbxSophos.Location = New System.Drawing.Point(12, 361)
        Me.GbxSophos.Name = "GbxSophos"
        Me.GbxSophos.Size = New System.Drawing.Size(614, 82)
        Me.GbxSophos.TabIndex = 4
        Me.GbxSophos.TabStop = False
        Me.GbxSophos.Text = "Installation de Sophos Endpoint Security And Control (analyse à la demande)"
        '
        'PcbSophosInfos
        '
        Me.PcbSophosInfos.Image = Global.Sophos_Analyzer_Setup.My.Resources.Resources.Infos
        Me.PcbSophosInfos.Location = New System.Drawing.Point(577, 29)
        Me.PcbSophosInfos.Name = "PcbSophosInfos"
        Me.PcbSophosInfos.Size = New System.Drawing.Size(23, 23)
        Me.PcbSophosInfos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbSophosInfos.TabIndex = 7
        Me.PcbSophosInfos.TabStop = False
        '
        'LblSophos
        '
        Me.LblSophos.AutoSize = True
        Me.LblSophos.Location = New System.Drawing.Point(46, 58)
        Me.LblSophos.Name = "LblSophos"
        Me.LblSophos.Size = New System.Drawing.Size(0, 15)
        Me.LblSophos.TabIndex = 5
        '
        'PcbSophos
        '
        Me.PcbSophos.Location = New System.Drawing.Point(14, 29)
        Me.PcbSophos.Name = "PcbSophos"
        Me.PcbSophos.Size = New System.Drawing.Size(23, 23)
        Me.PcbSophos.TabIndex = 4
        Me.PcbSophos.TabStop = False
        '
        'TxbSophos
        '
        Me.TxbSophos.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TxbSophos.Location = New System.Drawing.Point(43, 29)
        Me.TxbSophos.Name = "TxbSophos"
        Me.TxbSophos.ReadOnly = True
        Me.TxbSophos.Size = New System.Drawing.Size(528, 23)
        Me.TxbSophos.TabIndex = 1
        Me.TxbSophos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GbxInstallDir
        '
        Me.GbxInstallDir.Controls.Add(Me.PcbAnalyzerInfos)
        Me.GbxInstallDir.Controls.Add(Me.LblInstallDir)
        Me.GbxInstallDir.Controls.Add(Me.PcbInstallDir)
        Me.GbxInstallDir.Controls.Add(Me.TxbInstallDir)
        Me.GbxInstallDir.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GbxInstallDir.Location = New System.Drawing.Point(12, 185)
        Me.GbxInstallDir.Name = "GbxInstallDir"
        Me.GbxInstallDir.Size = New System.Drawing.Size(614, 82)
        Me.GbxInstallDir.TabIndex = 3
        Me.GbxInstallDir.TabStop = False
        Me.GbxInstallDir.Text = "Installation de Sophos Analyzer (Configurateur + Analyseur + Raccourci bureau + D" &
    "ésinstalleur)"
        '
        'PcbAnalyzerInfos
        '
        Me.PcbAnalyzerInfos.Image = Global.Sophos_Analyzer_Setup.My.Resources.Resources.Infos
        Me.PcbAnalyzerInfos.Location = New System.Drawing.Point(577, 29)
        Me.PcbAnalyzerInfos.Name = "PcbAnalyzerInfos"
        Me.PcbAnalyzerInfos.Size = New System.Drawing.Size(23, 23)
        Me.PcbAnalyzerInfos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbAnalyzerInfos.TabIndex = 6
        Me.PcbAnalyzerInfos.TabStop = False
        '
        'LblInstallDir
        '
        Me.LblInstallDir.AutoSize = True
        Me.LblInstallDir.Location = New System.Drawing.Point(46, 58)
        Me.LblInstallDir.Name = "LblInstallDir"
        Me.LblInstallDir.Size = New System.Drawing.Size(0, 15)
        Me.LblInstallDir.TabIndex = 4
        '
        'PcbInstallDir
        '
        Me.PcbInstallDir.Location = New System.Drawing.Point(14, 29)
        Me.PcbInstallDir.Name = "PcbInstallDir"
        Me.PcbInstallDir.Size = New System.Drawing.Size(23, 23)
        Me.PcbInstallDir.TabIndex = 3
        Me.PcbInstallDir.TabStop = False
        '
        'TxbInstallDir
        '
        Me.TxbInstallDir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TxbInstallDir.Location = New System.Drawing.Point(43, 29)
        Me.TxbInstallDir.Name = "TxbInstallDir"
        Me.TxbInstallDir.ReadOnly = True
        Me.TxbInstallDir.Size = New System.Drawing.Size(528, 23)
        Me.TxbInstallDir.TabIndex = 1
        Me.TxbInstallDir.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GgxAccount
        '
        Me.GgxAccount.Controls.Add(Me.PcbAccountInfos)
        Me.GgxAccount.Controls.Add(Me.LblAccount)
        Me.GgxAccount.Controls.Add(Me.PcbAccount)
        Me.GgxAccount.Controls.Add(Me.TxbAccount)
        Me.GgxAccount.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GgxAccount.Location = New System.Drawing.Point(12, 97)
        Me.GgxAccount.Name = "GgxAccount"
        Me.GgxAccount.Size = New System.Drawing.Size(614, 82)
        Me.GgxAccount.TabIndex = 2
        Me.GgxAccount.TabStop = False
        Me.GgxAccount.Text = "Compte utilisateur (pour les opérations d'analyse anti-virus)"
        '
        'PcbAccountInfos
        '
        Me.PcbAccountInfos.Image = Global.Sophos_Analyzer_Setup.My.Resources.Resources.Infos
        Me.PcbAccountInfos.Location = New System.Drawing.Point(577, 29)
        Me.PcbAccountInfos.Name = "PcbAccountInfos"
        Me.PcbAccountInfos.Size = New System.Drawing.Size(23, 23)
        Me.PcbAccountInfos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbAccountInfos.TabIndex = 5
        Me.PcbAccountInfos.TabStop = False
        '
        'LblAccount
        '
        Me.LblAccount.AutoSize = True
        Me.LblAccount.Location = New System.Drawing.Point(46, 58)
        Me.LblAccount.Name = "LblAccount"
        Me.LblAccount.Size = New System.Drawing.Size(0, 15)
        Me.LblAccount.TabIndex = 3
        '
        'PcbAccount
        '
        Me.PcbAccount.Location = New System.Drawing.Point(14, 29)
        Me.PcbAccount.Name = "PcbAccount"
        Me.PcbAccount.Size = New System.Drawing.Size(23, 23)
        Me.PcbAccount.TabIndex = 2
        Me.PcbAccount.TabStop = False
        '
        'TxbAccount
        '
        Me.TxbAccount.BackColor = System.Drawing.Color.White
        Me.TxbAccount.Location = New System.Drawing.Point(43, 29)
        Me.TxbAccount.Name = "TxbAccount"
        Me.TxbAccount.ReadOnly = True
        Me.TxbAccount.Size = New System.Drawing.Size(528, 23)
        Me.TxbAccount.TabIndex = 0
        Me.TxbAccount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'SpcBottom
        '
        Me.SpcBottom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SpcBottom.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SpcBottom.IsSplitterFixed = True
        Me.SpcBottom.Location = New System.Drawing.Point(0, 0)
        Me.SpcBottom.Name = "SpcBottom"
        Me.SpcBottom.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SpcBottom.Panel1
        '
        Me.SpcBottom.Panel1.Controls.Add(Me.BtnInstall)
        Me.SpcBottom.Panel1.Controls.Add(Me.LblNotification)
        '
        'SpcBottom.Panel2
        '
        Me.SpcBottom.Panel2.Controls.Add(Me.PgbInstall)
        Me.SpcBottom.Size = New System.Drawing.Size(638, 61)
        Me.SpcBottom.SplitterDistance = 29
        Me.SpcBottom.TabIndex = 0
        '
        'BtnInstall
        '
        Me.BtnInstall.BackColor = System.Drawing.Color.MintCream
        Me.BtnInstall.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BtnInstall.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnInstall.ForeColor = System.Drawing.Color.Black
        Me.BtnInstall.Location = New System.Drawing.Point(0, 0)
        Me.BtnInstall.Name = "BtnInstall"
        Me.BtnInstall.Size = New System.Drawing.Size(638, 29)
        Me.BtnInstall.TabIndex = 6
        Me.BtnInstall.UseVisualStyleBackColor = False
        '
        'LblNotification
        '
        Me.LblNotification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblNotification.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNotification.Location = New System.Drawing.Point(0, 0)
        Me.LblNotification.Name = "LblNotification"
        Me.LblNotification.Size = New System.Drawing.Size(638, 29)
        Me.LblNotification.TabIndex = 5
        Me.LblNotification.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PgbInstall
        '
        Me.PgbInstall.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PgbInstall.Location = New System.Drawing.Point(0, 0)
        Me.PgbInstall.Name = "PgbInstall"
        Me.PgbInstall.Size = New System.Drawing.Size(638, 28)
        Me.PgbInstall.Step = 1
        Me.PgbInstall.TabIndex = 1
        '
        'BgwInstall
        '
        Me.BgwInstall.WorkerReportsProgress = True
        Me.BgwInstall.WorkerSupportsCancellation = True
        '
        'TtInfos
        '
        Me.TtInfos.AutoPopDelay = 10000
        Me.TtInfos.InitialDelay = 100
        Me.TtInfos.ReshowDelay = 100
        Me.TtInfos.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.TtInfos.ToolTipTitle = "Information :"
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 602)
        Me.Controls.Add(Me.SpcMain)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.SpcMain.Panel1.ResumeLayout(False)
        Me.SpcMain.Panel2.ResumeLayout(False)
        Me.SpcMain.ResumeLayout(False)
        CType(Me.PcbMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SpcMiddle.Panel1.ResumeLayout(False)
        Me.SpcMiddle.Panel2.ResumeLayout(False)
        Me.SpcMiddle.ResumeLayout(False)
        Me.GbxSophosSav.ResumeLayout(False)
        Me.GbxSophosSav.PerformLayout()
        CType(Me.PcbSophosSavInfos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PcbSophosSav, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxFramework.ResumeLayout(False)
        Me.GbxFramework.PerformLayout()
        CType(Me.PcbFrameworkInfos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PcbFramework, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxSophos.ResumeLayout(False)
        Me.GbxSophos.PerformLayout()
        CType(Me.PcbSophosInfos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PcbSophos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxInstallDir.ResumeLayout(False)
        Me.GbxInstallDir.PerformLayout()
        CType(Me.PcbAnalyzerInfos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PcbInstallDir, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GgxAccount.ResumeLayout(False)
        Me.GgxAccount.PerformLayout()
        CType(Me.PcbAccountInfos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PcbAccount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SpcBottom.Panel1.ResumeLayout(False)
        Me.SpcBottom.Panel2.ResumeLayout(False)
        Me.SpcBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SpcMain As SplitContainer
    Friend WithEvents SpcMiddle As SplitContainer
    Friend WithEvents GgxAccount As GroupBox
    Friend WithEvents GbxSophos As GroupBox
    Friend WithEvents GbxInstallDir As GroupBox
    Friend WithEvents TxbSophos As TextBox
    Friend WithEvents TxbInstallDir As TextBox
    Friend WithEvents TxbAccount As TextBox
    Friend WithEvents GbxFramework As GroupBox
    Friend WithEvents TxbFramework As TextBox
    Friend WithEvents PcbFramework As PictureBox
    Friend WithEvents PcbSophos As PictureBox
    Friend WithEvents PcbInstallDir As PictureBox
    Friend WithEvents PcbAccount As PictureBox
    Friend WithEvents LblAccount As Label
    Friend WithEvents LblFramework As Label
    Friend WithEvents LblInstallDir As Label
    Friend WithEvents SpcBottom As SplitContainer
    Friend WithEvents LblSophos As Label
    Friend WithEvents PgbInstall As ProgressBar
    Friend WithEvents BgwInstall As System.ComponentModel.BackgroundWorker
    Friend WithEvents BtnInstall As Button
    Friend WithEvents LblNotification As Label
    Friend WithEvents PcbMain As PictureBox
    Friend WithEvents TtInfos As ToolTip
    Friend WithEvents PcbFrameworkInfos As PictureBox
    Friend WithEvents PcbSophosInfos As PictureBox
    Friend WithEvents PcbAnalyzerInfos As PictureBox
    Friend WithEvents PcbAccountInfos As PictureBox
    Friend WithEvents GbxSophosSav As GroupBox
    Friend WithEvents PcbSophosSavInfos As PictureBox
    Friend WithEvents LblSophosSav As Label
    Friend WithEvents PcbSophosSav As PictureBox
    Friend WithEvents TxbSophosSav As TextBox
End Class
