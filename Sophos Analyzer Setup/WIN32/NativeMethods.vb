﻿Imports System.Runtime.InteropServices
Imports System.Text

Namespace Win32
    Public NotInheritable Class NativeMethods

#Region " Helpers.Utils "
        Public Declare Auto Function NetGetJoinInformation Lib "Netapi32.dll" (server As String, ByRef IntPtr As IntPtr, ByRef status As NativeEnum.NetJoinStatus) As Integer
        Public Declare Auto Function NetApiBufferFree Lib "Netapi32.dll" (Buffer As IntPtr) As Integer

        <DllImport("user32.dll", SetLastError:=True)>
        Public Shared Function CancelShutdown() As Integer
        End Function
#End Region

#Region " Helpers.Profile "
        <DllImport("userenv.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
        Public Shared Function CreateProfile(<MarshalAs(UnmanagedType.LPWStr)> pszUserSid As String, <MarshalAs(UnmanagedType.LPWStr)> pszUserName As String, <Out, MarshalAs(UnmanagedType.LPWStr)> pszProfilePath As StringBuilder, cchProfilePath As UInteger) As Integer
        End Function

        <DllImport("userenv.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
        Public Shared Function DeleteProfile(<MarshalAs(UnmanagedType.LPWStr)> lpSidString As String, <MarshalAs(UnmanagedType.LPWStr)> lpProfilePath As String, <MarshalAs(UnmanagedType.LPWStr)> lpComputerName As String) As Integer
        End Function
#End Region

#Region " Core.UserInfos "
        <DllImport("wtsapi32.dll", SetLastError:=True)>
        Public Shared Function WTSLogoffSession(hServer As IntPtr, SessionId As Integer, bWait As Boolean) As Boolean
        End Function

        <DllImport("kernel32.dll")>
        Public Shared Function WTSGetActiveConsoleSessionId() As UInteger
        End Function

        <DllImport("Wtsapi32.dll")>
        Public Shared Function WTSQuerySessionInformation(hServer As IntPtr, sessionId As UInteger, wtsInfoClass As NativeEnum.WTS_INFO_CLASS, ByRef ppBuffer As IntPtr, ByRef pBytesReturned As UInteger) As Boolean
        End Function

        <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
        Public Shared Function WTSOpenServer(ByVal pServerName As String) As IntPtr
        End Function

        <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
        Public Shared Sub WTSCloseServer(ByVal hServer As IntPtr)
        End Sub

        <DllImport("wtsapi32.dll", BestFitMapping:=True, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Auto, EntryPoint:="WTSEnumerateSessions", SetLastError:=True, ThrowOnUnmappableChar:=True)>
        Public Shared Function WTSEnumerateSessions(ByVal hServer As IntPtr, <MarshalAs(UnmanagedType.U4)> ByVal Reserved As Integer, <MarshalAs(UnmanagedType.U4)> ByVal Version As Integer, ByRef ppSessionInfo As IntPtr, <MarshalAs(UnmanagedType.U4)> ByRef pCount As Integer) As Integer
        End Function

        <DllImport("wtsapi32.dll")>
        Public Shared Sub WTSFreeMemory(pMemory As IntPtr)
        End Sub

#End Region

    End Class
End Namespace
