﻿Imports System.Runtime.InteropServices

Namespace Win32
    Public NotInheritable Class NativeStruct

#Region " Core.UserInfos "
        <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
        Public Structure WTS_SESSION_INFO
            Dim SessionID As Integer 'DWORD integer
            Dim pWinStationName As String ' integer LPTSTR - Pointer to a null-terminated string containing the name of the WinStation for this session
            Dim State As NativeEnum.WTS_CONNECTSTATE_CLASS
        End Structure

        Public Structure strSessionsInfo
            Dim SessionID As Integer
            Dim ConnectionState As String
            Dim SessionUserName As String
        End Structure
#End Region

    End Class
End Namespace
