﻿Imports System.ComponentModel
Imports Sophos_Analyzer_Setup.Core
Imports Sophos_Analyzer_Setup.Engine
Imports Sophos_Analyzer_Setup.Helper

Public Class FrmMain

#Region " Fields "
    Private m_TaskOngoing As Boolean
    Private m_ZipExtractedAllFinished As Boolean
    Private m_Configurator As Configurator
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeComponent()
        m_Configurator = New Configurator(BgwInstall)
        m_Configurator.Load(New Sophos)
        m_Configurator.Load(New SophosSav)
        m_Configurator.Load(New Analyzer)
        m_Configurator.Load(New Account)
        m_Configurator.Load(New Framework)
        AddHandler m_Configurator.InitStatusEvent, AddressOf m_Configurator_InitStatusEvent
        If Reg.Mounted Then Reg.Unload()
    End Sub
#End Region

#Region " Methods "
    Private Sub FrmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CheckInit()
    End Sub

    Private Sub CheckInit()
        m_Configurator.Initialize()
        SetInstallBtn()
    End Sub

    Private Sub FrmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason.Equals(CloseReason.UserClosing) Then
            If m_TaskOngoing Then
                e.Cancel = True
                MessageBox.Show("Vous ne devez pas interrompre la tâche durant l'installation !" & vbNewLine & "La fermeture a été annulée, veuillez attendre la fin de la tâche.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        ElseIf e.CloseReason.Equals(CloseReason.WindowsShutDown) Then
            If m_TaskOngoing Then
                Utils.ShutdownCancel()
                MessageBox.Show("Vous ne devez pas interrompre la tâche durant l'installation !" & vbNewLine & "L'extinction a été annulée, veuillez attendre la fin de la tâche.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub

    Private Sub m_Configurator_InitStatusEvent(sender As Object, InitInfos As InitInfos)
        Select Case InitInfos.TaskName
            Case TaskNames.Framework
                Select Case InitInfos.State
                    Case InitStates.Invalid
                        PcbFramework.Image = My.Resources.Invalid
                        PcbFramework.Tag = InitStates.Invalid
                        LblFramework.ForeColor = Color.Red
                    Case InitStates.Valid
                        PcbFramework.Image = My.Resources.Valid
                        PcbFramework.Tag = InitStates.Valid
                        LblFramework.ForeColor = Color.Green
                    Case InitStates.Nearly
                        PcbFramework.Image = My.Resources.Nearly
                        PcbFramework.Tag = InitStates.Nearly
                        LblFramework.ForeColor = Color.DarkOrange
                End Select
                LblFramework.Text = InitInfos.ActionMessage
                TxbFramework.Text = InitInfos.StaticMessage
                PcbFrameworkInfos.Tag = InitInfos.Description
            Case TaskNames.Account
                Select Case InitInfos.State
                    Case InitStates.Valid
                        PcbAccount.Image = My.Resources.Valid
                        PcbAccount.Tag = InitStates.Valid
                        LblAccount.ForeColor = Color.Green
                    Case InitStates.Nearly
                        PcbAccount.Image = My.Resources.Nearly
                        PcbAccount.Tag = InitStates.Nearly
                        LblAccount.ForeColor = Color.Orange
                End Select
                LblAccount.Text = InitInfos.ActionMessage
                TxbAccount.Text = InitInfos.StaticMessage
                PcbAccountInfos.Tag = InitInfos.Description
            Case TaskNames.Analyzer
                Select Case InitInfos.State
                    Case InitStates.Valid
                        PcbInstallDir.Image = My.Resources.Valid
                        PcbInstallDir.Tag = InitStates.Valid
                        LblInstallDir.ForeColor = Color.Green
                    Case InitStates.Nearly
                        PcbInstallDir.Image = My.Resources.Nearly
                        PcbInstallDir.Tag = InitStates.Nearly
                        LblInstallDir.ForeColor = Color.Orange
                End Select
                LblInstallDir.Text = InitInfos.ActionMessage
                TxbInstallDir.Text = InitInfos.StaticMessage
                PcbAnalyzerInfos.Tag = InitInfos.Description
            Case TaskNames.SophosSav
                Select Case InitInfos.State
                    Case InitStates.Invalid
                        LblSophosSav.ForeColor = Color.Red
                        PcbSophosSav.Tag = InitStates.Invalid
                        PcbSophosSav.Image = My.Resources.Invalid
                    Case InitStates.Valid
                        LblSophosSav.ForeColor = Color.Green
                        PcbSophosSav.Tag = InitStates.Valid
                        PcbSophosSav.Image = My.Resources.Valid
                    Case InitStates.Nearly
                        PcbSophosSav.Image = My.Resources.Nearly
                        PcbSophosSav.Tag = InitStates.Nearly
                        LblSophosSav.ForeColor = Color.DarkOrange
                End Select
                LblSophosSav.Text = InitInfos.ActionMessage
                TxbSophosSav.Text = InitInfos.StaticMessage
                PcbSophosSavInfos.Tag = InitInfos.Description
            Case TaskNames.Sophos
                Select Case InitInfos.State
                    Case InitStates.Invalid
                        LblSophos.ForeColor = Color.Red
                        PcbSophos.Tag = InitStates.Invalid
                        PcbSophos.Image = My.Resources.Invalid
                    Case InitStates.Valid
                        LblSophos.ForeColor = Color.Green
                        PcbSophos.Tag = InitStates.Valid
                        PcbSophos.Image = My.Resources.Valid
                    Case InitStates.Nearly
                        PcbSophos.Image = My.Resources.Nearly
                        PcbSophos.Tag = InitStates.Nearly
                        LblSophos.ForeColor = Color.DarkOrange
                End Select
                LblSophos.Text = InitInfos.ActionMessage
                TxbSophos.Text = InitInfos.StaticMessage
                PcbSophosInfos.Tag = InitInfos.Description
        End Select
    End Sub

    Private Sub SetControlsState(Enabled As Boolean)
        GbxFramework.Enabled = Enabled
        GgxAccount.Enabled = Enabled
        GbxInstallDir.Enabled = Enabled
        GbxSophos.Enabled = Enabled
        GbxSophosSav.Enabled = Enabled
        BtnInstall.Visible = Enabled
    End Sub

    Private Sub SetInstallBtn()
        Dim BlnValues As Integer() = New Integer(4) {}
        BlnValues(0) = PcbFramework.Tag
        BlnValues(1) = PcbAccount.Tag
        BlnValues(2) = PcbInstallDir.Tag
        BlnValues(3) = PcbSophosSav.Tag
        BlnValues(4) = PcbSophos.Tag

        If BlnValues.Any(Function(f) f = InitStates.Invalid) Then
            BtnInstall.Enabled = False
            BtnInstall.Text = ""
        ElseIf BlnValues.Any(Function(f) f = InitStates.Nearly) Then
            BtnInstall.Enabled = True
            BtnInstall.Text = "Installer"
        ElseIf BlnValues.All(Function(f) f = InitStates.valid) Then
            BtnInstall.Enabled = True
            BtnInstall.Text = "Désinstaller"
        End If
    End Sub

    Private Sub BtnInstall_Click(sender As Object, e As EventArgs) Handles BtnInstall.Click
        If Not BgwInstall.IsBusy Then
            m_TaskOngoing = True

            SetControlsState(False)

            Dim BlnValues As Integer() = New Integer(4) {}
            BlnValues(0) = PcbFramework.Tag
            BlnValues(1) = PcbAccount.Tag
            BlnValues(2) = PcbInstallDir.Tag
            BlnValues(3) = PcbSophosSav.Tag
            BlnValues(4) = PcbSophos.Tag

            Dim Uninstall = BlnValues.All(Function(f) f = InitStates.Valid)

            BgwInstall.RunWorkerAsync(Uninstall)
        End If
    End Sub

    Private Sub BgwInstall_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwInstall.DoWork
        Dim uninstall As Boolean = e.Argument
        Try
            m_Configurator.Apply(uninstall)
            e.Result = Tuple.Create(True, If(uninstall, "La désinstallation s'est déroulée avec succès.", "L'installation s'est déroulée avec succès." & vbNewLine & "Le raccourci est accessible depuis votre bureau."))
        Catch ex As Exception
            e.Result = Tuple.Create(False, ex.Message)
        End Try
    End Sub

    Private Sub BgwInstall_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwInstall.ProgressChanged
        If e.ProgressPercentage = 200 Then
            Dim Tsk = TryCast(e.UserState, Task)
            Tsk.Init()
        Else
            LblNotification.Text = e.UserState.ToString
            PgbInstall.Value = e.ProgressPercentage
        End If
    End Sub

    Private Sub BgwInstall_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwInstall.RunWorkerCompleted
        PgbInstall.Value = 100
        If Not e.Result Is Nothing Then
            Dim tups = TryCast(e.Result, Tuple(Of Boolean, String))
            Select Case tups.Item1
                Case True
                    If tups.Item2.StartsWith("L'installation ") Then
                        MessageBox.Show(tups.Item2, "Opération réussie", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ElseIf tups.Item2.StartsWith("La désinstallation ") Then
                        MessageBox.Show(tups.Item2 & vbNewLine & "L'ordinateur va redémarrer !", "Opération réussie", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Utils.StartCommand(Utils.GetShutdownExeFilePath, "-r -t 0", True)
                    End If
                Case False
                    MessageBox.Show(tups.Item2, "Echec de l'opération", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Select

            SetInstallBtn()
            SetControlsState(True)
            m_TaskOngoing = False
        End If
    End Sub

    Private Sub PcbFrameworkInfos_MouseHover(sender As Object, e As EventArgs) Handles PcbSophosInfos.MouseHover, PcbSophosSavInfos.MouseHover, PcbFrameworkInfos.MouseHover, PcbAnalyzerInfos.MouseHover, PcbAccountInfos.MouseHover
        TtInfos.Show(sender.tag, sender)
    End Sub

#End Region

End Class
