﻿Imports System.Threading
Imports Sophos_Analyzer_Setup.Helper

Friend Class Program

#Region " Events "
    Private Shared WithEvents DLLDomain As AppDomain = AppDomain.CurrentDomain
#End Region

#Region " Constructor "
    Shared Sub New()
        AddHandler DLLDomain.AssemblyResolve, AddressOf DLL_AssemblyResolve
    End Sub
#End Region

#Region " Methods "
    Private Shared Function DLL_AssemblyResolve(sender As Object, args As ResolveEventArgs) As Reflection.Assembly
        If args.Name.Contains("Interop.WindowsInstaller") Then
            Return Reflection.Assembly.Load(My.Resources.Interop_WindowsInstaller)
        ElseIf args.Name.Contains("Interop.IWshRuntimeLibrary") Then
            Return Reflection.Assembly.Load(My.Resources.Interop_IWshRuntimeLibrary)
        ElseIf args.Name.Contains("Interop.TaskScheduler") Then
            Return Reflection.Assembly.Load(My.Resources.Interop_TaskScheduler)
        ElseIf args.Name.Contains("Ionic.Zip.Reduced") Then
            Return Reflection.Assembly.Load(My.Resources.Ionic_Zip_Reduced)
        Else
            Return Nothing
        End If
    End Function

    <STAThread()>
    Public Shared Sub Main(Args As String())
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Dim instanceCountOne As Boolean = False
        Using mtex As Mutex = New Mutex(True, Application.ProductName, instanceCountOne)
            If Utils.CurrentIsAdministrator Then
                If Utils.isCorrectOS Then
                    If Utils.IsComputerInDomain Then
                        MessageBox.Show("Ce programme ne peut pas être exécuté sur un ordinateur membre d'un domaine !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Else
                        If Args.Length = 0 Then
                            If instanceCountOne Then
                                Application.Run(New FrmMain)
                                mtex.ReleaseMutex()
                            End If
                        Else
                            Dim cmd As New Cmdline(Args)
                            If (cmd.Item("uninstall") Is Nothing) Then
                            Else
                                Dim cValue = cmd("uninstall")
                                If String.IsNullOrEmpty(cValue) Then
                                Else
                                    If Utils.ProcessIsRunning("Sophos Analyzer Setup") Then
                                        MessageBox.Show("Veuillez fermer ""Sophos Analyzer Setup"" avant de procéder à la désinstallation de ""Sophos Analyzer"" !", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Else
                                        Application.Run(New FrmUninstall)
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    MessageBox.Show("Ce programme est optimisé pour le système d'exploitation suivant : " & vbNewLine & "- " & Utils.GetOsInline, "Mauvais environnement", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show("Ce programme ne sera pas exécuté depuis une élévation de privilèges, veuillez l'exécuter depuis un compte membre du groupe ""Administrateurs""!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Using
    End Sub

#End Region

End Class
