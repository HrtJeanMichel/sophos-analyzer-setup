﻿Imports System.IO
Imports System.Threading
Imports Ionic.Zip
Imports Microsoft.Win32
Imports Sophos_Analyzer_Setup.Engine
Imports Sophos_Analyzer_Setup.Helper

Namespace Core
    Public Class Sophos
        Inherits Task

#Region " Fields "
        Private m_ZipExtractedAllFinished As Boolean
        Private Const m_UninstallRegistryKey = "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"
        Private Const m_UninstallRegistryKeyWow6432 = "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall"
        Private Const m_VirusDetectionDllTempPath = "C:\Temp\CIDs\S000\SAVSCFXP\savxp\program files\Sophos\Sophos Anti-Virus\VirusDetection.dll"
        Private Const m_SophosMajPath = "C:\maj_sophos"
#End Region

#Region " Properties "
        Public Overrides ReadOnly Property Name As String
            Get
                Return "Sophos"
            End Get
        End Property

        Public Overrides ReadOnly Property Description As String
            Get
                Return "Vérification de la présence du produit Sophos Endpoint Security And Control " & vbNewLine &
                    "Il est possible d'installer ou de désinstaller le produit et tous ses composants."
            End Get
        End Property

        Public Overrides ReadOnly Property InstallMessage As String
            Get
                Return "Décompression de l'archive et installation de Sophos Anti-Virus ..."
            End Get
        End Property

        Public Overrides ReadOnly Property UninstallMessage As String
            Get
                Return "Désinstallation de Sophos Anti-Virus ..."
            End Get
        End Property
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub
#End Region

#Region " Methods "
        Public Overrides Sub Init()
            If Utils.SophosInstalledFromDirectories() Then
                Dim fVersion = FileVersionInfo.GetVersionInfo(Utils.GetProgramFilesDir & "\Sophos\Sophos Anti-Virus\VirusDetection.dll")
                InitState = InitStates.Valid
                MyBase.OnInit(Me, New InitInfos With {
                                            .TaskName = TaskNames.Sophos,
                                            .State = InitState,
                                            .ActionMessage = "Sophos Anti-Virus est installé.",
                                            .StaticMessage = If(fVersion Is Nothing, String.Empty, fVersion.FileVersion),
                                            .Description = Description})
            Else
                Dim source As String = Path.Combine(My.Application.Info.DirectoryPath, "CIDs_sans_carte_reseau.zip")
                If File.Exists(source) Then
                    Dim result As Boolean = False
                    Try
                        Using zip As ZipFile = ZipFile.Read(source)
                            For Each f In zip.EntriesSorted
                                If f.FileName = "CIDs/S000/SAVSCFXP/savxp/program files/Sophos/Sophos Anti-Virus/VirusDetection.dll" Then
                                    f.Extract("C:\Temp", ExtractExistingFileAction.OverwriteSilently)
                                    If File.Exists(m_VirusDetectionDllTempPath) Then
                                        Dim fVersion = FileVersionInfo.GetVersionInfo(m_VirusDetectionDllTempPath)
                                        InitState = InitStates.Nearly
                                        MyBase.OnInit(Me, New InitInfos With {
                                                                        .TaskName = TaskNames.Sophos,
                                                                        .State = InitStates.Nearly,
                                                                        .ActionMessage = "Sophos Anti-Virus " & If(fVersion Is Nothing, String.Empty, fVersion.FileVersion) & " sera installé grâce au setup d'installation localisé à l'emplacement ci-dessus.",
                                                                        .StaticMessage = source,
                                                                        .Description = Description})

                                    End If
                                    Exit For
                                End If
                            Next
                        End Using
                    Catch ex As Exception
                        InitState = InitStates.Invalid
                        MyBase.OnInit(Me, New InitInfos With {
                                                                    .TaskName = TaskNames.Sophos,
                                                                    .State = InitState,
                                                                    .ActionMessage = "L'archive ""CIDs"" a rencontrée une erreur : " & ex.Message,
                                                                    .StaticMessage = String.Empty,
                                                                    .Description = Description})
                    End Try



                Else
                    InitState = InitStates.Invalid
                    MyBase.OnInit(Me, New InitInfos With {
                                                                .TaskName = TaskNames.Sophos,
                                                                .State = InitState,
                                                                .ActionMessage = "Sophos Anti-Virus n'est pas installé !",
                                                                .StaticMessage = String.Empty,
                                                                .Description = Description})

                End If
            End If
            MyBase.Init()
        End Sub

        Public Overrides Sub Apply(ForceUninstall As Boolean)
            If ForceUninstall Then
                Uninstall()
            Else
                Select Case InitState
                    Case InitStates.Valid
                        Uninstall()
                    Case InitStates.Nearly
                        Install()
                End Select
            End If
            MyBase.Apply(ForceUninstall)
        End Sub

        Private Sub Install()
            If Not Directory.Exists(m_SophosMajPath) Then
                Directory.CreateDirectory(m_SophosMajPath)
            End If
            Utils.DeleteDirectoryAndContent(m_SophosMajPath & "\CIDs")

            m_ZipExtractedAllFinished = False
            Dim source As String = Path.Combine(My.Application.Info.DirectoryPath, "CIDs_sans_carte_reseau.zip")
            If File.Exists(source) Then
                Dim result As Boolean = False
                Using zip As ZipFile = ZipFile.Read(source)
                    AddHandler zip.ExtractProgress, AddressOf Zip_ExtractProgress
                    zip.ExtractAll(m_SophosMajPath, ExtractExistingFileAction.OverwriteSilently)
                End Using

                Do
                    If m_ZipExtractedAllFinished = False Then
                        Thread.Sleep(1000)
                    Else
                        Utils.DeleteDirectoryAndContent("C:\Temp\CIDs")
                        Exit Do
                    End If
                Loop

                If File.Exists(m_SophosMajPath & "\CIDs\S000\SAVSCFXP\setup.exe") Then
                    Utils.StartCommand(m_SophosMajPath & "\CIDs\S000\SAVSCFXP\setup.exe", "-mng no -crt R -updp " & m_SophosMajPath & "\CIDs\S000\SAVSCFXP\ -ni", True)

                    File.WriteAllBytes("C:\ProgramData\Sophos\AutoUpdate\Config\iconn.cfg", My.Resources.iconn)
                    File.WriteAllBytes("C:\ProgramData\Sophos\AutoUpdate\Config\iconnlocal.cfg", My.Resources.iconnlocal)
                    File.WriteAllBytes("C:\ProgramData\Sophos\AutoUpdate\Config\isched.cfg", My.Resources.isched)
                    File.WriteAllBytes("C:\ProgramData\Sophos\AutoUpdate\Config\ischedlocal.cfg", My.Resources.ischedlocal)

                    Do
                        If IsProcessRunning("ALUpdate") Then
                            Thread.Sleep(1000)
                        Else
                            Exit Do
                        End If
                    Loop
                Else
                    'Zip file doesn't extracted correctly !
                End If
            Else
                'Zip file doesn't exists !
            End If
        End Sub

        Private Function IsProcessRunning(sProcessName As String) As Boolean
            Dim proc As Process() = Process.GetProcessesByName(sProcessName)
            Return If(proc.Length > 0, True, False)
        End Function

        Private Sub Zip_ExtractProgress(sender As Object, e As ExtractProgressEventArgs)
            If e.EventType = ZipProgressEventType.Extracting_AfterExtractAll Then
                m_ZipExtractedAllFinished = True
            End If
        End Sub

        Private Sub Uninstall()
            If Utils.SophosInstalledFromDirectories() Then
                Utils.StopTheService("SntpService")
                Utils.StopTheService("Sophos AutoUpdate Service")
                Utils.StopTheService("sophossps")
                Utils.StopTheService("Sophos Web Control Service")
                Utils.StopTheService("swi_service")

                For Each SProd In SophosProducts()
                    If SProd.UninstallString <> String.Empty Then
                        If SProd.IsMsiUninstaller Then
                            If SProd.UninstallString.Split(" ").Count = 2 Then
                                Utils.StartCommand(Utils.GetMsiExecExeFilePath, SProd.UninstallString.Split(" ")(1) & " /passive REMOVE=ALL REBOOT=SUPPRESS", True)
                            End If
                        Else
                            Utils.StartCommand(SProd.UninstallString, "/passive /norestart", True)
                        End If
                    End If
                Next
                Utils.DeleteDirectoryAndContent(m_SophosMajPath)
                Utils.DeleteDirectoryAndContent("C:\Program Files\Sophos\Endpoint Defense")
            End If
        End Sub

        Private Function SophosProducts() As List(Of SophosInfos)
            Dim SophosProductsInfos As New List(Of SophosInfos)

            Dim dic As New Dictionary(Of String, SophosInfos)
            dic.Add("Sophos Patch Agent", Nothing)
            dic.Add("Sophos Compliance Agent", Nothing)
            dic.Add("Sophos System Protection", Nothing)
            dic.Add("Sophos Client Firewall", Nothing)
            dic.Add("Sophos Endpoint Defense", Nothing)
            dic.Add("Sophos Network Threat Protection", Nothing)
            dic.Add("Sophos Anti-Virus", Nothing)
            dic.Add("Sophos Exploit Prevention", Nothing)
            dic.Add("Sophos Remote Management System", Nothing)
            dic.Add("Sophos Management Communication", Nothing)
            dic.Add("Sophos AutoUpdate", Nothing)

            Dim UninstallSyringKeys As String() = If(Utils.is64Bits(), New String() {m_UninstallRegistryKey, m_UninstallRegistryKeyWow6432}, New String() {m_UninstallRegistryKey})

            For Each stringKey In UninstallSyringKeys
                Try
                    Using GuidKey As RegistryKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, "").OpenSubKey(stringKey)
                        If Not GuidKey Is Nothing Then
                            For Each GuidSubKey In GuidKey.GetSubKeyNames
                                Using SkName = GuidKey.OpenSubKey(GuidSubKey)
                                    If Not SkName Is Nothing Then
                                        Dim ProdName = SkName.GetValue("DisplayName")
                                        Dim ProdVersion As String = String.Empty
                                        Dim ProdUninstall As String = String.Empty
                                        If Not ProdName Is Nothing AndAlso ProdName.ToLower.StartsWith("sophos ") Then
                                            Dim sInfos As New SophosInfos
                                            ProdVersion = SkName.GetValue("DisplayVersion")
                                            ProdUninstall = SkName.GetValue("UninstallString")

                                            With sInfos
                                                .DisplayName = ProdName
                                                .Version = ProdVersion
                                                .UninstallString = ProdUninstall
                                                If Not .UninstallString = String.Empty Then
                                                    .IsMsiUninstaller = If(.UninstallString.ToLower.StartsWith("msiexec.exe /x"), True, False)
                                                End If
                                            End With

                                            If dic.ContainsKey(ProdName) Then
                                                dic.Item(ProdName) = sInfos
                                            End If
                                        End If
                                    End If
                                End Using
                            Next
                        End If
                    End Using
                Catch ex As Exception
                    Exit For
                End Try
            Next

            For Each k In dic
                If Not k.Value Is Nothing Then
                    SophosProductsInfos.Add(k.Value)
                End If
            Next

            Return SophosProductsInfos
        End Function
#End Region

    End Class
End Namespace
