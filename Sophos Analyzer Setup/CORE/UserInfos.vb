﻿Imports System.DirectoryServices.AccountManagement
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports Microsoft.Win32
Imports Sophos_Analyzer_Setup.Helper
Imports Sophos_Analyzer_Setup.Win32

Namespace Core
    Public Class UserInfos

#Region " Fields "
        Private m_RegKeyProfilList As String = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList"
        Private m_RegKeyProfilList6432 As String = "SOFTWARE\WOW6432Node\Microsoft\Windows NT\CurrentVersion\ProfileList"
#End Region

#Region " Properties "
        Public Property Name As String
        Public Property Disabled As Boolean
        Public Property UserCannotChangePassword As Boolean
        Public Property PasswordNeverExpires As Boolean
        Public Property Description As String
        Public Property SID As String
        Public Property Group As String

        Public ReadOnly Property ProfileImagePath() As String
            Get
                Return GetProfileImagePath()
            End Get
        End Property

        Public ReadOnly Property NtUserDatFilePath() As String
            Get
                Return GetProfileImagePath() & "\NTUSER.DAT"
            End Get
        End Property
#End Region

#Region " Constructors "
        Public Sub New(AccountName As String, RetrieveAccount As Boolean)
            Name = AccountName
            If RetrieveAccount Then
                GetUserByName()
            End If
        End Sub
#End Region

#Region " Methods "
        Private Sub GetUserByName()
            Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                Using user As New UserPrincipal(ctx)
                    user.Name = "*"
                    Using ps As New PrincipalSearcher()
                        ps.QueryFilter = user
                        Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll()
                        For Each p As Principal In result
                            Using up As UserPrincipal = DirectCast(p, UserPrincipal)
                                If up.Name.ToLower = Name.ToLower Then
                                    Disabled = If(up.Enabled, False, True)
                                    Description = up.Description
                                    Dim groups As String = ""
                                    For Each g In up.GetGroups
                                        groups &= g.Name & "|"
                                    Next
                                    Group = groups.TrimEnd("|")
                                    UserCannotChangePassword = If(up.UserCannotChangePassword, True, False)
                                    PasswordNeverExpires = If(up.PasswordNeverExpires, True, False)
                                    SID = up.Sid.ToString
                                End If
                            End Using
                        Next
                    End Using
                End Using
            End Using
        End Sub

        Public Function IsLoggedIn() As Boolean
            If Not SID = String.Empty Then
                Using rk As RegistryKey = Registry.Users.OpenSubKey(SID)
                    Return (rk IsNot Nothing)
                End Using
            End If
            Return False
        End Function

        Private Function GetProfileImagePath() As String
            Dim ProfPath = Reg.SZ.GetValue(m_RegKeyProfilList & "\" & SID, "ProfileImagePath")
            If ProfPath = "" Then
                ProfPath = Reg.SZ.GetValue(m_RegKeyProfilList6432 & "\" & SID, "ProfileImagePath")
            End If
            If Not Directory.Exists(ProfPath) Then
                ProfPath = Environment.ExpandEnvironmentVariables(Reg.SZ.GetValue(m_RegKeyProfilList, "ProfilesDirectory"))
                Dim pPath As String = Path.GetPathRoot(ProfPath)
                If Not Directory.Exists(pPath) Then
                    Reg.SZ.SetValue(m_RegKeyProfilList, "ProfilesDirectory", "%SystemDrive%\Users", True)
                    Reg.SZ.SetValue(m_RegKeyProfilList & "\" & SID, "ProfileImagePath", "%SystemDrive%\Users\" & Name)
                    Reg.SZ.SetValue(m_RegKeyProfilList6432 & "\" & SID, "ProfileImagePath", "%SystemDrive%\Users\" & Name)
                    Return Environment.ExpandEnvironmentVariables("%SystemDrive%") & "\Users\" & Name
                Else
                    Reg.SZ.SetValue(m_RegKeyProfilList & "\" & SID, "ProfileImagePath", ProfPath & "\" & Name)
                    Reg.SZ.SetValue(m_RegKeyProfilList6432 & "\" & SID, "ProfileImagePath", ProfPath & "\" & Name)
                    Return ProfPath & "\" & Name
                End If
            End If
            Return ProfPath
        End Function

        Private Function GetNameBySessionId(sessionId As Integer, server As IntPtr) As String
            Dim buffer As IntPtr = IntPtr.Zero
            Dim count As UInteger = 0
            Dim userName As String = String.Empty
            Try
                NativeMethods.WTSQuerySessionInformation(server, sessionId, NativeEnum.WTS_INFO_CLASS.WTSUserName, buffer, count)
                userName = Marshal.PtrToStringAnsi(buffer).ToUpper().Trim()
            Finally
                NativeMethods.WTSFreeMemory(buffer)
            End Try
            Return userName
        End Function

        Public Function AccountExists(Optional ByVal ExcludeUserName As String = "") As Boolean
            Try
                Using ctx As New PrincipalContext(ContextType.Machine, Environment.MachineName)
                    Using user As New UserPrincipal(ctx)
                        user.Name = "*"
                        Using ps As New PrincipalSearcher()
                            ps.QueryFilter = user
                            Dim result As PrincipalSearchResult(Of Principal) = ps.FindAll()
                            For Each p As Principal In result
                                Using up As UserPrincipal = DirectCast(p, UserPrincipal)
                                    If ExcludeUserName = "" Then
                                        If up.Name.ToLower = Name.ToLower Then
                                            SID = up.Sid.ToString
                                            Return True
                                        End If
                                    Else
                                        If up.Name.ToLower = ExcludeUserName.ToLower Then
                                        Else
                                            If up.Name.ToLower = Name.ToLower Then
                                                SID = up.Sid.ToString
                                                Return True
                                            End If
                                        End If
                                    End If
                                End Using
                            Next
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                Return False
            End Try
            Return False
        End Function

        ''' <summary>
        ''' Créé un compte Windows et son profil : 
        ''' - vérifie si l'utilisateur existe
        ''' - créé le compte
        ''' - ajout de l'utilisateur au(x) groupe(s)
        ''' - créé l'arborescence du profile
        ''' </summary>
        ''' <param name="User">Nom utilisateur</param>
        ''' <param name="Bgw">Instance du thread principal</param>
        Public Function CreateAccount() As Boolean
            Try
                Using pc As New PrincipalContext(ContextType.Machine)
                    Using u As UserPrincipal = New UserPrincipal(pc)
                        u.SetPassword(String.Empty)
                        u.Name = Name
                        u.DisplayName = String.Empty
                        u.Description = Description
                        u.UserCannotChangePassword = UserCannotChangePassword
                        u.PasswordNeverExpires = PasswordNeverExpires
                        u.Save()
                        SID = u.Sid.ToString()

                        If Group.Contains("|") Then
                            Dim Grps = Group.Split("|")
                            For Each grp In Grps
                                If Not grp = String.Empty Then
                                    Dim gPc As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, grp)
                                    If Not gPc Is Nothing Then
                                        gPc.Members.Add(u)
                                        gPc.Save()
                                    End If
                                End If
                            Next
                        Else
                            If Not Group = String.Empty Then
                                Dim gPc As GroupPrincipal = GroupPrincipal.FindByIdentity(pc, Group)
                                If Not gPc Is Nothing Then
                                    gPc.Members.Add(u)
                                    gPc.Save()
                                End If
                            End If
                        End If

                        Dim MaxPath% = 240
                        Dim pathBuf As New StringBuilder(MaxPath)
                        Dim pathLen = CUInt(pathBuf.Capacity)
                        Dim Res = NativeMethods.CreateProfile(u.Sid.ToString(), Name.Trim, pathBuf, pathLen)

                        Return True
                    End Using
                End Using
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function DeleteAccountAndProfilByName() As Boolean
            Try
                Using computerContext As New PrincipalContext(ContextType.Machine)
                    Dim user As UserPrincipal = UserPrincipal.FindByIdentity(computerContext, Name)
                    If user IsNot Nothing Then
                        NativeMethods.DeleteProfile(SID, ProfileImagePath, computerContext.Name)
                        DeleteSid(SID)
                        user.Delete()
                        If Directory.Exists(ProfileImagePath) Then
                            Try
                                Directory.Delete(ProfileImagePath)
                            Catch ex As Exception
                            End Try
                        End If
                        Return True
                    End If
                    Return True
                End Using
                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Supprime le SID dans le registre.
        ''' </summary>
        ''' <param name="SID">SID utilisateur</param>
        Private Sub DeleteSid(SID As String)
            Try
                DeleteSubKey(m_RegKeyProfilList6432, SID)
                DeleteSubKey(m_RegKeyProfilList, SID)
            Catch ex As Exception
            End Try
        End Sub

        Private Sub DeleteSubKey(path As String, Sid As String)
            Using key As RegistryKey = Registry.LocalMachine.OpenSubKey(path, True)
                If Not key Is Nothing Then
                    For Each subkeyName As String In key.GetSubKeyNames()
                        If subkeyName.ToLower = Sid.ToLower OrElse subkeyName.ToLower = (Sid & ".bak").ToLower Then
                            key.DeleteSubKey(subkeyName)
                        End If
                    Next
                End If
            End Using
        End Sub

        Public Function LogoffSession() As Boolean
            Dim sessions As NativeStruct.strSessionsInfo() = GetSessions(Environment.MachineName)
            Dim server As IntPtr
            Dim b As Boolean = False
            Try
                server = NativeMethods.WTSOpenServer(Environment.MachineName)
                For Each u In sessions
                    If Name.Trim.ToUpper = u.SessionUserName.Trim.ToUpper Then
                        If u.ConnectionState = "Disconnected" Then
                            b = NativeMethods.WTSLogoffSession(server, u.SessionID, True)
                            Exit For
                        End If
                    End If
                Next
            Catch ex As Exception
                Return False
            Finally
                NativeMethods.WTSCloseServer(server)
            End Try
            Return b
        End Function

        Private Function GetSessions(ServerName As String) As NativeStruct.strSessionsInfo()
            Dim ptrOpenedServer As IntPtr
            Dim RetVal As NativeStruct.strSessionsInfo()
            Try
                ptrOpenedServer = NativeMethods.WTSOpenServer(ServerName)
                Dim FRetVal As Integer
                Dim ppSessionInfo As IntPtr = IntPtr.Zero
                Dim Count As Integer = 0
                Try
                    FRetVal = NativeMethods.WTSEnumerateSessions(ptrOpenedServer, 0, 1, ppSessionInfo, Count)
                    If FRetVal <> 0 Then
                        Dim sessionInfo() = New NativeStruct.WTS_SESSION_INFO(Count) {}
                        Dim i As Integer
                        Dim DataSize = Marshal.SizeOf(New NativeStruct.WTS_SESSION_INFO)
                        Dim current As Long
                        current = ppSessionInfo.ToInt64
                        For i = 0 To Count - 1
                            sessionInfo(i) = CType(Marshal.PtrToStructure(New IntPtr(current), GetType(NativeStruct.WTS_SESSION_INFO)), NativeStruct.WTS_SESSION_INFO)
                            current = current + DataSize
                        Next
                        NativeMethods.WTSFreeMemory(ppSessionInfo)
                        Dim tmpArr(sessionInfo.GetUpperBound(0)) As NativeStruct.strSessionsInfo
                        For i = 0 To tmpArr.GetUpperBound(0)
                            tmpArr(i).SessionID = sessionInfo(i).SessionID
                            tmpArr(i).ConnectionState = GetConnectionState(sessionInfo(i).State)
                            tmpArr(i).SessionUserName = GetNameBySessionId(sessionInfo(i).SessionID, ptrOpenedServer)
                        Next
                        ReDim sessionInfo(-1)
                        RetVal = tmpArr
                    Else
                        Throw New ApplicationException("Aucune donnée retournée !")
                    End If
                Catch ex As Exception
                    Throw New Exception(ex.Message & vbCrLf & Marshal.GetLastWin32Error)
                End Try
            Catch ex As Exception
                Throw New Exception(ex.Message)
                Exit Function
            Finally
                NativeMethods.WTSCloseServer(ptrOpenedServer)
            End Try

            Return RetVal
        End Function

        Private Function GetConnectionState(State As NativeEnum.WTS_CONNECTSTATE_CLASS) As String
            Dim RetVal As String
            Select Case State
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSActive
                    RetVal = "Active"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSConnected
                    RetVal = "Connected"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSConnectQuery
                    RetVal = "Query"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSDisconnected
                    RetVal = "Disconnected"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSDown
                    RetVal = "Down"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSIdle
                    RetVal = "Idle"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSInit
                    RetVal = "Initializing"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSListen
                    RetVal = "Listen"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSReset
                    RetVal = "reset"
                Case NativeEnum.WTS_CONNECTSTATE_CLASS.WTSShadow
                    RetVal = "Shadowing"
                Case Else
                    RetVal = "Unknown connect state"
            End Select
            Return RetVal
        End Function

#End Region

    End Class
End Namespace

