﻿Imports System.IO
Imports Microsoft.Win32
Imports Sophos_Analyzer_Setup.Engine
Imports Sophos_Analyzer_Setup.Helper

Namespace Core
    Public Class Framework
        Inherits Task

#Region " Fields "
        Private m_LastInstalledVersion As String
        Private m_FilePath As String
#End Region

#Region " Properties "
        Public ReadOnly Property LastInstalledVersion() As String
            Get
                Return m_LastInstalledVersion
            End Get
        End Property

        Public Overrides ReadOnly Property Name As String
            Get
                Return "Framework"
            End Get
        End Property

        Public Overrides ReadOnly Property Description As String
            Get
                Return "Vérification de la présence du Framework 4.0 ou 4.5.x.x." & vbNewLine &
                    "Il est possible d'installer les versions allant de la v4 à v4.5.x.x."
            End Get
        End Property

        Public Overrides ReadOnly Property InstallMessage As String
            Get
                Return "Installation du Framework (Soyez patient) ..."
            End Get
        End Property

        Public Overrides ReadOnly Property UninstallMessage As String
            Get
                Return " ..."
            End Get
        End Property

#End Region

#Region " Constructor "
        Public Sub New()
        End Sub
#End Region

#Region " Methods "
        Public Overrides Sub Init()
            'FRAMEWORK
            'v4 :                   dotNetFx40_Full_x86_x64.exe
            'Description :          Microsoft .NET Framework 4 Setup
            'Nom du produit :       Microsoft .NET Framework 4
            'Version du fichier :   4.0.30319.1
            '-------------
            'v4.5.1 :               NDP451-KB2858728-x86-x64-AllOS-ENU.exe
            'Description :          Microsoft .NET Framework 4.5.1 Setup
            'Nom du produit :       Microsoft .NET Framework 4.5.1
            'Version du fichier :   4.5.50938.18408
            '--------------
            'v4.5.2 :               NDP452-KB2901907-x86-x64-AllOS-ENU.exe
            'Description :          Microsoft .NET Framework 4.5.2 Setup
            'Nom du produit :       Microsoft .NET Framework 4.5.2            
            'Version du fichier :   4.5.51209.34209
            '--------------
            'v4.6 :                 NDP46-KB3045557-x86-x64-AllOS-ENU.exe
            'Description :          Microsoft .NET Framework 4.6 Setup
            'Nom du produit :       Microsoft .NET Framework 4.6            
            'Version du fichier :   4.6.81.0
            '--------------
            'v4.6.1 :               NDP461-KB3102436-x86-x64-AllOS-ENU.exe
            'Description :          Microsoft .NET Framework 4.6.1 Setup
            'Nom du produit :       Microsoft .NET Framework 4.6.1            
            'Version du fichier :   4.6.1055.0
            '--------------
            'v4.6.2 :               NDP462-KB3151800-x86-x64-AllOS-ENU.exe
            'Description :          Microsoft .NET Framework 4.6.2 Setup
            'Nom du produit :       Microsoft .NET Framework 4.6.2            
            'Version du fichier :   4.6.1590.0
            '--------------
            'v4.7 :                 NDP47-KB3186497-x86-x64-AllOS-ENU.exe
            'Description :          Microsoft .NET Framework 4.7 Setup
            'Nom du produit :       Microsoft .NET Framework 4.7            
            'Version du fichier :   4.7.2053.0
            '--------------
            'v4.7.1 :               NDP471-KB4033342-x86-x64-AllOS-ENU.exe
            'Description :          Microsoft .NET Framework 4.7.1 Setup
            'Nom du produit :       Microsoft .NET Framework 4.7.1            
            'Version du fichier :   4.7.2558.0
            '--------------
            m_LastInstalledVersion = "v" & GetLastFrameworkVersion()
            If m_LastInstalledVersion.ToLower.StartsWith("v4.0") OrElse m_LastInstalledVersion.ToLower.StartsWith("v4.5") Then
                InitState = InitStates.Valid
                m_FilePath = String.Empty
                MyBase.OnInit(Me, New InitInfos With {
                                                .TaskName = TaskNames.Framework,
                                                .State = InitStates.Valid,
                                                .ActionMessage = "La bonne version du framework est installée.",
                                                .StaticMessage = m_LastInstalledVersion,
                                                .Description = Description})
            Else
                Dim FrameworkFiles As New SortedList(Of String, String)
                For Each file In Directory.GetFiles(My.Application.Info.DirectoryPath, "*.exe")
                    Dim fVersion = FileVersionInfo.GetVersionInfo(file)
                    If Not fVersion Is Nothing Then
                        If fVersion.FileDescription.StartsWith("Microsoft .NET Framework 4") Then
                            'Check if it's not a web installer !
                            If New FileInfo(file).Length >= 50449456 Then
                                If fVersion.FileVersion.StartsWith("4.0") OrElse fVersion.FileVersion.StartsWith("4.5") Then
                                    FrameworkFiles.Add(fVersion.FileVersion, file)
                                End If
                            End If
                        End If
                    End If
                Next
                If FrameworkFiles.Count <> 0 Then
                    Dim FileVer = FrameworkFiles.Min(Function(f) f.Key)
                    Dim FilePath = FrameworkFiles.Where(Function(f) f.Key = FileVer).First.Value
                    Dim ver As String = String.Empty
                    Select Case FileVer
                        Case "4.0.30319.1"
                            ver = "4"
                        Case "4.5.50938.18408"
                            ver = "4.5.1"
                        Case "4.5.51209.34209"
                            ver = "4.5.2"
                            'Case "4.6.81.0"
                            '    ver = "4.6"
                            'Case "4.6.1055.0"
                            '    ver = "4.6.1"
                            'Case "4.6.1590.0"
                            '    ver = "4.6.2"
                            'Case "4.7.2053.0"
                            '    ver = "4.7"
                            'Case "4.7.2558.0"
                            '    ver = "4.7.1"
                    End Select
                    InitState = InitStates.Nearly
                    m_FilePath = FilePath
                    MyBase.OnInit(Me, New InitInfos With {
                                              .TaskName = TaskNames.Framework,
                                              .State = InitStates.Nearly,
                                              .ActionMessage = "Le framework " & ver & " sera installé grâce au setup d'installation localisé à l'emplacement ci-dessus.",
                                              .StaticMessage = FilePath,
                                              .Description = Description})
                Else
                    InitState = InitStates.Invalid
                    m_FilePath = String.Empty
                    MyBase.OnInit(Me, New InitInfos With {
                                           .TaskName = TaskNames.Framework,
                                           .State = InitStates.Invalid,
                                           .ActionMessage = "Veuillez installer manuellement le framework 4.0 (v4.5.x.x maxi) !",
                                           .StaticMessage = String.Empty,
                                           .Description = Description})
                End If
            End If
            MyBase.Init()
        End Sub

        Private Function GetLastFrameworkVersion() As String
            Using ndpKey As RegistryKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, "").OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\")
                If Not ndpKey Is Nothing Then
                    If Not ndpKey.GetValue("Release") Is Nothing Then
                        Return CheckFor45PlusVersion(ndpKey.GetValue("Release"))
                    Else
                        Return If(Not ndpKey.GetValue("Version") Is Nothing, "4.0", "3.5")
                    End If
                Else
                    Return "3.5"
                End If
            End Using
        End Function

        Private Function CheckFor45PlusVersion(releaseKey As Integer) As String
            If (releaseKey >= 528040) Then Return "4.8"
            If (releaseKey >= 461808) Then Return "4.7.2"
            If (releaseKey >= 461308) Then Return "4.7.1"
            If (releaseKey >= 460798) Then Return "4.7"
            If (releaseKey >= 394802) Then Return "4.6.2"
            If (releaseKey >= 394254) Then Return "4.6.1"
            If (releaseKey >= 393295) Then Return "4.6"
            If (releaseKey >= 379893) Then Return "4.5.2"
            If (releaseKey >= 378675) Then Return "4.5.1"
            If (releaseKey >= 378389) Then Return "4.5"
            Return "Framework inconnu !"
        End Function

        Public Overrides Sub Apply(ForceUninstall As Boolean)
            If ForceUninstall Then
                Uninstall()
            Else
                Select Case InitState
                    Case InitStates.Valid
                        Uninstall()
                    Case InitStates.Nearly
                        Install()
                End Select
            End If
            MyBase.Apply(ForceUninstall)
        End Sub

        Private Sub Install()
            If File.Exists(m_FilePath) Then Utils.StartCommand(m_FilePath, "/passive /norestart", True)
        End Sub

        Private Sub Uninstall()
            'Je ne prévois pas la désinstallation car c'est bien trop long et un redémarrage est requis à l'issu ! 
        End Sub
#End Region

    End Class
End Namespace
