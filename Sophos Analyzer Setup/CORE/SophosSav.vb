﻿Imports System.IO
Imports Ionic.Zip
Imports Sophos_Analyzer_Setup.Engine
Imports Sophos_Analyzer_Setup.Helper

Namespace Core
    Public Class SophosSav
        Inherits Task

#Region " Properties "
        Public Overrides ReadOnly Property Name As String
            Get
                Return "Sophos Sav"
            End Get
        End Property

        Public Overrides ReadOnly Property Description As String
            Get
                Return "Vérification de la présence de la mise à jour Sophos Sav"
            End Get
        End Property

        Public Overrides ReadOnly Property InstallMessage As String
            Get
                Return "Décompression de l'archive Sophos Sav ..."
            End Get
        End Property

        Public Overrides ReadOnly Property UninstallMessage As String
            Get
                Return "Suppression du répertoire Sophos Sav ..."
            End Get
        End Property
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub
#End Region

#Region " Methods "
        Public Overrides Sub Init()
            If Utils.SophosSavExists() Then
                Dim fVersion = File.ReadAllText("C:\maj_sophos\sav\crt\version.txt").Replace("Version ", "")
                InitState = InitStates.Valid
                MyBase.OnInit(Me, New InitInfos With {
                                            .TaskName = TaskNames.SophosSav,
                                            .State = InitState,
                                            .ActionMessage = "Sophos Sav est présent.",
                                            .StaticMessage = If(fVersion = String.Empty, String.Empty, fVersion),
                                            .Description = Description})
            Else
                Dim source As String = Path.Combine(My.Application.Info.DirectoryPath, "sav.zip")
                If File.Exists(source) Then

                    Dim result As Boolean = False
                    Try
                        Using zip As ZipFile = ZipFile.Read(source)
                            For Each f In zip.EntriesSorted
                                If f.FileName = "var/smg/maj_antivirus/Sophos/sav/crt/version.txt" Then
                                    f.Extract("C:\Temp", ExtractExistingFileAction.OverwriteSilently)
                                    If File.Exists("C:\Temp\var\smg\maj_antivirus\Sophos\sav\crt\version.txt") Then
                                        Dim fVersion = File.ReadAllText("C:\Temp\var\smg\maj_antivirus\Sophos\sav\crt\version.txt").Replace("Version ", String.Empty)
                                        InitState = InitStates.Nearly
                                        MyBase.OnInit(Me, New InitInfos With {
                                                                        .TaskName = TaskNames.SophosSav,
                                                                        .State = InitStates.Nearly,
                                                                        .ActionMessage = "Sophos Sav " & If(fVersion = String.Empty, String.Empty, fVersion) & " sera décompressé dans l'emplacement ci-dessus.",
                                                                        .StaticMessage = source,
                                                                        .Description = Description})

                                    End If
                                    Exit For
                                End If
                            Next
                        End Using
                    Catch ex As Exception
                        InitState = InitStates.Invalid
                        MyBase.OnInit(Me, New InitInfos With {
                                                                    .TaskName = TaskNames.SophosSav,
                                                                    .State = InitState,
                                                                    .ActionMessage = "L'archive ""SAV"" a rencontrée une erreur : " & ex.Message,
                                                                    .StaticMessage = String.Empty,
                                                                    .Description = Description})
                    End Try



                Else
                    InitState = InitStates.Invalid
                    MyBase.OnInit(Me, New InitInfos With {
                                                                .TaskName = TaskNames.SophosSav,
                                                                .State = InitState,
                                                                .ActionMessage = "L'archive Sophos SAV.zip n'existe pas, téléchargez et copiez-là dans le répertoire ""SAS"" !",
                                                                .StaticMessage = String.Empty,
                                                                .Description = Description})

                End If
            End If
            MyBase.Init()
        End Sub

        Public Overrides Sub Apply(ForceUninstall As Boolean)
            If ForceUninstall Then
                Uninstall()
            Else
                Select Case InitState
                    Case InitStates.Valid
                        Uninstall()
                    Case InitStates.Nearly
                        Install()
                End Select
            End If
            MyBase.Apply(ForceUninstall)
        End Sub

        Private Sub Install()
            If Not Directory.Exists("C:\maj_sophos\sav") Then
                Directory.CreateDirectory("C:\maj_sophos\sav")
            End If

            Dim source As String = Path.Combine(My.Application.Info.DirectoryPath, "sav.zip")
            If File.Exists(source) Then
                Dim result As Boolean = False
                Using zip As ZipFile = ZipFile.Read(source)
                    For Each entry In zip.Entries
                        If entry.FileName.StartsWith("var/smg/maj_antivirus/Sophos/sav/") Then
                            Dim newPath As String = entry.FileName
                            If entry.IsDirectory Then
                                Dim startIdx = newPath.TrimEnd("/").LastIndexOf("/sav/")
                                If Not startIdx <= 0 Then
                                    newPath = Path.Combine("C:\maj_sophos", newPath.Substring(startIdx).Replace("/", "\").TrimStart("\"))
                                    Directory.CreateDirectory(newPath)
                                End If
                            Else
                                Dim startIdx = newPath.LastIndexOf("/sav/")
                                If Not startIdx <= 0 Then
                                    newPath = Path.Combine("C:\maj_sophos", newPath.Substring(startIdx).Replace("/", "\").TrimStart("\"))
                                    Using fs As New FileStream(newPath, FileMode.Create)
                                        entry.Extract(fs)
                                    End Using
                                End If
                            End If
                        End If
                    Next
                End Using
            Else
                'Zip file doesn't exists !
            End If
        End Sub

        Private Sub Uninstall()
            If Utils.SophosSavExists() Then
                Utils.DeleteDirectoryAndContent("C:\maj_sophos\sav")
            End If
        End Sub
#End Region

    End Class
End Namespace
