﻿Imports Sophos_Analyzer_Setup.Engine
Imports Sophos_Analyzer_Setup.Helper

Namespace Core
    Public Class Account
        Inherits Task

#Region " Properties "
        Public Overrides ReadOnly Property Name As String
            Get
                Return "Account"
            End Get
        End Property

        Public Overrides ReadOnly Property Description As String
            Get
                Return "Vérification de la présence du compte  ""sophos_autonome"" et de sa configuration." & vbNewLine &
                    "Le compte peut être créé puis paramétré ou supprimé."
            End Get
        End Property

        Public Overrides ReadOnly Property InstallMessage As String
            Get
                Return "Création et paramètrage du compte utilisateur (sophos_autonome) ..."
            End Get
        End Property

        Public Overrides ReadOnly Property UninstallMessage As String
            Get
                Return "Suppression du compte utilisateur (sophos_autonome) ..."
            End Get
        End Property
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub
#End Region

#Region " Methods "
        Public Overrides Sub Init()
            Dim AccountInfos As New UserInfos("sophos_autonome", False)

            Dim AccountExists As Boolean = AccountInfos.AccountExists
            InitState = If(AccountExists, InitStates.Valid, InitStates.Nearly)
            MyBase.OnInit(Me, New InitInfos With {
                                             .TaskName = TaskNames.Account,
                                             .State = InitState,
                                             .ActionMessage = If(AccountExists, "Le compte utilisateur est paramétré", "Ce nom de compte n'existe pas. Il sera créé !"),
                                             .StaticMessage = If(AccountExists, AccountInfos.Name, "sophos_autonome"),
                                             .Description = Description})
            MyBase.Init()
        End Sub

        Public Overrides Sub Apply(ForceUninstall As Boolean)
            If ForceUninstall Then
                Uninstall()
            Else
                Select Case InitState
                    Case InitStates.Valid
                        Uninstall()
                    Case InitStates.Nearly
                        Install()
                End Select
            End If
            MyBase.Apply(ForceUninstall)
        End Sub

        Private Sub Install()
            Dim acc As New UserInfos("sophos_autonome", False)
            With acc
                .Disabled = False
                .Description = "Compte utilisateur pour Sophos Analyzer"
                .Group = If(Utils.SophosInstalledFromDirectories, If(Utils.GetInstalledUICulture = 1036, "Administrateurs|SophosAdministrator", "Administrators|SophosAdministrator"),
                    If(Utils.GetInstalledUICulture = 1036, "Administrateurs", "Administrators"))
                .UserCannotChangePassword = True
                .PasswordNeverExpires = True

                If .CreateAccount() Then
                Else
                    'Error, account does not created !
                End If
            End With
        End Sub

        Private Sub Uninstall()
            Dim AccountInfos As New UserInfos("sophos_autonome", True)
            If AccountInfos.AccountExists Then
                If AccountInfos.IsLoggedIn Then
                    If AccountInfos.LogoffSession Then
                        AccountInfos.DeleteAccountAndProfilByName()
                    Else
                        'Error can't logoff !
                    End If
                Else
                    AccountInfos.DeleteAccountAndProfilByName()
                End If
            Else
                'Account doesn't exists !
            End If
        End Sub

#End Region

    End Class
End Namespace
