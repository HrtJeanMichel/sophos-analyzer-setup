﻿Imports System.IO
Imports System.Reflection
Imports Sophos_Analyzer_Setup.Engine
Imports Sophos_Analyzer_Setup.Helper

Namespace Core
    Public Class Analyzer
        Inherits Task

#Region " Properties "
        Public Overrides ReadOnly Property Name As String
            Get
                Return "Analyzer"
            End Get
        End Property

        Public Overrides ReadOnly Property Description As String
            Get
                Return "Vérification de la présence du produit Sophos Analyzer comprenant le configurateur et le lanceur." & vbNewLine &
                    "Il est possible de l'installer ou de le désinstaller via ce Setup ou depuis ""Ajout/Suppression de programmes""."
            End Get
        End Property

        Public Overrides ReadOnly Property InstallMessage As String
            Get
                Return "Installation de Sophos Analyzer ..."
            End Get
        End Property

        Public Overrides ReadOnly Property UninstallMessage As String
            Get
                Return "Désinstallation de Sophos Analyzer ..."
            End Get
        End Property
#End Region

#Region " Constructor "
        Public Sub New()
        End Sub
#End Region

#Region " Methods "
        Public Overrides Sub Init()
            Dim SftDir As Boolean = Directory.Exists(Utils.GetProgramFilesDir & "\SophosAnalyzer")
            Dim MetroFrmk As Boolean = File.Exists(Utils.GetProgramFilesDir & "\SophosAnalyzer\MetroFramework.dll")
            Dim MetroFrmkDesign As Boolean = File.Exists(Utils.GetProgramFilesDir & "\SophosAnalyzer\MetroFramework.Design.dll")
            Dim Sft As Boolean = File.Exists(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.exe")
            Dim SftExt As Boolean = File.Exists(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.Extensions.dll")
            Dim SftCore As Boolean = File.Exists(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.Core.dll")
            Dim SftHelper As Boolean = File.Exists(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.Helper.dll")
            Dim SftWin32 As Boolean = File.Exists(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.Win32.dll")

            If SftDir AndAlso MetroFrmk AndAlso MetroFrmkDesign AndAlso Sft AndAlso SftExt AndAlso SftCore AndAlso SftHelper AndAlso SftWin32 Then
                Dim fVersion = FileVersionInfo.GetVersionInfo(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.exe")
                InitState = InitStates.Valid
                MyBase.OnInit(Me, New InitInfos With {
                                       .TaskName = TaskNames.Analyzer,
                                       .State = InitStates.Valid,
                                       .ActionMessage = "Sophos Analyzer est installé.",
                                       .StaticMessage = If(fVersion Is Nothing, String.Empty, fVersion.FileVersion),
                                       .Description = Description})
            Else
                InitState = InitStates.Nearly
                MyBase.OnInit(Me, New InitInfos With {
                                     .TaskName = TaskNames.Analyzer,
                                     .State = InitStates.Nearly,
                                     .ActionMessage = "Sophos Analyzer sera installé à l'emplacement ci-dessus.",
                                     .StaticMessage = Utils.GetProgramFilesDir & "\SophosAnalyzer\",
                                     .Description = Description})

            End If
            MyBase.Init()
        End Sub

        Public Overrides Sub Apply(ForceUninstall As Boolean)
            If ForceUninstall Then
                Uninstall()
            Else
                Select Case InitState
                    Case InitStates.Valid
                        Uninstall()
                    Case InitStates.Nearly
                        Install()
                End Select
            End If
            MyBase.Apply(ForceUninstall)
        End Sub

        Private Sub Install()
            Utils.KillProcess("Sophos Analyzer")
            Utils.DeleteDirectoryAndContent(Utils.GetProgramFilesDir & "\SophosAnalyzer")

            Try
                Directory.CreateDirectory(Utils.GetProgramFilesDir & "\SophosAnalyzer")
                File.WriteAllBytes(Utils.GetProgramFilesDir & "\SophosAnalyzer\MetroFramework.dll", My.Resources.MetroFramework)
                File.WriteAllBytes(Utils.GetProgramFilesDir & "\SophosAnalyzer\MetroFramework.Design.dll", My.Resources.MetroFramework_Design)
                File.WriteAllBytes(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.exe", My.Resources.Sophos_Analyzer)
                File.WriteAllBytes(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.Extensions.dll", My.Resources.Sophos_Analyzer_Extensions)
                File.WriteAllBytes(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.Core.dll", My.Resources.Sophos_Analyzer_Core)
                File.WriteAllBytes(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.Helper.dll", My.Resources.Sophos_Analyzer_Helper)
                File.WriteAllBytes(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.Win32.dll", My.Resources.Sophos_Analyzer_Win32)

                Utils.CreateShortcut(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Sophos Analyzer Configurator.lnk"), Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.exe", Utils.GetProgramFilesDir & "\SophosAnalyzer\", "Configuration de la session utilisateur gérant l'analyse anti-virus.")

                If File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Sophos Analyzer Configurator.lnk")) Then
                    Using fs As New FileStream(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Sophos Analyzer Configurator.lnk"), FileMode.Open, FileAccess.ReadWrite)
                        fs.Seek(21, SeekOrigin.Begin)
                        fs.WriteByte(34)
                        fs.Close()
                    End Using
                End If

                Dim EstimatedSize As Long = 0

                For Each f In Directory.GetFiles(Utils.GetProgramFilesDir & "\SophosAnalyzer")
                    Dim fi As New FileInfo(f)
                    EstimatedSize += fi.Length
                Next

                Reg.SZ.SetValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "DisplayName", "Sophos Analyzer")
                Reg.SZ.SetValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "DisplayVersion", FileVersionInfo.GetVersionInfo(Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.exe").FileVersion)
                Reg.SZ.SetValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "DisplayIcon", Utils.GetProgramFilesDir & "\SophosAnalyzer\Sophos Analyzer.exe,0")
                Reg.DWORD.SetValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "EstimatedSize", If((EstimatedSize >= 1024) = True, Math.Round(EstimatedSize / 1024).ToString, "0"))
                Reg.SZ.SetValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "InstallDate", Date.Now.ToString("yyyyMMdd"))
                Reg.DWORD.SetBlnValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "NoModify", True)
                Reg.DWORD.SetBlnValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "NoRepair", True)
                Reg.SZ.SetValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "Publisher", "Herault Jean-Michel")
                Reg.SZ.SetValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "UninstallString", "C:\ProgramData\SophosAnalyzer\Uninstall.exe" & " -uninstall:yes")
                Reg.SZ.SetValue("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer", "URLInfoAbout", "https://bitbucket.org/HrtJeanMichel/")

                If Not Directory.Exists("C:\ProgramData\SophosAnalyzer") Then Directory.CreateDirectory("C:\ProgramData\SophosAnalyzer")
                File.Copy(Assembly.GetExecutingAssembly.Location, "C:\ProgramData\SophosAnalyzer\Uninstall.exe", True)
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        End Sub

        Private Sub Uninstall()
            Utils.KillProcess("Sophos Analyzer")
            Utils.DeleteDirectoryAndContent(Utils.GetProgramFilesDir & "\SophosAnalyzer")

            Try
                Utils.deleteTask("Sophos Analyzer")
            Catch ex As Exception
            End Try

            Try
                If File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Sophos Analyzer Configurator.lnk")) Then
                    File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Sophos Analyzer Configurator.lnk"))
                End If
            Catch ex As Exception
            End Try

            Try
                Reg.DeleteSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SophosAnalyzer")
            Catch ex As Exception
            End Try

        End Sub
#End Region

    End Class
End Namespace