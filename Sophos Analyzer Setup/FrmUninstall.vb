﻿Imports System.ComponentModel
Imports System.Reflection
Imports Sophos_Analyzer_Setup.Core
Imports Sophos_Analyzer_Setup.Engine
Imports Sophos_Analyzer_Setup.Helper

Public Class FrmUninstall

#Region " Fields "
    Private m_Configurator As Configurator
    Private m_TaskOngoing As Boolean
    Private m_Uninstalled As Boolean
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeComponent()
        m_Configurator = New Configurator(BgwUninstall)
        m_Configurator.Load(New Analyzer)
        m_Configurator.Load(New Account)
        AddHandler m_Configurator.InitStatusEvent, AddressOf m_Configurator_InitStatusEvent
        If Reg.Mounted Then Reg.Unload()
    End Sub
#End Region

#Region " Methods "
    Private Sub FrmUninstall_Load(sender As Object, e As EventArgs) Handles Me.Load
        m_Configurator.Initialize()
    End Sub

    Private Sub FrmUninstall_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason.Equals(CloseReason.UserClosing) Then
            If m_TaskOngoing Then e.Cancel = True
        ElseIf e.CloseReason.Equals(CloseReason.WindowsShutDown) Then
            If m_TaskOngoing Then
                Utils.ShutdownCancel()
            End If
        End If
    End Sub

    Private Sub FrmUninstall_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        If m_Uninstalled Then
            Dim Info As ProcessStartInfo = New ProcessStartInfo()
            Info.Arguments = "/C choice /C Y /N /D Y /T 2 & Del " & Assembly.GetExecutingAssembly.Location
            Info.WindowStyle = ProcessWindowStyle.Hidden
            Info.CreateNoWindow = True
            Info.FileName = "cmd.exe"
            Process.Start(Info)
        End If
    End Sub

    Private Sub OK_Button_Click(ByVal sender As Object, ByVal e As EventArgs) Handles OK_Button.Click
        Me.DialogResult = DialogResult.OK
        If Not BgwUninstall.IsBusy Then
            m_TaskOngoing = True
            TlpMain.Visible = False
            PgbMain.Visible = True
            BgwUninstall.RunWorkerAsync(True)
        End If
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub BgwUninstall_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwUninstall.DoWork
        Try
            Dim Uninstall As Boolean = e.Argument
            m_Configurator.Apply(Uninstall)
            e.Result = Tuple.Create(True, "La désinstallation s'est déroulée avec succès.")
        Catch ex As Exception
            e.Result = Tuple.Create(False, ex.Message)
        End Try
    End Sub

    Private Sub BgwUninstall_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwUninstall.ProgressChanged
        If e.ProgressPercentage <> 200 Then
            LblMain.Text = e.UserState.ToString
            PgbMain.Value = e.ProgressPercentage
        End If
    End Sub

    Private Sub BgwUninstall_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwUninstall.RunWorkerCompleted
        PgbMain.Value = 100
        If Not e.Result Is Nothing Then
            Dim tups = TryCast(e.Result, Tuple(Of Boolean, String))
            Select Case tups.Item1
                Case True
                    LblMain.Text = "Opération réussie" & vbNewLine & tups.Item2
                    m_Uninstalled = True
                    Application.Exit()
                Case False
                    LblMain.Text = "Echec de l'opération" & vbNewLine & tups.Item2
            End Select
        End If
        TlpMain.Visible = True
        OK_Button.Visible = False
        Cancel_Button.Text = "Quitter"
        m_TaskOngoing = False
    End Sub

    Private Sub m_Configurator_InitStatusEvent(sender As Object, InitInfos As InitInfos)
        Select Case InitInfos.TaskName
            Case TaskNames.Account
                Select Case InitInfos.State
                    Case InitStates.Valid
                    Case InitStates.Nearly
                End Select
            Case TaskNames.Analyzer
                Select Case InitInfos.State
                    Case InitStates.Valid
                    Case InitStates.Nearly
                End Select
        End Select
    End Sub
#End Region

End Class
